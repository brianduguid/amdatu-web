/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.resourcehandler;

import static java.lang.String.format;
import static org.amdatu.web.resourcehandler.Constants.CACHE_TIMEOUT_DISABLED;
import static org.amdatu.web.resourcehandler.Constants.ONE_WEEK_IN_SECONDS;
import static org.amdatu.web.resourcehandler.Constants.WEB_RESOURCE_DEFAULT_PAGE;
import static org.amdatu.web.resourcehandler.Constants.WEB_RESOURCE_KEY;
import static org.amdatu.web.resourcehandler.Constants.WEB_RESOURCE_VERSION_1_1;
import static org.amdatu.web.resourcehandler.Constants.WEB_RESOURCE_VERSION_KEY;
import static org.osgi.service.http.whiteboard.HttpWhiteboardConstants.HTTP_WHITEBOARD_CONTEXT_NAME;
import static org.osgi.service.http.whiteboard.HttpWhiteboardConstants.HTTP_WHITEBOARD_CONTEXT_SELECT;
import static org.osgi.service.http.whiteboard.HttpWhiteboardConstants.HTTP_WHITEBOARD_SERVLET_PATTERN;
import static org.osgi.service.log.LogService.LOG_INFO;

import java.util.ArrayList;
import java.util.Dictionary;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import javax.servlet.Servlet;

import org.amdatu.web.resourcehandler.util.DefaultPageParser;
import org.amdatu.web.resourcehandler.util.InvalidEntryException;
import org.amdatu.web.resourcehandler.util.ResourceKeyParser;
import org.apache.felix.dm.Component;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.Bundle;
import org.osgi.framework.Version;
import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.cm.ManagedService;
import org.osgi.service.log.LogService;

public class ResourceHandler implements ManagedService {

    /** The time (in seconds) to sent back for cache headers. */
    private static final String KEY_CACHE_TIMEOUT = "cache.timeout";

    private final ConcurrentMap<Bundle, DependencyManager> m_components;
    // Injected by Felix DM...
    private volatile LogService m_log;

    public ResourceHandler() {
        m_components = new ConcurrentHashMap<Bundle, DependencyManager>();
    }

    /**
     * Adds the resources of the given bundle to a specified or the default
     * HttpContext.
     *
     * @param bundle
     *        the bundle to add the resources for, cannot be
     *        <code>null</code>.
     * @throws InvalidEntryException
     *         in case the bundle specified an invalid web resource key.
     */
    public void addResourceBundle(Bundle bundle) throws InvalidEntryException {
        Dictionary<String, String> headers = bundle.getHeaders();

        Version version = new Version(headers.get(WEB_RESOURCE_VERSION_KEY));

        String key = (String) headers.get(WEB_RESOURCE_KEY);
        ResourceEntryMap entryMap = ResourceKeyParser.getEntries(key);

        DefaultPages defaultPages = new DefaultPages();
        // As of version 1.1, we've added support for defining default pages.
        // So we need to versions that are equal or greater than 1.1...
        if (WEB_RESOURCE_VERSION_1_1.compareTo(version) <= 0) {
            key = (String) headers.get(WEB_RESOURCE_DEFAULT_PAGE);
            defaultPages = DefaultPageParser.parseDefaultPages(key);
        }

        for (String contextId : entryMap.getContextIDs()) {
            List<ResourceEntry> entries = entryMap.getEntry(contextId);

            DependencyManager manager = new DependencyManager(bundle.getBundleContext());

            for (ResourceEntry entry : entries) {
                Properties servletProperties = new Properties();
                if ((contextId != null) && !"".equals(contextId)) {
                    String contextSelectFilter = format("(%s=%s)", HTTP_WHITEBOARD_CONTEXT_NAME, contextId);
                    servletProperties.put(HTTP_WHITEBOARD_CONTEXT_SELECT, contextSelectFilter);
                }
                servletProperties.put(HTTP_WHITEBOARD_SERVLET_PATTERN,
                    entry.getAlias().endsWith("/") ? entry.getAlias().concat("*") : entry.getAlias().concat("/*"));
                Component component = manager.createComponent().setInterface(Servlet.class.getName(), servletProperties)
                    .setImplementation(new ResourceServlet(bundle, entry, defaultPages));

                m_components.putIfAbsent(bundle, manager);

                manager.add(component);
            }
        }
    }

    /**
     * Removes a given bundle as resource provider to the HTTP context.
     *
     * @param bundle
     *        the bundle to remove as resource provider, cannot be
     *        <code>null</code>.
     */
    public void removeResourceBundle(Bundle bundle) {
        DependencyManager dm = m_components.remove(bundle);
        if (dm != null) {
            dm.clear();
        }
    }

    @Override
    @SuppressWarnings("rawtypes")
    public void updated(Dictionary properties) throws ConfigurationException {
        long cacheTimeout = ONE_WEEK_IN_SECONDS;

        if (properties != null) {
            Object value = properties.get(KEY_CACHE_TIMEOUT);
            if (value != null) {
                if (value instanceof Number) {
                    cacheTimeout = ((Number) value).longValue();
                } else {
                    String strValue = value.toString().trim();

                    try {
                        cacheTimeout = Long.parseLong(strValue);
                    } catch (NumberFormatException e) {
                        throw new ConfigurationException(KEY_CACHE_TIMEOUT, "Invalid numeric value!");
                    }
                }
            }
        }
        // Any negative value is used to disable the cache timeout...
        if (cacheTimeout < 0) {
            cacheTimeout = CACHE_TIMEOUT_DISABLED;
        }

        List<DependencyManager> dms = new ArrayList<DependencyManager>(m_components.values());
        for (DependencyManager dm : dms) {
            List<Component> comps = dm.getComponents();
            for (Component component : comps) {
                Object instance = component.getInstance();
                if (instance instanceof ResourceServlet) {
                    ResourceServlet resourceServlet = (ResourceServlet) instance;
                    resourceServlet.setCacheTimeout(cacheTimeout);
                }
            }
        }

        if (cacheTimeout == CACHE_TIMEOUT_DISABLED) {
            m_log.log(LOG_INFO, "Cache timeout for resources disabled...");
        } else {
            m_log.log(LOG_INFO, "Cache timeout for resources set to " + cacheTimeout + " seconds...");
        }
    }
}
