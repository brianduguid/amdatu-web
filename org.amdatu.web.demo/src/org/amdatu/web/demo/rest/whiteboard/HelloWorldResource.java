/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.demo.rest.whiteboard;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.amdatu.web.rest.doc.Description;
import org.amdatu.web.rest.doc.ResponseMessage;
import org.amdatu.web.rest.doc.ResponseMessages;
import org.amdatu.web.rest.doc.ReturnType;
import org.amdatu.web.rest.jaxrs.AmdatuWebRestConstants;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.Property;

@Path("hello")
@Description("Demonstrates a REST endpoint where the JAX-RS annotations are on interface methods.")
@Component(provides=Object.class)
@Property(name = AmdatuWebRestConstants.JAX_RS_RESOURCE_BASE, value = "/rest")
public class HelloWorldResource {

    /**
     * @return a plain text string, when sending a GET-request with the header "Accept: text/plain".
     */
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    @Description("Will return a friendly message from your server.")
    @ResponseMessages({ @ResponseMessage(code = 200, message = "Friendly text message") })
    public String helloPlain() {
        return "hello world";
    }

    @GET
    @Path("response-with-headers")
    @Produces({ MediaType.APPLICATION_JSON })
    @Description("Will return a friendly message from your server with headers.")
    @ResponseMessages({ @ResponseMessage(code = 200, message = "Friendly JSON message with extra header") })
    @ReturnType(String.class)
    public Response helloResponse() {
        return Response.ok("\"hello world\"").header("Link", "http://www.amdatu.org/").build();
    }

    /**
     * @return a JSON text string, when sending a GET-request with the header "Accept: application/json" or "Accept: application/vnd.custom+json".
     */
    @GET
    @Path("custom-mediatype")
    @Produces({ MediaType.APPLICATION_JSON, "application/vnd.custom+json" })
    @Description("Will return a friendly message from your server.")
    @ResponseMessages({ @ResponseMessage(code = 200, message = "Friendly JSON message") })
    public String helloJSON() {
        return "\"hello world\"";
    }

    /**
     * @return a plain text string, when sending a GET-request with the header "Accept: text/plain".
     */
    @GET
    @Path("with-uri-info")
    @Produces("text/plain")
    @Description("Returns a message with some context information")
    public String hello2(@Context UriInfo uriInfo, @Context HttpServletRequest request) {
        return "Hello world from " + uriInfo.getAbsolutePath();
    }
}
