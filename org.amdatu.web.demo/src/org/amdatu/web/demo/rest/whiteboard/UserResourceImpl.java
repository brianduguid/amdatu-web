/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.demo.rest.whiteboard;

import java.util.Arrays;
import java.util.List;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.Path;

import org.amdatu.web.demo.rest.User;
import org.amdatu.web.demo.rest.UserResource;
import org.amdatu.web.rest.jaxrs.AmdatuWebRestConstants;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.Property;

@Path("/user")
@Component
@Property(name = AmdatuWebRestConstants.JAX_RS_RESOURCE_BASE, value = "/rest")
public class UserResourceImpl implements UserResource {

    private volatile User m_lastUser = new User("John", "Doe");
    private volatile List<User> m_users = Arrays.asList(m_lastUser);

    @Override
    public User user() {
        return m_lastUser;
    }

    @Override
    public void setUser(@FormParam("first") @DefaultValue("John") String firstName, @FormParam("last") @DefaultValue("Doe") String lastName) {
        m_lastUser = new User(firstName, lastName);
    }

    @Override
    public void setUserViaJSON(User user) {
        m_lastUser = user;
    }

    @Override
    public void saveArray(User[] users) {
        m_users = Arrays.asList(users);
    }

    @Override
    public void saveList(List<User> users) {
        m_users = users;
    }

    @Override
    public User[] getUserArray() {
        return m_users.toArray(new User[0]);
    }

    @Override
    public List<User> getUserList() {
        return m_users;
    }
}
