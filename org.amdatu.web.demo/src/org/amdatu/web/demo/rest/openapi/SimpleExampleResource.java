/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.demo.rest.openapi;

import static org.amdatu.web.rest.jaxrs.AmdatuWebRestConstants.JAX_RS_RESOURCE_BASE;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.amdatu.web.rest.doc.ApiOperation;
import org.amdatu.web.rest.doc.ApiParameter;
import org.amdatu.web.rest.doc.ApiResponse;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.Property;

@Path("hello")
@Component(provides=Object.class)
@Property(name = JAX_RS_RESOURCE_BASE, value = "/api/simple")
@Produces(MediaType.TEXT_PLAIN)
public class SimpleExampleResource {

    @GET
    @ApiOperation("Will return a friendly message from your server.")
    @ApiResponse(code = 200, description = "The server's friendly message", returnType = String.class)
    public Response helloPlain() {
        return Response.ok("hello world").build();
    }

    @POST
    @ApiOperation("Will return your friendly message sent to the server.")
    @ApiResponse(code = 200, description = "Your friendly message", returnType = String.class)
    public Response helloPlain(@QueryParam("helloText") @ApiParameter("Your friendly text message") String helloText) {
    		return Response.ok(helloText).build();
    }
}
