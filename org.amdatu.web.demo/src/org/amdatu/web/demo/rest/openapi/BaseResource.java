/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.demo.rest.openapi;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.HEAD;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.amdatu.web.rest.doc.ApiOperation;
import org.amdatu.web.rest.doc.ApiSecurityRequirements;
import org.amdatu.web.rest.doc.ApiTags;

public class BaseResource {

	public static final String API_KEY = "SuperSecretApiKey";

	@Context
	protected HttpServletRequest request;

	/**
	 * A method to check REST service is up and running.
	 */
	@HEAD
	@ApiOperation("Check if resource is available")
	@ApiSecurityRequirements() // no security
	@ApiTags("head")
	public Response head() {
		return Response.ok().build();
	}

	protected boolean checkApiKey() {

		if (request == null) {
			throw new WebApplicationException(Response.status(Status.FORBIDDEN).entity("\"Request is null\"").build());
		}

		String apiKey = request.getHeader("Authorization");

		if (apiKey == null || !API_KEY.equals(apiKey)) {
			System.out.println("Invalid API Key!");
			String msg = "API Key is not valid. See POST /api/full/auth for API Key and set 'Authorize' value.";
			throw new WebApplicationException(msg, Response.status(Status.FORBIDDEN).entity("\"" + msg + "\"").build());
		}

		return true;
	}
}
