/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.demo.rest.openapi.petshop;

import org.amdatu.web.rest.doc.ApiModel;
import org.amdatu.web.rest.doc.ApiProperty;

@ApiModel(name = "Dog", value = "Dog information.")
public class Dog extends Pet {

	@ApiProperty("Is the dog a puppy?")
	private boolean puppy;
	@ApiProperty("Is the dog tailless?")
	private boolean tailless;

	public Dog() {
		super();
	}

	public Dog(String name, String color) {
		super(PetType.DOG, name, color);
		this.puppy = true;
		this.tailless = false;
	}

	public boolean isPuppy() {
		return puppy;
	}

	public Dog setPuppy(boolean puppy) {
		this.puppy = puppy;
		return this;
	}

	public boolean isTailless() {
		return tailless;
	}

	public Dog setTailless(boolean tailless) {
		this.tailless = tailless;
		return this;
	}
}
