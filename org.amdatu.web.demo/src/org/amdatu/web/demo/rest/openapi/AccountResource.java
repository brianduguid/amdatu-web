/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.demo.rest.openapi;

import static org.amdatu.web.rest.doc.ApiSecurityType.API_KEY;
import static org.amdatu.web.rest.jaxrs.AmdatuWebRestConstants.JAX_RS_RESOURCE_BASE;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.amdatu.web.rest.doc.ApiExternalDocs;
import org.amdatu.web.rest.doc.ApiHeaderDefinition;
import org.amdatu.web.rest.doc.ApiOperation;
import org.amdatu.web.rest.doc.ApiResponse;
import org.amdatu.web.rest.doc.ApiResponseDefinition;
import org.amdatu.web.rest.doc.ApiResponseRefs;
import org.amdatu.web.rest.doc.ApiSecurityDefinition;
import org.amdatu.web.rest.doc.ApiSecurityProperty;
import org.amdatu.web.rest.doc.ApiSecurityRequirements;
import org.amdatu.web.rest.doc.ApiSecurityScheme;
import org.amdatu.web.rest.doc.ApiTagDefinition;
import org.amdatu.web.rest.doc.ApiTags;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.Property;

@Path("auth")
@Component(provides=Object.class)
@Property(name = JAX_RS_RESOURCE_BASE, value = "/api/full")
@Produces(MediaType.APPLICATION_JSON) // produces json by default
@ApiTagDefinition(name = "head", description = "Head endpoints")
@ApiTagDefinition(name = "authentication", description = "Authentication resources")
@ApiTagDefinition(name = "admin", description = "Administrative functions")
@ApiTagDefinition(name = "secret", description = "Secret functions")
@ApiTagDefinition(name = "user", description = "User management")
@ApiTagDefinition(name = "unused", description = "Unused tag for demonstration. It will not be in definition.")
@ApiResponseDefinition(name = "400_BadRequest", code = 400, description = "Bad Request")
@ApiResponseDefinition(name = "403_Forbidden", code = 403, description = "Invalid API Key")
@ApiResponseDefinition(name = "500_Error", code = 500, description = "Internal Server Error")
@ApiHeaderDefinition(name = "x-total-count", description = "Total count of results returned", valueType = Integer.class)
@ApiHeaderDefinition(name = "x-result-checksums", description = "Checksums of each result in order of the result", valueType = List.class, elementType = String.class)
@ApiHeaderDefinition(name = "x-error-detail", description = "Details of the error if possible", valueType = String.class)
@ApiSecurityDefinition(name = "apiKeyAuth", type = API_KEY,
		properties = {
				@ApiSecurityProperty(key = "in", value = "header"),
				@ApiSecurityProperty(key = "name", value = "Authorization")
		}
)
@ApiSecurityRequirements(
		value ={ @ApiSecurityScheme("apiKeyAuth") },
		global = true
		)
@ApiTags("user")
public class AccountResource extends BaseResource {

	private Map<String, Account> m_accounts;

	public AccountResource() {
		m_accounts = new HashMap<>();

		UserAccount account1 = new UserAccount(1, "jsmith", "Joe", "Smith");
		UserAccount account2 = new UserAccount(2, "cmacleod", "Connor", "MacLeod");
		UserAccount account3 = new UserAccount(3, "tpetty", "Tom", "Petty").setUserStatus(UserStatus.DISABLED);

		m_accounts.put(account1.getUsername(), account1);
		m_accounts.put(account2.getUsername(), account2);
		m_accounts.put(account3.getUsername(), account3);
	}

	@POST
	@Consumes(MediaType.TEXT_PLAIN)
    @ApiOperation(
    		value = "Authenticate to receive the API_KEY.",
    		description = "A 403 error is returned if the username and/or password are incorrect (i.e. null or empty). (HINT: put anything for the username and password)"
    		)
	@ApiResponse(code = 200, description = "API_KEY", returnType = String.class)
	@ApiResponse(code = 403, description = "Invalid username and/or password")
	@ApiResponseRefs("500_Error")
    @ApiExternalDocs(
    			url = "/index.html",
    			description = "Example External Docs Link"
    		)
    @ApiSecurityRequirements() // No security requirements needed for this endpoint
	@ApiTags("authentication")
	public Response authenticate(@QueryParam("username") String username, @QueryParam("password") String password) {

		if (username == null || username.isEmpty() || password == null || password.isEmpty()) {
			throw new WebApplicationException(Response.status(Status.FORBIDDEN).entity("\"Invalid username and/or password\"").build());
		}

		return Response.ok(Collections.singletonMap("API_KEY", API_KEY)).build();
	}

	@GET
	@Path("users")
	@ApiOperation("List user accounts.")
	@ApiResponse(code = 200, description = "List of user accounts", returnType = List.class, elementType = UserAccount.class, headerNames = "x-total-count")
	@ApiResponseRefs({ "403_Forbidden", "500_Error" })
	public Response listAccounts() {

		checkApiKey();

		return Response.ok(m_accounts.values()).header("x-total-count", m_accounts.size()).build();
	}

	@GET
	@Path("users/names")
	@Produces(MediaType.TEXT_PLAIN)
	@ApiOperation("List user account names.")
	@ApiResponse(code = 200, description = "List of user accounts", returnType = List.class, elementType = String.class, headerNames = "x-total-count")
	@ApiResponseRefs({ "403_Forbidden", "500_Error" })
	public Response listAccountNames() {

		checkApiKey();

		return Response.ok(m_accounts.keySet()).header("x-total-count", m_accounts.size()).build();
	}

	@GET
	@Path("users/{username}")
	@ApiOperation("Sets the user status")
	@ApiResponse(code = 200, description = "User account", returnType = UserAccount.class)
	@ApiResponseRefs({ "400_BadRequest", "403_Forbidden", "500_Error" })
	public Response findUserAccount(@PathParam("username") String username) {

		checkApiKey();

		if (username == null || username.isEmpty()) {
			throw new WebApplicationException(Response.status(Status.BAD_REQUEST).entity("\"Invalid input\"").build());
		}

		Account existingAccount = m_accounts.get(username);

		if (existingAccount == null || !(existingAccount instanceof UserAccount)) {
			throw new WebApplicationException(Response.status(Status.BAD_REQUEST).entity("\"Invalid username\"").build());
		}

		return Response.ok(existingAccount).build();
	}



	@POST
	@Path("users")
	@ApiOperation("Create a new user account")
	@ApiResponse(code = 200, description = "New user account", returnType = UserAccount.class)
	@ApiResponse(code = 409, description = "User account already exists")
	@ApiResponseRefs({ "400_BadRequest", "403_Forbidden", "500_Error" })
	@ApiTags("admin")
	public Response createUserAccount(UserAccount userAccount) {

		checkApiKey();

		if (userAccount == null) {
			throw new WebApplicationException(Response.status(Status.BAD_REQUEST).entity("\"Invalid input\"").build());
		}

		String username = userAccount.getUsername();

		if (username == null || username.isEmpty()) {
			throw new WebApplicationException(Response.status(Status.BAD_REQUEST).entity("\"Username required\"").build());
		}

		Account existingAccount = m_accounts.get(username);

		if (existingAccount != null) {
			throw new WebApplicationException(Response.status(Status.CONFLICT).entity("\"User account already exists\"").build());
		}

		m_accounts.put(username, userAccount);

		return Response.ok(userAccount).build();
	}

	@PUT
	@Path("users/{username}")
	@ApiOperation("Update an existing user account")
	@ApiResponse(code = 200, description = "Updated user account", returnType = UserAccount.class)
	@ApiResponseRefs({ "400_BadRequest", "403_Forbidden", "500_Error" })
	@ApiTags("admin")
	public Response updateUserAccount(@PathParam("username") String username, UserAccount userAccount) {

		checkApiKey();

		if (username == null || username.isEmpty() || userAccount == null) {
			throw new WebApplicationException(Response.status(Status.BAD_REQUEST).entity("\"Invalid input\"").build());
		}

		Account existingAccount = m_accounts.get(username);

		if (existingAccount == null || !(existingAccount instanceof UserAccount)) {
			throw new WebApplicationException(Response.status(Status.BAD_REQUEST).entity("\"Invalid username\"").build());
		}

		m_accounts.put(username, userAccount);

		return Response.ok(userAccount).build();
	}

	@DELETE
	@Path("users/{username}")
	@ApiOperation("Delete an existing user account")
	@ApiResponse(code = 200, description = "Deleted user account", returnType = UserAccount.class)
	@ApiResponseRefs({ "400_BadRequest", "403_Forbidden", "500_Error" })
	@ApiTags({ "admin", "secret" })
	public Response deleteUserAccount(@PathParam("username") String username) {

		checkApiKey();

		if (username == null || username.isEmpty()) {
			throw new WebApplicationException(Response.status(Status.BAD_REQUEST).entity("\"Invalid input\"").build());
		}

		Account existingAccount = m_accounts.get(username);

		if (existingAccount == null || !(existingAccount instanceof UserAccount)) {
			throw new WebApplicationException(Response.status(Status.BAD_REQUEST).entity("\"Invalid username\"").build());
		}

		m_accounts.remove(username);

		return Response.ok(existingAccount).build();
	}

	@PUT
	@Path("users/{username}/status")
	@ApiOperation("Sets the user status")
	@ApiResponse(code = 200, description = "Resulting user account", returnType = UserAccount.class)
	@ApiResponseRefs({ "400_BadRequest", "403_Forbidden", "500_Error" })
	@ApiTags("admin")
	public Response updateStatus(@PathParam("username") String username, @QueryParam("userStatus") UserStatus userStatus) {

		checkApiKey();

		if (username == null || username.isEmpty() || userStatus == null) {
			throw new WebApplicationException(Response.status(Status.BAD_REQUEST).entity("\"Invalid input\"").build());
		}

		Account userAccount = m_accounts.get(username);

		if (userAccount == null || !(userAccount instanceof UserAccount)) {
			throw new WebApplicationException(Response.status(Status.BAD_REQUEST).entity("\"Invalid username\"").build());
		}

		((UserAccount) userAccount).setUserStatus(userStatus);

		return Response.ok(userAccount).build();
	}
}
