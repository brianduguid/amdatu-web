/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.demo.rest.openapi.petshop;

import org.amdatu.web.rest.doc.ApiModel;
import org.amdatu.web.rest.doc.ApiProperty;

@ApiModel(name = "Fish", value = "Fish information.")
public class Fish extends Pet {

	@ApiProperty("Is the fish a juvenile?")
	private boolean juvenile;
	@ApiProperty("Is the fish a bottom-feeder?")
	private boolean bottomFeeder;

	public Fish() {
		super();
	}

	public Fish(String name, String color) {
		super(PetType.FISH, name, color);
		this.juvenile = true;
		this.bottomFeeder = false;
	}

	public boolean isJuvenile() {
		return juvenile;
	}

	public Fish setJuvenile(boolean juvenile) {
		this.juvenile = juvenile;
		return this;
	}

	public boolean isBottomFeeder() {
		return bottomFeeder;
	}

	public Fish setBottomFeeder(boolean bottomFeeder) {
		this.bottomFeeder = bottomFeeder;
		return this;
	}
}
