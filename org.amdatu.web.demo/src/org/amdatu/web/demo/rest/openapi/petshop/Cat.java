/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.demo.rest.openapi.petshop;

import org.amdatu.web.rest.doc.ApiModel;
import org.amdatu.web.rest.doc.ApiProperty;

@ApiModel(name = "Cat", value = "Cat information.")
public class Cat extends Pet {

	@ApiProperty("Is the cat a kitten?")
	private boolean kitten;
	@ApiProperty("Is the cat hairless?")
	private boolean hairless;

	public Cat() {
		super();
	}

	public Cat(String name, String color) {
		super(PetType.CAT, name, color);
		this.kitten = true;
		this.hairless = false;
	}

	public boolean isKitten() {
		return kitten;
	}

	public Cat setKitten(boolean kitten) {
		this.kitten = kitten;
		return this;
	}

	public boolean isHairless() {
		return hairless;
	}

	public Cat setHairless(boolean hairless) {
		this.hairless = hairless;
		return this;
	}
}
