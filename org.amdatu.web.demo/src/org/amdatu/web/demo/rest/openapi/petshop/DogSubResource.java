/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.demo.rest.openapi.petshop;

import java.util.List;
import java.util.stream.Collectors;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.amdatu.web.rest.doc.ApiOperation;
import org.amdatu.web.rest.doc.ApiResponse;
import org.amdatu.web.rest.doc.ApiResponseRefs;
import org.amdatu.web.rest.doc.ApiTagDefinition;

@ApiTagDefinition(name = "pet-shop-dogs", description = "Pet shop dogs endpoints")
public class DogSubResource extends PetSubResource {

	public DogSubResource(PetDatabase petDatabase) {
		super(petDatabase);
	}

	@GET
	@ApiOperation("List current dogs.")
	@ApiResponse(code = 200, description = "List of current dogs", returnType = List.class, elementType = Dog.class, headerNames = "x-total-count")
	@ApiResponseRefs({ "403_Forbidden", "500_Error" })
	public Response list() {

		List<Dog> dogs = m_petDatabase.getPets().stream()
								.filter(p -> PetType.DOG.equals(p.getPetType()))
								.map(p -> (Dog) p)
								.collect(Collectors.toList());

		return Response.ok(dogs).header("x-total-count", dogs.size()).build();
	}

	@POST
	@ApiOperation("Create a new dog")
	@ApiResponse(code = 200, description = "New dog", returnType = Dog.class)
	@ApiResponse(code = 409, description = "Dog already exists")
	@ApiResponseRefs({ "400_BadRequest", "403_Forbidden", "500_Error" })
	public Response create(Dog dog) {

		if (dog == null || dog.getName() == null || dog.getName().isEmpty() || dog.getId() > 0) {
			throw new WebApplicationException(Response.status(Status.BAD_REQUEST).entity("\"Invalid input\"").build());
		}

		dog.setPetType(PetType.DOG);

		boolean exists = m_petDatabase.getPets().stream().anyMatch(p -> p.getName().equals(dog.getName()));

		if (exists) {
			throw new WebApplicationException(Response.status(Status.CONFLICT).entity("\"Dog already exists\"").build());
		}

		return Response.ok(m_petDatabase.addPet(dog)).build();
	}

	@PUT
	@Path("{id}")
	@ApiOperation("Update an existing dog")
	@ApiResponse(code = 200, description = "Updated dog", returnType = Dog.class)
	@ApiResponseRefs({ "400_BadRequest", "403_Forbidden", "500_Error" })
	public Response update(@PathParam("id") int id, Dog dog) {

		if (id < 1 || dog == null || id != dog.getId()) {
			throw new WebApplicationException(Response.status(Status.BAD_REQUEST).entity("\"Invalid input\"").build());
		}

		Pet pet = m_petDatabase.findPet(id);

		if (pet == null || !(pet instanceof Dog)) {
			throw new WebApplicationException(Response.status(Status.BAD_REQUEST).entity("\"Invalid id\"").build());
		}

		return Response.ok(m_petDatabase.updatePet(dog)).build();
	}

	@DELETE
	@Path("{id}")
	@ApiOperation("Delete an existing dog")
	@ApiResponse(code = 200, description = "Deleted dog", returnType = Dog.class)
	@ApiResponseRefs({ "400_BadRequest", "403_Forbidden", "500_Error" })
	public Response delete(@PathParam("id") int id) {

		if (id < 1) {
			throw new WebApplicationException(Response.status(Status.BAD_REQUEST).entity("\"Invalid input\"").build());
		}

		Pet pet = m_petDatabase.findPet(id);

		if (pet == null || !(pet instanceof Dog)) {
			throw new WebApplicationException(Response.status(Status.BAD_REQUEST).entity("\"Invalid id\"").build());
		}

		return Response.ok(m_petDatabase.deletePet(id)).build();
	}
}