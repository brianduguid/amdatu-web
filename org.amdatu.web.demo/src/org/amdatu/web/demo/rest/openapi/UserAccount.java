/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.demo.rest.openapi;

import java.util.List;

import javax.ws.rs.DefaultValue;

import org.amdatu.web.rest.doc.ApiModel;
import org.amdatu.web.rest.doc.ApiProperty;

@ApiModel(name = "DemoUser", value = "User account information.")
public class UserAccount extends Account {

	@ApiProperty("Username of the user.")
    private String username;
	@ApiProperty("First name of the user.")
    private String first;
    @ApiProperty("Last name of the user.")
    private String last;
    @DefaultValue("ENABLED")
    @ApiProperty("User account status.")
    private UserStatus userStatus;
    @ApiProperty(value = "User aliases.", required = false)
    private List<String> aliases;

    public UserAccount() {
    		super();
    }

    public UserAccount(int identity, String username, String first, String last) {
        super(identity);
        this.username = username;
        this.first = first;
        this.last = last;
        this.userStatus = UserStatus.ENABLED;
    }

    public String getUsername() {
		return username;
	}

	public UserAccount setUsername(String username) {
		this.username = username;
		return this;
	}

	public String getFirst() {
        return first;
    }

    public UserAccount setFirst(String first) {
        this.first = first;
        return this;
    }

    public String getLast() {
        return last;
    }

    public UserAccount setLast(String last) {
        this.last = last;
        return this;
    }

    public UserStatus getUserStatus() {
		return userStatus;
	}

	public UserAccount setUserStatus(UserStatus userStatus) {
		this.userStatus = userStatus;
		return this;
	}

	public List<String> getAliases() {
		return aliases;
	}

	public UserAccount setAliases(List<String> aliases) {
		this.aliases = aliases;
		return this;
	}
}