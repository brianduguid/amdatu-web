/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.demo.rest.openapi.petshop;

import org.amdatu.web.rest.doc.ApiModel;
import org.amdatu.web.rest.doc.ApiProperty;

@ApiModel(name = "Pet", value = "Basic pet information.")
public class Pet {

	@ApiProperty("ID")
	protected int id;
	@ApiProperty("Type")
	protected PetType petType;
	@ApiProperty("Name")
	protected String name;
	@ApiProperty("Color")
	private String color;

	public Pet() {
	}

	public Pet(PetType petType, String name, String color) {
		super();
		this.petType = petType;
		this.name = name;
		this.color = color;
	}

	public Pet(int id, PetType petType, String name, String color) {
		super();
		this.id = id;
		this.petType = petType;
		this.name = name;
		this.color = color;
	}

	public int getId() {
		return id;
	}

	public Pet setId(int id) {
		this.id = id;
		return this;
	}

	public PetType getPetType() {
		return petType;
	}

	public Pet setPetType(PetType petType) {
		this.petType = petType;
		return this;
	}

	public String getName() {
		return name;
	}

	public Pet setName(String name) {
		this.name = name;
		return this;
	}

	public String getColor() {
		return color;
	}

	public Pet setColor(String color) {
		this.color = color;
		return this;
	}
}
