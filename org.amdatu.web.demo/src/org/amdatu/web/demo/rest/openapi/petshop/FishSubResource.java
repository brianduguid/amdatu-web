/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.demo.rest.openapi.petshop;

import java.util.List;
import java.util.stream.Collectors;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.amdatu.web.rest.doc.ApiOperation;
import org.amdatu.web.rest.doc.ApiResponse;
import org.amdatu.web.rest.doc.ApiResponseRefs;
import org.amdatu.web.rest.doc.ApiTagDefinition;

@ApiTagDefinition(name = "pet-shop-fish", description = "Pet shop fish endpoints")
public class FishSubResource extends PetSubResource {

	public FishSubResource(PetDatabase petDatabase) {
		super(petDatabase);
	}

	@GET
	@ApiOperation("List current fish.")
	@ApiResponse(code = 200, description = "List of current fish", returnType = List.class, elementType = Fish.class, headerNames = "x-total-count")
	@ApiResponseRefs({ "403_Forbidden", "500_Error" })
	public Response list() {

		List<Fish> fish = m_petDatabase.getPets().stream()
								.filter(p -> PetType.FISH.equals(p.getPetType()))
								.map(p -> (Fish) p)
								.collect(Collectors.toList());

		return Response.ok(fish).header("x-total-count", fish.size()).build();
	}

	@POST
	@ApiOperation("Create a new fish")
	@ApiResponse(code = 200, description = "New fish", returnType = Fish.class)
	@ApiResponse(code = 409, description = "Fish already exists")
	@ApiResponseRefs({ "400_BadRequest", "403_Forbidden", "500_Error" })
	public Response create(Fish fish) {

		if (fish == null || fish.getName() == null || fish.getName().isEmpty() || fish.getId() > 0) {
			throw new WebApplicationException(Response.status(Status.BAD_REQUEST).entity("\"Invalid input\"").build());
		}

		fish.setPetType(PetType.FISH);

		boolean exists = m_petDatabase.getPets().stream().anyMatch(p -> p.getName().equals(fish.getName()));

		if (exists) {
			throw new WebApplicationException(Response.status(Status.CONFLICT).entity("\"Fish already exists\"").build());
		}

		return Response.ok(m_petDatabase.addPet(fish)).build();
	}

	@PUT
	@Path("{id}")
	@ApiOperation("Update an existing fish")
	@ApiResponse(code = 200, description = "Updated fish", returnType = Fish.class)
	@ApiResponseRefs({ "400_BadRequest", "403_Forbidden", "500_Error" })
	public Response update(@PathParam("id") int id, Fish fish) {

		if (id < 1 || fish == null || id != fish.getId()) {
			throw new WebApplicationException(Response.status(Status.BAD_REQUEST).entity("\"Invalid input\"").build());
		}

		Pet pet = m_petDatabase.findPet(id);

		if (pet == null || !(pet instanceof Fish)) {
			throw new WebApplicationException(Response.status(Status.BAD_REQUEST).entity("\"Invalid id\"").build());
		}

		return Response.ok(m_petDatabase.updatePet(fish)).build();
	}

	@DELETE
	@Path("{id}")
	@ApiOperation("Delete an existing fish")
	@ApiResponse(code = 200, description = "Deleted fish", returnType = Fish.class)
	@ApiResponseRefs({ "400_BadRequest", "403_Forbidden", "500_Error" })
	public Response delete(@PathParam("id") int id) {

		if (id < 1) {
			throw new WebApplicationException(Response.status(Status.BAD_REQUEST).entity("\"Invalid input\"").build());
		}

		Pet pet = m_petDatabase.findPet(id);

		if (pet == null || !(pet instanceof Fish)) {
			throw new WebApplicationException(Response.status(Status.BAD_REQUEST).entity("\"Invalid id\"").build());
		}

		return Response.ok(m_petDatabase.deletePet(id)).build();
	}
}