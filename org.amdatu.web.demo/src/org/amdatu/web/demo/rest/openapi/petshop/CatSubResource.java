/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.demo.rest.openapi.petshop;

import java.util.List;
import java.util.stream.Collectors;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.amdatu.web.rest.doc.ApiOperation;
import org.amdatu.web.rest.doc.ApiResponse;
import org.amdatu.web.rest.doc.ApiResponseRefs;
import org.amdatu.web.rest.doc.ApiTagDefinition;

@ApiTagDefinition(name = "pet-shop-cats", description = "Pet shop cats endpoints")
public class CatSubResource extends PetSubResource {

	public CatSubResource(PetDatabase petDatabase) {
		super(petDatabase);
	}

	@GET
	@ApiOperation("List current cats.")
	@ApiResponse(code = 200, description = "List of current cats", returnType = List.class, elementType = Cat.class, headerNames = "x-total-count")
	@ApiResponseRefs({ "403_Forbidden", "500_Error" })
	public Response list() {

		List<Cat> cats = m_petDatabase.getPets().stream()
								.filter(p -> PetType.CAT.equals(p.getPetType()))
								.map(p -> (Cat) p)
								.collect(Collectors.toList());

		return Response.ok(cats).header("x-total-count", cats.size()).build();
	}

	@POST
	@ApiOperation("Create a new cat")
	@ApiResponse(code = 200, description = "New cat", returnType = Cat.class)
	@ApiResponse(code = 409, description = "Cat already exists")
	@ApiResponseRefs({ "400_BadRequest", "403_Forbidden", "500_Error" })
	public Response create(Cat cat) {

		if (cat == null || cat.getName() == null || cat.getName().isEmpty() || cat.getId() > 0) {
			throw new WebApplicationException(Response.status(Status.BAD_REQUEST).entity("\"Invalid input\"").build());
		}

		cat.setPetType(PetType.CAT);

		boolean exists = m_petDatabase.getPets().stream().anyMatch(p -> p.getName().equals(cat.getName()));

		if (exists) {
			throw new WebApplicationException(Response.status(Status.CONFLICT).entity("\"Cat already exists\"").build());
		}

		return Response.ok(m_petDatabase.addPet(cat)).build();
	}

	@PUT
	@Path("{id}")
	@ApiOperation("Update an existing cat")
	@ApiResponse(code = 200, description = "Updated cat", returnType = Cat.class)
	@ApiResponseRefs({ "400_BadRequest", "403_Forbidden", "500_Error" })
	public Response update(@PathParam("id") int id, Cat cat) {

		if (id < 1 || cat == null || id != cat.getId()) {
			throw new WebApplicationException(Response.status(Status.BAD_REQUEST).entity("\"Invalid input\"").build());
		}

		Pet pet = m_petDatabase.findPet(id);

		if (pet == null || !(pet instanceof Cat)) {
			throw new WebApplicationException(Response.status(Status.BAD_REQUEST).entity("\"Invalid id\"").build());
		}

		return Response.ok(m_petDatabase.updatePet(cat)).build();
	}

	@DELETE
	@Path("{id}")
	@ApiOperation("Delete an existing cat")
	@ApiResponse(code = 200, description = "Deleted cat", returnType = Cat.class)
	@ApiResponseRefs({ "400_BadRequest", "403_Forbidden", "500_Error" })
	public Response delete(@PathParam("id") int id) {

		if (id < 1) {
			throw new WebApplicationException(Response.status(Status.BAD_REQUEST).entity("\"Invalid input\"").build());
		}

		Pet pet = m_petDatabase.findPet(id);

		if (pet == null || !(pet instanceof Cat)) {
			throw new WebApplicationException(Response.status(Status.BAD_REQUEST).entity("\"Invalid id\"").build());
		}

		return Response.ok(m_petDatabase.deletePet(id)).build();
	}
}
