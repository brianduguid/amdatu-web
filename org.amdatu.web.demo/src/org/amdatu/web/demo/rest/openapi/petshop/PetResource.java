/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.demo.rest.openapi.petshop;

import static org.amdatu.web.rest.jaxrs.AmdatuWebRestConstants.JAX_RS_RESOURCE_BASE;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.amdatu.web.demo.rest.openapi.BaseResource;
import org.amdatu.web.rest.doc.ApiOperation;
import org.amdatu.web.rest.doc.ApiResponse;
import org.amdatu.web.rest.doc.ApiResponseRefs;
import org.amdatu.web.rest.doc.ApiTagDefinition;
import org.amdatu.web.rest.doc.ApiTags;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.Property;

@Path("pets")
@Component(provides=Object.class)
@Property(name = JAX_RS_RESOURCE_BASE, value = "/api/full")
@Produces(MediaType.APPLICATION_JSON) // produces json by default
@ApiTagDefinition(name = "pet-shop", description = "Pet shop's generic endpoints")
@ApiTagDefinition(name = "pet-shop-cats", description = "Pet shop's cats endpoints")
@ApiTagDefinition(name = "pet-shop-dogs", description = "Pet shop's dogs endpoints")
@ApiTagDefinition(name = "pet-shop-fish", description = "Pet shop's fish endpoints")
public class PetResource extends BaseResource {

	private PetDatabase m_petDatabase;

	private CatSubResource m_catSubResource;
	private DogSubResource m_dogSubResource;
	private FishSubResource m_fishSubResource;

	public PetResource() {

		m_petDatabase = new PetDatabase();

		m_petDatabase.addPet(new Cat("Theodore", "Orange"));
		m_petDatabase.addPet(new Cat("Freak", "Brown").setHairless(true));
		m_petDatabase.addPet(new Dog("Chandler", "Yellow"));
		m_petDatabase.addPet(new Dog("Junkyard", "Gray").setTailless(true));
		m_petDatabase.addPet(new Fish("Lightning", "Blue"));
		m_petDatabase.addPet(new Fish("Scrubs", "Black").setBottomFeeder(true));

		m_catSubResource = new CatSubResource(m_petDatabase);
		m_dogSubResource = new DogSubResource(m_petDatabase);
		m_fishSubResource = new FishSubResource(m_petDatabase);
	}

	@GET
	@ApiOperation("List current pets.")
	@ApiResponse(code = 200, description = "List of current pets", returnType = List.class, elementType = Pet.class, headerNames = "x-total-count")
	@ApiResponseRefs({ "403_Forbidden", "500_Error" })
	@ApiTags("pet-shop")
	public Response listPets() {
		checkApiKey();

		Collection<Pet> allPets = m_petDatabase.getPets();

		return Response.ok(allPets).header("x-total-count", allPets.size()).build();
	}

	@DELETE
	@Path("/all")
	@ApiOperation("Delete ALL existing cat")
	@ApiResponse(code = 200, description = "List of deleted pets", returnType = List.class, elementType = Pet.class, headerNames = "x-total-count")
	@ApiResponseRefs({ "403_Forbidden", "500_Error" })
	@ApiTags({"secret", "pet-shop"})
	@Deprecated
	public Response deleteAll() {

		Collection<Pet> deletedPets = m_petDatabase.getPets().stream()
													.filter(p -> m_petDatabase.deletePet(p.getId()) != null)
													.collect(Collectors.toList());

		return Response.ok(deletedPets).header("x-total-count", deletedPets.size()).build();
	}

	@Path("/cat")
	@ApiTags("pet-shop-cats")
	public CatSubResource processCats() {
		checkApiKey();

		return m_catSubResource;
	}

	@Path("dog")
	@ApiTags("pet-shop-dogs")
	public DogSubResource processDogs() {
		checkApiKey();

		return m_dogSubResource;
	}

	@Path("fish")
	@ApiTags("pet-shop-fish")
	public FishSubResource processFish() {
		checkApiKey();

		return m_fishSubResource;
	}
}
