/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.demo.rest;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.amdatu.web.rest.doc.Description;
import org.amdatu.web.rest.doc.Notes;
import org.amdatu.web.rest.doc.ResponseMessage;
import org.amdatu.web.rest.doc.ResponseMessages;

@Path("user")
@Description("Demonstrates how to create REST endpoints.")
public interface UserResource {

    /**
     * @return a JSON text string denoting a {@link User}, when sending a GET-request with the header "Accept: application/json".
     */
    @GET
    @Path("user")
    @Produces(MediaType.APPLICATION_JSON)
    @Description("Returns the first and last name of the user.")
    User user();

    /**
     * @param user the new user information, when sending a POST-request with the header "Content-Type: application/json" or "Content-Type: application/vnd.custom+json".
     */
    @POST
    @Path("user")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Description("Sets the first and last name for the user, needs a FORM object with the fields 'first' and 'last'.")
    @Notes("The order of the fields is 'first', 'last'.")
    @Deprecated()
    public void setUser(@FormParam("first") @DefaultValue("John") String firstName, @FormParam("last") @DefaultValue("Doe") String lastName);

    /**
     * @param user the new user information, when sending a POST-request with the header "Content-Type: application/json" or "Content-Type: application/vnd.custom+json".
     */
    @POST
    @Path("user")
    @Consumes({ MediaType.APPLICATION_JSON, "application/vnd.custom+json" })
    @Description("Sets the first and last name for the user, needs a JSON object with the fields 'first' and 'last'.")
    @Notes("The order of the fields is irrelevant.")
    @ResponseMessages({ @ResponseMessage(code = 415, message = "In case of an unknown body.") })
    public void setUserViaJSON(User user);

    @POST
    @Path("post-array")
    @Produces(MediaType.APPLICATION_JSON)
    @Description("Will save an array of users.")
    public void saveArray(User[] users);

    @POST
    @Path("post-list")
    @Produces(MediaType.APPLICATION_JSON)
    @Description("Will save a list of users.")
    public void saveList(List<User> users);

    @GET
    @Path("users-array")
    @Produces(MediaType.APPLICATION_JSON)
    @Description("Get an array of users.")
    public User[] getUserArray();

    @GET
    @Path("users-list")
    @Description("Get a list of users.")
    @Produces(MediaType.APPLICATION_JSON)
    public List<User> getUserList();
}
