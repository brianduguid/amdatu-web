/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.demo.rest.client;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.amdatu.web.rest.doc.Description;

@Path("hello")
@Description("Demonstrates how to create REST endpoints.")
@Consumes(MediaType.APPLICATION_JSON)
public interface HelloWorld {

    @GET
    String helloPlain();

    @GET
    @Path("response-with-headers")
    public Response helloResponse();

    /**
     * @return a JSON text string, when sending a GET-request with the header "Accept: application/json" or "Accept: application/vnd.custom+json".
     */
    @GET
    @Path("custom-mediatype")
    public String helloJSON();

    /**
     * @return a plain text string, when sending a GET-request with the header "Accept: text/plain".
     */
    @GET
    @Path("with-uri-info")
    public String hello2(@Context UriInfo uriInfo, @Context HttpServletRequest request);

}
