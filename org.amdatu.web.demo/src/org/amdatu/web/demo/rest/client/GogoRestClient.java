/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.demo.rest.client;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.amdatu.web.demo.rest.User;
import org.amdatu.web.demo.rest.UserResource;
import org.amdatu.web.rest.jaxrs.client.AmdatuClientBuilder;
import org.amdatu.web.rest.jaxrs.client.AmdatuClient;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.Property;
import org.apache.felix.dm.annotation.api.ServiceDependency;
import org.apache.felix.dm.annotation.api.Start;

import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;

@Component(provides = Object.class)
@Property(name = "osgi.command.scope", value = "rest")
@Property(name = "osgi.command.function", value = { "demo", "hello", "user" })
public class GogoRestClient {

    @ServiceDependency
    private volatile ClientBuilder m_clientBuilder;

    @ServiceDependency
    private volatile AmdatuClientBuilder m_amdatuClientBuilder;

    @Start
    protected final void start() {
        m_amdatuClientBuilder.register(JacksonJsonProvider.class);
    }

    public void demo() {
        Client client = m_clientBuilder.build();

        WebTarget target = client.target("http://localhost:8080/legacy/hello");
        Response response = target.request().get();
        String value = response.readEntity(String.class);
        int status = response.getStatus();
        response.close();

        System.out.println(String.format("Response (status: %s): '%s'", status, value));
    }

    public void hello() {
        AmdatuClient client = m_amdatuClientBuilder.build();

        HelloWorld helloWorld = client.target("http://localhost:8080/rest").proxy(HelloWorld.class);

        System.out.println(String.format("Response: '%s'", helloWorld.helloPlain()));
    }

    public void user(String... does) {
        AmdatuClient client = m_amdatuClientBuilder.build();

        UserResource userResource = client.target("http://localhost:8080/rest").proxyBuilder(UserResource.class)
            .defaultConsumes(MediaType.APPLICATION_JSON).build();

        if (does.length == 0) {
            System.out.println(String.format("Response: '%s'", userResource.getUserList()));
        } else {
            List<User> users = new ArrayList<>(does.length);
            for (String first : does) {
                users.add(new User(first, "Doe"));
            }
            userResource.saveList(users);
        }
    }

}
