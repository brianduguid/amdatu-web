/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.demo.rest.legacy;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.amdatu.web.rest.doc.Description;
import org.amdatu.web.rest.doc.ResponseMessage;
import org.amdatu.web.rest.doc.ResponseMessages;
import org.apache.felix.dm.annotation.api.Component;


@Path("hello")
@Description("Demonstrates how to register a resource using a legacy Application.")
@Component(provides = Object.class)
public class LegacyApplicationResource {

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    @Description("Will return a friendly message from your server.")
    @ResponseMessages({ @ResponseMessage(code = 200, message = "Friendly text message") })
    public String helloPlain() {
        return "hello from a legacy application resource";
    }

}
