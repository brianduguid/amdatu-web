/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.util.upload;

import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;
import java.util.OptionalLong;

import javax.servlet.http.HttpServletRequest;

/**
 * Represents an abstraction for uploaded files that are passed to an {@link UploadHandler}.
 * <p>
 * {@link UploadHandler}s have the guarantee that if they receive an {@link UploadedFile} instance, that its contents is
 * present and can be read at least once.<br>
 * Note that as soon as the {@link UploadHandler} returns, its caller is obliged to reclaim all resources that were used
 * for the {@link UploadedFile}s. This means that if you want to retain an uploaded file for later use, you <b>must</b>
 * copy its contents to another location.
 * </p>
 */
public interface UploadedFile {
    /**
     * @return the input stream that provides access to the actual content of the uploaded file, never
     *         <code>null</code>.
     */
    InputStream getContent() throws IOException;

    /**
     * @return the optional content type of the uploaded file.
     */
    Optional<String> getContentType();

    /**
     * @return the HTTP servlet request the file upload originates from, never <code>null</code>.
     */
    HttpServletRequest getRequest();

    /**
     * @return the name of the uploaded file, never <code>null</code>.
     */
    String getName();

    /**
     * @return the optional length of the uploaded file, in bytes.
     */
    OptionalLong getSize();
}