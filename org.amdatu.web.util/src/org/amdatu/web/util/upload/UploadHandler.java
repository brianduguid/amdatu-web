/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.util.upload;

import static java.util.stream.Collectors.toList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/**
 * Provides a service that is responsible for handling uploads.
 * <p>
 * Implementations of this handler should be responsible for dealing with one or more uploaded files. They can do with
 * these files anything they like, with the only constraint that if they want to retain their contents, a copy
 * <b>must</b> be created.<br>
 * The values returned by an {@link UploadHandler} can be used to report information back to the calling side. For
 * example, one can report the location and name on which the uploaded file can be retrieved again back to the user
 * agent. There is no one-to-one mapping between the given uploaded files and returned resources. An upload handler can
 * return any resource they desire, or even omit them completely.<br>
 * In case an uploaded file does not meet the preconditions of the upload handler, an {@link UploadException} can be
 * thrown to abort the processing and return an error to the calling side.
 * </p>
 */
public interface UploadHandler<T> {

    /**
     * Handles one or more uploaded files.
     * <p>
     * For each given file, the implementation <em>can</em> return a corresponding {@link UploadResource} that provides
     * information about the uploaded resources, for example, the location where the uploaded file can be retrieved
     * again. Note that implementations may omit this information, for example, because they consumed the uploaded file
     * and do not provide access to it.
     * </p>
     *
     * @param files uploaded files, cannot be <code>null</code> or empty.
     * @return an array with {@link UploadResource} containing the (meta) information of the resources that were added,
     *         never <code>null</code>, but an empty array is possible.
     * @throws UploadException in case the given uploaded file could not be accepted by this handler;
     * @throws IOException in case of I/O errors while reading one of the given uploaded files.
     */
    default List<T> addResources(UploadedFile[] files) throws UploadException, IOException {
        try {
            return Arrays.stream(files)
                .map(file -> {
                    try {
                        return addResource(file);
                    }
                    catch (IOException e) {
                        throw new UploadException(file.getName(), "Unable to read content!", e);
                    }
                })
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(toList());
        }
        catch (UploadException e) {
            // Unwrap any I/O exceptions as convenience to our caller...
            Throwable cause = e.getCause();
            if (cause instanceof IOException) {
                throw (IOException) cause;
            }
            throw e;
        }
    }

    /**
     * Handles an uploaded files.
     *
     * @param file the uploaded file, cannot be <code>null</code>.
     * @return an optional {@link UploadResource} containing the (meta) information of the resource that was added,
     *         never <code>null</code>.
     * @throws UploadException in case the given uploaded file could not be accepted by this handler;
     * @throws IOException in case of I/O errors while reading the given uploaded file.
     */
    Optional<T> addResource(UploadedFile file) throws UploadException, IOException;

}
