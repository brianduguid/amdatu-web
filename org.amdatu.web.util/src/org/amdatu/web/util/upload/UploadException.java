/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.util.upload;

import static java.util.Objects.requireNonNull;

/**
 * Thrown in case an {@link UploadHandler} cannot handle an uploaded resource due to whatever reason.
 */
public class UploadException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    private final String m_name;

    public UploadException(String name) {
        this(name, null, null);
    }

    public UploadException(String name, String message) {
        this(name, message, null);
    }

    public UploadException(String name, Throwable cause) {
        this(name, null, cause);
    }

    public UploadException(String name, String message, Throwable cause) {
        super(message, cause);
        m_name = requireNonNull(name);
    }

    /**
     * @return the name of the resource that failed to upload, never <code>null</code>.
     */
    public String getName() {
        return m_name;
    }
}
