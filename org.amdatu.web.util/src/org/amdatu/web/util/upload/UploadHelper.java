/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.util.upload;

import static java.util.Collections.emptyList;
import static java.util.Objects.requireNonNull;
import static java.util.Optional.empty;
import static java.util.Optional.of;
import static javax.servlet.http.HttpServletResponse.SC_BAD_REQUEST;
import static javax.servlet.http.HttpServletResponse.SC_CONFLICT;
import static javax.servlet.http.HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
import static javax.servlet.http.HttpServletResponse.SC_NO_CONTENT;
import static javax.servlet.http.HttpServletResponse.SC_OK;
import static javax.servlet.http.HttpServletResponse.SC_REQUEST_ENTITY_TOO_LARGE;
import static org.apache.commons.fileupload.servlet.ServletFileUpload.isMultipartContent;
import static org.osgi.service.log.LogService.LOG_DEBUG;
import static org.osgi.service.log.LogService.LOG_INFO;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Optional;
import java.util.OptionalLong;
import java.util.function.BiConsumer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadBase.FileSizeLimitExceededException;
import org.apache.commons.fileupload.FileUploadBase.SizeLimitExceededException;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItem;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

/**
 * Provides a helper class that is capable of handling resource uploads in a generic way.
 * <p>
 * This helper can be used from plain Servlets as well as from JAXRS resources and provides a functional abstraction on
 * top of commons-fileupload. This means that you only need to provide an implementation of {@link UploadHandler}, which
 * will be called for all uploaded files.
 * </p>
 * <p>
 * {@link UploadHandler}s are given each uploaded file in such way that they can read the uploaded contents at least
 * once. They <b>must</b> copy these contents to a safe location if they wish to access it later on! Once the upload
 * handler is finished, any temporary file that is created as part of the original upload will be removed.
 * </p>
 */
public class UploadHelper<T> {
    /**
     * Abstracts commons-fileupload {@link FileItem}s.
     */
    static class UploadedFileImpl implements UploadedFile {
        private final FileItem m_item;
        private final HttpServletRequest m_req;

        public UploadedFileImpl(FileItem item, HttpServletRequest req) {
            m_item = requireNonNull(item);
            m_req = requireNonNull(req);
        }

        public void cleanup() {
            m_item.delete();
        }

        @Override
        public InputStream getContent() throws IOException {
            return m_item.getInputStream();
        }

        @Override
        public Optional<String> getContentType() {
            return Optional.ofNullable(m_item.getContentType());
        }

        @Override
        public String getName() {
            return m_item.getName();
        }

        @Override
        public HttpServletRequest getRequest() {
            return m_req;
        }

        @Override
        public OptionalLong getSize() {
            long size = m_item.getSize();
            if (size < 0) {
                return OptionalLong.empty();
            }
            return OptionalLong.of(size);
        }
    }

    static class UploadedFileItemFactory implements FileItemFactory {
        @Override
        public FileItem createItem(String fieldName, String contentType, boolean isFormField, String fileName) {
            // Keep form-fields, we're not using them anyways. Non-empty files are written to disk...
            int sizeThreshold = isFormField ? 256 * 1024 : 1;
            return new DiskFileItem(fieldName, contentType, isFormField, fileName, sizeThreshold, null);
        }
    }

    private final UploadHandler<T> m_handler;
    private final BiConsumer<Integer, String> m_logHelper;
    private final FileItemFactory m_itemFactory;
    private final long m_maxFileSize;

    /**
     * Creates a new {@link UploadHelper} instance without any logging.
     *
     * @param handler the upload handler to pass off any uploaded files to, cannot be <code>null</code>;
     * @param maxFileSize the maximum size (in bytes) for an uploaded file to be accepted, &gt; 0. Use <tt>-1</tt> to
     *        set no limit.
     */
    public UploadHelper(UploadHandler<T> handler, long maxFileSize) {
        this(handler, maxFileSize, (l, m) -> {});
    }

    /**
     * Creates a new {@link UploadHelper} instance.
     *
     * @param handler the upload handler to pass off any uploaded files to, cannot be <code>null</code>;
     * @param maxFileSize the maximum size (in bytes) for an uploaded file to be accepted, &gt; 0. Use <tt>-1</tt> to
     *        set no limit;
     * @param logHelper the consumer for (debug) log messages, cannot be <code>null</code>.
     */
    public UploadHelper(UploadHandler<T> handler, long maxFileSize, BiConsumer<Integer, String> logHelper) {
        m_handler = handler;
        m_logHelper = logHelper;
        m_maxFileSize = maxFileSize;
        m_itemFactory = new UploadedFileItemFactory();
    }

    /**
     * Convenience method to handle requests that are expected to contain only a single file upload.
     * <p>
     * This method is a wrapper for {@link #handle(HttpServletRequest, HttpServletResponse)} and simply returns the
     * first uploaded resource from it.
     * </p>
     *
     * @param req the servlet request to use, cannot be <code>null</code>;
     * @param resp the servlet response to use, cannot be <code>null</code>;
     * @return an empty optional if the request was invalid and the upload failed, otherwise the list with uploaded
     *         resources.
     * @throws IOException in case of I/O problems.
     * @see #handleUpload(HttpServletRequest, HttpServletResponse)
     */
    public Optional<T> handleSingleUpload(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        return handle(req, resp, 1)
            .orElse(emptyList())
            .stream()
            .findFirst();
    }

    /**
     * Handles the upload for the given servlet request and passes each uploaded file to the contained handler.
     * <p>
     * This method will do all basic checks and error handling on behalf of the caller. This means that if this method
     * returns any non-empty result that the caller only need to return the desired information to the user agent.<br>
     * If an empty result is returned, the proper response codes are already set on the provided servlet response. The
     * following response codes are send:
     * </p>
     * <ul>
     * <li>in case of form submissions <em>without</em> any file, a "206 No Content" response is returned;</li>
     * <li>in case of incorrect method, content type or (non upload) exceptions, a "400 Bad Request" response is
     * returned;</li>
     * <li>in case of file uploads that exceed the maximum defined size, a "413 Request Entity Too Large" response is
     * returned;</li>
     * <li>in case of {@link UploadException}s, a "409 Conflict" response is returned.</li>
     * </ul>
     *
     * @param req the servlet request to use, cannot be <code>null</code>;
     * @param resp the servlet response to use, cannot be <code>null</code>;
     * @return an empty optional if the request was invalid and the upload failed, otherwise the list with uploaded
     *         resources.
     * @throws IOException in case of I/O problems.
     */
    public Optional<List<T>> handleUpload(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        return handle(req, resp, Integer.MAX_VALUE);
    }

    /**
     * Handles the upload for the given servlet request and passes each uploaded file to the contained handler.
     * <p>
     * This method will do all basic checks and error handling on behalf of the caller. This means that if this method
     * returns any non-empty result that the caller only need to return the desired information to the user agent.<br>
     * If an empty result is returned, the proper response codes are already set on the provided servlet response. The
     * following response codes are send:
     * </p>
     * <ul>
     * <li>in case of form submissions <em>without</em> any file, a "206 No Content" response is returned;</li>
     * <li>in case of {@link UploadException}s, a "409 Conflict" response is returned;</li>
     * <li>in case of {@link IOException}s, a "500 Internal Server Error" response is returned;</li>
     * <li>in case of file uploads that exceed the maximum defined size, a "413 Request Entity Too Large" response is
     * returned;</li>
     * <li>in case of incorrect method, content type or (non upload) exceptions, a "400 Bad Request" response is
     * returned;</li>
     * <li>otherwise, a "200 OK" response is returned with the list of uploaded resources.</li>
     * </ul>
     *
     * @param req the servlet request to use, cannot be <code>null</code>;
     * @param resp the servlet response to use, cannot be <code>null</code>;
     * @return an empty optional if the request was invalid and the upload failed, otherwise the list with uploaded
     *         resources.
     * @throws IOException in case of I/O problems.
     */
    protected Optional<List<T>> handle(HttpServletRequest req, HttpServletResponse resp, int maxFiles)
        throws IOException {
        // Check whether it is a POST, and contains something that looks like a multi-part form...
        if (!isMultipartContent(req)) {
            log(LOG_INFO, "Not a file upload request for '%s'!", req.getPathInfo());

            handleException(resp, SC_BAD_REQUEST, null);
            return empty();
        }

        UploadHandler<T> handler = m_handler;
        UploadedFile[] uploadedFiles = null;

        try {
            uploadedFiles = parseRequest(req, maxFiles);
            if (uploadedFiles.length == 0) {
                log(LOG_INFO, "Nothing uploaded, or no files included...");

                resp.setStatus(SC_NO_CONTENT);
                return empty();
            }

            log(LOG_DEBUG, "Uploaded %d files successfully. Calling handler...", uploadedFiles.length);

            List<T> addedResources = handler.addResources(uploadedFiles);

            log(LOG_DEBUG, "Handled %d of %d uploaded files successfully.", addedResources.size(),
                uploadedFiles.length);

            resp.setStatus(SC_OK);
            return of(addedResources);
        }
        catch (IOException e) {
            log(LOG_INFO, "Upload failed for '%s': %s!", req.getPathInfo(), e.getMessage(), e);

            handleException(resp, SC_INTERNAL_SERVER_ERROR, e);
            return empty();
        }
        catch (UploadException e) {
            log(LOG_INFO, "Upload failed for '%s': %s!", req.getPathInfo(), e.getMessage());

            handleException(resp, SC_CONFLICT, e);
            return empty();
        }
        catch (SizeLimitExceededException | FileSizeLimitExceededException e) {
            log(LOG_INFO, "Upload failed for '%s': entity too large!", req.getPathInfo());

            handleException(resp, SC_REQUEST_ENTITY_TOO_LARGE, e);
            return empty();
        }
        catch (FileUploadException e) {
            log(LOG_INFO, "Upload failed for '%s': %s!", req.getPathInfo(), e.getMessage());

            handleException(resp, SC_BAD_REQUEST, e);
            return empty();
        }
        finally {
            cleanup(uploadedFiles);
        }
    }

    /**
     * Takes care of handling a given exception for the given servlet response.
     *
     * @param response the servlet response to handle the exception for;
     * @param status the proposed response code to return to the callar;
     * @param e the exception to handle.
     * @throws IOException in case the response was already closed and could not be changed anymore.
     */
    protected void handleException(HttpServletResponse response, int status, Exception e) throws IOException {
        response.setStatus(status);
        // We need to explicitly flush the buffer to ensure that other frameworks, like
        // JAXRS, do not overwrite the response code we've just set...
        response.flushBuffer();
    }

    private void cleanup(UploadedFile[] uploadedFiles) {
        if (uploadedFiles == null || uploadedFiles.length < 1) {
            // Nothing to do...
            return;
        }

        for (UploadedFile uploadedFile : uploadedFiles) {
            ((UploadedFileImpl) uploadedFile).cleanup();
        }
    }

    private void log(int level, String msg, Object... args) {
        m_logHelper.accept(level, String.format(msg, args));
    }

    private UploadedFile[] parseRequest(HttpServletRequest req, int fileCount) throws FileUploadException {
        ServletFileUpload upload = new ServletFileUpload(m_itemFactory);
        upload.setFileSizeMax(m_maxFileSize);

        return upload.parseRequest(req).stream()
            .filter(item -> !item.isFormField())
            .map(item -> new UploadedFileImpl(item, req))
            .limit(fileCount)
            .toArray(s -> new UploadedFile[s]);
    }
}
