/*
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package org.amdatu.web.util.download;

import static java.nio.channels.Channels.newChannel;
import static java.nio.charset.StandardCharsets.ISO_8859_1;
import static javax.servlet.http.HttpServletResponse.SC_NOT_FOUND;
import static javax.servlet.http.HttpServletResponse.SC_NOT_MODIFIED;
import static javax.servlet.http.HttpServletResponse.SC_NO_CONTENT;
import static javax.servlet.http.HttpServletResponse.SC_OK;
import static javax.servlet.http.HttpServletResponse.SC_PARTIAL_CONTENT;
import static javax.servlet.http.HttpServletResponse.SC_PRECONDITION_FAILED;
import static javax.servlet.http.HttpServletResponse.SC_REQUESTED_RANGE_NOT_SATISFIABLE;
import static org.amdatu.web.util.HttpHeaderUtil.getContentDispositionHeader;
import static org.amdatu.web.util.HttpHeaders.ACCEPT_RANGES;
import static org.amdatu.web.util.HttpHeaders.CONTENT_DISPOSITION;
import static org.amdatu.web.util.HttpHeaders.CONTENT_LENGTH;
import static org.amdatu.web.util.HttpHeaders.CONTENT_RANGE;
import static org.amdatu.web.util.HttpHeaders.CONTENT_TYPE;
import static org.amdatu.web.util.HttpHeaders.ETAG;
import static org.amdatu.web.util.HttpHeaders.LAST_MODIFIED;
import static org.amdatu.web.util.HttpHeaders.RANGE;
import static org.amdatu.web.util.HttpRequestUtil.notModified;
import static org.amdatu.web.util.HttpRequestUtil.preconditionFailed;
import static org.amdatu.web.util.Range.parse;
import static org.osgi.service.log.LogService.LOG_DEBUG;
import static org.osgi.service.log.LogService.LOG_INFO;

import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.WritableByteChannel;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.OptionalLong;
import java.util.UUID;
import java.util.function.BiConsumer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.amdatu.web.util.ETag;
import org.amdatu.web.util.Range;
import org.amdatu.web.util.Range.InvalidRangeException;

/**
 * Provides a utility class that is capable of downloading resources in a generic way, providing support for
 * content-ranges and entity tags.
 * <p>
 * This helper can be used from plain Servlets as well as from JAXRS resources and provides a more functional resource
 * serving abstraction. This means that you only need to provide an implementation of {@link DownloadHandler}, which
 * will be called for all requests to download a particular resource.
 * </p>
 */
public class DownloadHelper {
    private static final String APPLICATION_OCTET_STREAM = "application/octet-stream";
    private static final int MIN_BUF_SIZE = 65536;

    private final DownloadHandler m_handler;
    private final BiConsumer<Integer, String> m_logHelper;
    private final String m_boundary;

    /**
     * Creates a new {@link DownloadHelper} instance without any logging.
     *
     * @param handler the download handler to use for providing the downloads, cannot be <code>null</code>.
     */
    public DownloadHelper(DownloadHandler handler) {
        this(handler, (l, m) -> {});
    }

    /**
     * Creates a new {@link DownloadHelper} instance.
     *
     * @param handler the download handler to use for providing the downloads, cannot be <code>null</code>;
     * @param logHelper the consumer for (debug) log messages, cannot be <code>null</code>.
     */
    public DownloadHelper(DownloadHandler handler, BiConsumer<Integer, String> logHelper) {
        m_handler = handler;
        m_logHelper = logHelper;
        m_boundary = UUID.randomUUID().toString();
    }

    /**
     * Handles the downloading of a particular resource as defined by the given servlet request.
     * <p>
     * This method will do all basic checks and error handling on behalf of the caller. The following response codes are
     * returned:
     * </p>
     * <ul>
     * <li>in case a request resource could not be found, a "404 Not Found" response is returned;</li>
     * <li>in case a request defines a precondition using <tt>If-Match</tt> and <tt>If-Unmodified-Since</tt> headers
     * that does <b>not</b> correspond to the information provided by the resource, a "412 Precondition Failed" response
     * is returned;</li>
     * <li>in case a request defines a precondition using <tt>If-None-Match</tt> and <tt>If-Modified-Since</tt> headers
     * that <b>do</b> correspond to the information provided by the resource, a "304 Not Modified" response is
     * returned;</li>
     * <li>in case a request defines one or more range(s) that can <b>not</b> be provided, a "416 Requested Range Not
     * Satisfiable" response is returned;</li>
     * <li>in case a request defines one or more range(s) that <b>can</b> be provided, a "206 Partial Content" response
     * is returned with the requested range(s) of the requested resource;</li>
     * <li>otherwise, a "200 OK" response is returned with the entire contents of the requested resource.</li>
     * </ul>
     *
     * @param req the servlet request to use, cannot be <code>null</code>;
     * @param resp the servlet response to use, cannot be <code>null</code>;
     * @param head <code>true</code> if the response should only provide the headers and not the actual downloadable
     *        content, <code>false</code> to stream the content in the response as well.
     * @throws IOException in case of I/O problems.
     */
    public void handle(HttpServletRequest req, HttpServletResponse resp, boolean head) throws IOException {
        resp.resetBuffer();

        Optional<DownloadResource> res = m_handler.getResource(req);
        if (!res.isPresent()) {
            log(LOG_INFO, "No resource found or matched: '%s'!", req.getPathInfo());

            resp.sendError(SC_NOT_FOUND);
            return;
        }

        DownloadResource resource = res.get();

        Optional<ETag> eTag = resource.getEntityTag().map(ETag::new);
        Optional<String> contentType = resource.getContentType();
        OptionalLong lastModified = resource.getLastModified();
        OptionalLong length = resource.getLength();

        if (preconditionFailed(req, eTag, lastModified)) {
            log(LOG_DEBUG, "Precondition failed for resource: %s", resource.getName());

            resp.sendError(SC_PRECONDITION_FAILED);
            return;
        }

        // Set cache headers...
        resp.setHeader(ACCEPT_RANGES, "bytes");
        eTag.ifPresent(v -> resp.setHeader(ETAG, v.getTag()));
        lastModified.ifPresent(v -> resp.setDateHeader(LAST_MODIFIED, v));

        if (notModified(req, eTag, lastModified)) {
            log(LOG_DEBUG, "Resource not modified: %s", resource.getName());

            resp.setStatus(SC_NOT_MODIFIED);
            return;
        }

        Range[] ranges;
        try {
            ranges = parse(req, eTag, lastModified, length);
        } catch (InvalidRangeException e) {
            log(LOG_DEBUG, "Invalid range set (%s) for resource: %s", req.getHeader(RANGE), resource.getName());

            length.ifPresent(len -> resp.setHeader(CONTENT_RANGE, "bytes */" + Long.toString(len)));
            resp.sendError(SC_REQUESTED_RANGE_NOT_SATISFIABLE);
            return;
        }

        try (WritableByteChannel output = newChannel(resp.getOutputStream())) {
            if (ranges.length == 0) {
                // Stream entire contents...
                long contentLen = length.orElse(-1L);
                // AMDATUWEB-70 - inform the UA what to expect...
                resp.setStatus(contentLen == 0 ? SC_NO_CONTENT : SC_OK);

                contentType.ifPresent(ct -> resp.setContentType(ct));
                if (contentLen >= 0) {
                    resp.setHeader(CONTENT_LENGTH, Long.toString(contentLen));
                }
                resp.setHeader(CONTENT_DISPOSITION,
                    getContentDispositionHeader(resource.getName(), resource.isAttachment()));

                if (head) {
                    return;
                }

                log(LOG_DEBUG, "Streaming entire content (no range set given): %d bytes for resource: %s",
                    length.orElse(-1L), resource.getName());

                streamContent(output, resource, new Range(0, -1L, length));
            } else {
                // Stream only parts of the content...
                resp.setStatus(SC_PARTIAL_CONTENT);

                if (ranges.length == 1) {
                    // Single range...
                    contentType.ifPresent(v -> resp.setContentType(v));
                    resp.setHeader(CONTENT_RANGE, "bytes " + ranges[0].toContentRange());
                    resp.setHeader(CONTENT_LENGTH, Long.toString(ranges[0].getLength()));

                    if (head) {
                        return;
                    }

                    log(LOG_DEBUG, "Streaming single range: %s for resource %s",
                        ranges[0].toContentRange(), resource.getName());

                    streamContent(output, resource, ranges[0]);
                } else {
                    // Multiple ranges...
                    byte[] crnl = { '\r', '\n' };
                    byte[] partSeparator = ("--" + m_boundary + "\r\n").getBytes(ISO_8859_1);
                    byte[] endingSeparator = ("\r\n--" + m_boundary + "--\r\n").getBytes(ISO_8859_1);
                    byte[] partContentType = (CONTENT_TYPE + ": " + contentType
                        .orElse(APPLICATION_OCTET_STREAM) + "\r\n").getBytes(ISO_8859_1);

                    // Calculate the length of the complete body *including* all leading and trailing headers...
                    long contentLength = endingSeparator.length;
                    Map<Range, ByteBuffer> rangeHeaders = new LinkedHashMap<>();

                    for (Range range : ranges) {
                        byte[] partContentRange = (CONTENT_RANGE + ": bytes " + range.toContentRange()
                            + "\r\n").getBytes(ISO_8859_1);
                        int partLength = crnl.length + partSeparator.length + partContentType.length
                            + partContentRange.length;

                        ByteBuffer bb = ByteBuffer.allocate(partLength);
                        bb.put(crnl);
                        bb.put(partSeparator);
                        bb.put(partContentType);
                        bb.put(partContentRange);

                        contentLength += partLength + range.getLength();

                        rangeHeaders.put(range, (ByteBuffer) bb.flip());
                    }

                    resp.setContentType("multipart/byteranges; boundary=" + m_boundary);
                    resp.setHeader(CONTENT_LENGTH, Long.toString(contentLength));

                    if (head) {
                        return;
                    }

                    for (Entry<Range, ByteBuffer> entry : rangeHeaders.entrySet()) {
                        Range range = entry.getKey();

                        log(LOG_DEBUG, "Streaming multi-ranges: %s for resource %s",
                            range.toContentRange(), resource.getName());

                        output.write(entry.getValue());

                        streamContent(output, resource, range);
                    }

                    output.write(ByteBuffer.wrap(endingSeparator));
                }
            }
        } catch (EOFException e) {
            // The reading side closed the channel, probably because we're proxying content from other
            // HTTP servers... Let's move on and see where it brings us...
            // NOTE: not catching this exception here will yield havoc in other places and probably will mess up the
            // calling side as well...
        }
    }

    private void log(int level, String msg, Object... args) {
        m_logHelper.accept(level, String.format(msg, args));
    }

    private void streamContent(WritableByteChannel output, DownloadResource resource, Range range) throws IOException {
        long offset = range.getStart();
        long length = range.getLength();

        int bufSize = (int) (length < 0 ? MIN_BUF_SIZE : Math.min(MIN_BUF_SIZE, length));
        if (bufSize == 0) {
            // AMDATUWEB-70 - if the length is zero, we're done :)
            return;
        }

        try (InputStream is = resource.openStream(offset, length); ReadableByteChannel inputChannel = newChannel(is)) {
            ByteBuffer buffer = ByteBuffer.allocate(bufSize);

            while (inputChannel.read(buffer) != -1) {
                buffer.flip();

                output.write(buffer);

                buffer.clear();
            }
        }
    }
}
