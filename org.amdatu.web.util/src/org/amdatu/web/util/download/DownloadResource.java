/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.util.download;

import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;
import java.util.OptionalLong;

/**
 * Defines a resource that can be served to a client.
 * <p>
 * Resources can be anything from a file to a (remote) static blob to some generated piece of information. Basically
 * everything for which an {@link InputStream} can be created can be used as a {@link DownloadResource}.
 * </p>
 */
public interface DownloadResource {

    /**
     * @return the content type (or MIME type) of this resource, like "text/plain", cannot be <code>null</code>.
     */
    default Optional<String> getContentType() {
        return Optional.empty();
    }

    /**
     * @return the entity tag for this resource, that identifies this particular version of resource.
     */
    default Optional<String> getEntityTag() {
        return Optional.empty();
    }

    /**
     * @return the timestamp (in milliseconds since Epoch) when the resource was last modified, can be empty if the
     *         timestamp is unknown.
     */
    default OptionalLong getLastModified() {
        return OptionalLong.empty();
    }

    /**
     * @return the length (in bytes) of the resource, or empty if the length is unknown.
     */
    default OptionalLong getLength() {
        return OptionalLong.empty();
    }

    /**
     * @return the name of the resource (UTF-8 encoded), never <code>null</code>.
     */
    String getName();

    /**
     * @return <code>true</code> if this resource should be downloaded as an attachment, <code>false</code> if this
     *         resource should be provided inline.
     */
    default boolean isAttachment() {
        return false;
    }

    /**
     * Returns a stream from which bytes from this resource can be read starting at the given offset and with the
     * maximum number of bytes that should be read.
     *
     * @param offset the (byte) offset in the stream to start reading from;
     * @param length the length (in bytes) to read from the stream, if <tt>-1</tt> all bytes until end-of-stream should be returned.
     * @return the input stream from which the requested bytes can be read, never <code>null</code>.
     * @throws IOException in case of I/O exceptions.
     */
    InputStream openStream(long offset, long length) throws IOException;
}