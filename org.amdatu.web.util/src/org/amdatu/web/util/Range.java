/*
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package org.amdatu.web.util;

import static java.util.Arrays.stream;
import static java.util.OptionalLong.of;
import static org.amdatu.web.util.HttpHeaders.IF_RANGE;
import static org.amdatu.web.util.HttpHeaders.RANGE;
import static org.amdatu.web.util.HttpRequestUtil.getDateHeader;
import static org.amdatu.web.util.HttpRequestUtil.modified;

import java.util.Optional;
import java.util.OptionalLong;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

/**
 * Defines a range of bytes, as defined in RFC 7233.
 */
public class Range {

    public static class InvalidRangeException extends RuntimeException {
        private static final long serialVersionUID = 1L;

        public InvalidRangeException() {
            super();
        }

        public InvalidRangeException(String msg) {
            super(msg);
        }
    }

    private static final Range[] EMPTY_RANGE = new Range[0];
    private static final Pattern RANGEDEF = Pattern.compile("^\\s*(\\d*)-(\\d*)\\s*$");

    private final long m_start;
    private final long m_end;
    private final OptionalLong m_length;

    /**
     * Creates a new {@link Range} instance.
     *
     * @param start the starting byte (inclusive), or <tt>-1</tt> if the range should start from the end;
     * @param end the ending byte (inclusive), or <tt>-1</tt> if an open ended range is to be created.
     * @throws IllegalArgumentException in case both arguments are less than zero, or the start is beyond the end of the
     *         range.
     */
    public Range(long start, long end, long length) {
        this(start, end, (length < 0) ? OptionalLong.empty() : of(length));
    }

    /**
     * Creates a new {@link Range} instance.
     *
     * @param start the starting byte (inclusive), or <tt>-1</tt> if the range should start from the end;
     * @param end the ending byte (inclusive), or <tt>-1</tt> if an open ended range is to be created.
     * @throws IllegalArgumentException in case both arguments are less than zero, or the start is beyond the end of the
     *         range.
     */
    public Range(long start, long end, OptionalLong length) {
        if (start < 0 && end < 0) {
            throw new InvalidRangeException("Start or end should be defined!");
        } else if (end >= 0 && start > end) {
            throw new InvalidRangeException("Start cannot be after end!");
        }
        m_start = start;
        m_end = end;
        m_length = length;
    }

    /**
     * @param req the servlet request to get the ranges for, cannot be <code>null</code>;
     * @param eTag the entity tag value of the <b>resource</b>, cannot be <code>null</code>;
     * @param lastModified the last modified value (milliseconds since Epoch) of the <b>resource</b>;
     * @param contentLength the length (in bytes) of the <b>resource</b>.
     * @return an array with requested range definitions. If the returned array is empty, the <b>complete</b> resource
     *         should be returned.
     * @throws InvalidRangeException in case the range specification was invalid and you should return a 416 "Requested
     *         Range Not Satisfiable".
     */
    public static Range[] parse(HttpServletRequest req, Optional<ETag> eTag,
        OptionalLong lastModified, OptionalLong contentLength) {
        if (!eTag.isPresent()) {
            return EMPTY_RANGE;
        }

        ETag tag = eTag.get();
        String rangeHdr = req.getHeader(RANGE);
        // If no range header is given, or we have a weak entity tag, we always return all content...
        if (rangeHdr == null || tag.isWeak()) {
            return EMPTY_RANGE;
        }

        String ifRange = req.getHeader(IF_RANGE);
        if (ifRange != null && !ifRange.equals(tag.getTag())) {
            boolean modified = getDateHeader(req, IF_RANGE)
                .map(ifRangeTime -> modified(ifRangeTime, lastModified))
                .orElse(Boolean.TRUE);

            if (modified) {
                return EMPTY_RANGE;
            }
        }

        return parseRangeSpec(rangeHdr, contentLength);
    }

    /**
     * Parses a range specification, as defined in RFC 7233.
     * <p>
     * This method only accepts byte ranges!
     * </p>
     *
     * @param rangeSpec the range specification to parse into individual ranges, cannot be <code>null</code> or empty.
     * @return a list with ranges, never <code>null</code>.
     */
    public static Range[] parseRangeSpec(String rangeSpec, OptionalLong contentLength) {
        if (rangeSpec == null || "".equals(rangeSpec)) {
            throw new InvalidRangeException("Empty range specification!");
        }

        rangeSpec = rangeSpec.trim();
        if (rangeSpec.startsWith("bytes=")) {
            rangeSpec = rangeSpec.substring(6);
        } else {
            throw new InvalidRangeException("Only byte ranges are accepted!");
        }

        return stream(rangeSpec.split("\\s*,\\s*"))
            .map(rangeDef -> parseRangeDef(rangeDef, contentLength))
            .toArray(s -> new Range[s]);
    }

    private static long parseLong(String input) {
        if (input == null || "".equals(input)) {
            return -1L;
        }
        return Long.parseLong(input);
    }

    private static Range parseRangeDef(String rangeDef, OptionalLong contentLength) {
        if (rangeDef == null || "".equals(rangeDef)) {
            throw new InvalidRangeException("Empty range definition!");
        }
        Matcher m = RANGEDEF.matcher(rangeDef);
        if (!m.matches()) {
            throw new InvalidRangeException("Invalid range definition!");
        }

        return new Range(parseLong(m.group(1)), parseLong(m.group(2)), contentLength);
    }

    /**
     * @return <code>true</code> if this range denotes an open ended range, <code>false</code> otherwise.
     */
    public boolean isOpenEnded() {
        return m_start < 0 || m_end < 0;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        Range other = (Range) obj;
        return m_start == other.m_start && m_end == other.m_end;
    }

    /**
     * @return the last (inclusive) byte of this range, or <tt>-1</tt> if the range is open ended.
     */
    public long getEnd() {
        if (m_end >= 0 && m_start >= 0) {
            return m_end;
        }
        if (m_length.isPresent()) {
            return m_length.getAsLong() - 1L;
        }
        return -1L;
    }

    /**
     * @return the total amount of bytes included in this range, or <tt>-1</tt> if the range is open ended and no
     *         content length is available.
     */
    public long getLength() {
        if (m_end >= 0 && m_start >= 0) {
            return (m_end - m_start) + 1;
        } else if (m_end >= 0) {
            return m_end;
        }
        if (m_length.isPresent()) {
            return m_length.getAsLong() - m_start;
        }
        return -1L;
    }

    /**
     * @return the first (inclusive) byte of this range, or <tt>-1</tt> if the range is considered from the end and no
     *         content length if available.
     */
    public long getStart() {
        if (m_start >= 0) {
            return m_start;
        }
        if (m_length.isPresent()) {
            return m_length.getAsLong() - m_end;
        }
        return -1L;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (int) (m_end ^ (m_end >>> 32));
        result = prime * result + (int) (m_start ^ (m_start >>> 32));
        return result;
    }

    public String toContentRange() {
        StringBuilder sb = new StringBuilder();

        long s = getStart();
        if (s >= 0) {
            sb.append(s);
        }
        sb.append("-");

        long e = getEnd();
        if (e >= 0) {
            sb.append(e);
        }
        sb.append("/");
        if (m_length.isPresent()) {
            sb.append(m_length.getAsLong());
        } else {
            sb.append('*');
        }
        return sb.toString();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        if (m_start >= 0) {
            sb.append(m_start);
        }
        sb.append("-");
        if (m_end >= 0) {
            sb.append(m_end);
        }
        return sb.toString();
    }
}
