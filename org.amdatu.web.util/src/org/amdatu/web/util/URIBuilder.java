/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.util;

import static java.lang.Character.forDigit;
import static java.lang.Character.toUpperCase;
import static java.nio.charset.CodingErrorAction.REPORT;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.Objects.requireNonNull;
import static java.util.Optional.ofNullable;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;
import static org.amdatu.web.util.HttpRequestUtil.isSecureRequest;
import static org.amdatu.web.util.URICodec.RFC3986_UNRESERVED;

import java.net.URI;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.CharsetEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Stream;

import javax.servlet.http.HttpServletRequest;

import org.amdatu.web.util.URICodec.Encoder;

/**
 * Provides a convenient builder for working with {@link URI}s.
 */
public class URIBuilder {
    private static final String HTTPS = "https";
    private static final String HTTP = "http";

    private String m_scheme;
    private String m_userInfo;
    private String m_host;
    private int m_port;
    private String[] m_pathSegments;
    private Map<String, List<String>> m_query;
    private String m_fragment;

    public URIBuilder() {
        m_scheme = null;
        m_host = null;
        m_port = -1;
        m_pathSegments = null;
        m_query = new LinkedHashMap<>();
        m_fragment = null;
    }

    public URIBuilder(CharSequence base) {
        this(URI.create(base.toString()));
    }

    public URIBuilder(URI base) {
        m_scheme = base.getScheme();
        m_userInfo = base.getUserInfo();
        m_host = base.getHost();
        m_port = base.getPort();
        m_pathSegments = toPathSegments(base.getRawPath());
        m_query = parseQueryParams(base.getRawQuery(), false /* encode */);
        m_fragment = base.getRawFragment();
    }

    /**
     * Creates a new {@link URIBuilder} instance based on the information from the given servlet request.
     * <p>
     * For convenience, the returned builder will use "https" if the given servlet request is considered
     * secure according to {@link HttpRequestUtil#isSecureRequest(HttpServletRequest)}.</p>
     *
     * @param req the {@link HttpServletRequest} to create the {@link URIBuilder} for, cannot be <code>null</code>.
     * @return a new {@link URIBuilder} instance with the request URL and query string from the given servlet request.
     */
    public static URIBuilder of(HttpServletRequest req) {
        return new URIBuilder(req.getRequestURL())
            .setQuery(req.getQueryString())
            .setScheme(isSecureRequest(req) ? HTTPS : HTTP);
    }

    @SafeVarargs
    static <T> List<T> asList(T... args) {
        List<T> result = new ArrayList<>();
        result.addAll(Arrays.asList(args));
        return result;
    }

    static String replaceTemplateParameters(String in, List<Object> args) {
        StringBuilder sb = new StringBuilder();
        replaceTemplateParameters(sb, in, args);
        return sb.toString();
    }

    private static void appendEntry(Map<String, List<String>> map, String key, List<Object> values, boolean encode) {
        map.computeIfAbsent(key, unused -> new ArrayList<>(values.size()))
            .addAll(values.stream()
                .map(v -> Objects.toString(v, ""))
                .map(v -> v.startsWith("{") && v.endsWith("}") ? null : v)
                .map(v -> normalize(v, encode))
                .collect(toList()));
    }

    private static int findFirstSubMatch(String[] haystack, String[] needle) {
        int needleLen = needle.length;
        int hayLen = haystack.length;

        if (needleLen < 1 || needleLen > hayLen) {
            // Not found...
            return -1;
        }

        int limit = hayLen - needleLen;
        for (int i = 0; i <= limit; i++) {
            if (!Objects.equals(haystack[i], needle[0])) {
                continue;
            }

            boolean match = true;
            for (int j = 1; match && j < needleLen; j++) {
                match = Objects.equals(haystack[i + j], needle[j]);
            }
            if (match) {
                return i;
            }
        }

        return -1;
    }

    private static String flattenParamEntry(String name, List<String> args, List<Object> templateArgs) {
        return args.stream()
            .map(arg -> arg == null ? "{}" : arg)
            .map(arg -> replaceTemplateParameters(arg, templateArgs))
            .map(arg -> name + ("".equals(arg) ? "" : "=" + arg))
            .collect(joining("&"));
    }

    private static String flattenQueryParams(Map<String, List<String>> query, List<Object> templateArgs) {
        if (query.isEmpty()) {
            return null;
        }
        return query.entrySet().stream()
            .map(e -> flattenParamEntry(e.getKey(), e.getValue(), templateArgs))
            .collect(joining("&"));
    }

    private static String normalize(String input, boolean encode) {
        if (input == null) {
            // Nulls indicate template markers...
            return input;
        }

        ByteBuffer buf;
        try {
            CharsetEncoder newEncoder = UTF_8.newEncoder()
                .onMalformedInput(REPORT)
                .onUnmappableCharacter(REPORT);

            buf = newEncoder.encode(CharBuffer.wrap(input));
        }
        catch (CharacterCodingException exception) {
            throw new IllegalArgumentException("Unable to map characters!", exception);
        }

        int len = buf.limit();

        StringBuilder out = new StringBuilder(len);

        for (int i = 0; i < len; i++) {
            byte b = buf.get();
            // If we come across percent encoded characters that do *not* need to be
            // encoded at all, make sure we decode those inline...
            if (b == '%' && i < (len - 2)) {
                int u = Character.digit(buf.get(i + 1), 16);
                int l = Character.digit(buf.get(i + 2), 16);
                if (u >= 0 && l >= 0) {
                    int ord = ((u << 4) + l);
                    if (RFC3986_UNRESERVED.get(ord)) {
                        out.append((char) ord);
                        // Move over the escape sequence...
                        buf.position(buf.position() + 2);
                        i += 2;
                        continue;
                    }
                }
                // If not a valid escape sequence, be lenient and continue normally...
            }
            if (RFC3986_UNRESERVED.get(b & 0xFF) || !encode) {
                out.append((char) b);
            }
            else {
                out.append('%');
                out.append(toUpperCase(forDigit((b >> 4) & 0xF, 16)));
                out.append(toUpperCase(forDigit(b & 0xF, 16)));
            }
        }

        return out.toString();
    }

    private static Map<String, List<String>> parseQueryParams(String query, boolean encode) {
        Map<String, List<String>> result = new LinkedHashMap<>();
        if (query != null) {
            for (String entry : query.split("&")) {
                String[] kv = entry.split("=");
                String key = normalize(kv[0], encode);
                String value = kv.length > 1 ? kv[1] : "";
                appendEntry(result, key, asList(value), encode);
            }
        }
        return result;
    }

    private static void replaceTemplateParameters(StringBuilder sb, String in, List<Object> args) {
        CharBuffer buf = CharBuffer.wrap(in);
        int i = 0;
        int len = buf.length();

        while (i < len) {
            char ch = in.charAt(i++);
            if (ch != '{') {
                // parameters always should start with a '{'...
                sb.append(ch);
            }
            else {
                // find first '}' after the '{'...
                int ii = i;
                int j = -1;

                while (ii < len) {
                    char la = in.charAt(ii);
                    if (la == '}') {
                        j = ii;
                        break;
                    }
                    ii++;
                }

                if (j < 0) {
                    // Not found, bail out!
                    sb.append('{');
                    sb.append(in, i, len);
                    break;
                }

                // replace placeholder...
                if (args.isEmpty()) {
                    sb.append('{');
                    sb.append(in, i, j);
                    sb.append('}');
                }
                else {
                    sb.append(args.remove(0));
                }

                i = j + 1;
            }
        }
    }

    private static String toPath(String[] segments) {
        if (segments == null) {
            return "";
        }
        if (segments.length == 0) {
            return "/";
        }
        return Arrays.stream(segments)
            .map(seg -> seg == null ? "{}" : seg)
            .collect(joining("/"));
    }

    private static String[] toPathSegments(String path) {
        if ("".equals(path)) {
            return null;
        }
        if ("/".equals(path)) {
            return new String[0];
        }
        return Arrays.stream(path.split("/"))
            .filter(seg -> !"".equals(seg))
            .map(seg -> seg.startsWith("{") && seg.endsWith("}") ? null : seg)
            .toArray(s -> new String[s]);
    }

    public URIBuilder appendToPath(Object sub) {
        if (sub == null || "".equals(sub)) {
            return this;
        }

        String[] subPath = toPathSegments(sub.toString());
        if (subPath == null || subPath.length < 1) {
            return this;
        }

        if (m_pathSegments == null) {
            int newLen = subPath.length;

            m_pathSegments = new String[newLen];
            for (int i = 0; i < newLen; i++) {
                m_pathSegments[i] = normalize(subPath[i], true /* encode */);
            }
        }
        else {
            int oldLen = m_pathSegments.length;

            m_pathSegments = Arrays.copyOf(m_pathSegments, oldLen + subPath.length);

            for (int i = oldLen; i < m_pathSegments.length; i++) {
                m_pathSegments[i] = normalize(subPath[i - oldLen], true /* encode */);
            }
        }

        return this;
    }

    public URIBuilder appendToQuery(String key, Object... values) {
        appendEntry(m_query, key, values.length == 0 ? asList("") : asList(values), true /* encode */);
        return this;
    }

    /**
     * Builds the resulting URI based on the information in this builder.
     * <p>
     * Note that the given template arguments are used <b>as-is</b>. This implies
     * that any specifics with respect to encoding should be done by the caller of
     * this method!
     *
     * @param templateArgs the (optional) positional template arguments to fill in.
     * @return the resulting URI, never <code>null</code>.
     * @throws IllegalArgumentException in case the creation of the URI leads to an invalid URI syntax.
     */
    public URI build(Object... templateArgs) {
        // Use the constructor that isn't doing nifty things to our query and fragment...
        return URI.create(toString(asList(templateArgs))).normalize();
    }

    /**
     * Removes the entire fragment from this builder.
     *
     * @return this builder.
     */
    public URIBuilder clearFragment() {
        m_fragment = null;
        return this;
    }

    /**
     * Removes the entire path from this builder.
     *
     * @return this builder.
     */
    public URIBuilder clearPath() {
        m_pathSegments = null;
        return this;
    }

    /**
     * Removes the entire query from this builder.
     *
     * @return this builder.
     */
    public URIBuilder clearQuery() {
        m_query.clear();
        return this;
    }

    public Optional<String> getFragment() {
        return ofNullable(m_fragment);
    }

    public Optional<String> getHost() {
        return ofNullable(m_host);
    }

    public Optional<String> getPath() {
        if (m_pathSegments == null) {
            return Optional.empty();
        }
        return Optional.of(toPath(m_pathSegments));
    }

    public Stream<Entry<String, List<String>>> getQuery() {
        return m_query.entrySet().stream();
    }

    /**
     * Returns the first available query parameter value with the given name.
     *
     * @param name the name of the query parameter key to return, cannot be <code>null</code>.
     * @return the first associated value of the requested query parameter, note that the value
     *         itself can be <code>null</code> if the query parameter did not specify a value
     *         (e.g., "?bar").
     */
    public Optional<String> getQueryValue(String name) {
        return getQueryValues(name)
            .findFirst();
    }

    /**
     * Returns the query parameter values with the given name.
     *
     * @param name the name of the query parameter key to return, cannot be <code>null</code>.
     * @return the values associated to the requested query parameter, note that a query value
     *         itself can be <code>null</code> if the query parameter did not specify a value
     *         (e.g., "?bar").
     */
    public Stream<String> getQueryValues(String name) {
        return getQuery()
            .filter(e -> name.equals(e.getKey()))
            .flatMap(e -> e.getValue().stream());
    }

    public Optional<String> getScheme() {
        return ofNullable(m_scheme);
    }

    public Optional<String> getUserInfo() {
        return ofNullable(m_userInfo);
    }

    public URIBuilder removeQueryParam(String key) {
        m_query.remove(key);
        return this;
    }

    public URIBuilder replaceQueryParam(String key, Object... values) {
        m_query.remove(key);
        appendEntry(m_query, key, values.length == 0 ? asList("") : asList(values), true /* encode */);
        return this;
    }

    /**
     * @param path the new path to set, will be normalized but <b>not</b> escaped, assuming the given path is already properly encoded.
     * @return this builder
     */
    public URIBuilder setEncodedPath(String path) {
        requireNonNull(path, "Path cannot be null!");
        setPath(path, false /* encode */);
        return this;
    }

    public URIBuilder setFragment(String fragment) {
        m_fragment = requireNonNull(fragment, "Fragment cannot be null!");
        return this;
    }

    public URIBuilder setHost(String host) {
        m_host = requireNonNull(host, "Host cannot be null!");
        return this;
    }

    /**
     * @param path the new path to set, will be normalized and escaped to contain only valid characters.
     * @return this builder.
     */
    public URIBuilder setPath(String path) {
        requireNonNull(path, "Path cannot be null!");
        setPath(path, true /* encode */);
        return this;
    }

    public URIBuilder setPort(int port) {
        m_port = port;
        return this;
    }

    public URIBuilder setQuery(Map<String, List<String>> query) {
        m_query = new LinkedHashMap<>(query);
        return this;
    }

    public URIBuilder setQuery(String query) {
        m_query = parseQueryParams(query, true /* encode */);
        return this;
    }

    public URIBuilder setScheme(String scheme) {
        m_scheme = requireNonNull(scheme, "Scheme cannot be null!");
        return this;
    }

    public URIBuilder setUserInfo(String userInfo) {
        m_userInfo = requireNonNull(userInfo, "UserInfo cannot be null!");
        return this;
    }

    public URIBuilder setUserInfo(String user, String password) {
        requireNonNull(user, "User cannot be null!");
        requireNonNull(password, "Password cannot be null!");

        Encoder encoder = URICodec.getStrictEncoder();
        m_userInfo = encoder.encode(user) + ":" + encoder.encode(password);

        return this;
    }

    /**
     * Updates the current path to the <em>first</em> sub-path that <b>ends</b> with the
     * given path.
     * <p>
     * For example, if the current path is set to "/a/b/c/d", calling this method with
     * "/b" will cause the path to be set to "/a/b".<br>
     * If the given path is <b>not</b> contained, the current path is set to an empty
     * string.
     * </p>
     *
     * @param path the new end of the path to set, cannot be <code>null</code>.
     * @return this builder.
     */
    public URIBuilder subPathEndingWith(String path) {
        requireNonNull(path, "Path cannot be null!");
        String[] subPath = toPathSegments(path);
        if (subPath.length < 1) {
            return this;
        }

        int idx = findFirstSubMatch(m_pathSegments, subPath);
        if (idx < 0) {
            m_pathSegments = null;
        }
        else {
            // Path itself should already be normalized, so no need to do this again...
            m_pathSegments = Arrays.copyOfRange(m_pathSegments, 0, idx + subPath.length);
        }
        return this;
    }

    @Override
    public String toString() {
        return toString(asList());
    }

    private void setPath(String path, boolean encode) {
        String[] pathSegments = toPathSegments(path);
        if (pathSegments != null) {
            int len = pathSegments.length;

            m_pathSegments = new String[len];
            for (int i = 0; i < len; i++) {
                m_pathSegments[i] = normalize(pathSegments[i], encode);
            }
        }
    }

    private String toString(List<Object> templateArgs) {
        StringBuilder sb = new StringBuilder();
        boolean needsSlash = false;
        if (m_scheme != null) {
            replaceTemplateParameters(sb, m_scheme, templateArgs);
            sb.append(':');
        }
        if (m_host != null) {
            sb.append("//");
            if (m_userInfo != null) {
                sb.append(m_userInfo).append('@');
            }
            replaceTemplateParameters(sb, m_host, templateArgs);
            needsSlash = true;
        }
        if (m_port > 0) {
            sb.append(':').append(m_port);
            needsSlash = true;
        }
        if (m_pathSegments != null) {
            if (needsSlash) {
                sb.append('/');
            }
            replaceTemplateParameters(sb, toPath(m_pathSegments), templateArgs);
        }
        if (!m_query.isEmpty()) {
            sb.append('?');
            sb.append(flattenQueryParams(m_query, templateArgs));
        }
        if (m_fragment != null) {
            sb.append('#');
            replaceTemplateParameters(sb, m_fragment, templateArgs);
        }
        return sb.toString();
    }
}
