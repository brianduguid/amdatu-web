/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.util;

import static java.util.Objects.requireNonNull;

import java.util.UUID;

/**
 * Denotes an entity tag.
 */
public class ETag {
    private final String m_eTag;

    /**
     * Creates a new UUID-based entity tag.
     */
    public ETag() {
        this(UUID.randomUUID().toString());
    }

    /**
     * Creates a new entity tag.
     */
    public ETag(String eTag) {
        if (eTag == null || "".equals(eTag)) {
            throw new IllegalArgumentException("ETag cannot be null or empty!");
        }
        eTag = eTag.trim();
        if (!eTag.startsWith("W/\"")) {
            if (!eTag.startsWith("\"")) {
                eTag = "\"".concat(eTag);
            }
        }
        if (!eTag.endsWith("\"")) {
            eTag = eTag.concat("\"");
        }
        m_eTag = eTag;
    }

    /**
     * Creates a new (weak) entity tag based on the given information.
     * <p>
     * This implementation does some trivial hashing of the given parameters to generate a hashcode that can be used to
     * generate a weak entity tag.
     * </p>
     */
    public ETag(String name, long length, long lastModified) {
        final long prime = 37;
        long result = 1;
        result = prime * result + requireNonNull(name).hashCode();
        result = prime * result + lastModified;
        result = prime * result + length;
        m_eTag = String.format("W/\"%08x\"", result);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        return m_eTag.equals(((ETag) obj).m_eTag);
    }

    /**
     * @return the actual entity tag value.
     */
    public String getTag() {
        return m_eTag;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((m_eTag == null) ? 0 : m_eTag.hashCode());
        return result;
    }

    /**
     * @return <code>true</code> if this entity tag is considered "weak", that is, has no strong guarantees that content
     *         is equal byte-for-byte; <code>false</code> if this is a strong entity tag.
     */
    public boolean isWeak() {
        return m_eTag.startsWith("W/");
    }

    @Override
    public String toString() {
        return m_eTag;
    }
}
