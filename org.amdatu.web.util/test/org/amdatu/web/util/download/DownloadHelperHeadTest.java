/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.util.download;

import static javax.servlet.http.HttpServletResponse.SC_NOT_FOUND;
import static javax.servlet.http.HttpServletResponse.SC_NOT_MODIFIED;
import static javax.servlet.http.HttpServletResponse.SC_OK;
import static javax.servlet.http.HttpServletResponse.SC_PARTIAL_CONTENT;
import static javax.servlet.http.HttpServletResponse.SC_PRECONDITION_FAILED;
import static javax.servlet.http.HttpServletResponse.SC_REQUESTED_RANGE_NOT_SATISFIABLE;
import static org.amdatu.web.util.HttpHeaders.ACCEPT_RANGES;
import static org.amdatu.web.util.HttpHeaders.CONTENT_DISPOSITION;
import static org.amdatu.web.util.HttpHeaders.CONTENT_LENGTH;
import static org.amdatu.web.util.HttpHeaders.CONTENT_RANGE;
import static org.amdatu.web.util.HttpHeaders.ETAG;
import static org.amdatu.web.util.HttpHeaders.IF_MATCH;
import static org.amdatu.web.util.HttpHeaders.IF_MODIFIED_SINCE;
import static org.amdatu.web.util.HttpHeaders.IF_NONE_MATCH;
import static org.amdatu.web.util.HttpHeaders.IF_RANGE;
import static org.amdatu.web.util.HttpHeaders.IF_UNMODIFIED_SINCE;
import static org.amdatu.web.util.HttpHeaders.LAST_MODIFIED;
import static org.amdatu.web.util.HttpHeaders.RANGE;
import static org.amdatu.web.util.HttpMockUtils.mockHttpRequest;
import static org.amdatu.web.util.HttpMockUtils.mockHttpResponse;
import static org.amdatu.web.util.download.TestDownloadHandler.createResourceProvider;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.startsWith;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Test;

public class DownloadHelperHeadTest {

    private DownloadHelper m_helper;

    @Test
    public void testHeadExistingFileCompleteFile() throws Exception {
        TestDownloadHandler handler = createResourceProvider("/file", 128);
        m_helper = new DownloadHelper(handler);

        HttpServletResponse resp = doRequest("/file");

        TestResource res = handler.getResource();
        String eTag = getETag(res);
        long lastModified = getLastModified(res);

        verify(resp).resetBuffer();
        verify(resp).setStatus(eq(SC_OK));
        verify(resp).setContentType(eq("text/plain"));
        verify(resp).setHeader(eq(ETAG), eq(eTag));
        verify(resp).setHeader(eq(ACCEPT_RANGES), eq("bytes"));
        verify(resp).setDateHeader(eq(LAST_MODIFIED), eq(lastModified));
        verify(resp).setHeader(eq(CONTENT_LENGTH), eq("128"));
        verify(resp).setHeader(eq(CONTENT_DISPOSITION), anyString());
        verify(resp).getOutputStream();
        verifyNoMoreInteractions(resp);
    }

    @Test
    public void testHeadExistingFileModifiedIfRange() throws Exception {
        TestDownloadHandler handler = createResourceProvider("/file", 128);
        m_helper = new DownloadHelper(handler);

        TestResource res = handler.getResource();
        String eTag = getETag(res);
        long lastModified = getLastModified(res);

        HttpServletResponse resp = doRequest("/file", //
            RANGE, "bytes=1-3",
            IF_RANGE, Long.toString(lastModified - 2000));

        verify(resp).resetBuffer();
        verify(resp).setStatus(eq(SC_OK));
        verify(resp).setDateHeader(eq(LAST_MODIFIED), eq(lastModified));
        verify(resp).setHeader(eq(ETAG), eq(eTag));
        verify(resp).setHeader(eq(ACCEPT_RANGES), eq("bytes"));
        verify(resp).setHeader(eq(CONTENT_LENGTH), eq("128"));
        verify(resp).setContentType(eq("text/plain"));
        verify(resp).setHeader(eq(CONTENT_DISPOSITION), anyString());
        verify(resp).getOutputStream();
        verifyNoMoreInteractions(resp);
    }

    @Test
    public void testHeadExistingFileModifiedSince() throws Exception {
        TestDownloadHandler handler = createResourceProvider("/file", 128);
        m_helper = new DownloadHelper(handler);

        TestResource res = handler.getResource();
        String eTag = getETag(res);
        long lastModified = getLastModified(res);

        HttpServletResponse resp = doRequest("/file", //
            IF_MODIFIED_SINCE, Long.toString(lastModified));

        verify(resp).resetBuffer();
        verify(resp).setStatus(eq(SC_NOT_MODIFIED));
        verify(resp).setDateHeader(eq(LAST_MODIFIED), eq(lastModified));
        verify(resp).setHeader(eq(ETAG), eq(eTag));
        verify(resp).setHeader(eq(ACCEPT_RANGES), eq("bytes"));
        verifyNoMoreInteractions(resp);
    }

    @Test
    public void testHeadExistingFileMultipleByteRange() throws Exception {
        TestDownloadHandler handler = createResourceProvider("/file", 128);
        m_helper = new DownloadHelper(handler);

        TestResource res = handler.getResource();
        String eTag = getETag(res);
        long lastModified = getLastModified(res);

        HttpServletResponse resp = doRequest("/file", //
            RANGE, "bytes=0-0,-1");

        verify(resp).resetBuffer();
        verify(resp).setStatus(eq(SC_PARTIAL_CONTENT));
        verify(resp).setDateHeader(eq(LAST_MODIFIED), eq(lastModified));
        verify(resp).setHeader(eq(ETAG), eq(eTag));
        verify(resp).setHeader(eq(ACCEPT_RANGES), eq("bytes"));
        verify(resp).setHeader(eq(CONTENT_LENGTH), anyString()); // It should have a certain length...
        verify(resp).setContentType(startsWith("multipart/byteranges; boundary="));
        verify(resp).getOutputStream();
        verifyNoMoreInteractions(resp);
    }

    @Test
    public void testHeadExistingFileNotModifiedNoneMatchingETag() throws Exception {
        TestDownloadHandler handler = createResourceProvider("/file", 128);
        m_helper = new DownloadHelper(handler);

        TestResource res = handler.getResource();
        String eTag = getETag(res);
        long lastModified = getLastModified(res);

        HttpServletResponse resp = doRequest("/file", //
            IF_NONE_MATCH, eTag, //
            IF_UNMODIFIED_SINCE, Long.toString(lastModified));

        verify(resp).resetBuffer();
        verify(resp).setStatus(eq(SC_NOT_MODIFIED));
        verify(resp).setHeader(eq(ETAG), eq(eTag));
        verify(resp).setDateHeader(eq(LAST_MODIFIED), eq(lastModified));
        verify(resp).setHeader(eq(ACCEPT_RANGES), eq("bytes"));
        verifyNoMoreInteractions(resp);
    }

    @Test
    public void testHeadExistingFilePreconditionETagMismatch() throws Exception {
        TestDownloadHandler handler = createResourceProvider("/file", 128);
        m_helper = new DownloadHelper(handler);

        HttpServletResponse resp = doRequest("/file", //
            IF_MATCH, "unknownETag");

        verify(resp).resetBuffer();
        verify(resp).sendError(eq(SC_PRECONDITION_FAILED));
        verifyNoMoreInteractions(resp);
    }

    @Test
    public void testHeadExistingFilePreconditionModifiedWithoutETag() throws Exception {
        TestDownloadHandler handler = createResourceProvider("/file", 128);
        m_helper = new DownloadHelper(handler);

        TestResource res = handler.getResource();
        String eTag = getETag(res);
        long lastModified = getLastModified(res);

        HttpServletResponse resp = doRequest("/file", //
            IF_MODIFIED_SINCE, Long.toString(lastModified + 1000L));

        verify(resp).resetBuffer();
        verify(resp).setHeader(eq(ETAG), eq(eTag));
        verify(resp).setDateHeader(eq(LAST_MODIFIED), eq(lastModified));
        verify(resp).setStatus(eq(SC_NOT_MODIFIED));
        verify(resp).setHeader(eq(ACCEPT_RANGES), eq("bytes"));
        verifyNoMoreInteractions(resp);
    }

    @Test
    public void testHeadExistingFilePreconditionNotModifiedMismatch() throws Exception {
        TestDownloadHandler handler = createResourceProvider("/file", 128);
        m_helper = new DownloadHelper(handler);

        long lastModified = getLastModified(handler.getResource()) - 2000L;

        HttpServletResponse resp = doRequest("/file", //
            IF_UNMODIFIED_SINCE, Long.toString(lastModified));

        verify(resp).resetBuffer();
        verify(resp).sendError(eq(SC_PRECONDITION_FAILED));
        verifyNoMoreInteractions(resp);
    }

    @Test
    public void testHeadExistingFileSingleByteRange() throws Exception {
        TestDownloadHandler handler = createResourceProvider("/file", 128);
        m_helper = new DownloadHelper(handler);

        TestResource res = handler.getResource();
        String eTag = getETag(res);
        long lastModified = getLastModified(res);

        HttpServletResponse resp = doRequest("/file", //
            RANGE, "bytes=1-3");

        verify(resp).resetBuffer();
        verify(resp).setStatus(eq(SC_PARTIAL_CONTENT));
        verify(resp).setDateHeader(eq(LAST_MODIFIED), eq(lastModified));
        verify(resp).setHeader(eq(ETAG), eq(eTag));
        verify(resp).setHeader(eq(ACCEPT_RANGES), eq("bytes"));
        verify(resp).setHeader(eq(CONTENT_RANGE), eq("bytes 1-3/128"));
        verify(resp).setHeader(eq(CONTENT_LENGTH), eq("3"));
        verify(resp).setContentType(eq("text/plain"));
        verify(resp).getOutputStream();
        verifyNoMoreInteractions(resp);
    }

    @Test
    public void testHeadExistingFileUnacceptableRangeSpecifier() throws Exception {
        TestDownloadHandler handler = createResourceProvider("/file", 128);
        m_helper = new DownloadHelper(handler);

        TestResource res = handler.getResource();
        String eTag = getETag(res);
        long lastModified = getLastModified(res);

        HttpServletResponse resp = doRequest("/file", //
            RANGE, "octets=1-3");

        verify(resp).resetBuffer();
        verify(resp).sendError(eq(SC_REQUESTED_RANGE_NOT_SATISFIABLE));
        verify(resp).setDateHeader(eq(LAST_MODIFIED), eq(lastModified));
        verify(resp).setHeader(eq(ETAG), eq(eTag));
        verify(resp).setHeader(eq(ACCEPT_RANGES), eq("bytes"));
        verify(resp).setHeader(eq(CONTENT_RANGE), eq("bytes */128"));
        verifyNoMoreInteractions(resp);
    }

    @Test
    public void testHeadNonExistingFile() throws Exception {
        m_helper = new DownloadHelper(createResourceProvider());

        HttpServletResponse resp = doRequest("/no_such_file");

        verify(resp).resetBuffer();
        verify(resp).sendError(eq(SC_NOT_FOUND));
        verifyNoMoreInteractions(resp);
    }

    private HttpServletResponse doRequest(String path, String... requestHeaders) throws IOException {
        HttpServletRequest req = mockHttpRequest(path, requestHeaders);
        HttpServletResponse resp = mockHttpResponse();

        m_helper.handle(req, resp, true /* head */);

        return resp;
    }

    private String getETag(TestResource res) {
        return res.getEntityTag().orElse(null);
    }

    private long getLastModified(TestResource res) {
        return res.getLastModified().orElse(-1L);
    }
}
