/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.util.download;

import static java.util.Arrays.asList;
import static org.amdatu.web.util.URICodec.getFormUrlEncoder;
import static org.amdatu.web.util.URICodec.getEncoder;
import static org.amdatu.web.util.URICodec.*;
import static org.junit.Assert.assertEquals;

import java.util.List;

import org.amdatu.web.util.URICodec;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

/**
 * Test cases for {@link URICodec}.
 */
@RunWith(Parameterized.class)
public class URICodecTest {

    @Parameters
    public static List<Object[]> data() {
        return asList(new Object[][] {
            // - empty string
            { "", /* input */
                "", /* form encoded */
                "", /* generic encoded */
                "" /* strict encoded */ },
            // - plain ASCII, no replacements
            { "abcd", /* input */
                "abcd", /* form encoded */
                "abcd", /* generic encoded */
                "abcd" /* strict encoded */ },
            // - ASCII with unicode characters
            { "\"A\" B ± \"", /* input */
                "%22A%22+B+%C2%B1+%22", /* form encoded */
                "%22A%22%20B%20%C2%B1%20%22", /* generic encoded */
                "%22A%22%20B%20%C2%B1%20%22" /* strict encoded */ },
            // - unicode surrogate pair
            { "a\uD800\uDFFFb", /* input */
                "a%F0%90%8F%BFb", /* form encoded */
                "a%F0%90%8F%BFb", /* generic encoded */
                "a%F0%90%8F%BFb" /* strict encoded */ },
            // - all reserved characters of RFC1738
            { " <>\"#%{}|\\^~[]`", /* input */
                "+%3C%3E%22%23%25%7B%7D%7C%5C%5E%7E%5B%5D%60", /* form encoded */
                "%20%3C%3E%22%23%25%7B%7D%7C%5C%5E~%5B%5D%60", /* generic encoded */
                "%20%3C%3E%22%23%25%7B%7D%7C%5C%5E~%5B%5D%60" /* strict encoded */ },
            // - all reserved characters of RFC2396
            { ";/?:@&=+$,", /* input */
                "%3B%2F%3F%3A%40%26%3D%2B$,", /* form encoded */
                "%3B%2F%3F%3A%40%26%3D%2B%24%2C", /* generic encoded */
                "%3B%2F%3F%3A%40%26%3D%2B%24%2C" /* strict encoded */ },
            // - all reserved characters of RFC3986
            { ":/?#[]@!$&'()*+,;=", /* input */
                "%3A%2F%3F%23%5B%5D%40!$%26'()*%2B,%3B%3D", /* form encoded */
                "%3A%2F%3F%23%5B%5D%40!%24%26'()*%2B%2C%3B%3D", /* generic encoded */
                "%3A%2F%3F%23%5B%5D%40%21%24%26%27%28%29%2A%2B%2C%3B%3D" /* strict encoded */ },
        });
    }

    @Parameter(value = 0)
    public String m_input;

    @Parameter(value = 1)
    public String m_expectedFormEncoded;

    @Parameter(value = 2)
    public String m_expectedGenericEncoded;

    @Parameter(value = 3)
    public String m_expectedStrictEncoded;

    @Test
    public void testFormDecodeInputOk() {
        assertEquals(m_input, getFormUrlDecoder().decode(m_expectedFormEncoded));
    }

    @Test
    public void testFormEncodeInputOk() {
        assertEquals(m_expectedFormEncoded, getFormUrlEncoder().encode(m_input));
    }

    @Test
    public void testGenericDecodeInputOk() {
        assertEquals(m_input, getDecoder().decode(m_expectedGenericEncoded));
    }

    @Test
    public void testGenericEncodeInputOk() {
        assertEquals(m_expectedGenericEncoded, getEncoder().encode(m_input));
    }

    @Test
    public void testStrictDecodeInputOk() {
        assertEquals(m_input, getStrictDecoder().decode(m_expectedStrictEncoded));
    }

    @Test
    public void testStrictEncodeInputOk() {
        assertEquals(m_expectedStrictEncoded, getStrictEncoder().encode(m_input));
    }
}
