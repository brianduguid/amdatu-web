/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.util.download;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.time.Instant;
import java.util.Optional;
import java.util.OptionalLong;

import org.amdatu.web.util.ETag;

class TestResource implements DownloadResource {
    private static byte[] generateContent(int length) {
        byte[] result = new byte[Math.max(0, length)];
        for (int i = 0; i < result.length; i++) {
            result[i] = (byte) (i % 255);
        }
        return result;
    }
    private String m_name;
    private int m_length;
    private Instant m_lastModified;
    private String m_etag;
    private byte[] m_content;

    private File m_file;

    public TestResource(String name, int length, Instant lastModified) {
        this(name, length, new ETag().getTag(), lastModified);
    }

    public TestResource(String name, int length, String eTag, Instant lastModified) {
        m_name = name;
        m_length = length;
        m_lastModified = lastModified;
        m_etag = eTag;
        m_content = generateContent(length);
    }

    @Override
    public Optional<String> getContentType() {
        return Optional.of("text/plain");
    }

    @Override
    public Optional<String> getEntityTag() {
        return Optional.ofNullable(m_etag);
    }

    @Override
    public OptionalLong getLastModified() {
        return OptionalLong.of(m_lastModified.toEpochMilli());
    }

    @Override
    public OptionalLong getLength() {
        return OptionalLong.of(m_length);
    }

    @Override
    public String getName() {
        return m_name;
    }

    @Override
    public InputStream openStream(long offset, long length) throws IOException {
        if (m_file != null) {
            FileInputStream fis = new FileInputStream(m_file);
            fis.skip(offset);

            return new InputStream() {
                private long remaining = length;

                @Override
                public void close() throws IOException {
                    fis.close();
                }

                @Override
                public int read() throws IOException {
                    return --remaining >= 0 ? fis.read() : -1;
                }
            };
        }
        return new ByteArrayInputStream(m_content, (int) offset, (int) length);
    }

}