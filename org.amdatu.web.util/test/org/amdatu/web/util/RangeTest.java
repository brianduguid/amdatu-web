/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.util;

import static java.util.OptionalLong.empty;
import static java.util.OptionalLong.of;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.amdatu.web.util.Range.InvalidRangeException;
import org.junit.Test;

public class RangeTest {

    @Test
    public void testCreateRangeOk() {
        new Range(1, 2, 3);
    }

    @Test(expected = InvalidRangeException.class)
    public void testCreateRangeWithStartAfterEndFail() {
        new Range(2, 1, 3);
    }

    @Test
    public void testCreateRangeWithStartEqualEndOk() {
        new Range(1, 1, 2);
    }

    @Test(expected = InvalidRangeException.class)
    public void testCreateUndefinedRangeFail() {
        new Range(-1, -1, 2);
    }

    @Test
    public void testIsOpenEndedRangeOk() {
        assertTrue(new Range(0, -1, 2).isOpenEnded());
        assertTrue(new Range(-1, 1, 2).isOpenEnded());
        assertFalse(new Range(0, 1, 2).isOpenEnded());
    }

    @Test(expected = InvalidRangeException.class)
    public void testParseEmptyRangeSpecFail() {
        assertNull(Range.parseRangeSpec("", empty()));
    }

    @Test(expected = InvalidRangeException.class)
    public void testParseInvalidRangeSpecFail() {
        assertNull(Range.parseRangeSpec("bytes=nonsense", empty()));
    }

    @Test(expected = InvalidRangeException.class)
    public void testParseInvalidRangeTypeFail() {
        assertNull(Range.parseRangeSpec("octets=0-499", empty()));
    }

    @Test
    public void testRangeSetFirstAndListBytesOk() {
        Range[] ranges = Range.parseRangeSpec("bytes=0-0,-1", of(3));
        assertEquals(2, ranges.length);

        assertEquals(0, ranges[0].getStart());
        assertEquals(0, ranges[0].getEnd());
        assertEquals(1, ranges[0].getLength());

        assertEquals(2, ranges[1].getStart());
        assertEquals(2, ranges[1].getEnd());
        assertEquals(1, ranges[1].getLength());
    }

    @Test
    public void testRangeSetForLast500BytesOk() {
        Range[] ranges = Range.parseRangeSpec("bytes=-500", of(1234));
        assertEquals(1, ranges.length);

        assertEquals(734, ranges[0].getStart());
        assertEquals(1233, ranges[0].getEnd());
        assertEquals(500, ranges[0].getLength());
    }

    @Test
    public void testRangeSetForLast500BytesWithoutContentLengthOk() {
        Range[] ranges = Range.parseRangeSpec("bytes=-500", empty());
        assertEquals(1, ranges.length);

        assertEquals(-1L, ranges[0].getStart());
        assertEquals(-1L, ranges[0].getEnd());
        assertEquals(500, ranges[0].getLength());
    }

    @Test
    public void testRangeSetSkipFirst500BytesOk() {
        Range[] ranges = Range.parseRangeSpec("bytes=500-", of(1234));
        assertEquals(1, ranges.length);

        assertEquals(500, ranges[0].getStart());
        assertEquals(1233, ranges[0].getEnd());
        assertEquals(734, ranges[0].getLength());
    }

    @Test
    public void testRangeSetSkipFirst500BytesWithoutContentLengthOk() {
        Range[] ranges = Range.parseRangeSpec("bytes=500-", empty());
        assertEquals(1, ranges.length);

        assertEquals(500, ranges[0].getStart());
        assertEquals(-1L, ranges[0].getEnd());
        assertEquals(-1L, ranges[0].getLength());
    }

    @Test
    public void testSingleRangeSetOk() {
        Range[] ranges = Range.parseRangeSpec("bytes=0-499", of(1234));
        assertEquals(1, ranges.length);

        assertEquals(0, ranges[0].getStart());
        assertEquals(499, ranges[0].getEnd());
        assertEquals(500, ranges[0].getLength());
    }

    @Test
    public void testSingleRangeSetWithoutContentLengthOk() {
        Range[] ranges = Range.parseRangeSpec("bytes=0-499", empty());
        assertEquals(1, ranges.length);

        assertEquals(0, ranges[0].getStart());
        assertEquals(499, ranges[0].getEnd());
        assertEquals(500, ranges[0].getLength());
    }

    @Test
    public void testToContentRangeOk() {
        assertEquals("0-499/1234", new Range(0, 499, of(1234)).toContentRange());
        assertEquals("500-999/1234", new Range(500, 999, of(1234)).toContentRange());
        assertEquals("500-1233/1234", new Range(500, 1233, of(1234)).toContentRange());
        assertEquals("734-1233/1234", new Range(-1, 500, of(1234)).toContentRange());
        assertEquals("500-1233/1234", new Range(500, -1, of(1234)).toContentRange());
        assertEquals("42-1233/*", new Range(42, 1233, empty()).toContentRange());
    }

}
