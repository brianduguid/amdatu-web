/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * Test cases for {@link ETag}.
 */
public class ETagTest {

    @Test
    public void testCreateETagOk() {
        assertEquals("\"xyz\"", new ETag("xyz").getTag());
        assertEquals("\"xyz\"", new ETag("\"xyz\"").getTag());
        assertEquals("\" xyz \"", new ETag("\" xyz \"").getTag());
        assertEquals("\"xyz\"", new ETag(" xyz ").getTag());
        assertEquals("W/\"d5c19f9917\"", new ETag("W/\"d5c19f9917\"").getTag());
    }

    @Test
    public void testGeneratedWeakETagOk() {
        assertEquals("W/\"d5c19f9917\"", new ETag("qux.quu", 1234, 56789).getTag());
    }

    @Test
    public void testIsWeakETagOk() {
        assertTrue(new ETag("qux.quu", 1234, 56789).isWeak());
        assertFalse(new ETag("xyz").isWeak());
    }

}
