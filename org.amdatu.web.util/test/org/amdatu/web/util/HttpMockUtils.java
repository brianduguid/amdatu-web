/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.util;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import javax.servlet.ReadListener;
import javax.servlet.ServletInputStream;
import javax.servlet.ServletOutputStream;
import javax.servlet.WriteListener;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class HttpMockUtils {

    public static String asString(InputStream is) {
        // See <weblogs.java.net/blog/pat/archive/2004/10/stupid_scanner_1.html>
        try (Scanner scanner = new Scanner(is, "UTF-8")) {
            scanner.useDelimiter("\\A");
            return scanner.hasNext() ? scanner.next() : null;
        }
    }

    public static HttpServletRequest mockHttpFilePostRequest(String fileName)
        throws IOException {
        return mockHttpFilePostRequest(new String[] { fileName });
    }

    public static HttpServletRequest mockHttpFilePostRequest(String[] fileNames)
        throws IOException {
        HttpServletRequest req = mock(HttpServletRequest.class);

        when(req.getMethod()).thenReturn("POST");
        when(req.getContentType()).thenReturn("multipart/form-data; boundary=----fake_boundary");

        StringBuilder sb = new StringBuilder();
        for (String fileName : fileNames) {
            sb.append("\r\n------fake_boundary\r\n");
            sb.append("Content-Disposition: form-data; name=\"uploadedfile\"; ");
            sb.append("filename=\"").append(fileName).append("\"\r\n");
            sb.append("Content-Type: text/plain\r\n\r\n");
            sb.append("hello world!");
            sb.append("\r\n------fake_boundary");
        }
        if (sb.length() > 0) {
            sb.append("--");
        }

        int length;
        if (fileNames.length > 0 && "mangled_body".equals(fileNames[0])) {
            length = sb.length() / 2;
        } else {
            length = sb.length();
        }

        doReturn(new ServletInputStream() {
            final ByteArrayInputStream bais = new ByteArrayInputStream(sb.toString().getBytes(UTF_8), 0, length);

            @Override
            public int read() throws IOException {
                return bais.read();
            }

            @Override
            public boolean isFinished() {
                return false;
            }

            @Override
            public boolean isReady() {
                return true;
            }

            @Override
            public void setReadListener(ReadListener arg0) {
                throw new RuntimeException("Not supported");
            }
        }).when(req).getInputStream();

        return req;
    }

    public static HttpServletRequest mockHttpFormPostRequest(Map<String, String> form, String... requestHeaders)
        throws IOException {
        HttpServletRequest req = mock(HttpServletRequest.class);

        Map<String, String> headers = toMap(requestHeaders);

        when(req.getMethod()).thenReturn("POST");
        when(req.getContentType()).thenReturn("multipart/form-data; boundary=----fake_boundary");
        when(req.getHeader(anyString())).thenAnswer(m -> {
            return headers.get(m.getArguments()[0]);
        });

        StringBuilder sb = new StringBuilder();
        for (Map.Entry<String, String> entry : form.entrySet()) {
            sb.append("\r\n------fake_boundary\r\n");
            sb.append("Content-Disposition: form-data; name=\"").append(entry.getKey()).append("\"\r\n\r\n");
            sb.append(entry.getValue());
            sb.append("\r\n------fake_boundary");
        }
        if (sb.length() > 0) {
            sb.append("--");
        }

        doReturn(new ServletInputStream() {
            final ByteArrayInputStream bais = new ByteArrayInputStream(sb.toString().getBytes(UTF_8));

            @Override
            public int read() throws IOException {
                return bais.read();
            }

            @Override
            public boolean isFinished() {
                return false;
            }

            @Override
            public boolean isReady() {
                return true;
            }

            @Override
            public void setReadListener(ReadListener arg0) {
                throw new RuntimeException("Not supported");
            }
        }).when(req).getInputStream();

        return req;
    }

    public static HttpServletRequest mockHttpRequest(String path, String... requestHeaders) {
        HttpServletRequest req = mock(HttpServletRequest.class);

        Map<String, String> headers = toMap(requestHeaders);

        when(req.getPathInfo()).thenReturn(path);
        when(req.getHeader(anyString())).thenAnswer(m -> {
            return headers.get(m.getArguments()[0]);
        });
        when(req.getDateHeader(anyString())).thenAnswer(m -> {
            String v = headers.get(m.getArguments()[0]);
            if (v == null) {
                return -1L;
            }
            return Long.valueOf(v);
        });

        return req;
    }

    public static HttpServletResponse mockHttpResponse() throws IOException {
        return mockHttpResponse(null);
    }

    public static HttpServletResponse mockHttpResponse(ByteBuffer buf) throws IOException {
        HttpServletResponse resp = mock(HttpServletResponse.class);

        when(resp.getOutputStream()).thenReturn(new ServletOutputStream() {
            @Override
            public void write(int b) throws IOException {
                if (buf != null) {
                    buf.put((byte) b);
                }
            }

            @Override
            public boolean isReady() {
                return true;
            }

            @Override
            public void setWriteListener(WriteListener arg0) {
                throw new RuntimeException("Not supported");
            }
        });

        return resp;
    }

    public static Map<String, String> toMap(String... attrs) {
        Map<String, String> result = new HashMap<>();
        for (int i = 0; i < attrs.length; i += 2) {
            result.put(attrs[i], attrs[i + 1]);
        }
        return result;
    }
}
