/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.util.test.download;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;
import java.util.OptionalLong;

import org.amdatu.web.util.download.DownloadResource;

public class TestResource implements DownloadResource {
    private final String m_contentType;
    private final File m_file;

    public TestResource(File file, String contentType) {
        m_contentType = contentType;
        m_file = file;
    }

    @Override
    public InputStream openStream(final long offset, final long length) throws IOException {
        final FileInputStream fis = new FileInputStream(m_file);
        if (fis.skip(offset) != offset) {
            throw new IOException();
        }
        
        return new InputStream() {
            private long remaining = length;

            @Override
            public void close() throws IOException {
                fis.close();
            }

            @Override
            public int read() throws IOException {
                return --remaining >= 0 ? fis.read() : -1;
            }
        };
    }
    
    @Override
    public Optional<String> getEntityTag() {
        return Optional.of("strong-etag");
    }

    @Override
    public Optional<String> getContentType() {
        return Optional.ofNullable(m_contentType);
    }

    @Override
    public OptionalLong getLength() {
        return OptionalLong.of(m_file.length());
    }

    @Override
    public OptionalLong getLastModified() {
        return OptionalLong.of(m_file.lastModified());
    }

    @Override
    public String getName() {
        return m_file.getName();
    }

}
