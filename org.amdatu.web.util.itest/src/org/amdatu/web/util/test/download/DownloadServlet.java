/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.util.test.download;

import static org.osgi.service.log.LogService.LOG_DEBUG;
import static org.osgi.service.log.LogService.LOG_INFO;

import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.amdatu.web.util.download.DownloadHandler;
import org.amdatu.web.util.download.DownloadHelper;
import org.osgi.service.log.LogService;

/**
 * Provides a {@link Servlet} instance that is capable of downloading resources in a generic way, providing support for
 * content-ranges and entity tags.
 */
public class DownloadServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    private final DownloadHelper m_helper;
    // Managed by Felix DM...
    private volatile LogService m_log;
    
    public DownloadServlet(DownloadHandler handler) {
        m_helper = new DownloadHelper(handler, (l, m) -> log(m));
    }

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        m_helper.handle(req, resp, false /* head */);
    }

    @Override
    public void doHead(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        m_helper.handle(req, resp, true /* head */);
    }

    @Override
    public final void log(String msg) {
        m_log.log(LOG_DEBUG, msg);
    }

    @Override
    public void log(String msg, Throwable t) {
        m_log.log(LOG_INFO, msg, t);
    }
}
