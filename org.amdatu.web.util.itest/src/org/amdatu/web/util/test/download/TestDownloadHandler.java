/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.util.test.download;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.amdatu.web.util.download.DownloadHandler;
import org.amdatu.web.util.download.DownloadResource;

public class TestDownloadHandler implements DownloadHandler {
    private final List<TestResource> m_resources;

    public TestDownloadHandler() {
        m_resources = new ArrayList<>();
    }

    public void addFile(File f, String contentType) {
        m_resources.add(new TestResource(f, contentType));
    }

    @Override
    public Optional<DownloadResource> getResource(HttpServletRequest request) {
        String pathInfo = request.getPathInfo();
        while (pathInfo.startsWith("/")) {
            pathInfo = pathInfo.substring(1);
        }

        for (TestResource r : m_resources) {
            if (r.getName().endsWith(pathInfo)) {
                return Optional.of(r);
            }
        }
        return Optional.empty();
    }
}
