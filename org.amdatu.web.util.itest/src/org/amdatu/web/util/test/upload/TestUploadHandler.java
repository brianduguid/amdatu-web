/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.util.test.upload;

import java.io.IOException;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

import org.amdatu.web.util.upload.UploadException;
import org.amdatu.web.util.upload.UploadHandler;
import org.amdatu.web.util.upload.UploadedFile;

/**
 * {@link UploadHandler} implementation used for unit tests.
 */
public class TestUploadHandler implements UploadHandler<UploadedResource> {
    private final AtomicInteger m_uploadCount = new AtomicInteger(0);

    @Override
    public Optional<UploadedResource> addResource(UploadedFile file) throws UploadException, IOException {
        if ("ioexception".equals(file.getName())) {
            throw new IOException("TEST EXCEPTION!");
        }
        String name = String.format("uploaded-%02d", m_uploadCount.incrementAndGet());
        return Optional.of(new UploadedResource(name));
    }

}
