/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.util.test.upload;

import static java.nio.charset.StandardCharsets.UTF_8;
import static javax.servlet.http.HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
import static javax.servlet.http.HttpServletResponse.SC_NO_CONTENT;
import static javax.servlet.http.HttpServletResponse.SC_OK;
import static org.amdatu.testing.configurator.TestConfigurator.cleanUp;
import static org.amdatu.testing.configurator.TestConfigurator.configure;
import static org.amdatu.testing.configurator.TestConfigurator.createComponent;
import static org.amdatu.testing.configurator.TestConfigurator.createServiceDependency;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.osgi.service.http.whiteboard.HttpWhiteboardConstants.HTTP_WHITEBOARD_SERVLET_INIT_PARAM_PREFIX;
import static org.osgi.service.http.whiteboard.HttpWhiteboardConstants.HTTP_WHITEBOARD_SERVLET_PATTERN;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Dictionary;
import java.util.Hashtable;

import javax.servlet.Servlet;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.osgi.service.log.LogService;

import com.github.kevinsawicki.http.HttpRequest;

public class UploadHandlerTest {
    private String m_uploadUrl;
    private TestUploadHandler m_resProvider;

    @Before
    public void before() {
        String httpPort = System.getProperty("org.osgi.service.http.port", "8080");
        m_uploadUrl = String.format("http://localhost:%s/res/upload", httpPort);

        m_resProvider = new TestUploadHandler();

        Dictionary<String, Object> props = new Hashtable<>();
        props.put(HTTP_WHITEBOARD_SERVLET_PATTERN, "/res/upload");
        props.put(HTTP_WHITEBOARD_SERVLET_INIT_PARAM_PREFIX + "debug", "true");
        props.put("type", "junit-test");

        configure(this)
            .add(createComponent()
                .setInterface(Servlet.class.getName(), props)
                .setImplementation(new UploadServlet(m_resProvider))
                .add(createServiceDependency().setService(LogService.class).setRequired(false)))
            .apply();
    }

    @After
    public void after() {
        cleanUp(this);
    }

    @Test
    public void testUploadSingleFileForDefaultContext() throws Exception {
        File f = createTempFile("hello world!");

        HttpRequest req = HttpRequest.post(m_uploadUrl);
        req.header("Accept", "text/plain");
        req.part("file", f.getName(), f);

        assertEquals(SC_OK, req.code());

        String body = req.body();
        assertEquals("text/plain;charset=utf-8", req.contentType());
        assertEquals("uploaded-01", body.trim());
    }

    @Test
    public void testUploadSingleFailsWithIOException() throws Exception {
        File f = createTempFile("hello world!");

        HttpRequest req = HttpRequest.post(m_uploadUrl);
        req.header("Accept", "text/plain");
        req.part("file", "ioexception", f);

        assertEquals(SC_INTERNAL_SERVER_ERROR, req.code());

        String body = req.body();
        assertEquals("text/plain;charset=utf-8", req.contentType());
        assertTrue(body, "".equals(body));
    }

    @Test
    public void testUploadWithHtmlMediaType() throws Exception {
        File f = createTempFile("hello world!");

        HttpRequest req = HttpRequest.post(m_uploadUrl);
        req.header("Accept", "text/html");
        req.part("file", f.getName(), f);

        assertEquals(SC_OK, req.code());

        String body = req.body();
        assertEquals("text/plain;charset=utf-8", req.contentType());
        assertEquals("uploaded-01", body.trim());
    }

    @Test
    public void testUploadWithFormFieldsOnly() throws Exception {
        HttpRequest req = HttpRequest.post(m_uploadUrl);
        req.part("file", "part");

        assertEquals(SC_NO_CONTENT, req.code());
        assertEquals("", req.body());
    }

    @Test
    public void testUploadMultipleFilesForDefaultContext() throws Exception {
        File f1 = createTempFile("hello world!");
        File f2 = createTempFile("goodbye world!");

        HttpRequest req = HttpRequest.post(m_uploadUrl);
        req.header("Accept", "text/plain");
        req.part("file", f1.getName(), f1);
        req.part("file", f2.getName(), f2);

        assertEquals(SC_OK, req.code());

        String body = req.body();
        assertEquals("text/plain;charset=utf-8", req.contentType());

        String[] lines = body.split(System.lineSeparator());
        assertEquals(2, lines.length);
        assertEquals("uploaded-01", lines[0]);
        assertEquals("uploaded-02", lines[1]);
    }

    private File createTempFile(String content) throws IOException {
        Path f = Files.createTempFile("rs", ".txt");
        Files.write(f, content.getBytes(UTF_8));
        File file = f.toFile();
        file.deleteOnExit();
        return file;
    }
}