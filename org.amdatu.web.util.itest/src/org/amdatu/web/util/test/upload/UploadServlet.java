/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.util.test.upload;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.osgi.service.log.LogService.LOG_DEBUG;
import static org.osgi.service.log.LogService.LOG_INFO;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.amdatu.web.util.upload.UploadHandler;
import org.amdatu.web.util.upload.UploadHelper;
import org.osgi.service.log.LogService;

/**
 * Provides a {@link Servlet} instance that is capable of handling resource uploads in a generic way, providing support
 * for content-ranges and entity tags.
 */
public class UploadServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    private static final String TEXT_PLAIN = "text/plain";

    private final UploadHelper<UploadedResource> m_helper;
    // Managed by Felix DM...
    private volatile LogService m_log;

    public UploadServlet(UploadHandler<UploadedResource> handler) {
        m_helper = new UploadHelper<>(handler, 1024 * 1024, (l, m) -> log(m));
    }

    @Override
    public final void log(String msg) {
        m_log.log(LOG_DEBUG, msg);
    }

    @Override
    public void log(String msg, Throwable t) {
        m_log.log(LOG_INFO, msg, t);
    }

    @Override
    public void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setCharacterEncoding(UTF_8.name());
        resp.setContentType(TEXT_PLAIN);

        m_helper.handleUpload(req, resp)
            .ifPresent(result -> writeResourcesAsText(result, resp));
    }

    private void writeResourcesAsText(List<UploadedResource> resources, HttpServletResponse resp) {
        try (PrintWriter pw = resp.getWriter()) {
            resources.stream()
                .map(res -> res.getName())
                .forEach(n -> pw.println(n));
        } catch (IOException e) {
            log("Unable to write resource output!", e);
        }
    }
}
