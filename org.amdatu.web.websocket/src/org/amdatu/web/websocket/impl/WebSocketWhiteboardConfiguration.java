/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.websocket.impl;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

@ObjectClassDefinition(pid = WebSocketWhiteboardConfiguration.PID)
public @interface WebSocketWhiteboardConfiguration {

    final String PID = "org.amdatu.web.websocket";

    final String DEFAULT_WEB_SOCKET_SERVLET_ENABLED = "defaultWebSocketServletEnabled";
    final String DEFAULT_WEB_SOCKET_SERVLET_PATTERN = "defaultWebSocketServletPattern";

    @AttributeDefinition(required = false, name = DEFAULT_WEB_SOCKET_SERVLET_ENABLED)
    boolean defaultWebSocketServletEnabled() default true;

    @AttributeDefinition(required = false, name = DEFAULT_WEB_SOCKET_SERVLET_PATTERN)
    String defaultWebSocketServletPattern() default "/websocket/*";

}
