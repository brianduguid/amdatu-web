/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.websocket.impl;

import static org.amdatu.web.websocket.WebSocketConstants.WEBSOCKET_DEFAULT_SERVLET_NAME;
import static org.amdatu.web.websocket.WebSocketConstants.WEBSOCKET_NAME;
import static org.amdatu.web.websocket.WebSocketConstants.WEBSOCKET_SERVLET_PATTERN;
import static org.amdatu.web.websocket.impl.WebSocketWhiteboardConfiguration.DEFAULT_WEB_SOCKET_SERVLET_ENABLED;
import static org.amdatu.web.websocket.impl.WebSocketWhiteboardConfiguration.DEFAULT_WEB_SOCKET_SERVLET_PATTERN;
import static org.amdatu.web.websocket.impl.WebSocketWhiteboardConfiguration.PID;

import java.util.Dictionary;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

import javax.servlet.Servlet;

import org.amdatu.web.websocket.WebSocketConstants;
import org.apache.felix.dm.Component;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.Filter;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;
import org.osgi.service.http.whiteboard.HttpWhiteboardConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Whiteboard service for websocket servlets and endpoints
 *
 * Tracks services with an {@link WebSocketConstants#WEBSOCKET_SERVLET_PATTERN} property to create web socket servlets
 * and register them at the pattern
 *
 * Tracks services with an {@link WebSocketConstants#WEBSOCKET_ENDPOINT} property to register web socket endpoints, an
 * additional {@link WebSocketConstants#WEBSOCKET_SERVLET_SELECT} property can be used to register the endpoint with a
 * non default servlet.
 */
public class Whiteboard {

    private static final Logger LOGGER = LoggerFactory.getLogger(AmdatuWebsocketServlet.class);

    private final Object lock = new Object();

    private final Map<ServiceReference<?>, Component> servletComponentMap = new ConcurrentHashMap<>();
    private final Map<ServiceReference<?>, Object> endpoints = new ConcurrentHashMap<>();

    // managed by Apache Felix Dependency Manager
    private volatile DependencyManager dependencyManager;

    private volatile Boolean started = false;
    private volatile Component defaultServletComponent;

    private Dictionary<String, Object> config;

    // dm callback
    protected final void start() {
        synchronized (started) {
            updateDefaultServlet();
            started = true;
        }
    }

    // dm callback
    protected final void onServletAdded(ServiceReference<?> ref) {
        String pattern = (String) ref.getProperty(WebSocketConstants.WEBSOCKET_SERVLET_PATTERN);
        String contextSelect = (String) ref.getProperty(HttpWhiteboardConstants.HTTP_WHITEBOARD_CONTEXT_SELECT);

        Properties properties = new Properties();
        properties.put(HttpWhiteboardConstants.HTTP_WHITEBOARD_SERVLET_PATTERN, pattern);
        if (contextSelect != null) {
            properties.put(HttpWhiteboardConstants.HTTP_WHITEBOARD_CONTEXT_SELECT, contextSelect);
        }

        synchronized (lock) {
            AmdatuWebsocketServlet implementation = new AmdatuWebsocketServlet();

            // Add tracked endpoints that match the servlet to the new servlet
            endpoints.entrySet().stream()
                .filter(endpointEntry -> endpointMatchesServlet(ref, endpointEntry.getKey()))
                .map(Entry::getValue)
                .forEach(implementation::addEndpoint);

            Component component = dependencyManager.createComponent()
                .setCallbacks("dmInit", "dmStart", "dmStop", "dmDestroy")
                .setInterface(Servlet.class.getName(), properties)
                .setImplementation(implementation);

            servletComponentMap.put(ref, component);
            dependencyManager.add(component);
        }
    }

    // dm callback
    protected final void onServletChanged(ServiceReference<?> ref) {
        onServletRemoved(ref);
        onServletAdded(ref);
    }

    // dm callback
    protected final void onServletRemoved(ServiceReference<?> ref) {
        Component component = servletComponentMap.remove(ref);
        if (component != null) {
            dependencyManager.remove(component);
        }
    }

    // dm callback
    protected final void onEndpointAdded(ServiceReference<?> ref, Object service) {
        synchronized (lock) {
            endpoints.put(ref, service);

            // Add the endpoint to servlets that match the the servlet select filter
            servletComponentMap.entrySet().stream()
                .filter(e -> {
                    return endpointMatchesServlet(e.getKey(), ref);
                })
                .map(Entry::getValue).map(Component::getInstance)
                .forEach(servlet -> ((AmdatuWebsocketServlet) servlet).addEndpoint(service));
        }
    }

    // dm callback
    protected final void onEndpointRemoved(ServiceReference<?> ref, Object service) {
        synchronized (lock) {
            endpoints.remove(ref);
        }

        servletComponentMap.entrySet().stream()
            .filter(e -> endpointMatchesServlet(e.getKey(), ref))
            .map(Entry::getValue).map(Component::getInstance)
            .forEach(servlet -> ((AmdatuWebsocketServlet) servlet).removeEndpoint(service));
    }

    // dm callback
    protected final void updated(Dictionary<String, Object> config) {
        this.config = config;
        synchronized (started) {
            if (started) {
                LOGGER.info("Configuration updated, updating default servlet");
                updateDefaultServlet();
            }
        }
    }

    private boolean endpointMatchesServlet(ServiceReference<?> servletRef, ServiceReference<?> endpointRef) {
        String servletSelect = (String) endpointRef.getProperty(WebSocketConstants.WEBSOCKET_SERVLET_SELECT);

        if (servletSelect == null && WEBSOCKET_DEFAULT_SERVLET_NAME.equals(servletRef.getProperty(WEBSOCKET_NAME))) {
            return true;
        }
        else if (servletSelect != null) {
            try {
                Filter filter = FrameworkUtil.createFilter(servletSelect);
                return filter.match(servletRef);
            }
            catch (InvalidSyntaxException e) {
                LOGGER.error("Invalid servlet select filter for endpoint message: '{}', service reference: '{}'",
                    e.getMessage(), endpointRef);
                return false;
            }
        }
        else {
            return false;
        }
    }

    private void updateDefaultServlet() {
        boolean servletEnabled =
            Boolean.valueOf(System.getProperty(PID + "." + DEFAULT_WEB_SOCKET_SERVLET_ENABLED, "true"));
        String servletPattern = System.getProperty(PID + "." + DEFAULT_WEB_SOCKET_SERVLET_PATTERN, "/websocket/*");

        if (config != null) {
            Object enabledProp = config.get(DEFAULT_WEB_SOCKET_SERVLET_ENABLED);
            if (enabledProp != null) {
                servletEnabled = Boolean.valueOf(Objects.toString(enabledProp));
            }
            Object patternProp = config.get(DEFAULT_WEB_SOCKET_SERVLET_PATTERN);
            if (patternProp != null) {
                servletPattern = (String) patternProp;
            }
        }

        if (servletEnabled) {
            if (defaultServletComponent == null) {
                LOGGER.info("Registering default web socket servlet with pattern '{}'", servletPattern);
                Properties properties = new Properties();
                properties.put(WEBSOCKET_SERVLET_PATTERN, servletPattern);
                properties.put(WEBSOCKET_NAME, WEBSOCKET_DEFAULT_SERVLET_NAME);

                defaultServletComponent = dependencyManager.createComponent()
                    .setInterface(Object.class.getName(), properties).setImplementation(Object.class);
                dependencyManager.add(defaultServletComponent);
            }
            else {
                Dictionary<Object, Object> serviceProperties = defaultServletComponent.getServiceProperties();
                if (!serviceProperties.get(WEBSOCKET_SERVLET_PATTERN).equals(servletPattern)) {
                    LOGGER.info("Change default web socket servlet pattern to: '{}'", servletPattern);

                    serviceProperties.put(WEBSOCKET_SERVLET_PATTERN, servletPattern);
                    defaultServletComponent.setServiceProperties(serviceProperties);
                }
            }
        }
        else {
            LOGGER.info("Default web socket servlet disabled");
            if (defaultServletComponent != null) {
                LOGGER.info("Unregistreing default web socket servlet");
                dependencyManager.remove(defaultServletComponent);
                defaultServletComponent = null;
            }
        }
    }
}
