/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.websocket.impl;

import static org.amdatu.web.websocket.WebSocketConstants.WEBSOCKET_ENDPOINT;
import static org.amdatu.web.websocket.WebSocketConstants.WEBSOCKET_SERVLET_PATTERN;

import javax.websocket.WebSocketContainer;

import org.apache.felix.dm.Component;
import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.eclipse.jetty.websocket.jsr356.ClientContainer;
import org.osgi.framework.BundleContext;

public class Activator extends DependencyActivatorBase {


    @Override
    public void init(BundleContext context, DependencyManager manager) throws Exception {
        manager.add(createComponent()
            .setImplementation(Whiteboard.class)
            .setAutoConfig(Component.class, false) // There is another Component field in the implementation prevent overwriting that
            .add(createConfigurationDependency()
                .setPid(WebSocketWhiteboardConfiguration.PID)
                .setCallback("updated")
                .setRequired(false))
            .add(createServiceDependency()
                .setService("(&(objectClass=*)(" + WEBSOCKET_SERVLET_PATTERN + "=*))")
                .setCallbacks("onServletAdded", "onServletChanged", "onServletRemoved")
                .setRequired(false))
            .add(createServiceDependency()
                .setService("(&(objectClass=*)(" + WEBSOCKET_ENDPOINT + "=true))")
                .setCallbacks("onEndpointAdded", "onEndpointRemoved")
                .setRequired(false)));

        // Register a WebSocketContainer service that can be used to create websocket clients
        manager.add(createComponent()
            .setInterface(WebSocketContainer.class.getName(), null)
            .setImplementation(ClientContainer.class)
            .setCallbacks(null, "start", "stop", null) // The client container has a start / stop method have dm call that
            );
    }

}
