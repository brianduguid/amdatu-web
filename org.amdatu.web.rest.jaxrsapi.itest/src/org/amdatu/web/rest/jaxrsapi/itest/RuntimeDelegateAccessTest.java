/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.jaxrsapi.itest;

import javax.ws.rs.ext.RuntimeDelegate;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class RuntimeDelegateAccessTest {

    @Test
    public void getRuntimeDelegate() {
        /*
         * JAX-RS api classes (for example Cookie) get the RuntimeDelegate using the static getInstance method. This doesn't work
         * if the RuntimeDelegate instance isn't initialized as it then attempts to load the Jersey implementation which is a
         * hardcoded default.
         *
         * Run this in a separat test project to make sure no other JAX-RS usage is initializing the RuntimeDelegate before we run the test.
         */
        RuntimeDelegate.getInstance();
    }

}
