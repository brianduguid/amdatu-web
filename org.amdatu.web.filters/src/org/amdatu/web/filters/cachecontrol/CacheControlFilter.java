/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.filters.cachecontrol;

import java.io.IOException;
import java.util.Dictionary;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

import org.osgi.service.cm.ConfigurationException;

/**
 * Sets the "Cache-Control" header on all responses that match the filter registration.
 */
public class CacheControlFilter implements Filter {
    private static final String CACHE_CONTROL = "cacheControl";

    private volatile String m_cacheControl;

    @Override
    public void destroy() {
        // do nothing
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
        HttpServletResponse response = (HttpServletResponse) res;

        response.setHeader("Cache-Control", m_cacheControl);

        chain.doFilter(req, response);
    }

    @Override
    public void init(FilterConfig cfg) throws ServletException {
        // do nothing
    }

    // Called by Felix DM *after* it created an instance of this filter...
    public void updated(Dictionary<String, ?> properties) throws ConfigurationException {
        String cacheControl = (String) properties.get(CACHE_CONTROL);

        if (cacheControl == null || "".equals(cacheControl.trim())) {
            throw new ConfigurationException(CACHE_CONTROL, "missing or empty configuration property!");
        }

        m_cacheControl = cacheControl;
    }
}
