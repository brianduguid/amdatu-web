/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.testing.http;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.function.Predicate;

import org.amdatu.testing.configurator.ConfigurationStep;
import org.amdatu.testing.configurator.TestConfigurationException;

/**
 * Provides a utility configuration step that allows your test case to wait until certain HTTP endpoints are available.
 */
public class HttpEndpointsAvailableStep implements ConfigurationStep {
    private final List<String> m_urls;
    private int m_duration;
    private TimeUnit m_timeUnit;
    private Predicate<String> m_testFunction;

    public HttpEndpointsAvailableStep() {
        m_urls = new ArrayList<>(3);
        m_duration = 5;
        m_timeUnit = TimeUnit.SECONDS;

        m_testFunction = HttpTestUtils::checkEndpointAvailable;
    }

    @Override
    public void apply(Object testCase) {
        String[] urls = m_urls.toArray(new String[m_urls.size()]);
        if (!HttpTestUtils.waitFor(m_duration, m_timeUnit, m_testFunction, urls)) {
            throw new TestConfigurationException("Time out waiting for HTTP endpoints to become available. Tested URLs: " + m_urls);
        }
    }

    @Override
    public void cleanUp(Object testCase) {
        // Nop
    }

    /**
     * Appends one or more URLs to the set of URLs that are checked for availability.
     *
     * @param urls the URLs to add to the check, should contain at least one URL.
     * @return this configuration step, never <code>null</code>.
     */
    public HttpEndpointsAvailableStep addURLs(String... urls) {
        if (urls.length < 1) {
            throw new IllegalArgumentException("Need at least one URL to wait for!");
        }
        for (String url : urls) {
            if (!m_urls.contains(url)) {
                m_urls.add(url);
            }
        }
        return this;
    }

    /**
     * Sets the URLs that are checked for availability.
     *
     * @param urls the URLs to set for the check, should contain at least one URL.
     * @return this configuration step, never <code>null</code>.
     */
    public HttpEndpointsAvailableStep setURLs(String... urls) {
        if (urls.length < 1) {
            throw new IllegalArgumentException("Need at least one URL to wait for!");
        }
        m_urls.clear();
        m_urls.addAll(Arrays.asList(urls));
        return this;
    }

    /**
     * Sets the timeout to wait for the availability of the endpoints.
     *
     * @param duration the duration, &gt; 0;
     * @param unit the time unit, cannot be <code>null</code>.
     * @return this configuration step, never <code>null</code>.
     */
    public HttpEndpointsAvailableStep setTimeout(int duration, TimeUnit unit) {
        Objects.requireNonNull(unit, "TimeUnit cannot be null!");
        m_duration = duration;
        m_timeUnit = unit;
        return this;
    }

    /**
     * Sets the test function to use in the check for the HTTP endpoints.
     *
     * @param predicate the predicate function to use, cannot be <code>null</code>. The
     *        argument supplied to the predicate is the endpoint URL that should be tested.
     * @return this configuration step, never <code>null</code>.
     */
    public HttpEndpointsAvailableStep setTest(Predicate<String> predicate) {
        Objects.requireNonNull(predicate, "Predicate cannot be null!");
        m_testFunction = predicate;
        return this;
    }
}
