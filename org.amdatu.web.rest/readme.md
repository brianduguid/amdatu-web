OpenAPI Definition Generator
============================

Known Issues
------------

1. [Swagger UI](https://github.com/swagger-api/swagger-ui), as of 3.5.0, is still being updated for the OpenAPI 3.0 specification.
1. Specifications NOT implemented: [Examples](https://swagger.io/docs/specification/adding-examples/), [Links](https://swagger.io/docs/specification/links/), and [Callbacks](https://swagger.io/docs/specification/callbacks/).
1. Reusable [components](https://swagger.io/docs/specification/components/) functionality NOT implemented: Parameters, Request Bodies, Examples, Links, and Callbacks.
1. Schema combiners [oneOf, allOf, and anyOf](https://swagger.io/docs/specification/data-models/oneof-anyof-allof-not/) is not implemented as a configurable option.
1. [Representing XML](https://swagger.io/docs/specification/data-models/representing-xml/) options are not supported.
1. As of Swagger UI 3.7.0, the lock icon is unlocked by default and locked after authentication. This is manually fixed in the index.html replacement.
