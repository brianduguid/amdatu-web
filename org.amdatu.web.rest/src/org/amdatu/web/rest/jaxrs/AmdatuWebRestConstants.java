/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.jaxrs;

import javax.ws.rs.core.Application;

import org.osgi.service.http.context.ServletContextHelper;
import org.osgi.service.http.whiteboard.HttpWhiteboardConstants;

/**
 * Defines constants for the Amdatu-Web REST whiteboard services.
 */
public class AmdatuWebRestConstants {

    private AmdatuWebRestConstants() {
        // non-instantiable
    }

    public static final String JAX_RS_FILTER_BASE = "osgi.jaxrs.filter.base";

    public static final String JAX_RS_INTERCEPTOR_BASE = "osgi.jaxrs.interceptor.base";
    /**
     * Service property specifying the base URI mapping for a JAX-RS resource service.
     *
     * The value of this service property must be of type String, and will have a "/" prepended if no "/" exists.
     */
    public static final String JAX_RS_RESOURCE_BASE = "osgi.jaxrs.resource.base";



    /**
     * Service property specifying the name of a JAX-RS resource.
     * <p>
     * Resource names should be unique among all resource service associated with a single whiteboard implementation.
     * <p>
     * The value of this service property must be of type String.
     */
    public static final String JAX_RS_RESOURCE_NAME = "osgi.jaxrs.name";

    /**
     * Service property specifying the base URI mapping for a JAX-RS application service, which implies that
     * all registered resources that belong to that application will be prefixed as well.
     * <p>
     * The value of this service property must be of type String, and will have a "/" prepended if no "/" exists.
     * If omitted, an empty value is used (this means no prefix will be used).
     */
    public static final String JAX_RS_APPLICATION_BASE = "osgi.jaxrs.application.base";

    /**
     * Service property specifying the name of the {@link ServletContextHelper} that should be used for the JAX-RS
     * application service.
     * <p>
     * The value of this service property must be of type String and should be provided for the {@link Application}
     * service. If omitted, the value {@link HttpWhiteboardConstants#HTTP_WHITEBOARD_DEFAULT_CONTEXT_NAME} will be
     * used as default.
     */
    public static final String JAX_RS_APPLICATION_CONTEXT = "org.amdatu.web.rest.application.context";

    /**
     * Service property specifying the name for a JAX-RS application.
     * <p>
     * The value of this service property must be of type String and should be provided on <b>both</b> the
     * {@link Application} and all REST endpoints that belongs to that application.
     * <p>
     * This property <b>must</b> be supplied for applications of type {@link #JAX_RS_APPLICATION_TYPE_STATIC} and
     * {@link #JAX_RS_APPLICATION_TYPE_DYNAMIC} and <b>may</b> be supplied for {@link #JAX_RS_APPLICATION_TYPE_DYNAMIC}.
     */
    public static final String JAX_RS_APPLICATION_NAME = "org.amdatu.web.rest.application.name";

    /**
     * Service property specifying the type for a JAX-RS application.
     * <p>
     * The value of this service property must be of type {@code String}, and the allowed values are:
     * {@link #JAX_RS_APPLICATION_TYPE_STATIC}, {@link #JAX_RS_APPLICATION_TYPE_DYNAMIC}, or
     * {@link #JAX_RS_APPLICATION_TYPE_LEGACY}. If omitted, the static application type is used as default (see
     * {@link #JAX_RS_APPLICATION_TYPE_STATIC}).
     * <p>
     * For types {@link #JAX_RS_APPLICATION_TYPE_STATIC} and {@link #JAX_RS_APPLICATION_TYPE_DYNAMIC} the
     * {@link #JAX_RS_APPLICATION_NAME} service property must be supplied as well.
     */
    public static final String JAX_RS_APPLICATION_TYPE = "org.amdatu.web.rest.application.type";

    /**
     * Possible value for the {@link #JAX_RS_APPLICATION_TYPE} property indicating the application is a static application.
     * <p>
     * Static types can be used in case the REST resources are known a priori. This implies that the {@link Application}
     * returns all REST resources as singleton (see {@link Application#getSingletons()}).
     */
    public static final String JAX_RS_APPLICATION_TYPE_STATIC = "org.amdatu.web.rest.application.type.static";

    /**
     * Possible value for the {@link #JAX_RS_APPLICATION_TYPE} property indicating the application is a dynamic application.
     * <p>
     * For a dynamic application all REST resources and provider services with a matching application name property (see
     * {@link #JAX_RS_APPLICATION_NAME}) along with a defined {@link #JAX_RS_APPLICATION_BASE} property will be added to
     * the application. This type can be used in a dynamic environment in which REST resources are registered from various
     * locations and it is not possible to register them a priori.
     */
    public static final String JAX_RS_APPLICATION_TYPE_DYNAMIC = "org.amdatu.web.rest.application.type.dynamic";

    /**
     * Possible value for the {@link #JAX_RS_APPLICATION_TYPE} property indicating the application is a legacy application
     * <p>
     * For a legacy application additional resource and provider services will be tracked in a way that is compatible with
     * the 2.x versions of the org.amdatu.web.rest.wink implementation. Resource and provider services that do <b>not</b>
     * have a {@link #JAX_RS_RESOURCE_BASE} or {@link #JAX_RS_APPLICATION_NAME} service property specified AND resources
     * with a matching {@link #JAX_RS_APPLICATION_NAME} will be added to the application.
     * <p>
     * Use this type only if you are migrating from a previous version of Amdatu-Web! For new implementations, always
     * consider using {@link #JAX_RS_APPLICATION_TYPE_DYNAMIC} instead of this type!
     */
    public static final String JAX_RS_APPLICATION_TYPE_LEGACY = "org.amdatu.web.rest.application.type.legacy";

}
