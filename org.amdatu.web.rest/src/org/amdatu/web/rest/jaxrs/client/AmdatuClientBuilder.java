/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.jaxrs.client;

import java.security.KeyStore;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.ws.rs.core.Configuration;

public interface AmdatuClientBuilder {

    Configuration getConfiguration();

    AmdatuClientBuilder property(String name, Object value);

    AmdatuClientBuilder register(Class<?> componentClass);

    AmdatuClientBuilder register(Object component);

    AmdatuClientBuilder register(Class<?> componentClass, int priority);

    AmdatuClientBuilder register(Class<?> componentClass, Class<?>... contracts);

    AmdatuClientBuilder register(Class<?> componentClass, Map<Class<?>, Integer> contracts);

    AmdatuClientBuilder register(Object component, int priority);

    AmdatuClientBuilder register(Object component, Class<?>... contracts);

    AmdatuClientBuilder register(Object component, Map<Class<?>, Integer> contracts);

    AmdatuClient build();

    AmdatuClientBuilder hostnameVerifier(HostnameVerifier verifier);

    AmdatuClientBuilder keyStore(KeyStore keystore, char[] password);

    AmdatuClientBuilder sslContext(SSLContext sslContext);

    AmdatuClientBuilder trustStore(KeyStore trustStore);

    AmdatuClientBuilder withConfig(Configuration config);

}
