/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.resteasy;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.SecurityContext;

import org.amdatu.web.rest.spi.ApplicationService;
import org.jboss.resteasy.core.SynchronousDispatcher;
import org.jboss.resteasy.core.ThreadLocalResteasyProviderFactory;
import org.jboss.resteasy.plugins.server.servlet.HttpRequestFactory;
import org.jboss.resteasy.plugins.server.servlet.HttpResponseFactory;
import org.jboss.resteasy.plugins.server.servlet.ServletContainerDispatcher;
import org.jboss.resteasy.plugins.server.servlet.ServletSecurityContext;
import org.jboss.resteasy.plugins.server.servlet.ServletUtil;
import org.jboss.resteasy.resteasy_jaxrs.i18n.LogMessages;
import org.jboss.resteasy.specimpl.ResteasyHttpHeaders;
import org.jboss.resteasy.spi.HttpRequest;
import org.jboss.resteasy.spi.HttpResponse;
import org.jboss.resteasy.spi.PropertyInjector;
import org.jboss.resteasy.spi.ResteasyDeployment;
import org.jboss.resteasy.spi.ResteasyProviderFactory;
import org.jboss.resteasy.spi.ResteasyUriInfo;
import org.jboss.resteasy.util.GetRestful;

public class OsgiServletContainerDispatcher extends ServletContainerDispatcher {

    private String m_servletMappingPrefix;

    protected void init(ApplicationService applicationService, String prefix, HttpRequestFactory requestFactory,
        HttpResponseFactory responseFactory) {

        m_servletMappingPrefix = prefix;
        this.requestFactory = requestFactory;
        this.responseFactory = responseFactory;

        ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
        try {
            // Set context ClassLoader as initialization depends on some service loading
            Thread.currentThread().setContextClassLoader(getClass().getClassLoader());
            ResteasyDeployment deployment = new ResteasyDeployment();
            deployment.setInjectorFactoryClass(AmdatuInjectorFactory.class.getName());
            deployment.start();
            dispatcher = deployment.getDispatcher();
            providerFactory = deployment.getProviderFactory();

            applicationService.getApplication().ifPresent(application -> {
                dispatcher.getDefaultContextObjects().put(Application.class, application);
                ResteasyProviderFactory.pushContext(Application.class, application);
                PropertyInjector propertyInjector = providerFactory.getInjectorFactory().createPropertyInjector(application.getClass(), providerFactory);
                propertyInjector.inject(application);

                processApplication(application);
            });

            for (Object obj : applicationService.getSingletons()) {
                if (GetRestful.isRootResource(obj.getClass())) {
                    dispatcher.getRegistry().addSingletonResource(obj);
                } else {
                   providerFactory.registerProviderInstance(obj);
                }
            }
        } finally {
            Thread.currentThread().setContextClassLoader(contextClassLoader);
        }
    }

    public void service(String httpMethod, HttpServletRequest request, HttpServletResponse response,
        boolean handleNotFound) throws IOException, NotFoundException {
        try {
            ResteasyProviderFactory defaultInstance = ResteasyProviderFactory.getInstance();
            if (defaultInstance instanceof ThreadLocalResteasyProviderFactory) {
                ThreadLocalResteasyProviderFactory.push(providerFactory);
            }
            ResteasyHttpHeaders headers = null;
            ResteasyUriInfo uriInfo = null;
            try {
                headers = ServletUtil.extractHttpHeaders(request);
                uriInfo = ServletUtil.extractUriInfo(request, m_servletMappingPrefix);
            } catch (Exception e) {
                response.sendError(HttpServletResponse.SC_BAD_REQUEST);
                // made it warn so that people can filter this.
                LogMessages.LOGGER.failedToParseRequest(e);
                return;
            }

            HttpResponse theResponse = responseFactory.createResteasyHttpResponse(response);
            HttpRequest in =
                requestFactory.createResteasyHttpRequest(httpMethod, request, headers, uriInfo, theResponse, response);

            try {
                ResteasyProviderFactory.pushContext(HttpServletRequest.class, request);
                ResteasyProviderFactory.pushContext(HttpServletResponse.class, response);

                ResteasyProviderFactory.pushContext(SecurityContext.class, new ServletSecurityContext(request));
                if (handleNotFound) {
                    dispatcher.invoke(in, theResponse);
                } else {
                    ((SynchronousDispatcher) dispatcher).invokePropagateNotFound(in, theResponse);
                }
            } finally {
                ResteasyProviderFactory.clearContextData();
            }
        } finally {
            ResteasyProviderFactory defaultInstance = ResteasyProviderFactory.getInstance();
            if (defaultInstance instanceof ThreadLocalResteasyProviderFactory) {
                ThreadLocalResteasyProviderFactory.pop();
            }
        }
    }
}
