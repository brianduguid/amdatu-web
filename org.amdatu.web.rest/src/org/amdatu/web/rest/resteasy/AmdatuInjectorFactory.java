/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.resteasy;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.amdatu.web.rest.jaxrs.JaxRsRequestInterceptor;
import org.amdatu.web.rest.jaxrs.RequestContext;
import org.jboss.resteasy.core.InjectorFactoryImpl;
import org.jboss.resteasy.core.MethodInjectorImpl;
import org.jboss.resteasy.resteasy_jaxrs.i18n.Messages;
import org.jboss.resteasy.spi.ApplicationException;
import org.jboss.resteasy.spi.Failure;
import org.jboss.resteasy.spi.HttpRequest;
import org.jboss.resteasy.spi.HttpResponse;
import org.jboss.resteasy.spi.InjectorFactory;
import org.jboss.resteasy.spi.InternalServerErrorException;
import org.jboss.resteasy.spi.MethodInjector;
import org.jboss.resteasy.spi.ResteasyProviderFactory;
import org.jboss.resteasy.spi.metadata.ResourceLocator;
import org.jboss.resteasy.spi.validation.GeneralValidator;
import org.jboss.resteasy.spi.validation.GeneralValidatorCDI;

/**
 * {@link InjectorFactory} implementation that creates a custom {@link MethodInjector} to support the
 * {@link JaxRsRequestInterceptor}.
 *
 */
public class AmdatuInjectorFactory extends InjectorFactoryImpl {

    public static class AmdatuMethodInjector extends MethodInjectorImpl {

        private Set<JaxRsRequestInterceptor> m_interceptors = new HashSet<>();

        public AmdatuMethodInjector(ResourceLocator resourceMethod, ResteasyProviderFactory factory) {
            super(resourceMethod, factory);
        }

        /**
         * This method is an altered copy of the {@link MethodInjectorImpl#invoke(HttpRequest, HttpResponse, Object)} method, before invoking
         * the method on the resource all registered {@link JaxRsRequestInterceptor} provider instances are called.
         */
        public Object invoke(HttpRequest request, HttpResponse httpResponse, Object resource) throws Failure, ApplicationException {
            Object[] args = injectArguments(request, httpResponse);

            // START CUSTOM CODE for JaxRsRequestInterceptor support
            Set<Object> providerInstances = factory.getProviderInstances();
            for (Object providerInstance : providerInstances) {
                if (providerInstance instanceof JaxRsRequestInterceptor) {
                    m_interceptors.add((JaxRsRequestInterceptor) providerInstance);
                }
            }

            if (!m_interceptors.isEmpty()) {
                HttpServletRequest httpServletRequest = ResteasyProviderFactory.getContextData(HttpServletRequest.class);
                HttpServletResponse httpServletResponse = ResteasyProviderFactory.getContextData(HttpServletResponse.class);
                RequestContext context = new RequestContext(resource, method.getMethod(), args, httpServletRequest, httpServletResponse);

                for (JaxRsRequestInterceptor interceptor : m_interceptors) {
                    interceptor.intercept(context);
                }
            }
            // END CUSTOM CODE for JaxRsRequestInterceptor support

            GeneralValidator validator = GeneralValidator.class.cast(request.getAttribute(GeneralValidator.class.getName()));
            if (validator != null) {
                validator.validateAllParameters(request, resource, method.getMethod(), args);
            }

            Method invokedMethod = method.getMethod();
            if (!invokedMethod.getDeclaringClass().isAssignableFrom(resource.getClass())) {
                // invokedMethod is for when the target object might be a proxy and
                // resteasy is getting the bean class to introspect.
                // In other words ResourceMethod.getMethod() does not have the same declared class as the proxy:
                // An example is a proxied Spring bean that is a resource
                // interface ProxiedInterface { String get(); }
                // @Path("resource") class MyResource implements ProxiedInterface {
                // @GET String get() {...}
                // }
                //
                invokedMethod = interfaceBasedMethod;
            }

            Object result = null;
            try {
                result = invokedMethod.invoke(resource, args);
            }
            catch (IllegalAccessException e) {
                throw new InternalServerErrorException(Messages.MESSAGES.notAllowedToReflectOnMethod(method.toString()), e);
            }
            catch (InvocationTargetException e) {
                Throwable cause = e.getCause();
                if (validator instanceof GeneralValidatorCDI) {
                    GeneralValidatorCDI.class.cast(validator).checkForConstraintViolations(request, e);
                }
                throw new ApplicationException(cause);
            }
            catch (IllegalArgumentException e) {
                String msg = Messages.MESSAGES.badArguments(method.toString() + "  (");
                if (args != null) {
                    boolean first = false;
                    for (Object arg : args) {
                        if (!first) {
                            first = true;
                        }
                        else {
                            msg += ",";
                        }
                        if (arg == null) {
                            msg += " null";
                            continue;
                        }
                        msg += " " + arg.getClass().getName() + " " + arg;
                    }
                }
                msg += " )";
                throw new InternalServerErrorException(msg, e);
            }
            if (validator != null) {
                validator.validateReturnValue(request, resource, method.getMethod(), result);
            }
            return result;
        }

    }

    @Override
    public MethodInjector createMethodInjector(ResourceLocator method, ResteasyProviderFactory factory) {
        return new AmdatuMethodInjector(method, factory);
    }

}
