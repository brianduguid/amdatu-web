/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.resteasy;

import static org.osgi.service.http.whiteboard.HttpWhiteboardConstants.HTTP_WHITEBOARD_CONTEXT_NAME;
import static org.osgi.service.http.whiteboard.HttpWhiteboardConstants.HTTP_WHITEBOARD_CONTEXT_SELECT;
import static org.osgi.service.http.whiteboard.HttpWhiteboardConstants.HTTP_WHITEBOARD_DEFAULT_CONTEXT_NAME;
import static org.osgi.service.http.whiteboard.HttpWhiteboardConstants.HTTP_WHITEBOARD_SERVLET_PATTERN;

import java.io.IOException;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.concurrent.atomic.AtomicReference;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.amdatu.web.rest.spi.ApplicationService;
import org.apache.felix.dm.Component;
import org.jboss.resteasy.core.Dispatcher;
import org.jboss.resteasy.core.SynchronousDispatcher;
import org.jboss.resteasy.plugins.server.servlet.HttpRequestFactory;
import org.jboss.resteasy.plugins.server.servlet.HttpResponseFactory;
import org.jboss.resteasy.plugins.server.servlet.HttpServletInputMessage;
import org.jboss.resteasy.plugins.server.servlet.HttpServletResponseWrapper;
import org.jboss.resteasy.plugins.server.servlet.ServletContainerDispatcher;
import org.jboss.resteasy.specimpl.ResteasyHttpHeaders;
import org.jboss.resteasy.spi.HttpRequest;
import org.jboss.resteasy.spi.HttpResponse;
import org.jboss.resteasy.spi.ResteasyUriInfo;
import org.osgi.framework.ServiceReference;

public class OsgiHttpServletDispatcher extends HttpServlet implements HttpRequestFactory, HttpResponseFactory {

    private static final long serialVersionUID = 1L;

    private volatile Component m_component;

    protected final AtomicReference<ServletContainerDispatcher> m_servletContainerDispatcherRef =
        new AtomicReference<>();
    private ApplicationService m_applicationService;

    public Dispatcher getDispatcher() {
        return getRequestProcessor().getDispatcher();
    }

    public void init(ServletConfig servletConfig) throws ServletException {
        super.init(servletConfig);
    }

    @Override
    protected void service(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse)
        throws ServletException, IOException {
        service(httpServletRequest.getMethod(), httpServletRequest, httpServletResponse);
    }

    public void service(String httpMethod, HttpServletRequest request, HttpServletResponse response)
        throws IOException {

        getRequestProcessor().service(httpMethod, request, response, true);
    }

    public HttpRequest createResteasyHttpRequest(String httpMethod, HttpServletRequest request,
        ResteasyHttpHeaders headers, ResteasyUriInfo uriInfo, HttpResponse theResponse, HttpServletResponse response) {
        return createHttpRequest(httpMethod, request, headers, uriInfo, theResponse, response);
    }

    public HttpResponse createResteasyHttpResponse(HttpServletResponse response) {
        return createServletResponse(response);
    }

    protected HttpRequest createHttpRequest(String httpMethod, HttpServletRequest request, ResteasyHttpHeaders headers,
        ResteasyUriInfo uriInfo, HttpResponse theResponse, HttpServletResponse response) {
        return new HttpServletInputMessage(request, response, getServletContext(), theResponse, headers, uriInfo,
            httpMethod.toUpperCase(), (SynchronousDispatcher) getDispatcher());
    }

    protected HttpResponse createServletResponse(HttpServletResponse response) {
        return new HttpServletResponseWrapper(response, getDispatcher().getProviderFactory());
    }

    protected final void onAdd(ServiceReference<ApplicationService> ref, ApplicationService applicationService) {
        m_applicationService = applicationService;
        updateServiceProperties();
    }

    protected final void onChange(ServiceReference<ApplicationService> ref) {
        updateServiceProperties();
        markRequestProcessorOutdated();
    }

    private void updateServiceProperties() {
        Dictionary<Object, Object> properties = m_component.getServiceProperties();
        if (properties == null) {
            properties = new Hashtable<>();
        }
        properties.put(HTTP_WHITEBOARD_SERVLET_PATTERN, m_applicationService.getBaseUri() + "/*");
        if (m_applicationService.getContextName() != null
            && !HTTP_WHITEBOARD_DEFAULT_CONTEXT_NAME.equals(m_applicationService.getContextName())) {
            properties.put(HTTP_WHITEBOARD_CONTEXT_SELECT,
                "(" + HTTP_WHITEBOARD_CONTEXT_NAME + "=" + m_applicationService.getContextName() + ")");
        } else {
            properties.remove(HTTP_WHITEBOARD_CONTEXT_SELECT);
        }

        m_component.setServiceProperties(properties);
    }

    /**
     * Used in JaxRsFilter to check if the path can be handled by wink
     */
    private ServletContainerDispatcher getRequestProcessor() {
        synchronized (m_servletContainerDispatcherRef) {
            ServletContainerDispatcher rp = m_servletContainerDispatcherRef.get();
            if (rp == null) {
                ServletContainerDispatcher newRP = createServletContainerDispatcher();
                do {
                    rp = m_servletContainerDispatcherRef.get();
                } while (!m_servletContainerDispatcherRef.compareAndSet(rp, newRP));
            }
            return m_servletContainerDispatcherRef.get();
        }
    }

    private ServletContainerDispatcher createServletContainerDispatcher() {
        OsgiServletContainerDispatcher osgiServletContainerDispatcher = new OsgiServletContainerDispatcher();

        osgiServletContainerDispatcher.init(m_applicationService, getContextUri(), this, this);

        return osgiServletContainerDispatcher;
    }

    private String getContextUri() {
        return m_applicationService.getBaseUri();
    }

    protected void markRequestProcessorOutdated() {
        synchronized (m_servletContainerDispatcherRef) {
            m_servletContainerDispatcherRef.set(null);
        }
    }

}
