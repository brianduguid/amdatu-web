/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.resteasy;

import static org.amdatu.web.rest.jaxrs.AmdatuWebRestConstants.JAX_RS_APPLICATION_BASE;
import static org.amdatu.web.rest.jaxrs.AmdatuWebRestConstants.JAX_RS_RESOURCE_BASE;

import javax.servlet.Servlet;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.Application;
import javax.ws.rs.ext.RuntimeDelegate;

import org.amdatu.web.rest.jaxrs.client.AmdatuClientBuilder;
import org.amdatu.web.rest.resteasy.client.AmdatuClientBuilderServiceFactory;
import org.amdatu.web.rest.resteasy.client.ClientBuilderServiceFactory;
import org.amdatu.web.rest.spi.ApplicationService;
import org.amdatu.web.rest.whiteboard.JaxRsWhiteboard;
import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.service.log.LogService;

public class Activator extends DependencyActivatorBase {

    @Override
    public void init(BundleContext context, DependencyManager manager) throws Exception {

        ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
        try {
            // set context class loader for service loading to work
            Thread.currentThread().setContextClassLoader(getClass().getClassLoader());

            // Initialize the runtime delegate (singleton) to make sure the instance is available
            RuntimeDelegate.getInstance();
        } finally {
            Thread.currentThread().setContextClassLoader(contextClassLoader);
        }

        //@formatter:off

        manager
            .add(createComponent().setImplementation(JaxRsWhiteboard.class)
            // Workaround for FELIX-5268: Service not unregistered while bundle is starting
            .add(createBundleDependency()
                    .setBundle(context.getBundle())
                    .setStateMask(Bundle.ACTIVE)
                    .setRequired(true))
            .add(createServiceDependency()
                    .setService(LogService.class)
                    .setRequired(false))
            .add(createServiceDependency()
                    .setService(Application.class, withServiceProperty(JAX_RS_APPLICATION_BASE))
                    .setCallbacks("applicationAdded", "applicationChanged", "applicationRemoved")
                    .setRequired(false))
            .add(createServiceDependency().setService(withServiceProperty(JAX_RS_RESOURCE_BASE))
                    .setCallbacks("resourceAdded", "resourceChanged", "resourceRemoved")
                    .setRequired(false))
            );

        manager
            .add(createAdapterService(ApplicationService.class, null, "onAdd", "onChange", null)
                .setInterface(Servlet.class.getName(), null)
                .setImplementation(OsgiHttpServletDispatcher.class)
                .setCallbacks("dmInit", null, null, null)
            );

        manager
            .add(createComponent()
                .setInterface(ClientBuilder.class.getName(), null)
                .setImplementation(ClientBuilderServiceFactory.class)
            );

        manager
        .add(createComponent()
            .setInterface(AmdatuClientBuilder.class.getName(), null)
            .setImplementation(AmdatuClientBuilderServiceFactory.class)
        );
        //@formatter:on
    }

    private static String withServiceProperty(String property) {
        return "(" + property + "=*)";
    }

}
