/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.resteasy.client;

import java.security.KeyStore;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.ws.rs.core.Configuration;

import org.amdatu.web.rest.jaxrs.client.AmdatuClientBuilder;
import org.amdatu.web.rest.jaxrs.client.AmdatuClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;

public class AmdatuClientBuilderImp implements AmdatuClientBuilder {

    private final ResteasyClientBuilder m_clientBuilder;

    public AmdatuClientBuilderImp(ResteasyClientBuilder clientBuilder) {
        m_clientBuilder = clientBuilder;
    }

    @Override
    public Configuration getConfiguration() {
        return m_clientBuilder.getConfiguration();
    }

    @Override
    public AmdatuClientBuilder property(String name, Object value) {
        m_clientBuilder.property(name, value);
        return this;
    }

    @Override
    public AmdatuClientBuilder register(Class<?> componentClass) {
        m_clientBuilder.register(componentClass);
        return this;
    }

    @Override
    public AmdatuClientBuilder register(Object component) {
        m_clientBuilder.register(component);
        return this;
    }

    @Override
    public AmdatuClientBuilder register(Class<?> componentClass, int priority) {
        m_clientBuilder.register(componentClass, priority);
        return this;
    }

    @Override
    public AmdatuClientBuilder register(Class<?> componentClass, Class<?>... contracts) {
        m_clientBuilder.register(componentClass, contracts);
        return this;
    }

    @Override
    public AmdatuClientBuilder register(Class<?> componentClass, Map<Class<?>, Integer> contracts) {
        m_clientBuilder.register(componentClass, contracts);
        return this;
    }

    @Override
    public AmdatuClientBuilder register(Object component, int priority) {
        m_clientBuilder.register(component, priority);
        return this;
    }

    @Override
    public AmdatuClientBuilder register(Object component, Class<?>... contracts) {
        m_clientBuilder.register(component, contracts);
        return this;
    }

    @Override
    public AmdatuClientBuilder register(Object component, Map<Class<?>, Integer> contracts) {
        m_clientBuilder.register(component, contracts);
        return this;
    }

    @Override
    public AmdatuClient build() {
        ResteasyClient build = m_clientBuilder.build();
        return new AmdatuClientImpl(build);
    }

    @Override
    public AmdatuClientBuilder hostnameVerifier(HostnameVerifier verifier) {
        m_clientBuilder.hostnameVerifier(verifier);
        return this;
    }

    @Override
    public AmdatuClientBuilder keyStore(KeyStore keystore, char[] password) {
        m_clientBuilder.keyStore(keystore, password);
        return this;
    }

    @Override
    public AmdatuClientBuilder sslContext(SSLContext sslContext) {
        m_clientBuilder.sslContext(sslContext);
        return this;
    }

    @Override
    public AmdatuClientBuilder trustStore(KeyStore trustStore) {
        m_clientBuilder.trustStore(trustStore);
        return this;
    }

    @Override
    public AmdatuClientBuilder withConfig(Configuration config) {
        m_clientBuilder.withConfig(config);
        return this;
    }

}
