/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.resteasy.client;

import java.net.URI;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.core.Configuration;
import javax.ws.rs.core.Link;
import javax.ws.rs.core.UriBuilder;

import org.amdatu.web.rest.jaxrs.client.AmdatuClient;
import org.amdatu.web.rest.jaxrs.client.AmdatuWebTarget;
import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;

public class AmdatuClientImpl implements AmdatuClient {

    private final ResteasyClient m_client;

    public AmdatuClientImpl(ResteasyClient client) {
        m_client = client;
    }

    @Override
    public void close() {
        m_client.close();
    }

    @Override
    public Configuration getConfiguration() {
        return m_client.getConfiguration();
    }

    @Override
    public HostnameVerifier getHostnameVerifier() {
        return m_client.getHostnameVerifier();
    }

    @Override
    public SSLContext getSslContext() {
        return m_client.getSslContext();
    }

    @Override
    public Builder invocation(Link link) {
        return m_client.invocation(link);
    }

    @Override
    public Client property(String name, Object value) {
        return m_client.property(name, value);
    }

    @Override
    public Client register(Class<?> componentClass, Class<?>... contracts) {
        return m_client.register(componentClass, contracts);
    }

    @Override
    public Client register(Class<?> componentClass, int priority) {
        return m_client.register(componentClass, priority);
    }

    @Override
    public Client register(Class<?> componentClass, Map<Class<?>, Integer> contracts) {
        return m_client.register(componentClass, contracts);
    }

    @Override
    public Client register(Class<?> componentClass) {
        return m_client.register(componentClass);
    }

    @Override
    public Client register(Object component, Class<?>... contracts) {
        return m_client.register(component, contracts);
    }

    @Override
    public Client register(Object component, int priority) {
        return m_client.register(component, priority);
    }

    @Override
    public Client register(Object component, Map<Class<?>, Integer> contracts) {
        return m_client.register(component, contracts);
    }

    @Override
    public Client register(Object component) {
        return m_client.register(component);
    }

    @Override
    public AmdatuWebTarget target(Link link) {
        ResteasyWebTarget target = m_client.target(link);
        return new AmdatuWebTargetImpl(target);
    }

    @Override
    public AmdatuWebTarget target(String uri) {
        ResteasyWebTarget target = m_client.target(uri);
        return new AmdatuWebTargetImpl(target);
    }

    @Override
    public AmdatuWebTarget target(URI uri) {
        ResteasyWebTarget target = m_client.target(uri);
        return new AmdatuWebTargetImpl(target);
    }

    @Override
    public AmdatuWebTarget target(UriBuilder uriBuilder) {
        ResteasyWebTarget target = m_client.target(uriBuilder);
        return new AmdatuWebTargetImpl(target);
    }

}
