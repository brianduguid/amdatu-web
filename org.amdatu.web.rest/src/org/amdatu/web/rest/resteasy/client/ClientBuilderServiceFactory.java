/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.resteasy.client;

import javax.ws.rs.client.ClientBuilder;

import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.jboss.resteasy.client.jaxrs.internal.LocalResteasyProviderFactory;
import org.jboss.resteasy.plugins.providers.RegisterBuiltin;
import org.jboss.resteasy.spi.ResteasyProviderFactory;
import org.osgi.framework.Bundle;
import org.osgi.framework.PrototypeServiceFactory;
import org.osgi.framework.ServiceRegistration;

public class ClientBuilderServiceFactory extends ResteasyClientBuilder
    implements PrototypeServiceFactory<ClientBuilder> {

    @Override
    public ClientBuilder getService(Bundle bundle, ServiceRegistration<ClientBuilder> registration) {
        ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
        try {
            // set context class loader for RegisterBuiltin.register service loading to work
            Thread.currentThread().setContextClassLoader(getClass().getClassLoader());

            LocalResteasyProviderFactory resteasyProviderFactory = new LocalResteasyProviderFactory(ResteasyProviderFactory.newInstance());
            RegisterBuiltin.register(resteasyProviderFactory);

            ResteasyClientBuilder resteasyClientBuilder = new ResteasyClientBuilder()
                            .providerFactory(resteasyProviderFactory);
            return resteasyClientBuilder;
        } finally {
            Thread.currentThread().setContextClassLoader(contextClassLoader);
        }
    }

    @Override
    public void ungetService(Bundle bundle, ServiceRegistration<ClientBuilder> registration, ClientBuilder service) {
        // no cleanup required
    }
}
