/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.resteasy.client;

import java.net.URI;
import java.util.Map;

import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Configuration;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

import org.amdatu.web.rest.jaxrs.client.AmdatuWebTarget;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;

public class AmdatuWebTargetImpl implements AmdatuWebTarget {

    private final ResteasyWebTarget m_target;

    static class ProxyBuilderImpl<T> implements ProxyBuilder<T> {

        org.jboss.resteasy.client.jaxrs.ProxyBuilder<T> m_proxyBuilder;

        public ProxyBuilderImpl(Class<T> proxyInterface, org.jboss.resteasy.client.jaxrs.ProxyBuilder<T> proxyBuilder) {
            m_proxyBuilder = proxyBuilder;

            ClassLoader classLoader = new ClassLoader(getClass().getClassLoader()) {

                @Override
                public Class<?> loadClass(String name) throws ClassNotFoundException {
                    try {
                        return super.loadClass(name);
                    } catch (ClassNotFoundException e) {
                        return proxyInterface.getClassLoader().loadClass(name);
                    }

                }

            };
            proxyBuilder.classloader(classLoader);
        }

        @Override
        public ProxyBuilder<T> classloader(ClassLoader cl) {
            m_proxyBuilder.classloader(cl);
            return this;
        }

        @Override
        public ProxyBuilder<T> defaultProduces(MediaType type) {
            m_proxyBuilder.defaultProduces(type);
            return this;
        }

        @Override
        public ProxyBuilder<T> defaultConsumes(MediaType type) {
            m_proxyBuilder.defaultConsumes(type);
            return this;
        }

        @Override
        public ProxyBuilder<T> defaultProduces(String type) {
            m_proxyBuilder.defaultProduces(type);
            return this;
        }

        @Override
        public ProxyBuilder<T> defaultConsumes(String type) {
            m_proxyBuilder.defaultConsumes(type);
            return this;
        }

        @Override
        public T build() {
            return m_proxyBuilder.build();
        }

    }

    public AmdatuWebTargetImpl(ResteasyWebTarget resteasyWebTarget) {
        m_target = resteasyWebTarget;
    }

    @Override
    public <T> T proxy(Class<T> proxyInterface) {
        // Always use the builder as that also manages the classloader
        return proxyBuilder(proxyInterface).build();
    }

    @Override
    public <T> ProxyBuilder<T> proxyBuilder(Class<T> proxyInterface) {
        return new ProxyBuilderImpl<T>(proxyInterface, m_target.proxyBuilder(proxyInterface));
    }

    @Override
    public Configuration getConfiguration() {
        return m_target.getConfiguration();
    }

    @Override
    public URI getUri() {
        return m_target.getUri();
    }

    @Override
    public UriBuilder getUriBuilder() {
        return m_target.getUriBuilder();
    }

    @Override
    public WebTarget matrixParam(String name, Object... values) {
        return m_target.matrixParam(name, values);
    }

    @Override
    public WebTarget path(String path) {
        return m_target.path(path);
    }

    @Override
    public WebTarget property(String name, Object value) {
        return m_target.property(name, value);
    }

    @Override
    public WebTarget queryParam(String name, Object... values) {
        return m_target.queryParam(name, values);
    }

    @Override
    public WebTarget register(Class<?> componentClass, Class<?>... contracts) {
        return m_target.register(componentClass, contracts);
    }

    @Override
    public WebTarget register(Class<?> componentClass, int priority) {
        return m_target.register(componentClass, priority);
    }

    @Override
    public WebTarget register(Class<?> componentClass, Map<Class<?>, Integer> contracts) {
        return m_target.register(componentClass, contracts);
    }

    @Override
    public WebTarget register(Class<?> componentClass) {
        return m_target.register(componentClass);
    }

    @Override
    public WebTarget register(Object component, Class<?>... contracts) {
        return m_target.register(component, contracts);
    }

    @Override
    public WebTarget register(Object component, int priority) {
        return m_target.register(component, priority);
    }

    @Override
    public WebTarget register(Object component, Map<Class<?>, Integer> contracts) {
        return m_target.register(component, contracts);
    }

    @Override
    public WebTarget register(Object component) {
        return m_target.register(component);
    }

    @Override
    public Builder request() {
        return m_target.request();
    }

    @Override
    public Builder request(MediaType... acceptedResponseTypes) {
        return m_target.request(acceptedResponseTypes);
    }

    @Override
    public Builder request(String... acceptedResponseTypes) {
        return m_target.request(acceptedResponseTypes);
    }

    @Override
    public WebTarget resolveTemplate(String name, Object value, boolean encodeSlashInPath) {
        return m_target.resolveTemplate(name, value, encodeSlashInPath);
    }

    @Override
    public WebTarget resolveTemplate(String name, Object value) {
        return m_target.resolveTemplate(name, value);
    }

    @Override
    public WebTarget resolveTemplateFromEncoded(String name, Object value) {
        return m_target.resolveTemplateFromEncoded(name, value);
    }

    @Override
    public WebTarget resolveTemplates(Map<String, Object> templateValues, boolean encodeSlashInPath) {
        return m_target.resolveTemplates(templateValues, encodeSlashInPath);
    }

    @Override
    public WebTarget resolveTemplates(Map<String, Object> templateValues) {
        return m_target.resolveTemplates(templateValues);
    }

    @Override
    public WebTarget resolveTemplatesFromEncoded(Map<String, Object> templateValues) {
        return m_target.resolveTemplatesFromEncoded(templateValues);
    }

}
