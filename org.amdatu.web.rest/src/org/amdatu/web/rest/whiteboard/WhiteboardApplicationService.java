/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.whiteboard;

import static org.amdatu.web.rest.jaxrs.AmdatuWebRestConstants.JAX_RS_FILTER_BASE;
import static org.amdatu.web.rest.jaxrs.AmdatuWebRestConstants.JAX_RS_INTERCEPTOR_BASE;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Application;
import javax.ws.rs.ext.ReaderInterceptor;
import javax.ws.rs.ext.WriterInterceptor;

import org.amdatu.web.rest.jaxrs.AmdatuWebRestConstants;
import org.amdatu.web.rest.spi.ApplicationService;
import org.osgi.framework.ServiceReference;

/**
 * {@link ApplicationService} implementation for {@link AmdatuWebRestConstants#JAX_RS_APPLICATION_TYPE_STATIC}
 *
 * This {@link ApplicationService} tracks
 * <ul>
 *  <li>{@link ContainerRequestFilter} services that are registered with a {@link AmdatuWebRestConstants#JAX_RS_FILTER_BASE} property</li>
 *  <li>{@link WriterInterceptor} and {@link ReaderInterceptor} services that are registered with a {@link AmdatuWebRestConstants#JAX_RS_INTERCEPTOR_BASE} property</li>
 * </ul>
 *
 * For both interceptors and filters the base must match the base of the application (application base starts with ...)
 *
 */
public class WhiteboardApplicationService extends AbstractApplicationService {

    private List<Object> m_resources;

    public WhiteboardApplicationService(String base, String context, String name, Application application) {
        super(base, context, name, application);
    }

    public WhiteboardApplicationService(String base, String context, String name, List<Object> resources) {
        super(base, context, name, null);
        m_resources = resources;
    }

    @Override
    public String getFilter() {
        return String.format("(|(%s=*)(%s=*))", JAX_RS_FILTER_BASE, JAX_RS_INTERCEPTOR_BASE);
    }

    @Override
    protected boolean acceptJaxRsService(ServiceReference<Object> serviceReference, Object service) {
        Class<? extends Object> serviceClass = service.getClass();
        if (isMatchingBase(JAX_RS_FILTER_BASE, serviceReference)
                        && ContainerRequestFilter.class.isAssignableFrom(serviceClass)) {
            return true;

        }
        if (isMatchingBase(JAX_RS_INTERCEPTOR_BASE, serviceReference)
            && (ReaderInterceptor.class.isAssignableFrom(serviceClass)
                || WriterInterceptor.class.isAssignableFrom(serviceClass))) {
            return true;
        }

        return false;
    }

    private boolean isMatchingBase(String propertyKey, ServiceReference<Object> serviceReference) {
        Object property = serviceReference.getProperty(propertyKey);
        if ((property instanceof String) && getBaseUri().startsWith((String)property)) {
            return true;
        }
        return false;
    }

    @Override
    public Object[] getSingletons() {
        if (m_resources == null) {
            return super.getSingletons();
        }
        ArrayList<Object> list = new ArrayList<>(m_resources);
        list.addAll(Arrays.asList(super.getSingletons()));

        return list.toArray();
    }

}
