/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.whiteboard;

import static org.amdatu.web.rest.jaxrs.AmdatuWebRestConstants.JAX_RS_APPLICATION_NAME;

import javax.ws.rs.Path;
import javax.ws.rs.core.Application;
import javax.ws.rs.ext.Provider;

import org.amdatu.web.rest.jaxrs.AmdatuWebRestConstants;
import org.amdatu.web.rest.spi.ApplicationService;
import org.osgi.framework.ServiceReference;

/**
 * {@link ApplicationService} implementation for {@link AmdatuWebRestConstants#JAX_RS_APPLICATION_TYPE_DYNAMIC}
 * 
 * This {@link ApplicationService} tracks all resources and providers that are registered with a 
 * {@link AmdatuWebRestConstants#JAX_RS_APPLICATION_NAME} property that matches the name of the application instance.
 *
 */
public class DynamicApplicationService extends AbstractApplicationService {

    public DynamicApplicationService(String base, String context, String name, Application application) {
        super(base, context, name, application);
    }

    @Override
    public String getFilter() {
        String targetingThisApplication = String.format("(%s=%s)", JAX_RS_APPLICATION_NAME, getName());
        return targetingThisApplication;
    }

    @Override
    protected boolean acceptJaxRsService(ServiceReference<Object> serviceReference, Object service) {
        Class<? extends Object> serviceClass = service.getClass();
        return serviceClass.isAnnotationPresent(Provider.class) || serviceClass.isAnnotationPresent(Path.class);
    }

}
