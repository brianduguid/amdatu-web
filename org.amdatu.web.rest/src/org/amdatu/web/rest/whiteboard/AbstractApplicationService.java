/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.whiteboard;

import static org.osgi.service.http.whiteboard.HttpWhiteboardConstants.HTTP_WHITEBOARD_CONTEXT_PATH;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import javax.ws.rs.Path;
import javax.ws.rs.core.Application;

import org.amdatu.web.rest.spi.ApplicationService;
import org.apache.felix.dm.Component;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.ServiceReference;
import org.osgi.service.http.context.ServletContextHelper;

/**
 * Base class for all {@link ApplicationService} implementations, this base class adds an additional
 * service dependency for tracking JAX-RS providers and resources.
 *
 */
public abstract class AbstractApplicationService implements ApplicationService {

    private final ConcurrentHashMap<ServiceReference<?>, Object> m_jaxRsResources;

    private volatile Component m_component;

    private final Optional<Application> m_application;

    private final String m_base;
    private final String m_context;
    private final String m_name;

    private volatile String m_contextPath;

    public AbstractApplicationService(String base, String context, String name, Application application) {
        m_base = base;
        m_context = context;
        m_name = name;
        m_application = Optional.ofNullable(application);
        m_jaxRsResources = new ConcurrentHashMap<>();
    }

    @Override
    public Optional<Application> getApplication() {
        return m_application;
    }

    @Override
    public Object[] getSingletons() {
        List<Object> singletons = new ArrayList<>();

        singletons.addAll(m_jaxRsResources.values());

        return singletons.toArray();
    }

    public abstract String getFilter();

    protected final void init(Component component) {
        String filter = getFilter();

        // @formatter:off
        DependencyManager dm = component.getDependencyManager();
        component.add(dm.createServiceDependency()
            .setService(filter)
            .setRequired(false)
            .setCallbacks("onAdded", "onRemoved"));
        // @formatter:on
    }

    protected abstract boolean acceptJaxRsService(ServiceReference<Object> serviceReference, Object service);

    /**
     * Called by Felix DM for each service registration.
     */
    protected final void onAdded(ServiceReference<Object> serviceReference, Object service) {
        if (acceptJaxRsService(serviceReference, service)) {
            m_jaxRsResources.putIfAbsent(serviceReference, service);
            updateServiceProperties();
        }
    }

    /**
     * Called by Felix DM for each service deregistration.
     */
    protected final void onRemoved(ServiceReference<Object> serviceReference, Object service) {
        if (m_jaxRsResources.remove(serviceReference, service)) {
            updateServiceProperties();
        }
    }

    private void updateServiceProperties() {
        Dictionary<Object,Object> properties = m_component.getServiceProperties();
        if (properties == null) {
            properties = new Hashtable<>();
        }

        properties.put("rootResourceEndpoints", getRootResourceEndpoints());
        m_component.setServiceProperties(properties);
    }

    private List<String> getRootResourceEndpoints() {
        return Arrays.stream(getSingletons()).filter(s -> s.getClass().isAnnotationPresent(Path.class))
            .map(s -> ((Path) s.getClass().getAnnotation(Path.class)).value()).collect(Collectors.toList());
    }


    /**
     * Called by Felix DM when the ServletContextHelper service is added
     */
    protected final void onContextAdded(ServiceReference<ServletContextHelper> ref) {
        m_contextPath = (String) ref.getProperty(HTTP_WHITEBOARD_CONTEXT_PATH);
    }

    /**
     * Called by Felix DM when the ServletContextHelper service is changed
     */
    protected final void onContextChanged(ServiceReference<ServletContextHelper> ref) {
        m_contextPath = (String) ref.getProperty(HTTP_WHITEBOARD_CONTEXT_PATH);
    }

    /**
     * Called by Felix DM when the ServletContextHelper service is removed
     */
    protected final void onContextRemoved(ServiceReference<ServletContextHelper> ref) {
        m_contextPath = null;
    }

    @Override
    public String getName() {
        return m_name;
    }

    @Override
    public String getBaseUri() {
        return m_base;
    }

    @Override
    public String getContextName() {
        return m_context;
    }

    @Override
    public String getContextPath() {
        return m_contextPath;
    }


}
