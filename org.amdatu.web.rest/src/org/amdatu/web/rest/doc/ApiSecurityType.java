/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.doc;

/**
 * <p>Enum defining the possible security scheme types.</p>
 *
 * <p>See <a href="https://swagger.io/docs/specification/authentication/">specification</a>.</p>
 *
 * @see ApiSecurityDefinition
 *
 * @since 1.0
 */
public enum ApiSecurityType {

	/**
	 * <p>Basic http security scheme.</p>
	 *
	 * <p>See <a href="https://swagger.io/docs/specification/authentication/basic-authentication/">specification</a>.</p>
	 */
	BASIC("http"),

	/**
	 * <p>Bearer http security scheme.</p>
	 *
	 * <p>See <a href="https://swagger.io/docs/specification/authentication/bearer-authentication/">specification</a>.</p>
	 */
	BEARER("http"),

	/**
	 * <p>API Key (or cookie) security scheme.</p>
	 *
	 * <p>See <a href="https://swagger.io/docs/specification/authentication/api-keys/">specification</a>.</p>
	 */
	API_KEY("apiKey"),

	/**
	 * <p>OpenID Connect (OIDC) security scheme.</p>
	 *
	 * <p>See <a href="https://swagger.io/docs/specification/authentication/openid-connect-discovery/">specification</a>.</p>
	 */
	OPEN_ID_CONNECT("openIdConnect"),

	/**
	 * <p>OAuth 2.0 security scheme.</p>
	 *
	 * <p>Currently, not fully configuration per specifications.</p>
	 *
	 * <p>See <a href="https://swagger.io/docs/specification/authentication/oauth2/">specification</a>.</p>
	 */
	OAUTH2("oauth2");

	/**
	 * Security type id used in the OpenAPI documentation.
	 */
	private String typeId;

	/**
	 * Constructs a new ApiSecurityType enum with the specified security type id.
	 *
	 * @param typeId OpenAPI documentation security type id
	 */
	private ApiSecurityType(String typeId) {
		this.typeId = typeId;
	}

	/**
	 * Returns the security type id for this security type used in the OpenAPI documentation.
	 *
	 * @return security type id used in the OpenAPI documentation
	 */
	public String getTypeId() {
		return typeId;
	}
}
