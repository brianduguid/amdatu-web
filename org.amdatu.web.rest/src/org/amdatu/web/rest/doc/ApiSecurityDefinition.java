/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.doc;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * <p>Defines a security scheme that is documented in the <code>securitySchemes</code> section of <code>components</code>.</p>
 *
 * <p>One or more security definitions are in a <code>@ApiSecurityDefinitions</code>.</p>
 *
 * <pre>
 *   &#064;ApiSecurityDefinition(name = "basicAuth", type = BASIC)
 *   &#064;ApiSecurityDefinition(name = "bearerAuth", type = BEARER)
 *   &#064;ApiSecurityDefinition(name = "apiKeyAuth", type = API_KEY,
 *                                properties = {
 *                                   &#064;ApiSecurityProperty(key = "in", value = "header"),
 *                                   &#064;ApiSecurityProperty(key = "name", value = "Authorization")
 *                                }
 *                              )
 *   &#064;ApiSecurityDefinition(name = "openIdAuth", type = OPEN_ID_CONNECT,
 *                                properties = {
 *                                   &#064;ApiSecurityProperty(key = "openIdConnectUrl", value = "https://example.com/.well-known/openid-configuration")
 *                                }
 *                              )
 *    public class HelloWorldResource {
 *       ...
 *    }
 * </pre>
 *
 * <p>See <a href="https://swagger.io/docs/specification/authentication/">specification</a>.</p>
 *
 * @see ApiSecurityDefinitions
 * @see ApiSecurityProperty
 * @see ApiSecurityRequirements
 *
 * @since 1.0
 */
@Documented
@Repeatable(ApiSecurityDefinitions.class)
@Retention(RUNTIME)
@Target({TYPE, ANNOTATION_TYPE})
public @interface ApiSecurityDefinition {

	/**
	 * Arbitrary security scheme name used to reference in the global components
	 * and operation security requirements.
	 *
	 * @return security scheme name
	 */
	public String name();

	/**
	 * The security type for this definition.
	 *
	 * @return definition's security type
	 * @see ApiSecurityType
	 */
	public ApiSecurityType type();

	/**
	 * Optional properties if the security type requires more information.
	 *
	 * @return optional security type properties
	 */
	public ApiSecurityProperty[] properties() default {};
}
