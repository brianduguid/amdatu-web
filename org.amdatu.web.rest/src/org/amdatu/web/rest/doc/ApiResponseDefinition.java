/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.doc;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * <p>Defines a response that is documented in the <code>responses</code> section of <code>components</code>.</p>
 *
 * <p><code>@ApiResponseDefinitions</code> is repeatable.  The response name is referenced in the
 * <code>@ApiResponseRefs</code> annotations.  Headers can be included using the names of the header definitions.</p>
 *
 * <pre>
 *    &#064;ApiResponseDefinition(name = "400_BadRequest", code = 400, description = "Bad Request", headerNames = "x-error-detail"),
 *    &#064;ApiResponseDefinition(name = "500_Error", code = 500, description = "Internal Server Error")
 * </pre>
 *
 * @see ApiResponseRefs
 * @see ApiResponseDefinitions
 * @see ApiHeaderDefinition
 *
 * @since 1.0
 */
@Documented
@Repeatable(ApiResponseDefinitions.class)
@Retention(RUNTIME)
@Target({TYPE, ANNOTATION_TYPE})
public @interface ApiResponseDefinition {

	/**
	 * Name used to reference the defined response.
	 *
	 * @return reference name
	 */
	public String name();

	/**
	 * Response HTTP status code.
	 *
	 * @return HTTP status code
	 */
	public int code();

	/**
	 * Description of the response. It should correspond with the HTTP status code.
	 *
	 * @return response description
	 */
	public String description();

	/**
	 * One or more media types produced in the response if the response
	 * has a return type.  If the produces is not defined and the return type
	 * has a value, "application/json" is used as default.
	 *
	 * @return one or more media types produced in the response
	 *
	 * @see javax.ws.rs.core.MediaType
	 */
	public String[] produces() default {};

	/**
	 * Optional type returned as the response value.
	 *
	 * @return response return type
	 */
	public Class<?> returnType() default void.class;

	/**
	 * Optional type of the return type's element, if the return type is a list, map, or dictionary.
	 *
	 * @return element type of the list, map, or dictionary value type
	 */
	public Class<?> elementType() default void.class;

	/**
	 * Optional header names of predefined header definitions.
	 *
	 * @return predefined header names
	 */
	public String[] headerNames() default {};

	/**
	 * Optional header array expected with the response.
	 *
	 * @return header array
	 */
	public ApiHeader[] headers() default {};
}
