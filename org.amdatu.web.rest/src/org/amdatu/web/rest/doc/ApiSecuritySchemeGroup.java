/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.doc;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * <p>Defines a group of security schemes that all must be satisfied before authentication is granted.</p>
 *
 * <p>The following example sets the default security requirement for all
 * methods in this resource since it is annotated on a type and not a method.</p>
 *
 * <p>Using <code>@ApiSecuritySchemeGroup</code> will require that all security schemes
 * within the group must be satisfied.</p>
 *
 * <p>The security requirements in the following block are:
 * <ul>
 * 	<li><code>apiKeyAuth</code>, OR</li>
 *  <li><code>bearerAuth</code> AND <code>apiKeyAuth</code></li>
 * </ul>
 *
 * <pre>
 *    &#064;ApiSecurityRequirements(
 *              schemes ={ &#064;ApiSecurityScheme(name = "apiKeyAuth") },
 *              groups = {
 *                         &#064;ApiSecuritySchemeGroup({
 *                               &#064;ApiSecurityScheme(name = "bearerAuth"),
 *                               &#064;ApiSecurityScheme(name = "apiKeyAuth")
 *                         })
 *                        },
 *    )
 *    public class HelloWorldResource {
 *       ...
 *    }
 * </pre>
 *
 * <p>See <a href="https://swagger.io/docs/specification/authentication/">specification</a>.</p>
 *
 * @see ApiSecurityRequirements
 * @see ApiSecurityScheme
 * @see ApiSecurityDefinition
 *
 * @since 1.0
 */
@Retention(RUNTIME)
@Target(ANNOTATION_TYPE)
public @interface ApiSecuritySchemeGroup {

	/**
	 * Two or more security schemes to group in a AND requirement.
	 *
	 * @return security schemes
	 */
	public ApiSecurityScheme[] value();
}
