/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.doc;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * <p>Documents the details of a header that may be a part of a response. One or more headers
 * can be in a single <code>@ApiResponse</code>.</p>
 *
 * <p>Example:
 * <pre>
 *    &#064;ApiResponse(code = 200, description = "OK", returnType = String.class,
 *                    headers = { &#064;ApiHeader(name = "Link", description = "Link to more.", valueType = String.class) }
 *                   ),
 *    &#064;ApiResponse(code = 500, description = "Internal Server Error")
 *    public Response helloResponse() {
 *       ...
 *    }
 * </pre>
 *
 * @see ApiResponse
 * @see ApiResponses
 *
 * @since 1.0
 */
@Documented
@Retention(RUNTIME)
@Target(ANNOTATION_TYPE)
public @interface ApiHeader {

	/**
	 * Name of the header. For example, <code>x-total-count</code>.
	 *
	 * @return header name
	 */
	public String name();

	/**
	 * Optional header description.
	 *
	 * @return header description
	 */
	public String description() default "";

	/**
	 * Type returned as the header value.
	 *
	 * @return header value type
	 */
	public Class<?> valueType();

	/**
	 * Optional type of the value type's element, if the value type is a list, map, or dictionary.
	 *
	 * @return element type of the list, map, or dictionary value type
	 */
	public Class<?> elementType() default void.class;
}
