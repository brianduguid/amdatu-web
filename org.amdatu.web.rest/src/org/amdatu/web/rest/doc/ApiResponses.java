/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.doc;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * <p>Applying <code>@ApiResponse</code> on the class is recommended; however, using the container
 * <code>@ApiResponses</code> is acceptable.</p>
 *
 * <p>Documents the details of one or more responses defined by <code>@ApiResponse</code> for the method.</p>
 *
 * <p>The <code>@ApiResponse</code> annotation is repeatable, so the <code>@ApiResponses</code> does not need to be explicitly used;
 * however, the following format is acceptable.</p>
 *
 * <p>Example:</p>
 * <pre>
 *    &#064;ApiResponses({
 *       &#064;ApiResponse(code = 200, description = "OK", returnType = String.class,
 *                    headers = { &#064;ApiHeader(name = "Link", description = "Link to more.", valueType = String.class) }
 *                   ),
 *       &#064;ApiResponse(code = 500, description = "Internal Server Error")
 *    })
 *    public Response helloResponse() {
 *       ...
 *    }
 * </pre>
 *
 * @see ApiResponse
 * @see ApiHeader
 * @see ApiResponseDefinitions
 * @see ApiResponseDefinition
 * @see ApiHeaderDefinitions
 * @see ApiHeaderDefinition
 *
 * @since 1.0
 */
@Documented
@Retention(RUNTIME)
@Target(METHOD)
public @interface ApiResponses {

	/**
	 * One or more responses return by the method response.
	 *
	 * @return response array
	 */
	public ApiResponse[] value();
}
