/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.doc;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * <p>Defines security scheme to be used as a security requirement for authentication.</p>
 *
 * <p>The following example sets the default security requirement for all
 * methods in this resource since it is annotated on a type and not a method.</p>
 *
 * <p>Using <code>@ApiSecuritySchemeGroup</code> will require that all security schemes
 * within the group must be satisfied.</p>
 *
 * <p>The following code block requires a single security scheme:
 *
 * <pre>
 *    &#064;ApiSecurityRequirements(&#064;ApiSecurityScheme("apiKeyAuth"))
 *    public class HelloWorldResource {
 *       ...
 *    }
 * </pre>
 *
 * <p>The security requirements in the following block are:
 * <ul>
 * 	<li><code>apiKeyAuth</code>, OR</li>
 *  <li><code>bearerAuth</code> AND <code>apiKeyAuth</code></li>
 * </ul>
 *
 * <pre>
 *    &#064;ApiSecurityRequirements(
 *              value ={ &#064;ApiSecurityScheme("apiKeyAuth") },
 *              groups = {
 *                         &#064;ApiSecuritySchemeGroup({
 *                               &#064;ApiSecurityScheme("bearerAuth"),
 *                               &#064;ApiSecurityScheme("apiKeyAuth")
 *                         })
 *                        },
 *    )
 *    public class HelloWorldResource {
 *       ...
 *    }
 * </pre>
 *
 * <p>See <a href="https://swagger.io/docs/specification/authentication/">specification</a>.</p>
 *
 * @see ApiSecurityRequirements
 * @see ApiSecuritySchemeGroup
 * @see ApiSecurityDefinition
 *
 * @since 1.0
 */
@Documented
@Retention(RUNTIME)
@Target(ANNOTATION_TYPE)
public @interface ApiSecurityScheme {

	/**
	 * Security scheme name defined by a security definition.
	 *
	 * @return security scheme name
	 */
	public String value();

	/**
	 * Optional array of scopes used to further define authorization.
	 *
	 * @return array of scopes
	 */
	public String[] scopes() default {};
}
