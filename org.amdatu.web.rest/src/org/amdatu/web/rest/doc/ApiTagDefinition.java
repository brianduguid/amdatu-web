/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.doc;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * <p>Documents the name and description of a tag to be used to organize operations.
 * An operation can have multiple tags and will appear under all of the tags it is
 * associated with.</p>
 *
 * <p>The recommend use is to place the <code>@ApiTagDefinition</code> within a
 * <code>@ApiTagDefinitions</code> annotation. This will document the tag for later use
 * and can be later referenced using the <code>@ApiTags</code> annotation.</p>
 *
 * <p>If the <code>@ApiTagDefinition</code> is annotated directly on a resource, then all operations
 * in the resource will be associated with this tag automatically. The tag definition can also be
 * referenced by other resources and operations using the <code>@ApiTags</code> annotation.</p>
 *
 * <p>The <code>@ApiTagDefinition</code> can also be annotated directly on a method which defines
 * the tag and tags the operation. The tag definition can also be referenced by other resources
 * and operations using the <code>@ApiTags</code> annotation.</p>
 *
 * <p>The following example defines two tags with  <code>@ApiTagDefinition</code> annotation
 * and then tags the resource with the newly defined tags. This will tag all of the operations
 * in the resource with the two new tags.</p>
 * <pre>
 *    &#064;ApiTagDefinition(name = "dynamic", description = "Dynamic REST resource using Component"),
 *    &#064;ApiTagDefinition(name = "text/plain", description = "Produces plain text ")
 *    &#064;Path("hello")
 *    &#064;ApiTags({"dynamic", "text/plain"})
 *    public class DynamicApplicationResource {
 *       ...
 *    }
 * </pre>
 *
 * @see ApiTags
 * @see ApiTagDefinitions
 *
 * @since 1.0
 */
@Documented
@Repeatable(ApiTagDefinitions.class)
@Retention(RUNTIME)
@Target({TYPE, METHOD, ANNOTATION_TYPE})
public @interface ApiTagDefinition {

	/**
	 * A unique name of the tag. Uniqueness is not enforced.
	 *
	 * @return tag name
	 */
	public String name();

	/**
	 * Description of the tag.
	 *
	 * @return tag description
	 */
	public String description();
}
