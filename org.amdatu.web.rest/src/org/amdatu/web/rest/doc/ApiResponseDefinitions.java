/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.doc;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * <p>Applying <code>@ApiResponseDefinition</code> on the class is recommended; however, using the container
 * <code>@ApiResponseDefinitions</code> is acceptable.</p>
 *
 * <p>Contains one or more response definitions that are documented in the <code>responses</code> section of <code>components</code>.</p>
 *
 * <pre>
 *    &#064;ApiResponseDefinitions({
 *       &#064;ApiResponseDefinition(name = "400_BadRequest", code = 400, description = "Bad Request", headerNames = "x-error-detail"),
 *       &#064;ApiResponseDefinition(name = "500_Error", code = 500, description = "Internal Server Error")
 *    })
 * </pre>
 *
 * @see ApiResponseRefs
 * @see ApiResponseDefinition
 * @see ApiHeaderDefinition
 *
 * @since 1.0
 */
@Documented
@Retention(RUNTIME)
@Target(TYPE)
public @interface ApiResponseDefinitions {

	/**
	 * One or more response definitions.
	 *
	 * @return response definitions
	 */
	public ApiResponseDefinition[] value();
}
