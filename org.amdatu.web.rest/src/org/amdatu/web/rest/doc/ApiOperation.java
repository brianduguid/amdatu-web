/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.doc;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * <p>Documents the operation (REST method) with a summary and optional description and operation id.</p>
 *
 * <p>Summary only:</p>
 * <pre>
 *    &#064;GET
 *    &#064;ApiOperation("Will return a friendly message.")
 *    public String sendHello() {
 *       ...
 *    }
 * </pre>
 *
 * <p>Summary and description:</p>
 * <pre>
 *    &#064;GET
 *    &#064;ApiOperation(
 *               value = "Will return a friendly message.",
 *               description = "The response will always be a 200 with a plan text `hello world` response."
 *               )
 *    public String sendHello() {
 *       ...
 *    }
 * </pre>
 *
 * @since 1.0
 */
@Documented
@Retention(RUNTIME)
@Target(METHOD)
public @interface ApiOperation {

	/**
	 * Short summary stating the purpose of the operation.
	 *
	 * @return short summary
	 */
	public String value();

	/**
	 * <p>Optional description of the operation.
	 *
	 * <p>See <a href="http://commonmark.org/help/">markdown</a>.</p>
	 *
	 * @return operation description
	 */
	public String description() default "";

	/**
	 * Optional unique operation id. Uniqueness is not enforced by the generator.
	 *
	 * @return unique operation id
	 */
	public String operationId() default "";
}
