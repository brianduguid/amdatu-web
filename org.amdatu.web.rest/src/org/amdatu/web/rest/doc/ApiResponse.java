/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.doc;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * <p>Documents the details of a potential response. The <code>@ApiResponse</code> is repeatable.</p>
 *
 * <p>Headers can be predefined. The header definition uses the <code>@ApiHeaderDefinition</code> annotation.
 * Predefined headers can be referenced using the headerNames property.</p>
 *
 * <p>Example:
 * <pre>
 *    &#064;ApiResponse(code = 200, description = "OK", returnType = String.class,
 *    	  	headerNames = { "x-total-count" },
 *        	headers = {
 *              &#064;ApiHeader(name = "Link", description = "Link to more.", valueType = String.class)
 *            }
 *    )
 *    &#064;ApiResponse(code = 500, description = "Internal Server Error")
 *    public Response helloResponse() {
 *       ...
 *    }
 * </pre>
 *
 * @see ApiHeader
 * @see ApiResponses
 * @see ApiHeaderDefinition
 *
 * @since 1.0
 */
@Documented
@Repeatable(ApiResponses.class)
@Retention(RUNTIME)
@Target({METHOD, ANNOTATION_TYPE})
public @interface ApiResponse {

	/**
	 * Response HTTP status code.
	 *
	 * @return HTTP status code
	 */
	public int code();

	/**
	 * Description of the response. It should correspond with the HTTP status code.
	 *
	 * @return response description
	 */
	public String description();

	/**
	 * Optional type returned as the response value.
	 *
	 * @return response return type
	 */
	public Class<?> returnType() default void.class;

	/**
	 * Optional type of the return type's element, if the return type is a list, map, or dictionary.
	 *
	 * @return element type of the list, map, or dictionary value type
	 */
	public Class<?> elementType() default void.class;

	/**
	 * Optional header names of predefined header definitions.
	 *
	 * @return predefined header names
	 */
	public String[] headerNames() default {};

	/**
	 * Optional header array expected with the response.
	 *
	 * @return header array
	 */
	public ApiHeader[] headers() default {};
}
