/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.doc;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * <p>Documents one or more tags the resource or operation should be associated with.</p>
 *
 * <p>It is recommended to have the tag defined using <code>@ApiTagDefinition</code>, but it
 * is not required.  If a tag does not have a definition, a description will not be available.</p>
 *
 * <p>The following operation method has been associated with two tags and the operation will
 * appear under both tags on the UI.</p>
 * <pre>
 *    &#064;GET
 *    &#064;Path("response-with-headers")
 *    &#064;ApiTags({ "json", "hello" })
 *    public Response helloResponse() {
 *       ...
 *    }
 * </pre>
 */
@Documented
@Retention(RUNTIME)
@Target({TYPE, METHOD})
public @interface ApiTags {

	/**
	 * One or more tag names to associate the resource or operation.
	 *
	 * @return one or more tag names
	 */
	public String[] value();
}
