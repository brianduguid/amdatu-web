/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.doc;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * <p>Optional security property to further expand a security definition.</p>
 *
 * <p>One or more security properties are in a <code>@ApiSecurityDefinition</code>.</p>
 *
 * <pre>
 *    &#064;ApiSecurityDefinition(name = "apiKeyAuth", type = API_KEY,
 *                                properties = {
 *                                   &#064;ApiSecurityProperty(key = "in", value = "header"),
 *                                   &#064;ApiSecurityProperty(key = "name", value = "Authorization")
 *                                }
 *                              )
 *    public class HelloWorldResource {
 *       ...
 *    }
 * </pre>
 *
 * <p>See <a href="https://swagger.io/docs/specification/authentication/">specification</a>.</p>
 *
 * @see ApiSecurityDefinition
 * @see ApiSecurityDefinitions
 *
 * @since 1.0
 */
@Retention(RUNTIME)
@Target(ANNOTATION_TYPE)
public @interface ApiSecurityProperty {

	/**
	 * Property key.
	 *
	 * @return key
	 */
	public String key();

	/**
	 * Property value.
	 *
	 * @return value
	 */
	public String value();
}
