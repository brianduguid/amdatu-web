/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.doc;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * <p>Applying <code>@ApiHeaderDefinition</code> on the class is recommended; however, using the container <code>@ApiHeaderDefinitions</code>
 * is acceptable.</p>
 *
 * <p>Contains one or more security definitions that are documented in the <code>securitySchemes</code> section of <code>components</code>.</p>
 *
 * <pre>
 *    &#064;ApiHeaderDefinitions({
 *       &#064;ApiHeaderDefinition(name = "x-total-count", description = "Total count of results returned", valueType = Integer.class),
 *       &#064;ApiHeaderDefinition(name = "x-result-checksums", description = "Checksums of each result in order of the result", valueType = List.class, elementType = String.class),
 *       &#064;ApiHeaderDefinition(name = "x-error-detail", description = "Details of the error if possible", valueType = String.class)
 *    })
 * </pre>
 *
 * @see ApiHeader
 * @see ApiHeaderDefinition
 *
 * @since 1.0
 */
@Documented
@Retention(RUNTIME)
@Target(TYPE)
public @interface ApiHeaderDefinitions {

	/**
	 * One or more header definitions.
	 *
	 * @return header definitions
	 */
	public ApiHeaderDefinition[] value();
}
