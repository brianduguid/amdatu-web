/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.doc;

import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * <p>Documents the description and optional restrictions of a rest method parameter.</p>
 *
 * <pre>
 *    public void resetPassword(&#064;QueryParam("password") &#064;ApiParameter(value = "New password", password = true) String password, User user) {
 *       ...
 *    }
 * </pre>
 *
 * <p>Request body objects (the above User object) are not documented by <code>@ApiParameter</code>. Use <code>@ApiModel</code> and
 * <code>@ApiProperty</code>.</p>
 *
 * @see ApiModel
 * @see ApiProperty
 *
 * @since 1.0
 */
@Documented
@Retention(RUNTIME)
@Target(PARAMETER)
public @interface ApiParameter {

	/**
	 * Parameter description.
	 *
	 * @return description
	 */
	public String value();

	/**
	 * <p>True to require a value for this parameter.</p>
	 *
	 * <p>Defaults to true.</p>
	 *
	 * @return true for required
	 */
	public boolean required() default true;

	/**
	 * <p>Optional formatting information for
	 * String values.</p>
	 *
	 * <p>Applies to String values only.</p>
	 *
	 * @return additional string formatting
	 */
	public String format() default "";

	/**
	 * Optional minimum value for numbers or minimum length for
	 * strings. Also applies to minimum array length.
	 *
	 * @return minimum value or length
	 */
	public int minimum() default Integer.MIN_VALUE;

	/**
	 * <p>True to exclude the minimum value.</p>
	 *
	 * <p>True: value &#62; minimum<br>
	 * False: value &#62;&#61; minimum</p>
	 *
	 * <p>Defaults to false.</p>
	 *
	 * @return true to exclude the minimum value
	 */
	public boolean exclusiveMinimum() default false;

	/**
	 * Optional maximum value for numbers or maximum length for
	 * strings. Also applies to maximum array length.
	 *
	 * @return maximum value or length
	 */
	public int maximum() default Integer.MAX_VALUE;

	/**
	 * <p>True to exclude the maximum value.</p>
	 *
	 * <p>True: value &#60; maximum<br>
	 * False: value &#60;&#61; maximum</p>
	 *
	 * <p>Defaults to false.</p>
	 *
	 * @return true to exclude the maximum value
	 */
	public boolean exclusiveMaximum() default false;

	/**
	 * <p>True to include in GET. If set to false, the
	 * parameter is set to write-only.</p>
	 *
	 * <p>If both read and write are false, then they are
	 * ignored.</p>
	 *
	 * <p>Defaults to true.</p>
	 *
	 * @return true to include in GET
	 */
	public boolean read() default true;

	/**
	 * <p>True to include in POST/PUT/PATCH. If set to false, the
	 * parameter is set to read-only.</p>
	 *
	 * <p>If both read and write are false, then they are
	 * ignored.</p>
	 *
	 * <p>Defaults to true.</p>
	 *
	 * @return true to include in POST/PUT/PATCH
	 */
	public boolean write() default true;

	/**
	 * <p>True to allow null as a value.</p>
	 *
	 * <p>Defaults to false.</p>
	 *
	 * @return true to allow null as a value
	 */
	public boolean nullable() default false;

	/**
	 * <p>True to set the String format to password.</p>
	 *
	 * <p>Setting to true will override the format value.</p>
	 *
	 * <p>Defaults to false.</p>
	 *
	 * @return true to set the String format to password
	 */
	public boolean password() default false;
}
