/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.doc;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * <p>Applying <code>@ApiTagDefinition</code> on the class is recommended; however, using the container
 * <code>@ApiTagDefinitions</code> is acceptable.</p>
 *
 * <p>Contains one or more <code>@ApiTagDefinition</code> annotations that define
 * a tag.  The tag definitions within the <code>@ApiTagDefinitions</code> annotation are
 * not associated with a resource or operation until the tag name is referenced in a
 * <code>@ApiTags</code> annotation.</p>
 *
 * <p>The following example defines two tags within the <code>@ApiTagDefinitions</code> annotation
 * and then tags the resource with the newly defined tags. This will give tag all of the operations
 * in the resource with the two tags.</p>
 * <pre>
 *    &#064;ApiTagDefinitions({
 *       &#064;ApiTagDefinition(name = "dynamic", description = "Dynamic REST resource using Component"),
 *       &#064;ApiTagDefinition(name = "text/plain", description = "Produces plain text ")
 *    })
 *    &#064;Path("hello")
 *    &#064;ApiTags({"dynamic", "text/plain"})
 *    public class DynamicApplicationResource {
 *       ...
 *    }
 * </pre>
 *
 * @see ApiTagDefinition
 * @see ApiTags
 *
 * @since 1.0
 */
@Documented
@Retention(RUNTIME)
@Target(TYPE)
public @interface ApiTagDefinitions {

	/**
	 * One or more tag definitions to be later referenced.
	 *
	 * @return one or more tag definitions
	 */
	public ApiTagDefinition[] value();
}
