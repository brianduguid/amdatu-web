/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.doc;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * <p>Defines a header that is documented in the <code>headers</code> section of <code>components</code>.</p>
 *
 * <p>The <code>@ApiHeaderDefinition</code> is repeatable.  The header name is referenced in the
 * <code>headerNames</code> property of <code>@ApiResponseDefinition</code> or <code>@ApiResponse</code> annotations.</p>
 *
 * <pre>
 *    &#064;ApiHeaderDefinition(name = "x-total-count", description = "Total count of results returned", valueType = Integer.class)
 *    &#064;ApiHeaderDefinition(name = "x-result-checksums", description = "Checksums of each result in order of the result", valueType = List.class, elementType = String.class)
 *    &#064;ApiHeaderDefinition(name = "x-error-detail", description = "Details of the error if possible", valueType = String.class)
 * </pre>
 *
 * @see ApiHeader
 * @see ApiHeaderDefinitions
 * @see ApiResponseDefinition
 * @see ApiResponse
 *
 * @since 1.0
 */
@Documented
@Repeatable(ApiHeaderDefinitions.class)
@Retention(RUNTIME)
@Target({TYPE, ANNOTATION_TYPE})
public @interface ApiHeaderDefinition {

	/**
	 * Name of the header. For example, <code>x-total-count</code>.
	 *
	 * @return header name
	 */
	public String name();

	/**
	 * Header description.
	 *
	 * @return header description
	 */
	public String description();

	/**
	 * Type returned as the header value.
	 *
	 * @return header value type
	 */
	public Class<?> valueType();

	/**
	 * Optional type of the value type's element, if the value type is a list, map, or dictionary.
	 *
	 * @return element type of the list, map, or dictionary value type
	 */
	public Class<?> elementType() default void.class;
}
