/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.doc;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * <p>Documents a hyperlink to external documentation for a resource method.</p>
 *
 * <p>Example:
 * <pre>
 *    &#064;ApiExternalDocs(url = "/docs/index.html", description = "Example External Docs Link")
 *    public String sendHello(&#064;QueryParam("helloText") String helloText) {
 *       ...
 *    }
 * </pre>
 *
 * <p>See <a href="https://swagger.io/docs/specification/paths-and-operations/">specification</a>.</p>
 *
 * @since 1.0
 */
@Documented
@Retention(RUNTIME)
@Target(METHOD)
public @interface ApiExternalDocs {

	/**
	 * URL of the external documentation.
	 *
	 * @return documentation url
	 */
	public String url();

	/**
	 * Hyperlink description.
	 *
	 * @return hyperlink description
	 */
	public String description();
}
