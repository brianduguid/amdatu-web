/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.doc;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * <p>Defines security requirements contains one or more security schemes and/or
 * one or more security scheme groups contain more than one security scheme.</p>
 *
 * <p>The following example sets the default security requirement for all
 * methods in this resource since it is annotated on a type and not a method.</p>
 *
 * <p>Using <code>@ApiSecuritySchemeGroup</code> will require that all security schemes
 * within the group must be satisfied.</p>
 *
 * <p>The following code block requires a single security scheme:
 *
 * <pre>
 *    &#064;ApiSecurityRequirements(&#064;ApiSecurityScheme("apiKeyAuth"))
 *    public class HelloWorldResource {
 *       ...
 *    }
 * </pre>
 *
 * <p>The security requirements in the following block are:
 * <ul>
 * 	<li><code>apiKeyAuth</code>, OR</li>
 *  <li><code>bearerAuth</code> AND <code>apiKeyAuth</code></li>
 * </ul>
 *
 * <pre>
 *    &#064;ApiSecurityRequirements(
 *              value ={ &#064;ApiSecurityScheme("apiKeyAuth") },
 *              groups = {
 *                         &#064;ApiSecuritySchemeGroup({
 *                               &#064;ApiSecurityScheme("bearerAuth"),
 *                               &#064;ApiSecurityScheme("apiKeyAuth")
 *                         })
 *                        },
 *    )
 *    public class HelloWorldResource {
 *       ...
 *    }
 * </pre>
 *
 * <p>Setting global to true will remove this as a default requirement for the resource and make
 * it the default for the API.  Only <code>@ApiSecurityRequirements</code> defined on a type can
 * be set as global requirement. The global property of any <code>@ApiSecurityRequirements</code>
 * defined on a method will be ignored.</p>
 *
 * <p>Using an empty <code>@ApiSecurityRequirements</code> will document that the operation does not
 * have security requirements.  This will override the global security scheme and the resource's
 * default security scheme.</p>
 *
 * <pre>
 *    &#064;GET
 *    &#064;ApiSecurityRequirements
 *    public String sendHello() {
 *       ...
 *    }
 * </pre>
 *
 * <p>See <a href="https://swagger.io/docs/specification/authentication/">specification</a>.</p>
 *
 * @see ApiSecurityScheme
 * @see ApiSecuritySchemeGroup
 *
 * @since 1.0
 */
@Documented
@Retention(RUNTIME)
@Target({TYPE, METHOD})
public @interface ApiSecurityRequirements {

	/**
	 * One or more security schemes required for operation authentication.
	 *
	 * @return security schemes
	 */
	public ApiSecurityScheme[] value() default {};

	/**
	 * One or more security groups containing multiple security schemes. When
	 * security schemes are grouped, they are both required to be satisfied before
	 * authentication is approved.
	 *
	 * @return security groups
	 */
	public ApiSecuritySchemeGroup[] groups() default {};

	/**
	 * Set to true to configure this security requirement as the global default security
	 * scheme. The global flag is only recognized on a type.
	 *
	 * @return true if this security requirement is a global default
	 */
	public boolean global() default false;
}
