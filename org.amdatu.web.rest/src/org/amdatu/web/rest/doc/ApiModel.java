/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.doc;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * <p>Indicates the type is a reusable model and added to the global components
 * documentation.</p>
 *
 * <p>An optional unique name can be specified, otherwise it will use the fully qualified
 * class name as the reference name.  Uniqueness is not enforced by the generator.</p>
 *
 * <p>See <a href="https://swagger.io/docs/specification/components/">specification</a>.</p>
 *
 * @since 1.0
 */
@Documented
@Retention(RUNTIME)
@Target({TYPE})
public @interface ApiModel {

	/**
	 * Description of the model.
	 *
	 * @return description
	 */
	public String value();

	/**
	 * Optional unique name to override the use of the fully qualified
	 * class name as the reusable model name.
	 *
	 * @return unique name
	 */
	public String name() default "";
}
