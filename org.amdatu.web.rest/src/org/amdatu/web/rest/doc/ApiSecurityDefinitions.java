/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.doc;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * <p>Applying <code>@ApiSecurityDefinition</code> on the class is recommended; however, using the container
 * <code>@ApiSecurityDefinitions</code> is acceptable.</p>
 *
 * <p>Contains one or more security definitions that are documented in the <code>securitySchemes</code> section of <code>components</code>.</p>
 *
 * <pre>
 *    &#064;ApiSecurityDefinitions({
 *       &#064;ApiSecurityDefinition(name = "basicAuth", type = BASIC),
 *       &#064;ApiSecurityDefinition(name = "bearerAuth", type = BEARER),
 *       &#064;ApiSecurityDefinition(name = "apiKeyAuth", type = API_KEY,
 *                                properties = {
 *                                   &#064;ApiSecurityProperty(key = "in", value = "header"),
 *                                   &#064;ApiSecurityProperty(key = "name", value = "Authorization")
 *                                }
 *                              ),
 *       &#064;ApiSecurityDefinition(name = "openIdAuth", type = OPEN_ID_CONNECT,
 *                                properties = {
 *                                   &#064;ApiSecurityProperty(key = "openIdConnectUrl", value = "https://example.com/.well-known/openid-configuration")
 *                                }
 *                              )
 *    })
 *    public class HelloWorldResource {
 *       ...
 *    }
 * </pre>
 *
 * <p>See <a href="https://swagger.io/docs/specification/authentication/">specification</a>.</p>
 *
 * @see ApiSecurityDefinition
 * @see ApiSecurityProperty
 * @see ApiSecurityRequirements
 *
 * @since 1.0
 */
@Documented
@Retention(RUNTIME)
@Target(TYPE)
public @interface ApiSecurityDefinitions {

	/**
	 * One or more security definitions.
	 *
	 * @return security definitions
	 */
	public ApiSecurityDefinition[] value();
}
