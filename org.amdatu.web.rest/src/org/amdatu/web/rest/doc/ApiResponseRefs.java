/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.doc;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * <p>The <code>@ApiResponseRefs</code> contains one or more references to predefined responses.</p>
 *
 * <p>Responses can be predefined and referenced by HTTP methods for re-usability. The response
 * definition uses the <code>@ApiResponseDefinition</code>. Predefined responses can be referenced
 * using the <code>@ApiResponseRefs</code>.</p>
 *
 * <p>Example:
 * <pre>
 *    &#064;ApiResponse(code = 200, description = "OK", returnType = String.class)
 *    &#064;ApiResponseRefs({ "403_Forbidden", "500_Error" })
 *    public Response helloResponse() {
 *       ...
 *    }
 * </pre>
 *
 * @see ApiResponse
 * @see ApiHeader
 * @see ApiResponseDefinitions
 * @see ApiResponseDefinition
 * @see ApiHeaderDefinitions
 * @see ApiHeaderDefinition
 *
 * @since 1.0
 */
@Documented
@Retention(RUNTIME)
@Target(METHOD)
public @interface ApiResponseRefs {

	/**
	 * Returns an array of response definition reference names.
	 *
	 * @return array of response definition reference names
	 */
	public String[] value();
}
