/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.doc.openapi;

import java.util.List;
import java.util.Map;

/**
 * <p>Optional configuration service to set the definition's title, description, version, and
 * other information.  It also provides a means to disable the definition generation or tag
 * filtering.</p>
 *
 * <p>Default values are provided if a configuration service is not available. Include the
 * optional <code>org.amdatu.web.rest.doc.openapi.config</code> bundle that provides a
 * managed service to provide the configuration details.</p>
 *
 * <p>If the <code>org.amdatu.web.rest.doc.openapi.config</code> bundle is not included, the
 * OpenAPI definition will be generated with default title, description, and version.</p>
 *
 * <p>See configuration keys below for managed service PID and options.</p>
 *
 * @since 1.0
 */
public interface OpenApiConfigService {

	/**
	 * <p>Managed service PID.</p>
	 *
	 * <pre>
	 *     org.amdatu.web.rest.doc.openapi
	 * </pre>
	 */
	public String PID = "org.amdatu.web.rest.doc.openapi";

	/**
	 * <p>Base URI that the configuration applies to. This is required; otherwise the
	 * configuration will be set as the default.</p>
	 *
	 * <pre>
	 *    base.uri = /example
	 * </pre>
	 *
	 * <p>To set the configuration as the default:</p>
	 *
	 * <pre>
	 *    base.uri = default
	 * </pre>
	 */
	public String CONFIG_KEY_BASE_URI = "base.uri";

	/**
	 * <p>API title.</p>
	 *
	 * <pre>
	 *    title = Example OpenAPI Definition
	 * </pre>
	 */
	public String CONFIG_KEY_TITLE = "title";

	/**
	 * <p>API description.</p>
	 *
	 * <pre>
	 *    description = This is an example OpenAPI definition.
	 * </pre>
	 */
	public String CONFIG_KEY_DESCRIPTION = "description";

	/**
	 * <p>API version.</p>
	 *
	 * <pre>
	 *    version = 2.0.0
	 * </pre>
	 */
	public String CONFIG_KEY_VERSION = "version";

	/**
	 * <p>Set to true to enable OpenAPI definition generation.  The default and
	 * file config service is set to true as default.</p>
	 *
	 * <pre>
	 *    enabled = true
	 * </pre>
	 */
	public String CONFIG_KEY_ENABLED = "enabled";

	/**
	 * <p>Prefix of a API server url followed by a number.</p>
	 *
	 * <pre>
	 *    api.server.url.1 = https://api.example.com/v1
	 *    api.server.description.1 = description: Production server (uses live data)
	 *
	 *    api.server.url.2 = https://sandbox-api.example.com:8443/v1
	 *    api.server.description.2 = description: Sandbox server (uses test data)
	 * </pre>
	 *
	 * <p>See <a href="https://swagger.io/docs/specification/api-host-and-base-path/">specification</a>.</p>
	 *
	 * @see CONFIG_KEY_API_SERVER_DESC_PREFIX
	 */
	public String CONFIG_KEY_API_SERVER_URL_PREFIX = "api.server.url.";

	/**
	 * <p>Prefix of a API server description followed by a number. The number
	 * needs to match a corresponding API server url configuration.</p>
	 *
	 * <pre>
	 *    api.server.url.1 = https://api.example.com/v1
	 *    api.server.description.1 = description: Production server (uses live data)
	 *
	 *    api.server.url.2 = https://sandbox-api.example.com:8443/v1
	 *    api.server.description.2 = description: Sandbox server (uses test data)
	 * </pre>
	 *
	 * <p>See <a href="https://swagger.io/docs/specification/api-host-and-base-path/">specification</a>.</p>
	 *
	 * @see CONFIG_KEY_API_SERVER_URL_PREFIX
	 */
	public String CONFIG_KEY_API_SERVER_DESC_PREFIX = "api.server.description.";

	/**
	 * <p>API terms of service url.</p>
	 *
	 * <p>See <a href="https://swagger.io/docs/specification/api-general-info/">specification</a>.</p>
	 */
	public String CONFIG_KEY_TERMS_OF_SERVICE_URL = "terms.of.service.url";

	/**
	 * <p>API license name.</p>
	 *
	 * <p>See <a href="https://swagger.io/docs/specification/api-general-info/">specification</a>.</p>
	 *
	 * @see CONFIG_KEY_LICENSE_URL
	 */
	public String CONFIG_KEY_LICENSE_NAME = "license.name";

	/**
	 * <p>API license url.</p>
	 *
	 * <p>See <a href="https://swagger.io/docs/specification/api-general-info/">specification</a>.</p>
	 *
	 * @see CONFIG_KEY_LICENSE_NAME
	 */
	public String CONFIG_KEY_LICENSE_URL = "license.url";

	/**
	 * <p>API external documentation url.</p>
	 *
	 * <p>See <a href="https://swagger.io/docs/specification/api-general-info/">specification</a>.</p>
	 *
	 * @see CONFIG_KEY_EXTERNAL_DOCS_DESC
	 */
	public String CONFIG_KEY_EXTERNAL_DOCS_URL = "external.docs.url";

	/**
	 * <p>API external documentation description.</p>
	 *
	 * <p>See <a href="https://swagger.io/docs/specification/api-general-info/">specification</a>.</p>
	 *
	 * @see CONFIG_KEY_EXTERNAL_DOCS_URL
	 */
	public String CONFIG_KEY_EXTERNAL_DOCS_DESC = "external.docs.description";

	/**
	 * <p>API support contact url.</p>
	 *
	 * <p>See <a href="https://swagger.io/docs/specification/api-general-info/">specification</a>.</p>
	 *
	 * @see CONFIG_KEY_SUPPORT_CONTACT_EMAIL
	 * @see CONFIG_KEY_SUPPORT_CONTACT_NAME
	 */
	public String CONFIG_KEY_SUPPORT_CONTACT_URL = "support.contact.url";

	/**
	 * <p>API support contact email address.</p>
	 *
	 * <p>See <a href="https://swagger.io/docs/specification/api-general-info/">specification</a>.</p>
	 *
	 * @see CONFIG_KEY_SUPPORT_CONTACT_URL
	 * @see CONFIG_KEY_SUPPORT_CONTACT_NAME
	 */
	public String CONFIG_KEY_SUPPORT_CONTACT_EMAIL = "support.contact.email";

	/**
	 * <p>API support contact url link name.</p>
	 *
	 * <p>See <a href="https://swagger.io/docs/specification/api-general-info/">specification</a>.</p>
	 *
	 * @see CONFIG_KEY_SUPPORT_CONTACT_EMAIL
	 * @see CONFIG_KEY_SUPPORT_CONTACT_NAME
	 */
	public String CONFIG_KEY_SUPPORT_CONTACT_NAME = "support.contact.name";

	/**
	 * <p>The optional tag filter method: none, exclude, or include.</p>
	 *
	 * <p>Omit or set the value to <code>none</code> to disable tag filtering.</p>
	 *
	 * <p>Set the value to <code>exclude</code> to document all API operations except those
	 * tag with one of the tags to be filtered.</p>
	 *
	 * <p>Set the value to <code>include</code> to not document an API operation unless it
	 * is tagged with one of the tags to be filtered.</p>
	 *
	 * <p>Configuration file example:</p>
	 * <pre>
	 * tag.filter.method = exclude
	 * filtered.tag.1 = admin
	 * filtered.tag.2 = secret
	 * </pre>
	 *
	 *  @see TagFilterMethod
	 *  @see CONFIG_KEY_FILTERED_TAG_PREFIX
	 */
	public String CONFIG_KEY_FILTERED_TAG_METHOD = "tag.filter.method";

	/**
	 * <p>Prefix to specify a tag to be filtered followed by a number to identify multple
	 * tags.</p>
	 *
	 * <p>Configuration file example:</p>
	 * <pre>
	 * tag.filter.method = exclude
	 * filtered.tag.1 = admin
	 * filtered.tag.2 = secret
	 * </pre>
	 *
	 * @see CONFIG_KEY_FILTERED_TAG_METHOD
	 */
	public String CONFIG_KEY_FILTERED_TAG_PREFIX = "filtered.tag.";

	/**
	 * <p>Prefix to designate one or more default consumes in the case a
	 * method and its rest resource does not have <code>@Consumes</code>
	 * annotated.</p>
	 *
	 * <p>The generator defaults to <code>application/json</code> if this
	 * value is not specified and the consumes value cannot be determined by
	 * annotation.</p>
	 *
	 * <p>Configuration file example:</p>
	 * <pre>
	 * default.consumes.1 = text/plain
	 * </pre>
	 */
	public String CONFIG_KEY_DEFAULT_CONSUMES_PREFIX = "default.consumes.";

	/**
	 * <p>Prefix to designate one or more default produces in the case a
	 * method and its rest resource does not have <code>@Produces</code>
	 * annotated.</p>
	 *
	 * <p>The generator defaults to <code>application/json</code> if this
	 * value is not specified and the produces value cannot be determined by
	 * annotation.</p>
	 *
	 * <p>Configuration file example:</p>
	 * <pre>
	 * default.produces.1 = text/plain
	 * </pre>
	 */
	public String CONFIG_KEY_DEFAULT_PRODUCES_PREFIX = "default.produces.";

	/**
	 * <p>A variable in the url to allow the server to determine the current
	 * request's protocol.</p>
	 *
	 * <p>Configuration file example:</p>
	 * <pre>
	 * external.docs.url = {protocol}://{server}:{port}/docs/index.html
	 * external.docs.description = External Docs Link Example
	 * </pre>
	 *
	 * @see URL_VARIABLE_FORMAT_SERVER
	 * @see URL_VARIABLE_FORMAT_PORT
	 */
	public String URL_VARIABLE_FORMAT_PROTOCOL = "{protocol}";

	/**
	 * <p>A variable in the url to allow the server to determine the current
	 * request's server.</p>
	 *
	 * <p>Configuration file example:</p>
	 * <pre>
	 * external.docs.url = {protocol}://{server}:{port}/docs/index.html
	 * external.docs.description = External Docs Link Example
	 * </pre>
	 *
	 * @see URL_VARIABLE_FORMAT_PROTOCOL
	 * @see URL_VARIABLE_FORMAT_PORT
	 */
	public String URL_VARIABLE_FORMAT_SERVER = "{server}";

	/**
	 * <p>A variable in the url to allow the server to determine the current
	 * request's port.</p>
	 *
	 * <p>Configuration file example:</p>
	 * <pre>
	 * external.docs.url = {protocol}://{server}:{port}/docs/index.html
	 * external.docs.description = External Docs Link Example
	 * </pre>
	 *
	 * @see URL_VARIABLE_FORMAT_PROTOCOL
	 * @see URL_VARIABLE_FORMAT_SERVER
	 */
	public String URL_VARIABLE_FORMAT_PORT = ":{port}";

	/**
	 * <p>Base URI that the configuration applies to. This is required; otherwise the
	 * configuration will be set as the default.</p>
	 *
	 * @return base uri
	 */
	public String getBaseUri();

	/**
	 * Returns the API's title.
	 *
	 * @return title
	 */
	public String getTitle();

	/**
	 * Returns the API description.
	 *
	 * @return description
	 */
	public String getDescription();

	/**
	 * Returns the API version.
	 *
	 * @return version
	 */
	public String getVersion();

	/**
	 * Returns true to enable the OpenAPI definition generation.
	 *
	 * @return true to enable definition generation
	 */
	public boolean isEnabled();

	/**
	 * Returns a map of the API server URL as the key
	 * and the description as the value.
	 *
	 * @return map of API server urls and descriptions, or null
	 */
	public Map<String, String> getServers();

	/**
	 * Returns the API terms of service url.
	 *
	 * @return terms of service url, or null
	 */
	public String getTermsOfServiceUrl();

	/**
	 * Returns the API license url.
	 *
	 * @return license url, or null
	 */
	public String getLicenseUrl();

	/**
	 * Returns the name of the API license.
	 *
	 * @return name of the license, or null
	 */
	public String getLicenseName();

	/**
	 * Returns the API external documentation url.
	 *
	 * @return external documentation url, or null
	 */
	public String getExternalDocsUrl();

	/**
	 * Returns the API external documentation url link name.
	 *
	 * @return external documentation url link name, or null
	 */
	public String getExternalDocsDescription();

	/**
	 * Returns the API support's contact url.
	 *
	 * @return support contact url, or null
	 */
	public String getSupportContactUrl();

	/**
	 * Returns the API support's contact email address.
	 *
	 * @return support contact email address, or null
	 */
	public String getSupportContactEmail();

	/**
	 * Returns the API support's contact url link name.
	 *
	 * @return support contact url link name, or null
	 */
	public String getSupportContactName();

	/**
	 * Returns the tag filter method.
	 *
	 * @return tag filter method, or null
	 * @see TagFilterMethod
	 */
	public TagFilterMethod getTagFilterMethod();

	/**
	 * Returns a list of tags to be filtered.
	 *
	 * @return tags to be filtered, or null
	 */
	public List<String> getFilteredTags();

	/**
	 * Returns the rest resource tag filter that will
	 * filter perform the tag filtering on rest resources.
	 *
	 * @return rest resource tag filter
	 */
	public RestResourceFilter getRestResourceFilter();

	/**
	 * Returns the rest endpoint filter that will
	 * filter perform the tag filtering on rest resources.
	 *
	 * @return rest endpoint filter
	 */
	public RestEndpointFilter getRestMethodFilter();

	/**
	 * Returns a list of default consumes to be used if not
	 * specified on a rest resource and method.
	 *
	 * @return default consumes, or null
	 */
	public List<String> getDefaultConsumes();

	/**
	 * Returns a list of default produces to be used if not
	 * specified on a rest resource and method.
	 *
	 * @return default produces, or null
	 */
	public List<String> getDefaultProduces();
}
