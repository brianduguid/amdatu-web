/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.doc.openapi.spec.datatype;

import java.util.List;

import org.amdatu.web.rest.doc.openapi.spec.OpenApiDataType;

import com.google.gson.annotations.SerializedName;

public class OpenApiDataTypeEnum extends OpenApiDataType {

	@SerializedName("enum")
	private List<String> enumValues;

	public OpenApiDataTypeEnum(List<String> enumValues) {
		super("string");
		this.enumValues = enumValues;
	}

	public List<String> getEnumValues() {
		return enumValues;
	}

	public OpenApiDataTypeEnum setEnumValues(List<String> enumValues) {
		this.enumValues = enumValues;
		return this;
	}

	@Override
	public String toString() {
		return "OpenApiDataTypeEnum [enumValues=" + enumValues + ", name=" + name + ", type=" + type + ", description="
				+ description + ", defaultValue=" + defaultValue + ", flaggedRequired=" + flaggedRequired + "]";
	}
}
