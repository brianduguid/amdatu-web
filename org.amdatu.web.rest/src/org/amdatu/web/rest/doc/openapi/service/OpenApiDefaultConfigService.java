/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.doc.openapi.service;

import static org.amdatu.web.rest.doc.openapi.Constants.DEFAULT_BASE_URI;
import static org.amdatu.web.rest.doc.openapi.Constants.DEFAULT_DESCRIPTION;
import static org.amdatu.web.rest.doc.openapi.Constants.DEFAULT_MEDIA_TYPE;
import static org.amdatu.web.rest.doc.openapi.Constants.DEFAULT_TITLE;
import static org.amdatu.web.rest.doc.openapi.Constants.DEFAULT_VERSION;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.amdatu.web.rest.doc.openapi.OpenApiConfigService;
import org.amdatu.web.rest.doc.openapi.RestEndpoint;
import org.amdatu.web.rest.doc.openapi.RestEndpointFilter;
import org.amdatu.web.rest.doc.openapi.RestResource;
import org.amdatu.web.rest.doc.openapi.RestResourceFilter;
import org.amdatu.web.rest.doc.openapi.TagFilterMethod;

public class OpenApiDefaultConfigService implements OpenApiConfigService, RestResourceFilter, RestEndpointFilter {

	@Override
	public String getBaseUri() {
		return DEFAULT_BASE_URI;
	}

	@Override
	public String getTitle() {
		return DEFAULT_TITLE;
	}

	@Override
	public String getDescription() {
		return DEFAULT_DESCRIPTION;
	}

	@Override
	public String getVersion() {
		return DEFAULT_VERSION;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

	@Override
	public Map<String, String> getServers() {
		return null;
	}

	@Override
	public String getTermsOfServiceUrl() {
		return null;
	}

	@Override
	public String getLicenseUrl() {
		return null;
	}

	@Override
	public String getLicenseName() {
		return null;
	}

	@Override
	public String getExternalDocsUrl() {
		return null;
	}

	@Override
	public String getExternalDocsDescription() {
		return null;
	}

	@Override
	public String getSupportContactUrl() {
		return null;
	}

	@Override
	public String getSupportContactEmail() {
		return null;
	}

	@Override
	public String getSupportContactName() {
		return null;
	}

	@Override
	public TagFilterMethod getTagFilterMethod() {
		return TagFilterMethod.NONE;
	}

	@Override
	public List<String> getFilteredTags() {
		return Collections.emptyList();
	}

	@Override
	public RestResourceFilter getRestResourceFilter() {
		return this;
	}

	@Override
	public RestEndpointFilter getRestMethodFilter() {
		return this;
	}

	@Override
	public boolean isIncluded(RestResource restResource) {
		return true;
	}

	@Override
	public boolean isIncluded(RestEndpoint restEndpoint) {
		return true;
	}

	@Override
	public List<String> getDefaultConsumes() {
		return Collections.singletonList(DEFAULT_MEDIA_TYPE);
	}

	@Override
	public List<String> getDefaultProduces() {
		return Collections.singletonList(DEFAULT_MEDIA_TYPE);
	}
}
