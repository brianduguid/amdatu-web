/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.doc.openapi;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>The OpenAPI definition service collects information from the REST resources and
 * generates the definition in a json format.</p>
 *
 * @since 1.0
 */
public interface OpenApiDefinitionService {

	/**
	 * <p>Returns a collection of information of the REST resource using the resource's class.
	 * A list of REST resources for the REST application is used to generate the OpenAPI
	 * definition.</p>
	 *
	 * @param baseUri base URI for the REST application
	 * @param resourceClass REST resource class
	 * @return rest resource information
	 *
	 * @see RestResource
	 */
	public RestResource createRestResource(String baseUri, Class<?> resourceClass);

	/**
	 * <p>Returns the OpenAPI definition in json format generated from the list of REST resource
	 * classes.</p>
	 *
	 * @param request HTTP servlet request
	 * @param restResources list of REST resources
	 * @return OpenAPI definition in json format
	 */
	public String createOpenApiDefinitionJson(HttpServletRequest request, List<RestResource> restResources);
}
