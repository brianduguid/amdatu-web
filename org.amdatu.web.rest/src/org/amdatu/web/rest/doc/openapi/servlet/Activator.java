/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.doc.openapi.servlet;

import javax.servlet.Servlet;

import org.amdatu.web.rest.doc.openapi.OpenApiDefinitionService;
import org.amdatu.web.rest.spi.ApplicationService;
import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleContext;

public class Activator extends DependencyActivatorBase {

	@Override
	public void init(BundleContext context, DependencyManager manager) throws Exception {

		manager
	        .add(createAdapterService(ApplicationService.class, null, "onAdd", "onChange", null)
	            .setInterface(Servlet.class.getName(), null)
	            .setImplementation(OpenApiServlet.class)
	            .setCallbacks("dmInit", "dmStart", "dmStop", "dmDestroy") // avoid clashes with Servlet API
	            .add(createServiceDependency()
						.setService(OpenApiDefinitionService.class).setRequired(true))
	        );
	}
}
