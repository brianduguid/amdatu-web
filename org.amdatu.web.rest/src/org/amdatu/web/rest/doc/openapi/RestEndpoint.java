/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.doc.openapi;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.List;

/**
 * <p>Provides the properties of a rest endpoint discovered by the OpenAPI definition generator. This data
 * is used by the rest endpoint filter and to later generate rest operation definitions. <code>RestResource</code>s
 * contain <code>RestEndpoint</code>s.</p>
 *
 * @see RestResource
 *
 * @since 1.0
 */
public interface RestEndpoint {

	/**
	 * Returns the endpoint path based on the rest resources base uri
	 * and the method's path set by <code>@Path</code>, i.e. /rest/hello.
	 *
	 * @return endpoint path
	 */
	public String getEndpointPath();

	/**
	 * Returns the HTTP operation method: GET, POST, PUT, etc.
	 *
	 * @return HTTP operation method
	 */
	public String getHttpMethod();

	/**
	 * Returns the reflection method for the rest operation.
	 *
	 * @return reflection method
	 */
	public Method getRestMethod();

	/**
	 * Returns the parent rest resource that contains this
	 * rest endpoint operation.
	 *
	 * @return parent rest resource
	 */
	public RestResource getParentRestResource();

	/**
	 * Returns true if the annotation class is present on the reflection method. This is
	 * a convenience method that is equivalent to
	 * <code>restEndpoint.getRestMethod().isAnnotationPresent(annotationClass)</code>.
	 *
	 * @param annotationClass annotation class to search for
	 * @return true if the annotation class is present
	 */
	public boolean isAnnotationPresent(Class<? extends Annotation> annotationClass);

	/**
	 * Returns the reflection method's annotation in an array.
	 *
	 * @return array of annotations
	 */
	public Annotation[] getAnnotations();

	/**
	 * Returns a list of produces that were specified on this reflective method by using
	 * the <code>@Produces</code> annotation. If none specified, then null is returned.
	 *
	 * @return list of produces, or null
	 */
	public List<String> getProduces();

	/**
	 * Returns a list of consumes that were specified on this reflective method by using
	 * the <code>@Consumes</code> annotation. If none specified, then null is returned.
	 *
	 * @return list of consumes, or null
	 */
	public List<String> getConsumes();

	/**
	 * Returns a list of tags that were specified on this reflective method by using
	 * the <code>@ApiTags</code> annotation. If none specified, then null is returned.
	 *
	 * @return list of tags, or null
	 */
	public List<String> getTags();
}
