/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.doc.openapi.spec;

public class OpenApiContact {

	private String url;
	private String email;
	private String name;

	public OpenApiContact(String url, String email, String name) {
		super();
		this.url = url;
		this.email = email;
		this.name = name;
	}

	public String getUrl() {
		return url;
	}

	public OpenApiContact setUrl(String url) {
		this.url = url;
		return this;
	}

	public String getEmail() {
		return email;
	}

	public OpenApiContact setEmail(String email) {
		this.email = email;
		return this;
	}

	public String getName() {
		return name;
	}

	public OpenApiContact setName(String name) {
		this.name = name;
		return this;
	}

	@Override
	public String toString() {
		return "OpenApiContact [url=" + url + ", email=" + email + ", name=" + name + "]";
	}
}
