/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.doc.openapi.spec;

import com.google.gson.annotations.SerializedName;

public class OpenApiHeader {

	private transient String name;
	private OpenApiDataType schema;
	private String description;
	@SerializedName("$ref")
	private String headerRef;

	public OpenApiHeader(String name) {
		this.name = name;
	}

	public OpenApiHeader(String name, OpenApiDataType schema) {
		this.name = name;
		setSchema(schema);
	}

	public String getName() {
		return name;
	}

	public OpenApiHeader setName(String name) {
		this.name = name;
		return this;
	}

	public OpenApiDataType getSchema() {
		return schema;
	}

	public OpenApiHeader setSchema(OpenApiDataType schema) {
		this.schema = schema;
		return this;
	}

	public String getDescription() {
		return description;
	}

	public OpenApiHeader setDescription(String description) {
		this.description = description;
		return this;
	}

	public String getHeaderRef() {
		return headerRef;
	}

	public OpenApiHeader setHeaderRef(String headerRef) {
		this.headerRef = headerRef;
		return this;
	}

	@Override
	public String toString() {
		return "OpenApiHeader [name=" + name + ", schema=" + schema + ", description=" + description + ", headerRef="
				+ headerRef + "]";
	}
}
