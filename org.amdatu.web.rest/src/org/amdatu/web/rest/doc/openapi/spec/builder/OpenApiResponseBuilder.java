/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.doc.openapi.spec.builder;

import java.util.Arrays;
import java.util.List;

import org.amdatu.web.rest.doc.ApiHeader;
import org.amdatu.web.rest.doc.openapi.spec.OpenApiCombineMethod;
import org.amdatu.web.rest.doc.openapi.spec.OpenApiContent;
import org.amdatu.web.rest.doc.openapi.spec.OpenApiDataType;
import org.amdatu.web.rest.doc.openapi.spec.OpenApiResponse;
import org.amdatu.web.rest.doc.openapi.spec.OpenApiReusableMap;

public final class OpenApiResponseBuilder {

	private OpenApiReusableMap openApiReusableMap;
	private String name;
	private int code;
	private String description;
	private List<String> produces;
	private Class<?> returnType;
	private Class<?> elementType;
	private List<String> headerNames;
	private List<ApiHeader> headers;

	public static OpenApiResponseBuilder create(OpenApiReusableMap openApiReusableMap) {
		return new OpenApiResponseBuilder(openApiReusableMap);
	}

	private OpenApiResponseBuilder(OpenApiReusableMap openApiReusableMap) {
		super();
		this.openApiReusableMap = openApiReusableMap;
	}

	public String getName() {
		return name;
	}

	public OpenApiResponseBuilder setName(String name) {
		this.name = name;
		return this;
	}

	public int getCode() {
		return code;
	}

	public OpenApiResponseBuilder setCode(int code) {
		this.code = code;
		return this;
	}

	public String getDescription() {
		return description;
	}

	public OpenApiResponseBuilder setDescription(String description) {
		this.description = description;
		return this;
	}

	public List<String> getProduces() {
		return produces;
	}

	public OpenApiResponseBuilder setProduces(List<String> produces) {
		this.produces = produces;
		return this;
	}

	public OpenApiResponseBuilder setProduces(String[] produces) {

		if (produces == null || produces.length == 0) {
			return this;
		}

		this.produces = Arrays.asList(produces);
		return this;
	}

	public Class<?> getReturnType() {
		return returnType;
	}

	public OpenApiResponseBuilder setReturnType(Class<?> returnType) {
		this.returnType = returnType;
		return this;
	}

	public Class<?> getElementType() {
		return elementType;
	}

	public OpenApiResponseBuilder setElementType(Class<?> elementType) {
		this.elementType = elementType;
		return this;
	}

	public List<ApiHeader> getHeaders() {
		return headers;
	}

	public List<String> getHeaderNames() {
		return headerNames;
	}

	public OpenApiResponseBuilder setHeaderNames(List<String> headerNames) {
		this.headerNames = headerNames;
		return this;
	}

	public OpenApiResponseBuilder setHeaderNames(String[] headerNames) {

		if (headerNames == null || headerNames.length == 0) {
			return this;
		}

		this.headerNames = Arrays.asList(headerNames);
		return this;
	}

	public OpenApiResponseBuilder setHeaders(List<ApiHeader> headers) {
		this.headers = headers;
		return this;
	}

	public OpenApiResponseBuilder setHeaders(ApiHeader[] headers) {

		if (headers == null || headers.length == 0) {
			return this;
		}

		this.headers = Arrays.asList(headers);
		return this;
	}

	@Override
	public String toString() {
		return "OpenApiResponseBuilder [openApiReusableMap=" + openApiReusableMap + ", name=" + name + ", code=" + code
				+ ", description=" + description + ", produces=" + produces + ", returnType=" + returnType
				+ ", elementType=" + elementType + ", headerNames=" + headerNames + ", headers=" + headers + "]";
	}

	public OpenApiResponse build() {

		OpenApiResponse openApiResponse = new OpenApiResponse(code)
													.setName(name)
													.setDescription(description);

		if (!void.class.equals(returnType)) {

			OpenApiDataTypeBuilder openApiDataTypeBuilder = OpenApiDataTypeBuilder.create(openApiReusableMap, returnType)
																	.setElementType(elementType);

			OpenApiDataType openApiDataType = openApiDataTypeBuilder.build();

			for (String mediaType : produces) {
				openApiResponse.addContent(OpenApiCombineMethod.ONEOF, new OpenApiContent(mediaType, openApiDataType));
			}
		}

		if (headerNames != null && !headerNames.isEmpty()) {
			for (String headerName : headerNames) {
				openApiResponse.addHeader(openApiReusableMap.getHeaderRefByName(headerName));
			}
		}

		if (headers != null && !headers.isEmpty()) {
			for (ApiHeader header : headers) {

				OpenApiHeaderBuilder openApiHeaderBuilder = OpenApiHeaderBuilder.create(openApiReusableMap, header.name())
																				.setDescription(header.description())
																				.setValueType(header.valueType())
																				.setElementType(header.elementType());

				openApiResponse.addHeader(openApiHeaderBuilder.build());
			}
		}

		return openApiResponse;
	}
}
