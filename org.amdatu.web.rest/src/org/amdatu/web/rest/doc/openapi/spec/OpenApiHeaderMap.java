/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.doc.openapi.spec;

import java.util.HashMap;

public class OpenApiHeaderMap extends HashMap<String, OpenApiHeader> {

	private static final long serialVersionUID = -774605832894643146L;

	public boolean add(OpenApiHeader header) {
		return this.put(header.getName(), header) == null;
	}

}
