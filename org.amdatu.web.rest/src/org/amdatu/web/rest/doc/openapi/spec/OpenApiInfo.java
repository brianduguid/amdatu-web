/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.doc.openapi.spec;

public class OpenApiInfo {

	private transient boolean enabled;
	private String title;
	private String description;
	private String version;
	private String termsOfService;
	private OpenApiLicense license;
	private OpenApiContact contact;

	public OpenApiInfo() {
		super();
	}

	public boolean isEnabled() {
		return enabled;
	}

	public OpenApiInfo setEnabled(boolean enabled) {
		this.enabled = enabled;
		return this;
	}

	public String getTitle() {
		return title;
	}

	public OpenApiInfo setTitle(String title) {
		this.title = title;
		return this;
	}

	public String getDescription() {
		return description;
	}

	public OpenApiInfo setDescription(String description) {
		this.description = description;
		return this;
	}

	public String getVersion() {
		return version;
	}

	public OpenApiInfo setVersion(String version) {
		this.version = version;
		return this;
	}

	public String getTermsOfService() {
		return termsOfService;
	}

	public OpenApiInfo setTermsOfService(String termsOfService) {
		this.termsOfService = termsOfService;
		return this;
	}

	public OpenApiLicense getLicense() {
		return license;
	}

	public OpenApiInfo setLicense(OpenApiLicense license) {
		this.license = license;
		return this;
	}

	public OpenApiContact getContact() {
		return contact;
	}

	public OpenApiInfo setContact(OpenApiContact contact) {
		this.contact = contact;
		return this;
	}

	@Override
	public String toString() {
		return "OpenApiInfo [enabled=" + enabled + ", title=" + title + ", description=" + description + ", version="
				+ version + ", termsOfService=" + termsOfService + ", license=" + license + ", contact=" + contact
				+ "]";
	}
}
