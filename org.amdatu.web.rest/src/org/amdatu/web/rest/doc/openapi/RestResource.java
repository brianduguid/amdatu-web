/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.doc.openapi;

import java.lang.annotation.Annotation;
import java.util.List;

/**
 * <p>Provides the properties of a rest resource discovered by the OpenAPI definition generator. This data
 * is used by the rest resource filter and to later generate rest operation definitions. <code>RestResource</code>s
 * contain <code>RestEndpoint</code>s.</p>
 *
 * @see RestEndpoint
 *
 * @since 1.0
 */
public interface RestResource {

	/**
	 * Returns the base URI for this application.
	 *
	 * @return application' base uri
	 */
	public String getBaseUri();

	/**
	 * Returns the rest resource's reflection class.
	 *
	 * @return reflection class
	 */
	public Class<?> getRestResourceClass();

	/**
	 * Returns the rest resource's uri that is set by <code>@Path</code> on the
	 * resource class.
	 *
	 * @return resource uri
	 */
	public String getResourceUri();

	/**
	 * Returns true if the annotation class is present on the reflection class. This is
	 * a convenience method that is equivalent to
	 * <code>restResource.getRestResourceClass().isAnnotationPresent(annotationClass)</code>.
	 *
	 * @param annotationClass annotation class to search for
	 * @return true if the annotation class is present
	 */
	public boolean isAnnotationPresent(Class<? extends Annotation> annotationClass);

	/**
	 * Returns the reflection class's annotation in an array.
	 *
	 * @return array of annotations
	 */
	public Annotation[] getAnnotations();

	/**
	 * Returns a list of produces that were specified on this reflective class by using
	 * the <code>@Produces</code> annotation. If none specified, then null is returned.
	 *
	 * @return list of produces, or null
	 */
	public List<String> getProduces();

	/**
	 * Returns a list of consumes that were specified on this reflective class by using
	 * the <code>@Consumes</code> annotation. If none specified, then null is returned.
	 *
	 * @return list of consumes, or null
	 */
	public List<String> getConsumes();

	/**
	 * Returns a list of tags that were specified on this reflective class by using
	 * the <code>@ApiTags</code> annotation. If none specified, then null is returned.
	 *
	 * @return list of tags, or null
	 */
	public List<String> getTags();

	/**
	 * Returns the list of rest endpoints (HTTP operation methods) contained by this
	 * rest resource class.
	 *
	 * @return list of rest endpoints
	 */
	public List<RestEndpoint> getRestEndpoints();
}
