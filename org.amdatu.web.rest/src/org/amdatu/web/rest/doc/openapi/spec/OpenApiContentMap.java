/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.doc.openapi.spec;

import java.util.TreeMap;

import org.amdatu.web.rest.doc.openapi.spec.datatype.OpenApiDataTypeCombined;

public class OpenApiContentMap extends TreeMap<String, OpenApiContent> {

	private static final long serialVersionUID = 5277638811217955694L;

	public boolean add(OpenApiCombineMethod combineMethod, OpenApiContent content) {

		OpenApiContent existingContent = this.get(content.getMediaType());

		if (existingContent == null) {
			return this.put(content.getMediaType(), content) == null;

		} else {

			OpenApiDataType existingSchema = existingContent.getSchema();

			if (!(existingSchema instanceof OpenApiDataTypeCombined)) {
				existingSchema = addCombinedDataTypes(combineMethod, new OpenApiDataTypeCombined(), existingSchema);
				existingContent.setSchema(existingSchema);
			}

			addCombinedDataTypes(combineMethod, (OpenApiDataTypeCombined) existingSchema, content.getSchema());
			return false; // not new to the map
		}
	}

	private OpenApiDataTypeCombined addCombinedDataTypes(OpenApiCombineMethod combineMethod, OpenApiDataTypeCombined openApiDataTypeCombined, OpenApiDataType openApiDataType) {

		switch(combineMethod) {
			case ONEOF:
				openApiDataTypeCombined.addOneOf(openApiDataType);
				break;
			case ANYOF:
				openApiDataTypeCombined.addAnyOf(openApiDataType);
				break;
			case ALLOF:
				openApiDataTypeCombined.addAllOf(openApiDataType);
				break;
			case NOT:
				openApiDataTypeCombined.addNot(openApiDataType);
				break;
			default:
				break;
		}

		return openApiDataTypeCombined;
	}

}
