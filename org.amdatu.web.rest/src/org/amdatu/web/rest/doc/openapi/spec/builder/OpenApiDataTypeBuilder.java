/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.doc.openapi.spec.builder;

import static org.amdatu.web.rest.doc.openapi.spec.builder.AnnotationUtil.findAnnotation;
import static org.amdatu.web.rest.doc.openapi.spec.builder.AnnotationUtil.findDefaultValue;
import static org.amdatu.web.rest.doc.openapi.spec.builder.AnnotationUtil.isAnnotationPresent;
import static org.amdatu.web.rest.doc.openapi.spec.builder.BuilderTools.isNotBlank;
import static org.amdatu.web.rest.doc.openapi.spec.builder.LegacyAnnotationProcessor.findLegacyDescription;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.WildcardType;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.Dictionary;
import java.util.List;
import java.util.Map;

import org.amdatu.web.rest.doc.ApiModel;
import org.amdatu.web.rest.doc.ApiProperty;
import org.amdatu.web.rest.doc.openapi.spec.ComponentType;
import org.amdatu.web.rest.doc.openapi.spec.OpenApiDataType;
import org.amdatu.web.rest.doc.openapi.spec.OpenApiReusableMap;
import org.amdatu.web.rest.doc.openapi.spec.datatype.OpenApiDataTypeArray;
import org.amdatu.web.rest.doc.openapi.spec.datatype.OpenApiDataTypeBoolean;
import org.amdatu.web.rest.doc.openapi.spec.datatype.OpenApiDataTypeEnum;
import org.amdatu.web.rest.doc.openapi.spec.datatype.OpenApiDataTypeMap;
import org.amdatu.web.rest.doc.openapi.spec.datatype.OpenApiDataTypeNumber;
import org.amdatu.web.rest.doc.openapi.spec.datatype.OpenApiDataTypeObject;
import org.amdatu.web.rest.doc.openapi.spec.datatype.OpenApiDataTypeRef;
import org.amdatu.web.rest.doc.openapi.spec.datatype.OpenApiDataTypeString;

public final class OpenApiDataTypeBuilder {

	private OpenApiReusableMap openApiReusableMap;
	private Class<?> type;
	private Class<?> elementType; // if dataType is a generic collection
	private String name;
	private String description;
	private String defaultValue;
	private Annotation[] annotations;
	private boolean required;
	private String format;
	private Integer minimum;
	private Boolean exclusiveMinimum;
	private Integer maximum;
	private Boolean exclusiveMaximum;
	private Boolean nullable;
	private boolean read;
	private boolean write;
	private boolean password;

	public static OpenApiDataTypeBuilder create(OpenApiReusableMap openApiReusableMap, Class<?> type) {
		return create(openApiReusableMap, type, type.getAnnotations());
	}

	public static OpenApiDataTypeBuilder create(OpenApiReusableMap openApiReusableMap, Class<?> type, Annotation[] annotations) {

		String name = type.getName();
		String defaultValue = findDefaultValue(annotations);

		return new OpenApiDataTypeBuilder(openApiReusableMap, type)
									.setName(name)
									.setDefaultValue(defaultValue)
									.setAnnotations(annotations);
	}

	private OpenApiDataTypeBuilder(OpenApiReusableMap openApiReusableMap, Class<?> type) {
		super();
		this.openApiReusableMap = openApiReusableMap;
		this.type = type;
	}

	public OpenApiReusableMap getOpenApiReusableMap() {
		return openApiReusableMap;
	}

	public OpenApiDataTypeBuilder setOpenApiReusableMap(OpenApiReusableMap openApiReusableMap) {
		this.openApiReusableMap = openApiReusableMap;
		return this;
	}

	public Class<?> getType() {
		return type;
	}

	public OpenApiDataTypeBuilder setType(Class<?> type) {
		this.type = type;
		return this;
	}

	public Class<?> getElementType() {
		return elementType;
	}

	public OpenApiDataTypeBuilder setElementType(Class<?> elementType) {
		this.elementType = elementType;
		return this;
	}

	public String getName() {
		return name;
	}

	public OpenApiDataTypeBuilder setName(String name) {
		this.name = name;
		return this;
	}

	public String getDescription() {
		return description;
	}

	public OpenApiDataTypeBuilder setDescription(String description) {
		if (description != null && !description.isEmpty()) {
			this.description = description;
		}
		return this;
	}

	public String getDefaultValue() {
		return defaultValue;
	}

	public OpenApiDataTypeBuilder setDefaultValue(String defaultValue) {
		if (defaultValue != null && !defaultValue.isEmpty()) {
			this.defaultValue = defaultValue;
		}
		return this;
	}

	public Annotation[] getAnnotations() {
		return annotations;
	}

	public OpenApiDataTypeBuilder setAnnotations(Annotation[] annotations) {
		this.annotations = annotations;
		return this;
	}

	public boolean isRequired() {
		return required;
	}

	public OpenApiDataTypeBuilder setRequired(boolean required) {
		this.required = required;
		return this;
	}

	public String getFormat() {
		return format;
	}

	public OpenApiDataTypeBuilder setFormat(String format) {
		if (format != null && !format.isEmpty()) {
			this.format = format;
		}
		return this;
	}

	public Integer getMinimum() {
		return minimum;
	}

	public OpenApiDataTypeBuilder setMinimum(Integer minimum) {
		if (minimum == Integer.MIN_VALUE) {
			this.minimum = null;
		} else {
			this.minimum = minimum;
		}
		return this;
	}

	public Boolean getExclusiveMinimum() {
		return exclusiveMinimum;
	}

	public OpenApiDataTypeBuilder setExclusiveMinimum(Boolean exclusiveMinimum) {
		if (exclusiveMinimum) {
			this.exclusiveMinimum = exclusiveMinimum;
		} else {
			this.exclusiveMinimum = null;
		}
		return this;
	}

	public Integer getMaximum() {
		return maximum;
	}

	public OpenApiDataTypeBuilder setMaximum(Integer maximum) {
		if (maximum == Integer.MAX_VALUE) {
			this.maximum = null;
		} else {
			this.maximum = maximum;
		}
		return this;
	}

	public Boolean getExclusiveMaximum() {
		return exclusiveMaximum;
	}

	public OpenApiDataTypeBuilder setExclusiveMaximum(Boolean exclusiveMaximum) {
		if (exclusiveMaximum) {
			this.exclusiveMaximum = exclusiveMaximum;
		} else {
			this.exclusiveMaximum = null;
		}
		return this;
	}

	public Boolean getNullable() {
		return nullable;
	}

	public OpenApiDataTypeBuilder setNullable(Boolean nullable) {
		if (nullable) {
			this.nullable = nullable;
		} else {
			this.nullable = null;
		}
		return this;
	}

	public boolean isRead() {
		return read;
	}

	public OpenApiDataTypeBuilder setRead(boolean read) {
		this.read = read;
		return this;
	}

	public boolean isWrite() {
		return write;
	}

	public OpenApiDataTypeBuilder setWrite(boolean write) {
		this.write = write;
		return this;
	}

	public boolean isPassword() {
		return password;
	}

	public OpenApiDataTypeBuilder setPassword(boolean password) {
		this.password = password;
		return this;
	}

	@Override
	public String toString() {
		return "OpenApiDataTypeBuilder [openApiReusableMap=" + openApiReusableMap + ", type=" + type + ", elementType="
				+ elementType + ", name=" + name + ", description=" + description + ", defaultValue=" + defaultValue
				+ ", annotations=" + Arrays.toString(annotations) + ", required=" + required + ", format=" + format
				+ ", minimum=" + minimum + ", exclusiveMinimum=" + exclusiveMinimum + ", maximum=" + maximum
				+ ", exclusiveMaximum=" + exclusiveMaximum + ", nullable=" + nullable + ", read=" + read + ", write="
				+ write + ", password=" + password + "]";
	}

	public OpenApiDataType build() {

		OpenApiDataType openApiDataType = null;

		if (Void.TYPE.equals(type)) {
            return null;

        } else if (Integer.TYPE.equals(type) || Integer.class.isAssignableFrom(type)) {
            openApiDataType = createOpenApiDataTypeNumber("integer", "int32");

        } else if (Long.TYPE.equals(type) || Long.class.isAssignableFrom(type)) {
            openApiDataType = createOpenApiDataTypeNumber("integer", "int64");

        } else if (Float.TYPE.equals(type) || Float.class.isAssignableFrom(type)) {
            openApiDataType = createOpenApiDataTypeNumber("number", "float");

        } else if (Double.TYPE.equals(type) || Double.class.isAssignableFrom(type)) {
            openApiDataType = createOpenApiDataTypeNumber("number", "double");

        } else if (Byte.TYPE.equals(type) || Byte.class.isAssignableFrom(type)) {
            openApiDataType = createOpenApiDataTypeString("byte");

        } else if (Boolean.TYPE.equals(type) || Boolean.class.isAssignableFrom(type)) {
            openApiDataType = new OpenApiDataTypeBoolean();

        } else if (Number.class.isAssignableFrom(type)) {
            openApiDataType = createOpenApiDataTypeNumber("number", null);

        } else if (String.class.equals(type)) {
            openApiDataType = createOpenApiDataTypeString(format);

        } else if (Date.class.equals(type)) {
            openApiDataType = createOpenApiDataTypeString("date-time");

        } else if (type.isEnum()) {
            openApiDataType = new OpenApiDataTypeEnum(getEnumValues(type));

        } else if (type.isArray()) {
        	 	OpenApiDataTypeBuilder componentTypeBuilder = create(openApiReusableMap, getComponentType(type));
            openApiDataType = new OpenApiDataTypeArray(componentTypeBuilder.build());

        } else if (Map.class.isAssignableFrom(type) || Dictionary.class.isAssignableFrom(type)) {
            openApiDataType = new OpenApiDataTypeMap(createElementTypeBuilder().build());

        } else if (Collection.class.isAssignableFrom(type)) {
        		openApiDataType = new OpenApiDataTypeArray(createElementTypeBuilder().build());

        } else {
            openApiDataType = createOpenApiDataTypeObject();
        }

		if (read != write) {
			if (read) {
				openApiDataType.setReadOnly(true);
				openApiDataType.setWriteOnly(null);

			} else if (write) {
				openApiDataType.setWriteOnly(true);
				openApiDataType.setReadOnly(null);

			} else {
				openApiDataType.setReadOnly(null);
				openApiDataType.setWriteOnly(null);
			}
		}

		openApiDataType.setNullable(nullable);

		if (requiresMetadata(openApiDataType)) {
			openApiDataType.setName(isNotBlank(name) ? name : type.getName());
			openApiDataType.setDescription(isNotBlank(description) ? description : null);
			openApiDataType.setDefaultValue(isNotBlank(defaultValue) ? defaultValue : null);
		}

		return openApiDataType;
	}

	private OpenApiDataType createOpenApiDataTypeNumber(String type, String format) {

		return new OpenApiDataTypeNumber(type, format)
							.setMinimum(minimum)
							.setExclusiveMinimum(exclusiveMinimum)
							.setMaximum(maximum)
							.setExclusiveMaximum(exclusiveMaximum);
	}

	private OpenApiDataType createOpenApiDataTypeString(String format) {
		OpenApiDataTypeString openApiDataTypeString = new OpenApiDataTypeString(format)
																	.setMinLength(minimum)
																	.setMaxLength(maximum);

		if (password) {
			openApiDataTypeString.setFormat("password");
		}

		return openApiDataTypeString;
	}

	private List<String> getEnumValues(Class<?> dataTypeClass) {
        List<String> enumValues = new ArrayList<>();
        for (Object constant : dataTypeClass.getEnumConstants()) {
        		enumValues.add(constant.toString());
        }
        return enumValues;
    }

	private OpenApiDataTypeBuilder createElementTypeBuilder() {

		if (elementType != null) {
			return create(openApiReusableMap, elementType);
		}

		return create(openApiReusableMap, Object.class, annotations);
	}

	private OpenApiDataType createOpenApiDataTypeObject() {

		boolean reusable = false;

		Class<?> dataTypeClass = type;
		String className = dataTypeClass.getName();

		if (isAnnotationPresent(ApiModel.class, annotations)) {
			if (openApiReusableMap.containsComponentRef(ComponentType.SCHEMA, className)) {
				return new OpenApiDataTypeRef(openApiReusableMap.getComponentRefByName(ComponentType.SCHEMA, className));
			}
			// otherwise add it when its been mapped
			ApiModel apiModelAnn = findAnnotation(ApiModel.class, annotations);

			String modelName = apiModelAnn.name();
			if (isNotBlank(modelName)) {
				className = modelName;
			}

			String description = apiModelAnn.value();
			if (isNotBlank(description)) {
				this.setDescription(description);
			}

			reusable = true;
		} else {

			/**
			 * Legacy Annotation Support
			 * TODO Remove when legacy annotation support is no longer required.
			 */
			String description = findLegacyDescription(annotations);
			if (isNotBlank(description)) {
				this.setDescription(description);
			}
		}

		OpenApiDataTypeObject apenApiDataTypeObject = new OpenApiDataTypeObject(className).setDescription(description);

		while (dataTypeClass != null && !Object.class.equals(dataTypeClass)) {

            Field[] fields = dataTypeClass.getDeclaredFields();
            for (Field field : fields) {
                String name = field.getName();
                int modifiers = field.getModifiers();
                Class<?> fieldType = field.getType();
                Annotation[] fieldAnnotations = field.getDeclaredAnnotations();

                if (Modifier.isStatic(modifiers) || fieldType.isSynthetic()) {
                    continue;
                }

                OpenApiDataTypeBuilder fieldTypeBuilder = create(openApiReusableMap, fieldType, fieldAnnotations)
                															.setName(name);

                ApiProperty apiPropertyAnn = field.getAnnotation(ApiProperty.class);

                if (apiPropertyAnn != null) {
                		fieldTypeBuilder.setDescription(apiPropertyAnn.value())
                						.setRequired(apiPropertyAnn.required())
                						.setFormat(apiPropertyAnn.format())
                						.setMinimum(apiPropertyAnn.minimum())
                						.setExclusiveMinimum(apiPropertyAnn.exclusiveMinimum())
                						.setMaximum(apiPropertyAnn.maximum())
                						.setExclusiveMaximum(apiPropertyAnn.exclusiveMaximum())
                						.setNullable(apiPropertyAnn.nullable())
                						.setRead(apiPropertyAnn.read())
                						.setWrite(apiPropertyAnn.write())
                						.setPassword(apiPropertyAnn.password());
                } else {

					/**
					 * Legacy Annotation Support
					 * TODO Remove when legacy annotation support is no longer required.
					 */
					String description = findLegacyDescription(annotations);
					if (isNotBlank(description)) {
						fieldTypeBuilder.setDescription(description);
					}
                }

                if (fieldType.isArray() || Collection.class.isAssignableFrom(fieldType)) {
                		Class<?> componentType = getComponentType(field.getGenericType());
                		fieldTypeBuilder.setElementType(componentType);
                }

                apenApiDataTypeObject.addProperty(fieldTypeBuilder.build());

            }
            dataTypeClass = dataTypeClass.getSuperclass();
        }

		if (reusable) {
			return new OpenApiDataTypeRef(openApiReusableMap.getComponentRef(ComponentType.SCHEMA, apenApiDataTypeObject));
		}

		return apenApiDataTypeObject;
	}

    private Class<?> getComponentType(Type type) {

        if (type instanceof Class<?>) {
            Class<?> clazz = (Class<?>) type;
            if (clazz.isArray()) {
                return clazz.getComponentType();
            } else {
                return clazz;
            }
        } else if (type instanceof GenericArrayType) {
            Type compType = ((GenericArrayType) type).getGenericComponentType();
            return getComponentType(compType);
        } else if (type instanceof ParameterizedType) {
            ParameterizedType paramType = (ParameterizedType) type;

            Type[] actualTypeArguments = paramType.getActualTypeArguments();
            for (Type t : actualTypeArguments) {
                Class<?> ct = getComponentType(t);
                if (ct != null) {
                    return ct;
                }
            }
        } else if (type instanceof WildcardType) {
            // Upper bound is stronger than lower, so return the this one, if defined...
            Type[] upperBounds = ((WildcardType) type).getUpperBounds();
            for (Type upperBound : upperBounds) {
                Class<?> ubt = getComponentType(upperBound);
                if (ubt != null) {
                    return ubt;
                }
            }
            Type[] lowerBounds = ((WildcardType) type).getLowerBounds();
            for (Type lowerBound : lowerBounds) {
                Class<?> lbt = getComponentType(lowerBound);
                if (lbt != null) {
                    return lbt;
                }
            }
        }

        return null;
    }

    private boolean requiresMetadata(OpenApiDataType openApiDataType) {
    		return !(openApiDataType instanceof OpenApiDataTypeRef);
    }
}
