/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.doc.openapi.spec.datatype;

import org.amdatu.web.rest.doc.openapi.spec.OpenApiDataType;

public class OpenApiDataTypeString extends OpenApiDataType {

	private String format; // see https://swagger.io/docs/specification/data-models/data-types/#string
	private Integer minLength;
	private Integer maxLength;

	public OpenApiDataTypeString() {
		super("string");
		this.format = null;
		this.minLength = null;
		this.maxLength = null;
	}

	public OpenApiDataTypeString(String format) {
		super("string");
		this.format = format;
		this.minLength = null;
		this.maxLength = null;
	}

	public String getFormat() {
		return format;
	}

	public OpenApiDataTypeString setFormat(String format) {
		this.format = format;
		return this;
	}

	public Integer getMinLength() {
		return minLength;
	}

	public OpenApiDataTypeString setMinLength(Integer minLength) {
		this.minLength = minLength;
		return this;
	}

	public Integer getMaxLength() {
		return maxLength;
	}

	public OpenApiDataTypeString setMaxLength(Integer maxLength) {
		this.maxLength = maxLength;
		return this;
	}

	@Override
	public String toString() {
		return "OpenApiDataTypeString [format=" + format + ", minLength=" + minLength + ", maxLength=" + maxLength
				+ ", name=" + name + ", type=" + type + ", description=" + description + ", defaultValue="
				+ defaultValue + ", flaggedRequired=" + flaggedRequired + "]";
	}
}
