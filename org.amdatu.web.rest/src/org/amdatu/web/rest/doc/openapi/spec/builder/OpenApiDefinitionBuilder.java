/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.doc.openapi.spec.builder;

import static org.amdatu.web.rest.doc.openapi.Constants.DEFAULT_MEDIA_TYPE;
import static org.amdatu.web.rest.doc.openapi.spec.builder.AnnotationUtil.findAnnotation;
import static org.amdatu.web.rest.doc.openapi.spec.builder.BuilderTools.filterUrl;
import static org.amdatu.web.rest.doc.openapi.spec.builder.BuilderTools.findCurrentPath;
import static org.amdatu.web.rest.doc.openapi.spec.builder.BuilderTools.findCurrentServerUrl;
import static org.amdatu.web.rest.doc.openapi.spec.builder.BuilderTools.isBlank;
import static org.amdatu.web.rest.doc.openapi.spec.builder.BuilderTools.isEmpty;
import static org.amdatu.web.rest.doc.openapi.spec.builder.LegacyAnnotationProcessor.findLegacyDescription;
import static org.amdatu.web.rest.doc.openapi.spec.builder.LegacyAnnotationProcessor.isLegacyDescriptionPresent;
import static org.amdatu.web.rest.doc.openapi.spec.builder.OpenApiSecurityHelper.createOpenApiSecurityDefinition;
import static org.amdatu.web.rest.doc.openapi.spec.builder.OpenApiSecurityHelper.findOpenApiSecuritySchemes;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.amdatu.web.rest.doc.ApiHeaderDefinition;
import org.amdatu.web.rest.doc.ApiHeaderDefinitions;
import org.amdatu.web.rest.doc.ApiResponseDefinition;
import org.amdatu.web.rest.doc.ApiResponseDefinitions;
import org.amdatu.web.rest.doc.ApiSecurityDefinition;
import org.amdatu.web.rest.doc.ApiSecurityDefinitions;
import org.amdatu.web.rest.doc.ApiTagDefinition;
import org.amdatu.web.rest.doc.ApiTagDefinitions;
import org.amdatu.web.rest.doc.openapi.OpenApiConfigService;
import org.amdatu.web.rest.doc.openapi.RestResource;
import org.amdatu.web.rest.doc.openapi.TagFilterMethod;
import org.amdatu.web.rest.doc.openapi.spec.OpenApiContact;
import org.amdatu.web.rest.doc.openapi.spec.OpenApiDefinition;
import org.amdatu.web.rest.doc.openapi.spec.OpenApiExternalDocs;
import org.amdatu.web.rest.doc.openapi.spec.OpenApiInfo;
import org.amdatu.web.rest.doc.openapi.spec.OpenApiLicense;
import org.amdatu.web.rest.doc.openapi.spec.OpenApiOperation;
import org.amdatu.web.rest.doc.openapi.spec.OpenApiPath;
import org.amdatu.web.rest.doc.openapi.spec.OpenApiPathMap;
import org.amdatu.web.rest.doc.openapi.spec.OpenApiReusableMap;
import org.amdatu.web.rest.doc.openapi.spec.OpenApiSecurityDefinition;
import org.amdatu.web.rest.doc.openapi.spec.OpenApiSecurityScheme;
import org.amdatu.web.rest.doc.openapi.spec.OpenApiServer;
import org.amdatu.web.rest.doc.openapi.spec.OpenApiTag;

public class OpenApiDefinitionBuilder {

	private OpenApiConfigService openApiConfigService;
	private HttpServletRequest request;
	private List<String> defaultConsumes;
	private List<String> defaultProduces;
	private OpenApiReusableMap builderReusableMap = new OpenApiReusableMap();
	private List<OpenApiSecurityDefinition> openApiSecurityDefinitions = new ArrayList<>();
	private List<OpenApiSecurityScheme> globalOpenApiSecuritySchemes = new ArrayList<>();
	private List<OpenApiHeaderBuilder> openApiHeaderBuilders = new ArrayList<>();
	private List<OpenApiResponseBuilder> openApiResponseBuilders = new ArrayList<>();
	private List<OpenApiTag> openApiTagDefinitions = new ArrayList<>();
	private List<OpenApiOperationBuilder> openApiOperationBuilders = new ArrayList<>();
	private Set<String> allTagsFound = new HashSet<>();

	private OpenApiDefinition openApiDefinition;

	public static OpenApiDefinitionBuilder create(OpenApiConfigService openApiConfigService, HttpServletRequest request) {
		return new OpenApiDefinitionBuilder(openApiConfigService, request)
						.setDefaultProduces(createDefaultProduces(openApiConfigService))
						.setDefaultConsumes(createDefaultConsumes(openApiConfigService));
	}

	private OpenApiDefinitionBuilder(OpenApiConfigService openApiConfigService, HttpServletRequest request) {
		super();
		this.openApiConfigService = openApiConfigService;
		this.request = request;
	}

	public OpenApiConfigService getOpenApiConfigService() {
		return openApiConfigService;
	}

	public OpenApiDefinitionBuilder setOpenApiConfigService(OpenApiConfigService openApiConfigService) {
		this.openApiConfigService = openApiConfigService;
		return this;
	}

	public HttpServletRequest getRequest() {
		return request;
	}

	public OpenApiDefinitionBuilder setRequest(HttpServletRequest request) {
		this.request = request;
		return this;
	}

	public OpenApiReusableMap getOpenApiReusableMap() {
		return builderReusableMap;
	}

	public OpenApiDefinitionBuilder addOpenApiOperationBuilder(OpenApiOperationBuilder openApiOperationBuilder) {
		if (openApiOperationBuilder != null) {
			this.openApiOperationBuilders.add(openApiOperationBuilder);
		}
		return this;
	}

	public List<String> getDefaultConsumes() {
		return defaultConsumes;
	}

	public OpenApiDefinitionBuilder setDefaultConsumes(List<String> defaultConsumes) {
		this.defaultConsumes = defaultConsumes;
		return this;
	}

	public List<String> getDefaultProduces() {
		return defaultProduces;
	}

	public OpenApiDefinitionBuilder setDefaultProduces(List<String> defaultProduces) {
		this.defaultProduces = defaultProduces;
		return this;
	}

	public OpenApiDefinitionBuilder addDefinitions(List<RestResource> restResources) {
		/**
		 * The restResource is an unfiltered list, but we need to get the global definitions.
		 */

		if (restResources == null || restResources.isEmpty()) {
			return this;
		}

		for (RestResource restResource : restResources) {

			/**
			 * Look for security definitions annotation containing one or more security definitions.
			 */
			addSecurityDefinitions(restResource);

			/**
			 * Look for security schemes intended for global reuse.
			 */
			addGlobalSecuritySchemes(restResource);

			/**
			 * Look for header definitions.
			 */
			addHeaderDefinitions(restResource);

			/**
			 * Look for response definitions.
			 */
			addResponseDefinitions(restResource);

			/**
			 * Look for tag definitions
			 */
			addTagDefinitions(restResource);
		}
		return this;
	}

	private void addSecurityDefinitions(RestResource restResource) {
		// A part of finding reusable components

		ApiSecurityDefinition apiSecurityDefinition = findAnnotation(ApiSecurityDefinition.class, restResource.getAnnotations());

		if (apiSecurityDefinition != null) {
			OpenApiSecurityDefinition openApiSecurityDefinition = createOpenApiSecurityDefinition(apiSecurityDefinition);
			if (openApiSecurityDefinition != null) {
				openApiSecurityDefinitions.add(openApiSecurityDefinition);
			}
		}

		ApiSecurityDefinitions apiSecurityDefinitions = findAnnotation(ApiSecurityDefinitions.class, restResource.getAnnotations());

		if (apiSecurityDefinitions != null) {
			for (ApiSecurityDefinition securityDefinitionAnn : apiSecurityDefinitions.value()) {
				OpenApiSecurityDefinition openApiSecurityDefinition = createOpenApiSecurityDefinition(securityDefinitionAnn);
				if (openApiSecurityDefinition != null) {
					openApiSecurityDefinitions.add(openApiSecurityDefinition);
				}
			}
		}
	}

	private void addGlobalSecuritySchemes(RestResource restResource) {
		// A part of finding reusable components

		List<OpenApiSecurityScheme> resourceSecurity = findOpenApiSecuritySchemes(restResource.getAnnotations());

		if (resourceSecurity == null || resourceSecurity.isEmpty()) {
			return;
		}

		List<OpenApiSecurityScheme> foundSecuritySchemes = new ArrayList<>();

		for (OpenApiSecurityScheme openApiSecurityScheme : resourceSecurity) {
			if (openApiSecurityScheme.isGlobal()) {
				foundSecuritySchemes.add(openApiSecurityScheme);
			}
		}

		if (foundSecuritySchemes != null && !foundSecuritySchemes.isEmpty()) {
			globalOpenApiSecuritySchemes.addAll(foundSecuritySchemes);
		}

		/**
		 * RestEndpoints are not permitted to have global security schemes.
		 */
	}

	private void addHeaderDefinitions(RestResource restResource) {
		// A part of finding reusable components

		ApiHeaderDefinition apiHeaderDefinitionAnn = findAnnotation(ApiHeaderDefinition.class, restResource.getAnnotations());

		if (apiHeaderDefinitionAnn != null) {
			openApiHeaderBuilders.add(createOpenApiHeaderBuilder(apiHeaderDefinitionAnn));
		}

		ApiHeaderDefinitions apiHeaderDefinitionsAnn = findAnnotation(ApiHeaderDefinitions.class, restResource.getAnnotations());

		if (apiHeaderDefinitionsAnn != null) {
			for (ApiHeaderDefinition headerDefinitionAnn : apiHeaderDefinitionsAnn.value()) {
				openApiHeaderBuilders.add(createOpenApiHeaderBuilder(headerDefinitionAnn));
			}
		}

	}

	private OpenApiHeaderBuilder createOpenApiHeaderBuilder(ApiHeaderDefinition apiHeaderDefinitionAnn) {
		return OpenApiHeaderBuilder
					.create(builderReusableMap, apiHeaderDefinitionAnn.name())
					.setDescription(apiHeaderDefinitionAnn.description())
					.setValueType(apiHeaderDefinitionAnn.valueType())
					.setElementType(apiHeaderDefinitionAnn.elementType());
	}

	private void addResponseDefinitions(RestResource restResource) {
		// A part of finding reusable components

		ApiResponseDefinition apiResponseDefinitionAnn = findAnnotation(ApiResponseDefinition.class, restResource.getAnnotations());

		if (apiResponseDefinitionAnn != null) {
			openApiResponseBuilders.add(createOpenApiResponseBuilder(apiResponseDefinitionAnn));
		}

		ApiResponseDefinitions apiResponseDefinitionsAnn = findAnnotation(ApiResponseDefinitions.class, restResource.getAnnotations());

		if (apiResponseDefinitionsAnn != null) {
			for (ApiResponseDefinition responseDefinition : apiResponseDefinitionsAnn.value()) {
				openApiResponseBuilders.add(createOpenApiResponseBuilder(responseDefinition));
			}
		}
	}

	private OpenApiResponseBuilder createOpenApiResponseBuilder(ApiResponseDefinition apiResponseDefinition) {
		return OpenApiResponseBuilder
				.create(builderReusableMap)
				.setName(apiResponseDefinition.name())
				.setCode(apiResponseDefinition.code())
				.setDescription(apiResponseDefinition.description())
				.setProduces(apiResponseDefinition.produces())
				.setReturnType(apiResponseDefinition.returnType())
				.setElementType(apiResponseDefinition.elementType())
				.setHeaderNames(apiResponseDefinition.headerNames())
				.setHeaders(apiResponseDefinition.headers());
	}

	private void addTagDefinitions(RestResource restResource) {

		Class<?> restResourceClass = restResource.getRestResourceClass();

		// check for individually defined tags
		ApiTagDefinition apiTagDefinitionAnn = restResourceClass.getAnnotation(ApiTagDefinition.class);
		if (apiTagDefinitionAnn != null) {
			openApiTagDefinitions.add(new OpenApiTag(apiTagDefinitionAnn.name(), apiTagDefinitionAnn.description()));
		}

		// Check for globally defined tags
		ApiTagDefinitions apiTagDefinitionsAnn = restResourceClass.getAnnotation(ApiTagDefinitions.class);
		if (apiTagDefinitionsAnn != null) {
			for (ApiTagDefinition tagDefinitionAnn : apiTagDefinitionsAnn.value()) {
				openApiTagDefinitions.add(new OpenApiTag(tagDefinitionAnn.name(), tagDefinitionAnn.description()));
			}
		}

		/**
		 * Legacy Annotation Support
		 * TODO Remove when legacy annotation support is no longer required.
		 */
		if (apiTagDefinitionsAnn == null && apiTagDefinitionAnn == null && isLegacyDescriptionPresent(restResourceClass.getAnnotations())) {
			String tagName = restResource.getResourceUri();
			String description = findLegacyDescription(restResourceClass.getAnnotations());
			openApiTagDefinitions.add(new OpenApiTag(tagName, description));
		}
	}

	@Override
	public String toString() {
		return "OpenApiDefinitionBuilder [openApiConfigService=" + openApiConfigService + ", request=" + request
				+ ", defaultConsumes=" + defaultConsumes + ", defaultProduces=" + defaultProduces
				+ ", builderReusableMap=" + builderReusableMap + ", openApiSecurityDefinitions="
				+ openApiSecurityDefinitions + ", globalOpenApiSecuritySchemes=" + globalOpenApiSecuritySchemes
				+ ", openApiHeaderBuilders=" + openApiHeaderBuilders + ", openApiResponseBuilders="
				+ openApiResponseBuilders + ", openApiTagDefinitions=" + openApiTagDefinitions
				+ ", openApiOperationBuilders=" + openApiOperationBuilders + ", allTagsFound=" + allTagsFound
				+ ", openApiDefinition=" + openApiDefinition + "]";
	}

	public OpenApiDefinition build() {

		OpenApiInfo openApiInfo = createOpenApiInfo();

		openApiDefinition = new OpenApiDefinition()
									.setInfo(openApiInfo);

		if (!openApiInfo.isEnabled()) {
			// Its disabled. Show the bare minimum info.
			return openApiDefinition;
		}

		openApiDefinition.setComponents(builderReusableMap);
		openApiDefinition.setExternalDocs(createOpenApiExternalDocs()); // TODO The openAPI spec has this in the info level, but Swagger UI is expecting it in the root level.

		openApiDefinition.setDefaultConsumes(defaultConsumes);
		openApiDefinition.setDefaultProduces(defaultProduces);

		addServers();
		addReusableComponents();
		addOperations();

		// Add tag definitions last since operations may affect final list
		addTagDefinitions();

		return openApiDefinition;
	}

	private OpenApiInfo createOpenApiInfo() {

		OpenApiInfo openApiInfo = new OpenApiInfo()
									.setEnabled(openApiConfigService.isEnabled())
									.setTitle(openApiConfigService.getTitle())
									.setDescription(openApiConfigService.getDescription())
									.setVersion(openApiConfigService.getVersion());

		if (!openApiInfo.isEnabled()) {
			// Its disabled, stop here!
			return openApiInfo;
		}

		openApiInfo.setTermsOfService(filterUrl(request, openApiConfigService.getTermsOfServiceUrl()))
					.setLicense(createOpenApiLicense())
					.setContact(createOpenApiContact());

		// TODO The openAPI spec has this in the info level, but Swagger UI is expecting it in the root level.

		return openApiInfo;
	}

	private OpenApiExternalDocs createOpenApiExternalDocs() {

		String url = openApiConfigService.getExternalDocsUrl();

		if (isBlank(url)) {
			return null;
		}

		url = filterUrl(request, url);

		String description = openApiConfigService.getExternalDocsDescription();

		if (isBlank(description)) {
			description = "External Documentation";
		}

		return new OpenApiExternalDocs(url, description);
	}

	private OpenApiLicense createOpenApiLicense() {

		String url = openApiConfigService.getLicenseUrl();

		if (isBlank(url)) {
			return null;
		}

		url = filterUrl(request, url);

		String name = openApiConfigService.getLicenseName();

		return new OpenApiLicense(url, name);
	}

	private OpenApiContact createOpenApiContact() {

		String url = openApiConfigService.getSupportContactUrl();
		String email = openApiConfigService.getSupportContactEmail();

		if (isBlank(url) && isBlank(email)) {
			return null;
		}

		url = filterUrl(request, url);

		String name = openApiConfigService.getSupportContactName();

		return new OpenApiContact(url, email, name);
	}

	private void addServers() {

		Map<String, String> servers = openApiConfigService.getServers();

		if (servers == null || servers.isEmpty()) {
			openApiDefinition.addServer(new OpenApiServer(findCurrentServerUrl(request) + findCurrentPath(request), "Current Server URL"));
			return;
		}

		servers.forEach((k, v) -> openApiDefinition.addServer(createOpenApiServer(k, v, request)));
	}

	private OpenApiServer createOpenApiServer(String url, String description, HttpServletRequest request) {

		if (isBlank(url)) {
			url = findCurrentServerUrl(request);
		}

		url = filterUrl(request, url);

		if (isBlank(description)) {
			description = null;
		}

		return new OpenApiServer(url, description);
	}

	private void addReusableComponents() {

		if (openApiSecurityDefinitions != null && !openApiSecurityDefinitions.isEmpty()) {
			for (OpenApiSecurityDefinition openApiSecurityDefinition : openApiSecurityDefinitions) {
				builderReusableMap.getSecuritySchemeRef(openApiSecurityDefinition);
			}
		}

		if (globalOpenApiSecuritySchemes != null && !globalOpenApiSecuritySchemes.isEmpty()) {
			for (OpenApiSecurityScheme openApiSecurityScheme : globalOpenApiSecuritySchemes) {
				openApiDefinition.addSecurity(openApiSecurityScheme);
			}
		}

		if (openApiHeaderBuilders != null && !openApiHeaderBuilders.isEmpty()) {
			for (OpenApiHeaderBuilder openApiHeaderBuilder : openApiHeaderBuilders) {
				builderReusableMap.getHeaderRef(openApiHeaderBuilder.build());
			}
		}

		if (openApiResponseBuilders != null && !openApiResponseBuilders.isEmpty()) {
			for (OpenApiResponseBuilder openApiResponseBuilder : openApiResponseBuilders) {
				builderReusableMap.getResponseRef(openApiResponseBuilder.build());
			}
		}
	}

	private void addOperations() {

		TagFilterMethod tagFilterMethod = openApiConfigService.getTagFilterMethod();
		List<String> explicitTags = null;

		if (TagFilterMethod.EXPLICIT.equals(tagFilterMethod)) {
			explicitTags = openApiConfigService.getFilteredTags();
		}

		OpenApiPathMap openApiPathMap = openApiDefinition.getPaths();

		for (OpenApiOperationBuilder openApiOperationBuilder : openApiOperationBuilders) {

			OpenApiOperation openApiOperation = openApiOperationBuilder.setExplicitTags(explicitTags).build();

			List<String> operationTags = openApiOperation.getTags();
			if (operationTags != null) {
				allTagsFound.addAll(operationTags);
			}

			OpenApiPath openApiPath = openApiPathMap.get(openApiOperation.getEndpointPath());
			openApiPath.add(openApiOperation);
		}
	}

	private void addTagDefinitions() {
		for (OpenApiTag openApiTag : getFilteredTagDefinitions()) {
			openApiDefinition.addTag(openApiTag);
		}
	}

	private List<OpenApiTag> getFilteredTagDefinitions() {

		/**
		 * Filter unused definitions
		 */
		List<OpenApiTag> filteredTagDefinitions = openApiTagDefinitions
														.stream()
														.filter(d -> allTagsFound.stream().anyMatch(f -> f.equalsIgnoreCase(d.getName())))
														.collect(Collectors.toList());

		TagFilterMethod tagFilterMethod = openApiConfigService.getTagFilterMethod();
		Set<String> filteredTags = new HashSet<>(openApiConfigService.getFilteredTags());

		if (TagFilterMethod.NONE.equals(tagFilterMethod) ||
				filteredTags == null ||
				filteredTags.isEmpty() ||
				openApiTagDefinitions == null ||
				openApiTagDefinitions.isEmpty()) {
			return filteredTagDefinitions;
		}

		if (TagFilterMethod.INCLUDE.equals(tagFilterMethod)) {
			/**
			 * The operations have already been filtered, so adding
			 * all of the tags found will include the tag definitions
			 * on the included operations.
			 */
			filteredTags.addAll(allTagsFound);
		}

		List<OpenApiTag> matchedTags = filteredTagDefinitions
													.stream()
													.filter(d -> filteredTags.stream().anyMatch(f -> f.equalsIgnoreCase(d.getName())))
													.collect(Collectors.toList());

		if (TagFilterMethod.EXCLUDE.equals(tagFilterMethod)) {
			filteredTagDefinitions.removeAll(matchedTags);

		} else {
			filteredTagDefinitions.retainAll(matchedTags);
		}

		return filteredTagDefinitions;
	}

	private static List<String> createDefaultConsumes(OpenApiConfigService openApiConfigService) {

		List<String> consumes = openApiConfigService.getDefaultConsumes();

		if (isEmpty(consumes)) {
			return Collections.singletonList(DEFAULT_MEDIA_TYPE);
		}

		return consumes;
	}

	private static List<String> createDefaultProduces(OpenApiConfigService openApiConfigService) {

		List<String> produces = openApiConfigService.getDefaultProduces();

		if (isEmpty(produces)) {
			return Collections.singletonList(DEFAULT_MEDIA_TYPE);
		}

		return produces;
	}
}
