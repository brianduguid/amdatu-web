/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.doc.openapi.spec.datatype;

import org.amdatu.web.rest.doc.openapi.spec.OpenApiDataType;

public class OpenApiDataTypeNumber extends OpenApiDataType {

	private String format;
	private Integer minimum;
	private Boolean exclusiveMinimum;
	private Integer maximum;
	private Boolean exclusiveMaximum;

	public OpenApiDataTypeNumber(String type) {
		super(type);
		this.format = null;
		this.minimum = null;
		this.maximum = null;
	}

	public OpenApiDataTypeNumber(String type, String format) {
		super(type);
		this.format = format;
		this.minimum = null;
		this.maximum = null;
	}

	public String getFormat() {
		return format;
	}

	public OpenApiDataTypeNumber setFormat(String format) {
		this.format = format;
		return this;
	}

	public Integer getMinimum() {
		return minimum;
	}

	public OpenApiDataTypeNumber setMinimum(Integer minimum) {
		this.minimum = minimum;
		return this;
	}

	public Boolean getExclusiveMinimum() {
		return exclusiveMinimum;
	}

	public OpenApiDataTypeNumber setExclusiveMinimum(Boolean exclusiveMinimum) {
		this.exclusiveMinimum = exclusiveMinimum;
		return this;
	}

	public Integer getMaximum() {
		return maximum;
	}

	public OpenApiDataTypeNumber setMaximum(Integer maximum) {
		this.maximum = maximum;
		return this;
	}

	public Boolean getExclusiveMaximum() {
		return exclusiveMaximum;
	}

	public OpenApiDataTypeNumber setExclusiveMaximum(Boolean exclusiveMaximum) {
		this.exclusiveMaximum = exclusiveMaximum;
		return this;
	}

	@Override
	public String toString() {
		return "OpenApiDataTypeNumber [format=" + format + ", minimum=" + minimum + ", exclusiveMinimum="
				+ exclusiveMinimum + ", maximum=" + maximum + ", exclusiveMaximum=" + exclusiveMaximum + ", name="
				+ name + ", type=" + type + ", description=" + description + ", defaultValue=" + defaultValue
				+ ", flaggedRequired=" + flaggedRequired + "]";
	}
}
