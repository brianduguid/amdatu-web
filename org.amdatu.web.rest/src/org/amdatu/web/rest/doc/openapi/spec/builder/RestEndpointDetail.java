/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.doc.openapi.spec.builder;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.amdatu.web.rest.doc.openapi.RestEndpoint;
import org.amdatu.web.rest.doc.openapi.RestResource;
import org.amdatu.web.rest.doc.openapi.spec.OpenApiSecurityScheme;

public class RestEndpointDetail implements RestEndpoint {

	private String endpointPath;
	private String httpMethod;
	private Method restMethod;
	private RestResource parentRestResource;
	private Annotation[] annotations;
	private List<String> produces;
	private List<String> consumes;
	private List<String> tags;
	private List<OpenApiSecurityScheme> security;
	private RestEndpointDetail locatorDetail;

	@Override
	public String getEndpointPath() {
		return endpointPath;
	}

	public RestEndpointDetail setEndpointPath(String endpointPath) {
		this.endpointPath = endpointPath;
		return this;
	}

	@Override
	public String getHttpMethod() {
		return httpMethod;
	}

	public RestEndpointDetail setHttpMethod(String httpMethod) {
		this.httpMethod = httpMethod;
		return this;
	}

	@Override
	public Method getRestMethod() {
		return restMethod;
	}

	public RestEndpointDetail setRestMethod(Method restMethod) {
		this.restMethod = restMethod;
		return this;
	}

	@Override
	public RestResource getParentRestResource() {
		return parentRestResource;
	}

	public RestEndpointDetail setParentRestResource(RestResource parentRestResource) {
		this.parentRestResource = parentRestResource;
		return this;
	}

	@Override
	public boolean isAnnotationPresent(Class<? extends Annotation> annotationClass) {
		return restMethod.isAnnotationPresent(annotationClass);
	}

	@Override
	public Annotation[] getAnnotations() {
		return annotations;
	}

	public RestEndpointDetail setAnnotations(Annotation[] annotations) {
		this.annotations = annotations;
		return this;
	}

	@Override
	public List<String> getProduces() {
		return new ArrayList<>(produces);
	}

	public RestEndpointDetail setProduces(List<String> produces) {
		this.produces = produces;
		return this;
	}

	@Override
	public List<String> getConsumes() {
		return new ArrayList<>(consumes);
	}

	public RestEndpointDetail setConsumes(List<String> consumes) {
		this.consumes = consumes;
		return this;
	}

	@Override
	public List<String> getTags() {
		return new ArrayList<>(tags);
	}

	public RestEndpointDetail setTags(List<String> tags) {
		this.tags = tags;
		return this;
	}

	public List<OpenApiSecurityScheme> getSecurity() {
		return security;
	}

	public RestEndpointDetail setSecurity(List<OpenApiSecurityScheme> security) {
		this.security = security;
		return this;
	}

	public RestEndpointDetail getLocatorDetail() {
		return locatorDetail;
	}

	public RestEndpointDetail setLocatorDetail(RestEndpointDetail locatorDetail) {
		this.locatorDetail = locatorDetail;
		return this;
	}

	public boolean isLocator() {
		return httpMethod == null;
	}

	@Override
	public String toString() {
		return "RestEndpointDetail [endpointPath=" + endpointPath + ", httpMethod=" + httpMethod + ", restMethod="
				+ restMethod + ", parentRestResource=" + parentRestResource + ", annotations="
				+ Arrays.toString(annotations) + ", produces=" + produces + ", consumes=" + consumes + ", tags=" + tags
				+ ", security=" + security + ", locatorDetail=" + locatorDetail + "]";
	}
}
