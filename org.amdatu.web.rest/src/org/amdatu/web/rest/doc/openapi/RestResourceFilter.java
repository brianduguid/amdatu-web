/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.doc.openapi;

/**
 * Optional rest endpoint filter. By default, the tag filtering is provided by this
 * functionality; however, custom filtering can be performed using this interface.
 */
public interface RestResourceFilter {

	/**
	 * <p>Returns true if the REST resource is to be included in the OpenAPI definition.</p>
	 *
	 * @param restEndpoint REST resource to evaluate
	 * @return true to include the REST resource
	 */
	public boolean isIncluded(RestResource restResource);
}
