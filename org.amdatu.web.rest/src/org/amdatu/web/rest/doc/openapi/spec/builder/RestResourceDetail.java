/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.doc.openapi.spec.builder;

import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.amdatu.web.rest.doc.openapi.RestEndpoint;
import org.amdatu.web.rest.doc.openapi.RestResource;
import org.amdatu.web.rest.doc.openapi.spec.OpenApiSecurityScheme;

public class RestResourceDetail implements RestResource {

	private String baseUri;
	private Class<?> resourceClass;
	private String resourceUri;
	private Annotation[] annotations;
	private List<String> produces;
	private List<String> consumes;
	private List<String> tags = new ArrayList<>();
	private List<RestEndpointDetail> restEndpointDetails;
	private List<OpenApiSecurityScheme> securitySchemes;

	public RestResourceDetail(String baseUri, Class<?> resourceClass, String resourceUri) {
		super();
		this.baseUri = baseUri;
		this.resourceClass = resourceClass;
		this.resourceUri = resourceUri;
		this.annotations = resourceClass.getAnnotations();
	}

	@Override
	public String getBaseUri() {
		return baseUri;
	}

	@Override
	public Class<?> getRestResourceClass() {
		return resourceClass;
	}

	@Override
	public String getResourceUri() {
		return resourceUri;
	}

	@Override
	public boolean isAnnotationPresent(Class<? extends Annotation> annotationClass) {
		return resourceClass.isAnnotationPresent(annotationClass);
	}

	@Override
	public Annotation[] getAnnotations() {
		return annotations;
	}

	@Override
	public List<String> getProduces() {
		return new ArrayList<>(produces);
	}

	public RestResourceDetail setProduces(List<String> produces) {
		this.produces = produces;
		return this;
	}

	@Override
	public List<String> getConsumes() {
		return new ArrayList<>(consumes);
	}

	public RestResourceDetail setConsumes(List<String> consumes) {
		this.consumes = consumes;
		return this;
	}

	public void addTag(String tag) {
		tags.add(tag);
	}

	@Override
	public List<String> getTags() {
		return new ArrayList<>(tags);
	}

	public RestResourceDetail setTags(List<String> tags) {
		this.tags = tags;
		return this;
	}

	public List<RestEndpointDetail> getRestEndpointDetails() {
		return restEndpointDetails;
	}

	public RestResourceDetail setRestEndpointDetails(List<RestEndpointDetail> restEndpointDetails) {
		this.restEndpointDetails = restEndpointDetails;
		return this;
	}

	@Override
	public List<RestEndpoint> getRestEndpoints() {

		if (restEndpointDetails == null) {
			return Collections.emptyList();
		}

		return restEndpointDetails.stream()
									.map(red -> (RestEndpoint) red)
									.collect(Collectors.toList());
	}

	public List<OpenApiSecurityScheme> getSecuritySchemes() {
		if (securitySchemes == null) {
			return null;
		}
		return new ArrayList<>(securitySchemes);
	}

	public List<OpenApiSecurityScheme> getGlobalSecuritySchemes() {
		if (securitySchemes == null) {
			return null;
		}
		List<OpenApiSecurityScheme> global = securitySchemes.stream()
															.filter(s -> s.isGlobal())
															.collect(Collectors.toList());

		if (global.isEmpty() && !securitySchemes.isEmpty()) {
			/**
			 * No global when local was removed, return nothing
			 */
			return null;
		} else {
			return global;
		}
	}

	public List<OpenApiSecurityScheme> getLocalSecuritySchemes() {
		if (securitySchemes == null) {
			return null;
		}
		List<OpenApiSecurityScheme> local = securitySchemes.stream()
															.filter(s -> !s.isGlobal())
															.collect(Collectors.toList());

		if (local.isEmpty() && !securitySchemes.isEmpty()) {
			/**
			 * No local when global was removed, return nothing
			 */
			return null;
		} else {
			return local;
		}
	}

	public RestResourceDetail setSecuritySchemes(List<OpenApiSecurityScheme> securitySchemes) {
		this.securitySchemes = securitySchemes;
		return this;
	}

	@Override
	public String toString() {
		return "RestResourceDetail [baseUri=" + baseUri + ", resourceClass=" + resourceClass + ", resourceUri="
				+ resourceUri + ", produces=" + produces + ", consumes=" + consumes + ", tags=" + tags
				+ ", restEndpointDetails=" + restEndpointDetails + ", securitySchemes=" + securitySchemes + "]";
	}
}
