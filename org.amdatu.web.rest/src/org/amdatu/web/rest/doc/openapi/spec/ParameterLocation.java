/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.doc.openapi.spec;

import java.lang.annotation.Annotation;

import javax.ws.rs.CookieParam;
import javax.ws.rs.FormParam;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;

public enum ParameterLocation {

	PATH,
	QUERY,
	HEADER,
	FORM,
	COOKIE,
	CONTEXT,
	BODY;

	public static ParameterLocation valueOfAnnotation(Annotation annotation) {

		if (annotation instanceof PathParam) {
            return PATH;
        } else if (annotation instanceof QueryParam) {
            return QUERY;
        } else if (annotation instanceof HeaderParam) {
            return HEADER;
        } else if (annotation instanceof FormParam) {
            return FORM;
        } else if (annotation instanceof CookieParam) {
            return COOKIE;
        } else if (annotation instanceof Context) {
            return CONTEXT;
        } else {
        		return BODY;
        }
	}
}
