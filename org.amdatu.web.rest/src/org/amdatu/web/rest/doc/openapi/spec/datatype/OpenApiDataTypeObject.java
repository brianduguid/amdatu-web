/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.doc.openapi.spec.datatype;

import java.util.ArrayList;
import java.util.List;

import org.amdatu.web.rest.doc.openapi.spec.OpenApiDataType;
import org.amdatu.web.rest.doc.openapi.spec.OpenApiPropertyMap;

public class OpenApiDataTypeObject extends OpenApiDataType {

	private OpenApiPropertyMap properties = new OpenApiPropertyMap();
	private List<String> required;

	public OpenApiDataTypeObject(String name) {
		super("object");
		this.name = name;
	}

	public void addProperty(OpenApiDataType property) {
		properties.add(property);
		if (property.isFlaggedRequired()) {
			addRequiredFieldName(property.getName());
		}
	}

	private void addRequiredFieldName(String name) {
		if (required == null) {
			required = new ArrayList<>();
		}
		required.add(name);
	}

	public String getName() {
		return name;
	}

	@Override
	public OpenApiDataTypeObject setName(String name) {
		super.setName(name);
		return this;
	}

	@Override
	public OpenApiDataTypeObject setDescription(String description) {
		super.setDescription(description);
		return this;
	}

	public OpenApiPropertyMap getProperties() {
		return properties;
	}

	public List<String> getRequired() {
		return required;
	}

	@Override
	public String toString() {
		return "OpenApiDataTypeObject [properties=" + properties + ", required=" + required + ", name=" + name
				+ ", type=" + type + ", description=" + description + ", defaultValue=" + defaultValue
				+ ", flaggedRequired=" + flaggedRequired + "]";
	}
}
