/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.doc.openapi.spec.builder;

import static org.amdatu.web.rest.doc.openapi.Constants.DEFAULT_MEDIA_TYPE;
import static org.amdatu.web.rest.doc.openapi.spec.builder.AnnotationUtil.findAnnotation;
import static org.amdatu.web.rest.doc.openapi.spec.builder.AnnotationUtil.findConsumes;
import static org.amdatu.web.rest.doc.openapi.spec.builder.AnnotationUtil.findProduces;
import static org.amdatu.web.rest.doc.openapi.spec.builder.AnnotationUtil.isAnnotationPresent;
import static org.amdatu.web.rest.doc.openapi.spec.builder.BuilderTools.isEmpty;
import static org.amdatu.web.rest.doc.openapi.spec.builder.BuilderTools.isNotBlank;
import static org.amdatu.web.rest.doc.openapi.spec.builder.BuilderTools.isNotEmpty;
import static org.amdatu.web.rest.doc.openapi.spec.builder.LegacyAnnotationProcessor.findLegacyDescription;
import static org.amdatu.web.rest.doc.openapi.spec.builder.LegacyAnnotationProcessor.findLegacyNotes;
import static org.amdatu.web.rest.doc.openapi.spec.builder.LegacyAnnotationProcessor.findLegacyResponseMessages;
import static org.amdatu.web.rest.doc.openapi.spec.builder.LegacyAnnotationProcessor.findLegacyReturnType;
import static org.amdatu.web.rest.doc.openapi.spec.builder.LegacyAnnotationProcessor.isLegacyDescriptionPresent;
import static org.amdatu.web.rest.doc.openapi.spec.builder.LegacyAnnotationProcessor.isLegacyResponseTypePresent;

import java.lang.annotation.Annotation;
import java.lang.reflect.Parameter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.amdatu.web.rest.doc.ApiExternalDocs;
import org.amdatu.web.rest.doc.ApiOperation;
import org.amdatu.web.rest.doc.ApiResponse;
import org.amdatu.web.rest.doc.ApiResponseRefs;
import org.amdatu.web.rest.doc.ApiResponses;
import org.amdatu.web.rest.doc.openapi.spec.OpenApiCombineMethod;
import org.amdatu.web.rest.doc.openapi.spec.OpenApiContent;
import org.amdatu.web.rest.doc.openapi.spec.OpenApiDataType;
import org.amdatu.web.rest.doc.openapi.spec.OpenApiExternalDocs;
import org.amdatu.web.rest.doc.openapi.spec.OpenApiOperation;
import org.amdatu.web.rest.doc.openapi.spec.OpenApiParameter;
import org.amdatu.web.rest.doc.openapi.spec.OpenApiRequestBody;
import org.amdatu.web.rest.doc.openapi.spec.OpenApiResponse;
import org.amdatu.web.rest.doc.openapi.spec.OpenApiReusableMap;
import org.amdatu.web.rest.doc.openapi.spec.OpenApiSecurityScheme;
import org.amdatu.web.rest.doc.openapi.spec.ParameterLocation;
import org.amdatu.web.rest.doc.openapi.spec.datatype.OpenApiDataTypeCombined;
import org.amdatu.web.rest.doc.openapi.spec.datatype.OpenApiDataTypeObject;

public class OpenApiOperationBuilder {

	public static final String FORM_INPUT_MEDIA_TYPE = MediaType.APPLICATION_FORM_URLENCODED;
	public static final String FORM_INPUT_OBJECT_NAME = "FormInputObject";

	private OpenApiReusableMap openApiReusableMap;
	private RestResourceDetail restResourceDetail;
	private RestEndpointDetail restEndpointDetail;
	private String httpOperation;
	private String endpointPath;
	private Annotation[] annotations;
	private List<String> defaultConsumes;
	private List<String> defaultProduces;
	private List<String> explicitTags;

	private OpenApiOperation openApiOperation;

	public static OpenApiOperationBuilder create(OpenApiReusableMap openApiReusableMap, RestResourceDetail restResourceDetail, RestEndpointDetail restEndpointDetail) {
		return new OpenApiOperationBuilder(openApiReusableMap, restResourceDetail, restEndpointDetail)
						.setHttpOperation(restEndpointDetail.getHttpMethod())
						.setEndpointPath(restEndpointDetail.getEndpointPath())
						.setAnnotations(restEndpointDetail.getAnnotations());
	}

	private OpenApiOperationBuilder(OpenApiReusableMap openApiReusableMap, RestResourceDetail restResourceDetail, RestEndpointDetail restEndpointDetail) {
		super();
		this.openApiReusableMap = openApiReusableMap;
		this.restResourceDetail = restResourceDetail;
		this.restEndpointDetail = restEndpointDetail;
	}

	public RestResourceDetail getRestResourceDetail() {
		return restResourceDetail;
	}

	public OpenApiOperationBuilder setRestResourceDetail(RestResourceDetail restResourceDetail) {
		this.restResourceDetail = restResourceDetail;
		return this;
	}

	public RestEndpointDetail getRestEndpointDetail() {
		return restEndpointDetail;
	}

	public OpenApiOperationBuilder setRestEndpointDetail(RestEndpointDetail restEndpointDetail) {
		this.restEndpointDetail = restEndpointDetail;
		return this;
	}

	public String getHttpOperation() {
		return httpOperation;
	}

	public OpenApiOperationBuilder setHttpOperation(String httpOperation) {
		this.httpOperation = httpOperation;
		return this;
	}

	public String getEndpointPath() {
		return endpointPath;
	}

	public OpenApiOperationBuilder setEndpointPath(String endpointPath) {
		this.endpointPath = endpointPath;
		return this;
	}

	public Annotation[] getAnnotations() {
		return annotations;
	}

	public OpenApiOperationBuilder setAnnotations(Annotation[] annotations) {
		this.annotations = annotations;
		return this;
	}

	public OpenApiReusableMap getOpenApiReusableMap() {
		return openApiReusableMap;
	}

	public List<String> getDefaultConsumes() {
		return defaultConsumes;
	}

	public OpenApiOperationBuilder setDefaultConsumes(List<String> defaultConsumes) {
		this.defaultConsumes = defaultConsumes;
		return this;
	}

	public List<String> getDefaultProduces() {
		return defaultProduces;
	}

	public OpenApiOperationBuilder setDefaultProduces(List<String> defaultProduces) {
		this.defaultProduces = defaultProduces;
		return this;
	}

	public List<String> getExplicitTags() {
		return explicitTags;
	}

	public OpenApiOperationBuilder setExplicitTags(List<String> explicitTags) {
		this.explicitTags = explicitTags;
		return this;
	}

	@Override
	public String toString() {
		return "OpenApiOperationBuilder [openApiReusableMap=" + openApiReusableMap + ", restResourceDetail="
				+ restResourceDetail + ", restEndpointDetail=" + restEndpointDetail + ", httpOperation=" + httpOperation
				+ ", endpointPath=" + endpointPath + ", annotations=" + Arrays.toString(annotations)
				+ ", defaultConsumes=" + defaultConsumes + ", defaultProduces=" + defaultProduces + "]";
	}

	public OpenApiOperation build() {
		openApiOperation = new OpenApiOperation(endpointPath, httpOperation);

		// lets make sure there is at least a 200
		openApiOperation.addResponse(OpenApiResponse.createOkResponse());

		for (RestEndpointDetail endpointDetail : getRestEndpointBuildList()) {
			addMetadata(endpointDetail);
			checkForDepreciated(endpointDetail);
			addExternalDocs(endpointDetail);
			addSecurity(endpointDetail);
			addTags(endpointDetail);
			addProduces(endpointDetail);
			addConsumes(endpointDetail);
			addParameters(endpointDetail);
			addResponses(endpointDetail);
		}

		return openApiOperation;
	}

	private List<RestEndpointDetail> getRestEndpointBuildList() {

		List<RestEndpointDetail> restEndpointDetails = new ArrayList<>();

		RestEndpointDetail locator = restEndpointDetail.getLocatorDetail();
		if (locator != null) {
			restEndpointDetails.add(locator);
		}

		restEndpointDetails.add(restEndpointDetail);

		return restEndpointDetails;
	}

	private void addMetadata(RestEndpointDetail endpointDetail) {

		ApiOperation apiOperation = findAnnotation(ApiOperation.class, endpointDetail.getAnnotations());

		if (apiOperation != null) {

			String summary = apiOperation.value();
			if (isNotBlank(summary)) {
				openApiOperation.setSummary(summary);
			}

			String description = apiOperation.description();
			if (isNotBlank(description)) {
				openApiOperation.setDescription(description);
			}

			String operationId = apiOperation.operationId();
			if (isNotBlank(operationId)) {
				openApiOperation.setOperationId(operationId);
			}

		} else {

			/**
			 * Legacy Annotation Support
			 * TODO Remove when legacy annotation support is no longer required.
			 */
			String summary = findLegacyDescription(endpointDetail.getAnnotations());
			if (isNotBlank(summary)) {
				openApiOperation.setSummary(summary);
			}

			String description = findLegacyNotes(endpointDetail.getAnnotations());
			if (isNotBlank(description)) {
				openApiOperation.setDescription(description);
			}
		}
	}

	private void checkForDepreciated(RestEndpointDetail endpointDetail) {

		if (isAnnotationPresent(Deprecated.class, endpointDetail.getAnnotations())) {
			openApiOperation.setDeprecated(true);
		}
	}

	private void addExternalDocs(RestEndpointDetail endpointDetail) {

		ApiExternalDocs apiExternalDocsAnn = findAnnotation(ApiExternalDocs.class, endpointDetail.getAnnotations());

		if (apiExternalDocsAnn != null) {
			OpenApiExternalDocs externalDocs = new OpenApiExternalDocs(apiExternalDocsAnn.url(), apiExternalDocsAnn.description());
			openApiOperation.setExternalDocs(externalDocs);
		}
	}

	private void addTags(RestEndpointDetail endpointDetail) {

		Set<String> newTags = new HashSet<>(endpointDetail.getTags());

		if (!endpointDetail.isLocator()) {
			List<String> parentTags = restResourceDetail.getTags();
			newTags.addAll(parentTags);

			/**
			 * Legacy Annotation Support
			 * TODO Remove when legacy annotation support is no longer required.
			 */
			if ((parentTags == null || parentTags.isEmpty()) && isLegacyDescriptionPresent(restResourceDetail.getAnnotations())) {
				newTags.add(restResourceDetail.getResourceUri());
			}
		}

		if (explicitTags != null && !explicitTags.isEmpty()) {
			newTags.retainAll(explicitTags);
		}

		openApiOperation.addTags(newTags);
	}

	private void addSecurity(RestEndpointDetail endpointDetail) {

		List<OpenApiSecurityScheme> openApiSecuritySchemes = endpointDetail.getSecurity();

		if (openApiSecuritySchemes == null && !endpointDetail.isLocator()) {
			/**
			 * Empty is permitted, but if the list doesn't exist, lets see if they
			 * were setting the security requirement at the resource level.
			 */
			openApiSecuritySchemes = restResourceDetail.getLocalSecuritySchemes();
		}

		openApiOperation.addSecurity(openApiSecuritySchemes);
	}

	private void addProduces(RestEndpointDetail endpointDetail) {

		List<String> produces = findProduces(endpointDetail.getAnnotations());

		if (isEmpty(produces) && !endpointDetail.isLocator()) {
			// Need to find an alternative
			List<String> parentProduces = restResourceDetail.getProduces();

			if (isNotEmpty(parentProduces)) {
				produces = parentProduces;

			} else if (isNotEmpty(defaultProduces)) {
				produces = defaultProduces;

			} else {
				produces = Collections.singletonList(DEFAULT_MEDIA_TYPE);
			}
		}

		for (String produce : produces) {
			openApiOperation.addProduce(produce);
		}
	}

	private void addConsumes(RestEndpointDetail endpointDetail) {

		List<String> consumes = findConsumes(endpointDetail.getAnnotations());

		if (isEmpty(consumes) && !endpointDetail.isLocator()) {
			// Need to find an alternative
			List<String> parentConsumes = restResourceDetail.getConsumes();

			if (isNotEmpty(parentConsumes)) {
				consumes = parentConsumes;

			} else if (isNotEmpty(defaultConsumes)) {
				consumes = defaultConsumes;

			} else {
				consumes = Collections.singletonList(DEFAULT_MEDIA_TYPE);
			}
		}

		for (String consume : consumes) {
			openApiOperation.addConsume(consume);
		}
	}

	private void addParameters(RestEndpointDetail endpointDetail) {

		for (Parameter parameter : endpointDetail.getRestMethod().getParameters()) {

			Annotation[] paramAnnotations = parameter.getDeclaredAnnotations();

			OpenApiParameter openApiParameter = OpenApiParameterBuilder.create(openApiReusableMap)
																					.setParameter(parameter)
																					.setAnnotations(paramAnnotations)
																					.build();

			ParameterLocation parameterLocation = openApiParameter.getParameterLocation();

			if (ParameterLocation.CONTEXT.equals(parameterLocation)) {
				/**
				 * Do not report CONTEXT locations. They are automatically injected by JAX-RS.
				 * Need to build the parameter just in case the data type or parameter are
				 * reusable.
				 */
				return;

			} else if (ParameterLocation.BODY.equals(parameterLocation)) {
				/**
				 * We need to add a body parameter to the request body contents.
				 */
				for (String mediaType : openApiOperation.getConsumes()) {
					openApiOperation.addRequestBodyContent(OpenApiCombineMethod.ALLOF, new OpenApiContent(mediaType, openApiParameter.getSchema()));
				}

			}  else if (ParameterLocation.FORM.equals(parameterLocation)) {
				/**
				 * FORM is also handled differently.
				 */
				OpenApiDataType schema = openApiParameter.getSchema()
														.setName(openApiParameter.getName())
														.setDescription(openApiParameter.getDescription()); // move description back down from parameter level
				addFormInput(schema);

			} else {
				openApiOperation.addParameter(openApiParameter);
			}
		}
	}

	private void addFormInput(OpenApiDataType formField) {

		OpenApiRequestBody openApiRequestBody = openApiOperation.getRequestBody();

		if (openApiRequestBody == null) {
			/**
			 * No request body yet, lets create one by putting a form input object there
			 */
			openApiOperation.addRequestBodyContent(OpenApiCombineMethod.ONEOF, new OpenApiContent(FORM_INPUT_MEDIA_TYPE, new OpenApiDataTypeObject(FORM_INPUT_OBJECT_NAME)));
			openApiRequestBody = openApiOperation.getRequestBody();
		}

		OpenApiContent content = openApiRequestBody.getOpenApiContentByMediaType(FORM_INPUT_MEDIA_TYPE);

		OpenApiDataType formInputObject = content.getSchema();

		if (formInputObject instanceof OpenApiDataTypeCombined) {
			/**
			 * There are multiple FORM_INPUT_MEDIA_TYPE, lets find the one with FORM_INPUT_OBJECT_NAME
			 */
			for (OpenApiDataType dataType : ((OpenApiDataTypeCombined)formInputObject).getOneOf()) {
				if (FORM_INPUT_OBJECT_NAME.equals(dataType.getName())) {
					formInputObject = dataType;
					break;
				}
			}
		} else if (!FORM_INPUT_OBJECT_NAME.equals(formInputObject.getName())) {
			/**
			 * There is a FORM_INPUT_MEDIA_TYPE content already, but it isn't FORM_INPUT_OBJECT_NAME
			 */
			formInputObject = new OpenApiDataTypeObject(FORM_INPUT_OBJECT_NAME);
			openApiOperation.addRequestBodyContent(OpenApiCombineMethod.ONEOF, new OpenApiContent(FORM_INPUT_MEDIA_TYPE, formInputObject));
		}

		((OpenApiDataTypeObject) formInputObject).addProperty(formField);
	}

	private void addResponses(RestEndpointDetail endpointDetail) {

		ApiResponse apiResponseAnn = findAnnotation(ApiResponse.class, endpointDetail.getAnnotations());
		ApiResponses apiResponsesAnn = findAnnotation(ApiResponses.class, endpointDetail.getAnnotations());
		ApiResponseRefs apiResponseRefsAnn = findAnnotation(ApiResponseRefs.class, endpointDetail.getAnnotations());

		boolean noResponses = apiResponseAnn == null && apiResponsesAnn == null && apiResponseRefsAnn == null;

		if (noResponses && isLegacyResponseTypePresent(endpointDetail.getAnnotations())) {
			/**
			 * Legacy Annotation Support
			 * TODO Remove when legacy annotation support is no longer required.
			 */
			addLegacyResponses(endpointDetail);

			return; // done

		} else if (noResponses && !endpointDetail.isLocator()) {

			/**
			 * Lets try to figure out the return type if there is one and its not
			 * a jaxrs Response.
			 */
			Class<?> returnType = endpointDetail.getRestMethod().getReturnType();
			if (!void.class.equals(returnType) && !Response.class.equals(returnType)) {
				addOkResponse(returnType);
			}

			return; // done!
		}

		if (noResponses) {
			// Nothing left to do.
			return;
		}

		/**
		 * We have responses to process.
		 */

		if (apiResponseAnn != null) {
			openApiOperation.addResponse(createOpenApiResponseBuilder(apiResponseAnn).build());
		}

		if (apiResponsesAnn != null) {
			for (ApiResponse responseAnn : apiResponsesAnn.value()) {
				openApiOperation.addResponse(createOpenApiResponseBuilder(responseAnn).build());
			}
		}

		if (apiResponseRefsAnn != null) {
			for (String responseRef : apiResponseRefsAnn.value()) {
				/**
				 * We're expecting reusable responses that were defined with the
				 * response definitions annotation.
				 */
				OpenApiResponse reusableResponse = openApiReusableMap.getResponseRefByName(responseRef);
				if (reusableResponse != null) {
					openApiOperation.addResponse(reusableResponse);
				}
			}
		}
	}

	/**
	 * Convenience method to create an OpenApiResponseBuilder from an ApiResponse annotation.
	 *
	 * @param apiResponseAnn response annotation
	 * @return response builder
	 */
	private OpenApiResponseBuilder createOpenApiResponseBuilder(ApiResponse apiResponseAnn) {
		return OpenApiResponseBuilder
				.create(openApiReusableMap)
				.setCode(apiResponseAnn.code())
				.setDescription(apiResponseAnn.description())
				.setProduces(openApiOperation.getProduces())
				.setReturnType(apiResponseAnn.returnType())
				.setElementType(apiResponseAnn.elementType())
				.setHeaderNames(apiResponseAnn.headerNames())
				.setHeaders(apiResponseAnn.headers());
	}

	/**
	 * Legacy annotation support for ReturnType, ResponseMessage, and ResponseMessages.
	 *
	 * Legacy Annotation Support
	 * TODO Remove when legacy annotation support is no longer required.
	 *
	 * @param endpointDetail
	 */
	private void addLegacyResponses(RestEndpointDetail endpointDetail) {

		/**
		 * If the return type annotation exists, create an OK and add the class data type.
		 */
		Class<?> returnType = findLegacyReturnType(endpointDetail.getAnnotations());
		if (returnType != null) {
			addOkResponse(returnType);
		}

		Map<Integer, String> legacyResponsesMap = findLegacyResponseMessages(endpointDetail.getAnnotations());

		/**
		 * Process are response message annotations.
		 */
		for (Integer statusCode : legacyResponsesMap.keySet()) {

			OpenApiResponse openApiResponse = openApiOperation.getResponse(statusCode);

			if (openApiResponse == null) {
				openApiResponse = new OpenApiResponse(statusCode);
				openApiOperation.addResponse(openApiResponse);
			}

			openApiResponse.setDescription(legacyResponsesMap.get(statusCode));

		}
	}

	private void addOkResponse(Class<?> returnType) {

		OpenApiResponse openApiResponseOk = openApiOperation.getResponse(200);

		if (openApiResponseOk == null) {
			openApiResponseOk = OpenApiResponse.createOkResponse();
			openApiOperation.addResponse(openApiResponseOk);
		}

		if (returnType != null && !void.class.equals(returnType) && !Response.class.equals(returnType)) {

			OpenApiDataTypeBuilder openApiDataTypeBuilder = OpenApiDataTypeBuilder.create(openApiReusableMap, returnType);
			OpenApiDataType openApiDataType = openApiDataTypeBuilder.build();
			List<String> produces = openApiOperation.getProduces();
			for (String mediaType : produces) {
				openApiResponseOk.addContent(OpenApiCombineMethod.ONEOF, new OpenApiContent(mediaType, openApiDataType));
			}
		}
	}
}
