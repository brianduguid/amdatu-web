/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.doc.openapi.spec.datatype;

import java.util.ArrayList;
import java.util.List;

import org.amdatu.web.rest.doc.openapi.spec.OpenApiDataType;

public class OpenApiDataTypeCombined extends OpenApiDataType {

	public List<OpenApiDataType> oneOf;
	public List<OpenApiDataType> allOf;
	public List<OpenApiDataType> anyOf;
	public List<OpenApiDataType> not;

	public OpenApiDataTypeCombined() {
		super(null);
	}

	public List<OpenApiDataType> getOneOf() {
		return oneOf;
	}

	public OpenApiDataTypeCombined addOneOf(OpenApiDataType openApiDataType) {
		if (oneOf == null) {
			oneOf = new ArrayList<>();
		}

		oneOf.add(openApiDataType);
		return this;
	}

	public List<OpenApiDataType> getAllOf() {
		return allOf;
	}

	public OpenApiDataTypeCombined addAllOf(OpenApiDataType openApiDataType) {
		if (allOf == null) {
			allOf = new ArrayList<>();
		}

		allOf.add(openApiDataType);
		return this;
	}

	public List<OpenApiDataType> getAnyOf() {
		return anyOf;
	}

	public OpenApiDataTypeCombined addAnyOf(OpenApiDataType openApiDataType) {
		if (anyOf == null) {
			anyOf = new ArrayList<>();
		}

		anyOf.add(openApiDataType);
		return this;
	}

	public List<OpenApiDataType> getNot() {
		return not;
	}

	public OpenApiDataTypeCombined addNot(OpenApiDataType openApiDataType) {
		if (not == null) {
			not = new ArrayList<>();
		}

		not.add(openApiDataType);
		return this;
	}

	@Override
	public String toString() {
		return "OpenApiDataTypeCombined [oneOf=" + oneOf + ", allOf=" + allOf + ", anyOf=" + anyOf + ", not=" + not
				+ ", name=" + name + ", type=" + type + ", description=" + description + ", defaultValue="
				+ defaultValue + ", flaggedRequired=" + flaggedRequired + "]";
	}
}
