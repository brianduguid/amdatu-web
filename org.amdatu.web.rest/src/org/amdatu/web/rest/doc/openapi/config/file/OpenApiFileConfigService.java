/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.doc.openapi.config.file;

import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Dictionary;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.amdatu.web.rest.doc.ApiTagDefinition;
import org.amdatu.web.rest.doc.ApiTags;
import org.amdatu.web.rest.doc.openapi.OpenApiConfigService;
import org.amdatu.web.rest.doc.openapi.RestEndpoint;
import org.amdatu.web.rest.doc.openapi.RestEndpointFilter;
import org.amdatu.web.rest.doc.openapi.RestResource;
import org.amdatu.web.rest.doc.openapi.RestResourceFilter;
import org.amdatu.web.rest.doc.openapi.TagFilterMethod;
import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.cm.ManagedService;

public class OpenApiFileConfigService implements OpenApiConfigService, RestResourceFilter, RestEndpointFilter, ManagedService {

	private FileConfig m_fileConfig = new FileConfig();

	@Override
	public String getBaseUri() {
		return m_fileConfig.getBaseUri();
	}

	@Override
	public String getTitle() {
		return m_fileConfig.getTitle();
	}

	@Override
	public String getDescription() {
		return m_fileConfig.getDescription();
	}

	@Override
	public String getVersion() {
		return m_fileConfig.getVersion();
	}

	@Override
	public boolean isEnabled() {
		return m_fileConfig.isEnabled();
	}

	@Override
	public Map<String, String> getServers() {
		return m_fileConfig.getServers();
	}

	@Override
	public String getTermsOfServiceUrl() {
		return m_fileConfig.getTermsOfServiceUrl();
	}

	@Override
	public String getLicenseUrl() {
		return m_fileConfig.getLicenseUrl();
	}

	@Override
	public String getLicenseName() {
		return m_fileConfig.getLicenseName();
	}

	@Override
	public String getExternalDocsUrl() {
		return m_fileConfig.getExternalDocsUrl();
	}

	@Override
	public String getExternalDocsDescription() {
		return m_fileConfig.getExternalDocsDescription();
	}

	@Override
	public String getSupportContactUrl() {
		return m_fileConfig.getSupportContactUrl();
	}

	@Override
	public String getSupportContactEmail() {
		return m_fileConfig.getSupportContactEmail();
	}

	@Override
	public String getSupportContactName() {
		return m_fileConfig.getSupportContactName();
	}

	@Override
	public TagFilterMethod getTagFilterMethod() {
		return m_fileConfig.getTagFilterMethod();
	}

	@Override
	public List<String> getFilteredTags() {
		return m_fileConfig.getFilteredTags();
	}

	@Override
	public RestResourceFilter getRestResourceFilter() {
		return this;
	}

	@Override
	public RestEndpointFilter getRestMethodFilter() {
		return this;
	}

	@Override
	public List<String> getDefaultConsumes() {
		return m_fileConfig.getDefaultConsumes();
	}

	@Override
	public List<String> getDefaultProduces() {
		return m_fileConfig.getDefaultProduces();
	}

	@Override
	public boolean isIncluded(RestResource restResource) {

		if (restResource == null) {
			return false;
		}

		if (isFilterDisabled()) {
			return true;
		}

		boolean tagFound = isFilterTagFoundOnRestResource(restResource);

		if (TagFilterMethod.EXCLUDE.equals(m_fileConfig.getTagFilterMethod())) {
			return !tagFound;

		} else {
			return tagFound;
		}
	}

	@Override
	public boolean isIncluded(RestEndpoint restEndpoint) {

		if (restEndpoint == null) {
			return false;
		}

		if (isFilterDisabled()) {
			return true;
		}

		boolean tagFound = isFilterTagFoundOnRestEndpoint(restEndpoint);

		if (TagFilterMethod.EXCLUDE.equals(m_fileConfig.getTagFilterMethod())) {
			return !tagFound;

		} else {
			return tagFound;
		}
	}

	private boolean isFilterDisabled() {

		List<String> filteredTags = m_fileConfig.getFilteredTags();

		return
				TagFilterMethod.NONE.equals(m_fileConfig.getTagFilterMethod()) ||
				filteredTags == null ||
				filteredTags.isEmpty();
	}

	private boolean isFilterTagFoundOnRestResource(RestResource restResource) {

		TagFilterMethod tagFilterMethod = m_fileConfig.getTagFilterMethod();
		List<String> filteredTags = m_fileConfig.getFilteredTags();
		List<String> tagsFound = new ArrayList<>();
		List<String> tags = restResource.getTags();

		if (!tags.isEmpty()) {
			tagsFound.addAll(tags
								.stream()
								.filter(t -> filteredTags.contains(t))
								.collect(Collectors.toList())
								);
		}

		if (TagFilterMethod.INCLUDE.equals(tagFilterMethod) || TagFilterMethod.EXPLICIT.equals(tagFilterMethod)) {
			/**
			 * Tag filter method INCLUDE will exclude all resources and methods
			 * that are not specifically tagged to be included. We need to see if
			 * there are any endpoints on this resource that should be included.
			 */
			for (RestEndpoint restEndpoint : restResource.getRestEndpoints()) {
				tagsFound.addAll(findTags(restEndpoint.getAnnotations()));
			}
		}

		if (tagsFound.isEmpty()) {
			return false;
		}

		return filteredTags.stream().anyMatch(t -> tagsFound.contains(t));
	}

	private boolean isFilterTagFoundOnRestEndpoint(RestEndpoint restEndpoint) {

		List<String> filteredTags = m_fileConfig.getFilteredTags();
		List<String> tagsFound = new ArrayList<>();
		List<String> tags = restEndpoint.getTags();

		if (!tags.isEmpty()) {
			tagsFound.addAll(tags
								.stream()
								.filter(t -> filteredTags.contains(t))
								.collect(Collectors.toList())
								);
		}

		if (tagsFound.isEmpty()) {
			return false;
		}

		return filteredTags.stream().anyMatch(t -> tagsFound.contains(t));
	}

	@Override
	public void updated(Dictionary<String, ?> properties) throws ConfigurationException {

		if (properties == null) {
			return;
		}

		FileConfig tempFileConfig = new FileConfig();

		Enumeration<String> keys = properties.keys();
		Map<String, ConfigPair> tempServerMap = new HashMap<>();
		List<String> tempFilteredTags = new ArrayList<>();
		List<String> tempDefaultConsumes = new ArrayList<>();
		List<String> tempDefaultProduces = new ArrayList<>();

		while (keys.hasMoreElements()) {

			String key = keys.nextElement();
			Object value = properties.get(key);
			String string = value != null ? value.toString() : null;

			if (key == null || string == null) {
				continue;
			}

			if (CONFIG_KEY_BASE_URI.equalsIgnoreCase(key)) {
				tempFileConfig.setBaseUri(string);

			} else if (CONFIG_KEY_TITLE.equalsIgnoreCase(key)) {
				tempFileConfig.setTitle(string);

			} else if (CONFIG_KEY_DESCRIPTION.equalsIgnoreCase(key)) {
				tempFileConfig.setDescription(string);

			} else if (CONFIG_KEY_VERSION.equalsIgnoreCase(key)) {
				tempFileConfig.setVersion(string);

			} else if (CONFIG_KEY_ENABLED.equalsIgnoreCase(key)) {
				tempFileConfig.setEnabled(Boolean.valueOf(string));

			} else if (CONFIG_KEY_TERMS_OF_SERVICE_URL.equalsIgnoreCase(key)) {
				tempFileConfig.setTermsOfServiceUrl(string);

			} else if (CONFIG_KEY_LICENSE_NAME.equalsIgnoreCase(key)) {
				tempFileConfig.setLicenseName(string);

			} else if (CONFIG_KEY_LICENSE_URL.equalsIgnoreCase(key)) {
				tempFileConfig.setLicenseUrl(string);

			} else if (CONFIG_KEY_SUPPORT_CONTACT_URL.equalsIgnoreCase(key)) {
				tempFileConfig.setSupportContactUrl(string);

			} else if (CONFIG_KEY_SUPPORT_CONTACT_EMAIL.equalsIgnoreCase(key)) {
				tempFileConfig.setSupportContactEmail(string);

			} else if (CONFIG_KEY_SUPPORT_CONTACT_NAME.equalsIgnoreCase(key)) {
				tempFileConfig.setSupportContactName(string);

			} else if (CONFIG_KEY_EXTERNAL_DOCS_URL.equalsIgnoreCase(key)) {
				tempFileConfig.setExternalDocsUrl(string);

			} else if (CONFIG_KEY_EXTERNAL_DOCS_DESC.equalsIgnoreCase(key)) {
				tempFileConfig.setExternalDocsDescription(string);

			} else if (CONFIG_KEY_FILTERED_TAG_METHOD.equalsIgnoreCase(key)) {
				tempFileConfig.setTagFilterMethod(TagFilterMethod.findValueOf(string));

			} else if (key.startsWith(CONFIG_KEY_FILTERED_TAG_PREFIX)) {
				tempFilteredTags.add(string);

			} else if (key.startsWith(CONFIG_KEY_DEFAULT_CONSUMES_PREFIX)) {
				tempDefaultConsumes.add(string);

			} else if (key.startsWith(CONFIG_KEY_DEFAULT_PRODUCES_PREFIX)) {
				tempDefaultProduces.add(string);

			} else if (key.startsWith(CONFIG_KEY_API_SERVER_URL_PREFIX)) {
				setConfigPairKey(tempServerMap, CONFIG_KEY_API_SERVER_URL_PREFIX, key, string);

			} else if (key.startsWith(CONFIG_KEY_API_SERVER_DESC_PREFIX)) {
				setConfigPairValue(tempServerMap, CONFIG_KEY_API_SERVER_DESC_PREFIX, key, string);

			}
		}

		tempFileConfig.setServers(createConfigMap(tempServerMap))
						.setFilteredTags(tempFilteredTags)
						.setDefaultConsumes(tempDefaultConsumes)
						.setDefaultProduces(tempDefaultProduces);

		m_fileConfig = tempFileConfig;
	}

	private void setConfigPairKey(Map<String, ConfigPair> configPairMap, String cfgKeyPrefix, String cfgKey, String configPairKey) {

		if (configPairKey == null || configPairKey.length() == 0) {
			return;
		}

		String number = cfgKey.replace(cfgKeyPrefix, "");

		if (number.isEmpty()) {
			return;
		}

		ConfigPair apiServer = getConfigPair(configPairMap, number);

		apiServer.setKey(configPairKey);
	}

	private void setConfigPairValue(Map<String, ConfigPair> configPairMap, String cfgKeyPrefix, String cfgKey, String configPairValue) {

		if (configPairValue == null || configPairValue.length() == 0) {
			return;
		}

		String number = cfgKey.replace(cfgKeyPrefix, "");

		if (number.isEmpty()) {
			return;
		}

		ConfigPair apiServer = getConfigPair(configPairMap, number);

		apiServer.setValue(configPairValue);
	}

	private ConfigPair getConfigPair(Map<String, ConfigPair> configPairMap, String number) {

		ConfigPair configPair = configPairMap.get(number);

		if (configPair == null) {
			configPair = new ConfigPair();
			configPairMap.put(number, configPair);
		}

		return configPair;
	}

	private Map<String, String> createConfigMap(Map<String, ConfigPair> configPairMap) {

		if (configPairMap == null || configPairMap.isEmpty()) {
			return null;
		}

		Map<String, String> configMap = configPairMap.values()
				.stream()
				.filter(v -> v.getKey() != null)
				.collect(Collectors.toMap(ConfigPair::getKey, ConfigPair::getValue));

		return configMap;
	}

	private static List<String> findTags(Annotation[] annotations) {
		List<String> tags = new ArrayList<>();

		ApiTags apiTags = findAnnotation(ApiTags.class, annotations);
		if (apiTags != null) {
			tags.addAll(Arrays.asList(apiTags.value()));
		}

		ApiTagDefinition apiTagDefinition = findAnnotation(ApiTagDefinition.class, annotations);
		if (apiTagDefinition != null) {
			tags.add(apiTagDefinition.name());
		}

        return tags.isEmpty() ? Collections.emptyList() : tags;
    }

	private static <T extends Annotation> T findAnnotation(Class<T> type, Annotation[] annotations) {
		if (annotations == null || annotations.length == 0 || type == null) {
			return null;
		}
        for (Annotation ann : annotations) {
            if (type.isInstance(ann)) {
                return type.cast(ann);
            }
        }
        return null;
    }
}
