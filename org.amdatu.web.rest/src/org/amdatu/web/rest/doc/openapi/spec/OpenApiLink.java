/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.doc.openapi.spec;

import java.util.List;

public class OpenApiLink {

	private transient String name;
	private String operationId;
	private String operationRef;
	private List<OpenApiParameter> parameters;
	private OpenApiRequestBody requestBody;
	private String description;

	public OpenApiLink(String name) {
		super();
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public OpenApiLink setName(String name) {
		this.name = name;
		return this;
	}

	public String getOperationId() {
		return operationId;
	}

	public OpenApiLink setOperationId(String operationId) {
		this.operationId = operationId;
		return this;
	}

	public String getOperationRef() {
		return operationRef;
	}

	public OpenApiLink setOperationRef(String operationRef) {
		this.operationRef = operationRef;
		return this;
	}

	public List<OpenApiParameter> getParameters() {
		return parameters;
	}

	public OpenApiLink setParameters(List<OpenApiParameter> parameters) {
		this.parameters = parameters;
		return this;
	}

	public OpenApiRequestBody getRequestBody() {
		return requestBody;
	}

	public OpenApiLink setRequestBody(OpenApiRequestBody requestBody) {
		this.requestBody = requestBody;
		return this;
	}

	public String getDescription() {
		return description;
	}

	public OpenApiLink setDescription(String description) {
		this.description = description;
		return this;
	}

	@Override
	public String toString() {
		return "OpenApiLink [name=" + name + ", operationId=" + operationId + ", operationRef=" + operationRef
				+ ", parameters=" + parameters + ", requestBody=" + requestBody + ", description=" + description + "]";
	}
}
