/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.doc.openapi.spec;

import java.util.TreeMap;

public class OpenApiResponseMap extends TreeMap<Integer, OpenApiResponse> {

	private static final long serialVersionUID = 8564824220837947848L;

	public OpenApiResponseMap() {
		super();
	}

	/**
	 * Adds, or replaces, the response.
	 *
	 * @param response open api response to add
	 * @return true if the add is new and not a replacement
	 */
	public boolean add(OpenApiResponse response) {
		return this.put(response.getStatusCode(), response) == null;
	}
}
