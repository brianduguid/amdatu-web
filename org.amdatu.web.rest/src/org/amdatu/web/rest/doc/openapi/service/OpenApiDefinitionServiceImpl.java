/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.doc.openapi.service;

import static org.amdatu.web.rest.doc.openapi.Constants.DEFAULT_BASE_URI;
import static org.amdatu.web.rest.doc.openapi.spec.builder.AnnotationUtil.findConsumes;
import static org.amdatu.web.rest.doc.openapi.spec.builder.AnnotationUtil.findHttpOperation;
import static org.amdatu.web.rest.doc.openapi.spec.builder.AnnotationUtil.findPath;
import static org.amdatu.web.rest.doc.openapi.spec.builder.AnnotationUtil.findProduces;
import static org.amdatu.web.rest.doc.openapi.spec.builder.AnnotationUtil.findTags;
import static org.amdatu.web.rest.doc.openapi.spec.builder.AnnotationUtil.isHttpMethod;
import static org.amdatu.web.rest.doc.openapi.spec.builder.AnnotationUtil.isSubResourceLocator;
import static org.amdatu.web.rest.doc.openapi.spec.builder.BuilderTools.concatPaths;
import static org.amdatu.web.rest.doc.openapi.spec.builder.BuilderTools.isNotBlank;
import static org.amdatu.web.rest.doc.openapi.spec.builder.LegacyAnnotationProcessor.isLegacyNoDocumentationPresent;
import static org.amdatu.web.rest.doc.openapi.spec.builder.OpenApiSecurityHelper.findOpenApiSecuritySchemes;
import static org.osgi.service.log.LogService.LOG_DEBUG;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Path;

import org.amdatu.web.rest.doc.ApiExcluded;
import org.amdatu.web.rest.doc.openapi.OpenApiConfigService;
import org.amdatu.web.rest.doc.openapi.OpenApiDefinitionService;
import org.amdatu.web.rest.doc.openapi.RestEndpoint;
import org.amdatu.web.rest.doc.openapi.RestEndpointFilter;
import org.amdatu.web.rest.doc.openapi.RestResource;
import org.amdatu.web.rest.doc.openapi.RestResourceFilter;
import org.amdatu.web.rest.doc.openapi.spec.OpenApiReusableMap;
import org.amdatu.web.rest.doc.openapi.spec.OpenApiSecurityScheme;
import org.amdatu.web.rest.doc.openapi.spec.builder.OpenApiDefinitionBuilder;
import org.amdatu.web.rest.doc.openapi.spec.builder.OpenApiOperationBuilder;
import org.amdatu.web.rest.doc.openapi.spec.builder.RestEndpointDetail;
import org.amdatu.web.rest.doc.openapi.spec.builder.RestResourceDetail;
import org.osgi.framework.ServiceReference;
import org.osgi.service.log.LogService;

import com.google.gson.Gson;

/**
 * Entry point to the definition generation process.
 */
public class OpenApiDefinitionServiceImpl implements OpenApiDefinitionService {

	private volatile LogService m_logService;

	private Map<ServiceReference<?>, OpenApiConfigService> m_configServices = new HashMap<>();
	private Map<String, ServiceReference<?>> m_configServiceBaseUris = new HashMap<>();

	/*
	 * createRestResource
	 * @see org.amdatu.web.rest.doc.openapi.OpenApiDefinitionService#createRestResource(java.lang.String, java.lang.Class)
	 */
	@Override
	public RestResource createRestResource(String baseUri, Class<?> resourceClass) {

		if (resourceClass == null) {
			return null;
		}

		return createRestResourceDetail(baseUri, resourceClass);
	}

	/**
	 * Returns a REST resource detail based on the baseUri and resource class.
	 *
	 * @param baseUri REST application base uri
	 * @param resourceClass REST resource classs
	 * @return REST resource detail
	 */
	private RestResourceDetail createRestResourceDetail(String baseUri, Class<?> resourceClass) {

		Annotation[] annotations = resourceClass.getAnnotations();
		String resourceUri = concatPaths(baseUri, findPath(annotations));
		List<String> produces = findProduces(annotations);
		List<String> consumes = findConsumes(annotations);
		List<String> tags = findTags(annotations);

		RestResourceDetail restResourceDetail = new RestResourceDetail(
																	baseUri,
																	resourceClass,
																	resourceUri)
															.setProduces(produces)
															.setConsumes(consumes)
															.setTags(tags);

		List<RestEndpointDetail> restEndpointDetails = new ArrayList<>();
		restEndpointDetails.addAll(createRestEndpointDetails(restResourceDetail, resourceUri, resourceClass.getMethods()));

		restResourceDetail.setRestEndpointDetails(createRestEndpointDetails(restResourceDetail, resourceUri, resourceClass.getMethods()))
							.setSecuritySchemes(findOpenApiSecuritySchemes(annotations));

		return restResourceDetail;
	}

	/**
	 * Returns a list of REST endpoint details created from HTTP methods from the REST resource detail or a sub-resource.
	 *
	 * @param restResourceDetail parent REST resource detail
	 * @param resourceUri resource URI
	 * @param methods HTTP methods or sub-resource methods
	 * @return REST endpoint details list
	 */
	private List<RestEndpointDetail> createRestEndpointDetails(RestResourceDetail restResourceDetail, String resourceUri, Method... methods) {

		List<RestEndpointDetail> restEndpointDetails = new ArrayList<>();

		for (Method method : methods) {

			if (method.isAnnotationPresent(ApiExcluded.class) ||
					isLegacyNoDocumentationPresent(method.getAnnotations()) || // Legacy Annotation Support; TODO Remove when legacy annotation support is no longer required.
					(!isHttpMethod(method) && !isSubResourceLocator(method))) {
				/**
				 * Cannot include methods with ApiExcluded and it
				 * has to be a http method or have a path.
				 */
				continue;
			}

			String httpMethod = findHttpOperation(method);
			String endpointPath = concatPaths(resourceUri, findPath(method.getAnnotations()));
			Annotation[] annotations = method.getAnnotations();
			List<String> produces = findProduces(annotations);
			List<String> consumes = findConsumes(annotations);
			List<String> tags = findTags(annotations);
			List<OpenApiSecurityScheme> security = findOpenApiSecuritySchemes(annotations);

			RestEndpointDetail restEndpointDetail = new RestEndpointDetail()
															.setHttpMethod(httpMethod)
															.setRestMethod(method)
															.setEndpointPath(endpointPath)
															.setParentRestResource(restResourceDetail)
															.setAnnotations(annotations)
															.setProduces(produces)
															.setConsumes(consumes)
															.setTags(tags)
															.setSecurity(security);

			if (isSubResourceLocator(method)) {

				Class<?> subResourceClass = method.getReturnType();
				if (!Void.TYPE.equals(subResourceClass)) {

					String subResourcePath = concatPaths(resourceUri, findPath(method.getAnnotations()));
					List<RestEndpointDetail> subResourceDetails = createRestEndpointDetails(restResourceDetail, subResourcePath, subResourceClass.getMethods());

					for (RestEndpointDetail subRestEndpoint : subResourceDetails) {

						subRestEndpoint.setLocatorDetail(restEndpointDetail);
						if (isNotBlank(subRestEndpoint.getHttpMethod())) {
							restEndpointDetails.add(subRestEndpoint);
						}
					}
				}

			} else if (isNotBlank(restEndpointDetail.getHttpMethod())) {
				restEndpointDetails.add(restEndpointDetail);
			}
		}

		return restEndpointDetails;
	}

	/*
	 * createOpenApiDefinitionJson
	 * @see org.amdatu.web.rest.doc.openapi.OpenApiDefinitionService#createOpenApiDefinitionJson(javax.servlet.http.HttpServletRequest, java.util.List)
	 */
	@Override
	public String createOpenApiDefinitionJson(HttpServletRequest request, List<RestResource> restResources) {

		m_logService.log(LOG_DEBUG, "Creating OpenAPI definition.");

		if (request == null) {
			m_logService.log(LOG_DEBUG, "HTTP servlet request is null. Cannot continue");
			throw new IllegalArgumentException("HTTP servlet request cannot be null.");
		}

		if (restResources == null || restResources.isEmpty()) {
			m_logService.log(LOG_DEBUG, "Rest resource list is null or empty. Cannot continue");
			throw new IllegalArgumentException("Rest resource list is null or empty.");
		}

		String baseUri = restResources.stream().findFirst().get().getBaseUri();

		OpenApiConfigService openApiConfigService = getOpenApiConfigService(baseUri);
		OpenApiDefinitionBuilder openApiDefinitionBuilder = OpenApiDefinitionBuilder.create(openApiConfigService, request);

		if (!openApiConfigService.isEnabled()) {
			m_logService.log(LOG_DEBUG, "OpenAPI definition generation is disabled.");
			return new Gson().toJson(openApiDefinitionBuilder.build());
		}

		/**
		 * Always check for security definitions, global security schemes, and
		 * reusable components
		 */
		openApiDefinitionBuilder.addDefinitions(restResources);

		/**
		 * Filter the rest resources and the rest endpoints.
		 */
		List<RestResourceDetail> restResourceDetails = filter(openApiConfigService, restResources);

		/**
		 * Add the operations
		 */
		for (RestResourceDetail restResourceDetail : restResourceDetails) {

			m_logService.log(LOG_DEBUG, "Processing REST resource for operations: " + restResourceDetail.getRestResourceClass() + " " + restResourceDetail.getResourceUri());

			for (RestEndpointDetail restEndpointDetail : restResourceDetail.getRestEndpointDetails()) {

				m_logService.log(LOG_DEBUG, "Processing REST endpoint for operations: " + restEndpointDetail.getRestMethod().getName() + " " +
																						restEndpointDetail.getEndpointPath() + " " +
																						restEndpointDetail.getHttpMethod());

				OpenApiReusableMap openApiReusableMap = openApiDefinitionBuilder.getOpenApiReusableMap();
				OpenApiOperationBuilder openApiOperationBuilder = OpenApiOperationBuilder.create(openApiReusableMap, restResourceDetail, restEndpointDetail)
																		.setDefaultProduces(openApiDefinitionBuilder.getDefaultProduces())
																		.setDefaultConsumes(openApiDefinitionBuilder.getDefaultConsumes());

				openApiDefinitionBuilder.addOpenApiOperationBuilder(openApiOperationBuilder);
			}
		}

		return new Gson().toJson(openApiDefinitionBuilder.build());
	}

	/**
	 * Returns a REST resource detail list filtered by <code>@ApiExcluded</code> not present,
	 * <code>@Path</code> is present, and the REST resource filter returns true.  The REST resources
	 * are either cast or converted to REST resource details in the process.
	 *
	 * @param openApiConfigService default or base uri specific config service
	 * @param restResources REST resources to filter
	 * @return REST resource detail list
	 */
	private List<RestResourceDetail> filter(OpenApiConfigService openApiConfigService, List<RestResource> restResources) {

		RestResourceFilter restResourceFilter = openApiConfigService.getRestResourceFilter();

		List<RestResourceDetail> filtered = new ArrayList<>();

		for (RestResource restResource : restResources) {

			if (restResource == null) {
				continue;
			}

			/**
			 * It cannot have @ApiExcluded, it must have @Path,
			 * and it must pass the custom filter.
			 */
			if (!(restResource.isAnnotationPresent(ApiExcluded.class) ||
					isLegacyNoDocumentationPresent(restResource.getAnnotations())) && // Legacy Annotation Support; TODO Remove when legacy annotation support is no longer required.
					restResource.isAnnotationPresent(Path.class) &&
					restResourceFilter.isIncluded(restResource)) {

				/*
				 * They could have passed custom RestEndpoints, so we need the method
				 * to match it up with the RestResourceDetail's RestEndpointDetails
				 * that we either cast for actually create.
				 */
				List<Method> methods = filterRestEndpointMethods(openApiConfigService, restResource.getRestEndpoints());

				RestResourceDetail restResourceDetail = convertToRestResourceDetail(restResource);

				List<RestEndpointDetail> restEndpointDetails = restResourceDetail.getRestEndpointDetails()
																				.stream()
																				.filter(red -> methods.contains(red.getRestMethod()))
																				.collect(Collectors.toList());

				restResourceDetail.setRestEndpointDetails(restEndpointDetails);

				filtered.add(restResourceDetail);



			}
		}

		return filtered;
	}

	/**
	 * Returns a method list filtered by <code>@ApiExcluded</code> not present and
	 * the REST method filter returns true.
	 *
	 * @param openApiConfigService default or base uri specific config service
	 * @param restEndpoints REST endpoints to filter
	 * @return method list
	 */
	private List<Method> filterRestEndpointMethods(OpenApiConfigService openApiConfigService, List<RestEndpoint> restEndpoints) {

		RestEndpointFilter restMethodFilter = openApiConfigService.getRestMethodFilter();

		List<Method> restEndpointMethods = new ArrayList<>();

		for (RestEndpoint restEndpoint : restEndpoints) {

			/**
			 * It cannot have @ApiExcluded and it must pass the custom filter.
			 */
			if (!(restEndpoint.isAnnotationPresent(ApiExcluded.class) ||
					isLegacyNoDocumentationPresent(restEndpoint.getAnnotations())) && // Legacy Annotation Support; TODO Remove when legacy annotation support is no longer required.
					restMethodFilter.isIncluded(restEndpoint)) {

				restEndpointMethods.add(restEndpoint.getRestMethod());
			}
		}

		return restEndpointMethods;
	}

	/**
	 * Returns a REST resource detail after evaluating the REST resource object. The REST
	 * resource is either cast as or converted to a detail object.
	 *
	 * @param restResource REST resource to cast or convert
	 * @return REST resource detail
	 */
	private RestResourceDetail convertToRestResourceDetail(RestResource restResource) {

		if (restResource instanceof RestResourceDetail) {
			return (RestResourceDetail) restResource;

		} else {
			/**
			 * This service was not originally used to create the rest resource. We need the RestResourceDetail to continue.
			 * This is possible since we expose the RestResource and RestEndpoint interfaces.
			 */
			return createRestResourceDetail(restResource.getBaseUri(), restResource.getRestResourceClass());
		}
	}

	/**
	 * Returns the configuration service specific to the base URI or the default configuration service
	 * if one was not specified.
	 *
	 * @param baseUri current base URI
	 * @return base URI configuration service or default
	 */
	private OpenApiConfigService getOpenApiConfigService(String baseUri) {

		OpenApiConfigService openApiConfigService = m_configServices.get(m_configServiceBaseUris.get(baseUri));

		if (openApiConfigService == null) {
			return m_configServices.get(m_configServiceBaseUris.get(DEFAULT_BASE_URI));
		}

		return openApiConfigService;
	}

	/**
	 * Adds the configuration service to the maps.
	 *
	 * @param reference service reference
	 * @param openApiConfigService configuration service
	 */
	protected final void configServiceAdded(ServiceReference<?> reference, OpenApiConfigService openApiConfigService) {

		String baseUri = openApiConfigService.getBaseUri();

		m_configServices.put(reference, openApiConfigService);
		m_configServiceBaseUris.put(baseUri, reference);
	}

	/**
	 * Removes the configuration service from the maps.
	 *
	 * @param reference service reference
	 */
	protected final void configServiceRemoved(ServiceReference<?> reference) {

		String baseUri = m_configServiceBaseUris.entrySet().stream()
								  .filter(e -> e.getValue().equals(reference))
								  .map(Map.Entry::getKey)
								  .findFirst()
								  .orElse(null);

		m_configServices.remove(reference);
		m_configServiceBaseUris.remove(baseUri);
	}
}
