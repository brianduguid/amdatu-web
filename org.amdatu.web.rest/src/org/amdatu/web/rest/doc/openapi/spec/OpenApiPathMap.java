/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.doc.openapi.spec;

import java.util.TreeMap;

public class OpenApiPathMap extends TreeMap<String, OpenApiPath> {

	private static final long serialVersionUID = -2612179635868946434L;

	/**
	 * Adds, or replaces, the path for the endpoint path.
	 *
	 * @param path open api path to add
	 * @return true if the add is new and not a replacement
	 */
	public boolean add(OpenApiPath path) {
		return this.put(path.getEndpointPath(), path) == null;
	}

	@Override
	public OpenApiPath get(Object key) {
		String endpointPath = key.toString();

		if (!endpointPath.startsWith("/")) {
			endpointPath = "/" + endpointPath;
		}

		OpenApiPath openApiPath =  super.get(endpointPath);
		if (openApiPath == null) {
			openApiPath = new OpenApiPath(endpointPath);
			add(openApiPath);
		}
		return openApiPath;
	}
}
