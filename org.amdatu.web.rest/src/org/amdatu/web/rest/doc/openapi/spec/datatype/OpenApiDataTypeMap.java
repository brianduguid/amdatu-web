/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.doc.openapi.spec.datatype;

import org.amdatu.web.rest.doc.openapi.spec.OpenApiDataType;

public class OpenApiDataTypeMap extends OpenApiDataType {

	private OpenApiDataType additionalProperties;

	public OpenApiDataTypeMap(OpenApiDataType additionalProperties) {
		super("object");
		this.additionalProperties = additionalProperties;
	}

	public OpenApiDataType getAdditionalProperties() {
		return additionalProperties;
	}

	public void setAdditionalProperties(OpenApiDataType additionalProperties) {
		this.additionalProperties = additionalProperties;
	}

	@Override
	public String toString() {
		return "OpenApiDataTypeMap [additionalProperties=" + additionalProperties + ", name=" + name + ", type=" + type
				+ ", description=" + description + ", defaultValue=" + defaultValue + ", flaggedRequired="
				+ flaggedRequired + "]";
	}
}
