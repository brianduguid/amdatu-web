/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.doc.openapi.spec;

import static org.amdatu.web.rest.doc.openapi.Constants.OPEN_API_VERSION;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class OpenApiDefinition {

	private String openapi = OPEN_API_VERSION;
	private OpenApiInfo info;
	private String basePath;
	private Set<OpenApiTag> tags;
	private Set<OpenApiServer> servers;
	private OpenApiPathMap paths = new OpenApiPathMap();
	private List<OpenApiSecurityScheme> security; // global security requirement
	private OpenApiExternalDocs externalDocs; // TODO Swagger UI has it here, but the specs have it in info
	private OpenApiReusableMap components = new OpenApiReusableMap();
	private transient List<String> defaultConsumes;
	private transient List<String> defaultProduces;

	public String getOpenapi() {
		return openapi;
	}

	public OpenApiDefinition setOpenapi(String openapiVersion) {
		this.openapi = openapiVersion;
		return this;
	}

	public OpenApiInfo getInfo() {
		return info;
	}

	public OpenApiDefinition setInfo(OpenApiInfo info) {
		this.info = info;
		return this;
	}

	public String getBasePath() {
		return basePath;
	}

	public OpenApiDefinition setBasePath(String basePath) {
		this.basePath = basePath;
		return this;
	}

	public Set<OpenApiTag> getTags() {
		return tags;
	}

	public OpenApiDefinition addTag(OpenApiTag tag) {
		if (tags == null) {
			tags = new TreeSet<>();
		}

		// Lets not add more than one tag with the same name
		if (tags.stream().anyMatch(t -> t.getName().equalsIgnoreCase(tag.getName()))) {
			return this;
		}

		this.tags.add(tag);
		return this;
	}

	public OpenApiDefinition removeTag(OpenApiTag tag) {
		if (tags == null || tags.isEmpty() || tag == null) {
			return this;
		}

		this.tags.remove(tag);
		return this;
	}

	public Set<OpenApiServer> getServers() {
		return servers;
	}

	public OpenApiDefinition addServer(OpenApiServer server) {
		if (servers == null) {
			servers = new TreeSet<>();
		}

		// Lets not add more than one tag with the same name
		if (servers.stream().anyMatch(s -> s.getUrl().equalsIgnoreCase(server.getUrl()))) {
			return this;
		}

		this.servers.add(server);
		return this;
	}

	public OpenApiReusableMap getComponents() {
		return components;
	}

	public OpenApiDefinition setComponents(OpenApiReusableMap components) {
		this.components = components;
		return this;
	}

	public OpenApiPathMap getPaths() {
		return paths;
	}

	public OpenApiDefinition setPaths(OpenApiPathMap paths) {
		this.paths = paths;
		return this;
	}

	public List<OpenApiSecurityScheme> getSecurity() {
		return security;
	}

	public OpenApiDefinition addSecurity(OpenApiSecurityScheme openApiSecurityScheme) {
		if (security == null) {
			security = new ArrayList<>();
		}

		security.add(openApiSecurityScheme);
		return this;
	}

	public OpenApiExternalDocs getExternalDocs() {
		return externalDocs;
	}

	public OpenApiDefinition setExternalDocs(OpenApiExternalDocs externalDocs) {
		this.externalDocs = externalDocs;
		return this;
	}

	public List<String> getDefaultConsumes() {
		return defaultConsumes;
	}

	public void setDefaultConsumes(List<String> defaultConsumes) {
		this.defaultConsumes = defaultConsumes;
	}

	public List<String> getDefaultProduces() {
		return defaultProduces;
	}

	public void setDefaultProduces(List<String> defaultProduces) {
		this.defaultProduces = defaultProduces;
	}

	@Override
	public String toString() {
		return "OpenApiDefinition [openapi=" + openapi + ", info=" + info + ", basePath=" + basePath + ", tags=" + tags
				+ ", servers=" + servers + ", paths=" + paths + ", security=" + security + ", externalDocs="
				+ externalDocs + ", components=" + components + ", defaultConsumes=" + defaultConsumes
				+ ", defaultProduces=" + defaultProduces + "]";
	}
}
