/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.doc.openapi.spec;

import java.util.TreeMap;

public class OpenApiPath extends TreeMap<String, OpenApiOperation> {

	private static final long serialVersionUID = -1918487474961881299L;

	private transient String endpointPath;

	public OpenApiPath(String endpointPath) {
		super();
		this.endpointPath = endpointPath;
	}

	public String getEndpointPath() {
		return endpointPath;
	}

	public OpenApiPath setEndpointPath(String endpointPath) {
		this.endpointPath = endpointPath;
		return this;
	}

	/**
	 * Adds, or replaces, the operations for the operation type.
	 *
	 * @param operation open api operation to add
	 * @return true if the add is new and not a replacement
	 */
	public boolean add(OpenApiOperation operation) {
		return this.put(operation.getHttpOperation(), operation) == null;

	}

	@Override
	public OpenApiOperation get(Object key) {
		String httpOperation = key.toString().toLowerCase();
		return super.get(httpOperation);
	}
}
