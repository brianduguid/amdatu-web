/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.doc.openapi.spec.builder;

import static org.amdatu.web.rest.doc.openapi.spec.builder.AnnotationUtil.findAnnotation;
import static org.amdatu.web.rest.doc.openapi.spec.builder.BuilderTools.isBlank;

import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.amdatu.web.rest.doc.ApiSecurityDefinition;
import org.amdatu.web.rest.doc.ApiSecurityProperty;
import org.amdatu.web.rest.doc.ApiSecurityRequirements;
import org.amdatu.web.rest.doc.ApiSecurityScheme;
import org.amdatu.web.rest.doc.ApiSecuritySchemeGroup;
import org.amdatu.web.rest.doc.ApiSecurityType;
import org.amdatu.web.rest.doc.openapi.spec.OpenApiSecurityDefinition;
import org.amdatu.web.rest.doc.openapi.spec.OpenApiSecurityScheme;

public final class OpenApiSecurityHelper {

	public static OpenApiSecurityDefinition createOpenApiSecurityDefinition(ApiSecurityDefinition securityDefinitionAnn) {

		if (securityDefinitionAnn == null) {
			return null;
		}

		String name = securityDefinitionAnn.name();

		if (isBlank(name)) {
			return null;
		}

		ApiSecurityType apiSecurityType = securityDefinitionAnn.type();
		Map<String, String> securityPropertyMap = createSecurityPropertyMap(securityDefinitionAnn.properties());

		final OpenApiSecurityDefinition openApiSecurityDefinition = new OpenApiSecurityDefinition(apiSecurityType, name);

		switch(apiSecurityType) {
			case BASIC:
				openApiSecurityDefinition.put("scheme", "basic");
				break;

			case BEARER:
				openApiSecurityDefinition.put("scheme", "bearer");
				break;

			default:
				break;
		}

		securityPropertyMap.forEach((k, v) -> openApiSecurityDefinition.addSecurityProperty(k, v));



		return openApiSecurityDefinition;
	}

	public static List<OpenApiSecurityScheme> findOpenApiSecuritySchemes(Annotation[] annotations) {

		ApiSecurityRequirements apiSecurityRequirementsAnn = findAnnotation(ApiSecurityRequirements.class, annotations);

		if (apiSecurityRequirementsAnn == null) {
			return null;
		}

		List<OpenApiSecurityScheme> securitySchemeList = new ArrayList<>();

		boolean global = apiSecurityRequirementsAnn.global();
		ApiSecurityScheme[] apiSecuritySchemeAnns = apiSecurityRequirementsAnn.value();
		ApiSecuritySchemeGroup[] apiSecuritySchemeGroupAnns = apiSecurityRequirementsAnn.groups();

		for (ApiSecuritySchemeGroup apiSecuritySchemeGroupAnn : apiSecuritySchemeGroupAnns) {
			OpenApiSecurityScheme openApiSecurityScheme = new OpenApiSecurityScheme().setGlobal(global);
			for (ApiSecurityScheme apiSecuritySchemeAnn : apiSecuritySchemeGroupAnn.value()) {
				openApiSecurityScheme.addSecurityScheme(apiSecuritySchemeAnn.value(),
														Arrays.asList(apiSecuritySchemeAnn.scopes()));
			}
			securitySchemeList.add(openApiSecurityScheme);
		}

		for (ApiSecurityScheme apiSecuritySchemeAnn : apiSecuritySchemeAnns) {
			securitySchemeList.add(new OpenApiSecurityScheme()
											.addSecurityScheme(apiSecuritySchemeAnn.value(),
																Arrays.asList(apiSecuritySchemeAnn.scopes()))
											.setGlobal(global));
		}

		return securitySchemeList;
	}

	private static Map<String, String> createSecurityPropertyMap(ApiSecurityProperty[] apiSecurityProperties) {

		if (apiSecurityProperties == null || apiSecurityProperties.length == 0) {
			return Collections.emptyMap();
		}

		return Arrays.stream(apiSecurityProperties)
					.collect(Collectors.toMap(ApiSecurityProperty::key, ApiSecurityProperty::value));
	}
}
