/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.doc.openapi;

import javax.ws.rs.core.MediaType;

/**
 * Constants used for the OpenAPI service.
 */
public interface Constants {

    /**
     * <p>The version of the Open API specification.</p>
     *
     * <p>Value: <code>{@value}</code></p>
     */
    String OPEN_API_VERSION = "3.0.0";

    /**
     * <p>The default protocol scheme.</p>
     *
     * <p>Value: <code>{@value}</code></p>
     */
    String DEFAULT_SCHEME = "http";

    /**
     * <p>OSGi HTTP Whiteboard Context Name for the Amdatu OpenAPI implementation.</p>
     *
     * <p>Value: <code>{@value}</code></p>
     */
    String HTTP_WHITEBOARD_CONTEXT_NAME_OPENAPI = "org.amdatu.web.rest.doc.openapi";

    /**
     * <p>The default base URI keyword used to designate the default configuration service.</p>
     *
     * <p>Value: <code>{@value}</code></p>
     *
     * @see OpenApiConfigService
     */
    String DEFAULT_BASE_URI = "default";

    /**
     * <p>Default Web API title for the Swagger UI if not specified by the configuration service.</p>
     *
     * <p>Value: <code>{@value}</code></p>
     *
     * @see OpenApiConfigService
     */
    String DEFAULT_TITLE = "OpenAPI Based Documentation";

    /**
     * <p>Default Web API description for the Swagger UI if not specified by the configuration service.</p>
     *
     * <p><code>{@value}</code></p>
     *
     * @see OpenApiConfigService
     */
	String DEFAULT_DESCRIPTION = "Web API documentation using the OpenAPI version " + OPEN_API_VERSION + " specification.";

	/**
     * <p>Default Web API version for the Swagger UI if not specified by the configuration service.</p>
     *
     * <p>Value: <code>{@value}</code></p>
     *
     * @see OpenApiConfigService
     */
	String DEFAULT_VERSION = "unversioned";

	/**
     * <p>Default consumes and produces media type if not specified by the configuration service or annotation.</p>
     *
     * <p>Value: <code>{@value}</code></p>
     *
     * @see OpenApiConfigService
     */
	String DEFAULT_MEDIA_TYPE = MediaType.APPLICATION_JSON;
}
