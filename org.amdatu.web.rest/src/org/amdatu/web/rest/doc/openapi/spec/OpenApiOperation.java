/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.doc.openapi.spec;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class OpenApiOperation {

	private transient String endpointPath;
	private transient String httpOperation;
	private String summary;
	private String description;
	private String operationId;
	private Boolean deprecated;
	private List<OpenApiSecurityScheme> security;
	private List<String> tags;
	private transient List<String> consumes;
	private transient List<String> produces;
	private List<OpenApiParameter> parameters;
	private OpenApiRequestBody requestBody;
	private OpenApiResponseMap responses = new OpenApiResponseMap();
	private OpenApiExternalDocs externalDocs;

	public OpenApiOperation(String endpointPath, String httpOperation) {
		super();
		this.endpointPath = endpointPath;
		this.httpOperation = httpOperation.toLowerCase();
	}

	public String getEndpointPath() {
		return endpointPath;
	}

	public String getHttpOperation() {
		return httpOperation;
	}

	public String getSummary() {
		return summary;
	}

	public OpenApiOperation setSummary(String summary) {
		this.summary = summary;
		return this;
	}

	public String getDescription() {
		return description;
	}

	public OpenApiOperation setDescription(String description) {
		this.description = description;
		return this;
	}

	public String getOperationId() {
		return operationId;
	}

	public OpenApiOperation setOperationId(String operationId) {
		this.operationId = operationId;
		return this;
	}

	public Boolean getDeprecated() {
		return deprecated == null ? false : deprecated;
	}

	public void setDeprecated(Boolean deprecated) {
		this.deprecated = (deprecated == null || !deprecated.booleanValue()) ? null : true;
	}

	public List<OpenApiSecurityScheme> getSecurity() {
		return security;
	}

	public OpenApiOperation addSecurity(List<OpenApiSecurityScheme> openApiSecuritySchemes) {

		/*
		 * Empty lists are allowed, but nulls are not.
		 */
		if (openApiSecuritySchemes == null) {
			return this;
		}

		if (security == null) {
			security = new ArrayList<>();
		}

		security.addAll(openApiSecuritySchemes);
		return this;
	}

	public List<String> getTags() {
		return tags;
	}

	public OpenApiOperation addTags(Collection<String> newTags) {

		if (newTags == null || newTags.isEmpty()) {
			return this;
		}

		if (this.tags == null) {
			this.tags = new ArrayList<>();
		}

		List<String> filtered = newTags.stream()
										.filter(t -> !this.tags.contains(t))
										.collect(Collectors.toList());

		this.tags.addAll(filtered);
		return this;
	}

	public List<String> getConsumes() {
		return consumes;
	}

	public OpenApiOperation setConsumes(List<String> consumes) {
		this.consumes = consumes;
		return this;
	}

	public OpenApiOperation addConsume(String consume) {

		if (consume == null || consume.isEmpty()) {
			return this;
		}

		if (consumes == null) {
			consumes = new ArrayList<>();
		}

		if (!consumes.contains(consume)) {
			this.consumes.add(consume);
		}

		return this;
	}

	public List<String> getProduces() {
		return produces;
	}

	public OpenApiOperation setProduces(List<String> produces) {
		this.produces = produces;
		return this;
	}

	public OpenApiOperation addProduce(String produce) {

		if (produce == null || produce.isEmpty()) {
			return this;
		}

		if (produces == null) {
			produces = new ArrayList<>();
		}

		if (!produces.contains(produce)) {
			this.produces.add(produce);
		}

		return this;
	}

	public List<OpenApiParameter> getParameters() {
		return parameters;
	}

	public OpenApiOperation addParameter(OpenApiParameter parameter) {

		if (parameters == null) {
			parameters = new ArrayList<>();
		}
		this.parameters.add(parameter);
		return this;
	}

	public OpenApiRequestBody getRequestBody() {
		return requestBody;
	}

	public boolean addRequestBodyContent(OpenApiCombineMethod combineMethod, OpenApiContent content) {
		if (requestBody == null) {
			requestBody = new OpenApiRequestBody(endpointPath + "[" + httpOperation + "]");  // TODO name too specific
		}
		return requestBody.addContent(combineMethod, content);
	}

	public void setRequestBody(OpenApiRequestBody requestBody) {
		this.requestBody = requestBody;
	}

	public OpenApiResponseMap getResponses() {
		return responses;
	}

	public OpenApiOperation addResponse(OpenApiResponse response) {
		if (response != null) {
			this.responses.add(response);
		}
		return this;
	}

	public OpenApiResponse getResponse(int statusCode) {
		if (this.responses == null) {
			return null;
		}
		return this.responses.get(statusCode);
	}

	public OpenApiExternalDocs getExternalDocs() {
		return externalDocs;
	}

	public OpenApiOperation setExternalDocs(OpenApiExternalDocs externalDocs) {
		this.externalDocs = externalDocs;
		return this;
	}

	@Override
	public String toString() {
		return "OpenApiOperation [endpointPath=" + endpointPath + ", httpOperation=" + httpOperation + ", summary="
				+ summary + ", description=" + description + ", operationId=" + operationId + ", deprecated="
				+ deprecated + ", security=" + security + ", tags=" + tags + ", consumes=" + consumes + ", produces="
				+ produces + ", parameters=" + parameters + ", requestBody=" + requestBody + ", responses=" + responses
				+ ", externalDocs=" + externalDocs + "]";
	}
}
