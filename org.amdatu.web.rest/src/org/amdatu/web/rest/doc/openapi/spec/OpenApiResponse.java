/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.doc.openapi.spec;

import com.google.gson.annotations.SerializedName;

public class OpenApiResponse {

	public transient int statusCode;
	private transient String name;
	public String description;
	public OpenApiHeaderMap headers;
	public OpenApiContentMap content;
	@SerializedName("$ref")
	private String responseRef;

	public static OpenApiResponse createOkResponse() {
		return new OpenApiResponse(200).setDescription("OK");
	}

	public OpenApiResponse(int statusCode) {
		super();
		this.statusCode = statusCode;
	}

	public int getStatusCode() {
		return statusCode;
	}

	public OpenApiResponse setStatusCode(int statusCode) {
		this.statusCode = statusCode;
		return this;
	}

	public String getName() {
		return name;
	}

	public OpenApiResponse setName(String name) {
		this.name = name;
		return this;
	}

	public String getDescription() {
		return description;
	}

	public OpenApiResponse setDescription(String description) {
		this.description = description;
		return this;
	}

	public OpenApiResponse addHeader(OpenApiHeader openApiHeader) {

		if (openApiHeader == null) {
			return this;
		}

		if (headers == null) {
			headers = new OpenApiHeaderMap();
		}
		headers.add(openApiHeader);
		return this;
	}

	public OpenApiResponse addContent(OpenApiCombineMethod combineMethod, OpenApiContent openApiContent) {

		if (combineMethod == null || openApiContent == null) {
			return this;
		}

		if (content == null) {
			content = new OpenApiContentMap();
		}
		content.add(combineMethod, openApiContent);
		return this;
	}

	public String getResponseRef() {
		return responseRef;
	}

	public OpenApiResponse setResponseRef(String responseRef) {
		this.responseRef = responseRef;
		return this;
	}

	@Override
	public String toString() {
		return "OpenApiResponse [statusCode=" + statusCode + ", name=" + name + ", description=" + description
				+ ", headers=" + headers + ", content=" + content + ", responseRef=" + responseRef + "]";
	}
}
