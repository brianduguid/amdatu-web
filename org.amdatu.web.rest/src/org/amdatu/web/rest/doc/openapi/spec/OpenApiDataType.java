/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.doc.openapi.spec;

import com.google.gson.annotations.SerializedName;

public abstract class OpenApiDataType {

	protected transient String name;
	protected String type;
	protected String description;
    @SerializedName("default")
    protected String defaultValue;
    protected transient boolean flaggedRequired = true;
    private Boolean nullable;
    private Boolean readOnly;
    private Boolean writeOnly;

	public OpenApiDataType(String type) {
		super();
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public OpenApiDataType setName(String name) {
		this.name = name;
		return this;
	}

	public String getType() {
		return type;
	}

	public String getDescription() {
		return description;
	}

	public OpenApiDataType setDescription(String description) {
		this.description = description;
		return this;
	}

	public String getDefaultValue() {
		return defaultValue;
	}

	public OpenApiDataType setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
		return this;
	}

	public boolean isFlaggedRequired() {
		return flaggedRequired;
	}

	public OpenApiDataType setFlaggedRequired(boolean flaggedRequired) {
		this.flaggedRequired = flaggedRequired;
		return this;
	}

	public Boolean getNullable() {
		return nullable;
	}

	public OpenApiDataType setNullable(Boolean nullable) {
		this.nullable = nullable;
		return this;
	}

	public Boolean getReadOnly() {
		return readOnly;
	}

	public OpenApiDataType setReadOnly(Boolean readOnly) {
		this.readOnly = readOnly;
		return this;
	}

	public Boolean getWriteOnly() {
		return writeOnly;
	}

	public OpenApiDataType setWriteOnly(Boolean writeOnly) {
		this.writeOnly = writeOnly;
		return this;
	}


}
