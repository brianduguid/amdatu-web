/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.doc.openapi.spec;

public class OpenApiTag implements Comparable<OpenApiTag> {

	private String name;
	private String description;
	private OpenApiExternalDocs externalDocs;

	public OpenApiTag(String name, String description) {
		super();
		this.name = name;
		this.description = description;
	}

	public String getName() {
		return name;
	}

	public OpenApiTag setName(String name) {
		this.name = name;
		return this;
	}

	public String getDescription() {
		return description;
	}

	public OpenApiTag setDescription(String description) {
		this.description = description;
		return this;
	}

	public OpenApiExternalDocs getExternalDocs() {
		return externalDocs;
	}

	public OpenApiTag setExternalDocs(OpenApiExternalDocs externalDocs) {
		this.externalDocs = externalDocs;
		return this;
	}

	@Override
	public int compareTo(OpenApiTag tag) {
		if (tag == null) {
			return -1;
		}
		return name.compareToIgnoreCase(tag.getName());
	}

	@Override
	public String toString() {
		return "OpenApiTag [name=" + name + ", description=" + description + ", externalDocs=" + externalDocs + "]";
	}
}
