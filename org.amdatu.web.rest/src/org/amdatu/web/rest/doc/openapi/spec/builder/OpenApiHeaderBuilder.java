/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.doc.openapi.spec.builder;

import static org.amdatu.web.rest.doc.openapi.spec.builder.BuilderTools.isNotBlank;

import org.amdatu.web.rest.doc.openapi.spec.OpenApiDataType;
import org.amdatu.web.rest.doc.openapi.spec.OpenApiHeader;
import org.amdatu.web.rest.doc.openapi.spec.OpenApiReusableMap;

public class OpenApiHeaderBuilder {

	private OpenApiReusableMap openApiReusableMap;
	private String name;
	private String description;
	private Class<?> valueType;
	private Class<?> elementType;

	public static OpenApiHeaderBuilder create(OpenApiReusableMap openApiReusableMap, String name) {
		return new OpenApiHeaderBuilder(openApiReusableMap, name);
	}

	private OpenApiHeaderBuilder(OpenApiReusableMap openApiReusableMap, String name) {
		super();
		this.openApiReusableMap = openApiReusableMap;
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public OpenApiHeaderBuilder setName(String name) {
		this.name = name;
		return this;
	}

	public String getDescription() {
		return description;
	}

	public OpenApiHeaderBuilder setDescription(String description) {
		this.description = description;
		return this;
	}

	public Class<?> getValueType() {
		return valueType;
	}

	public OpenApiHeaderBuilder setValueType(Class<?> valueType) {
		this.valueType = valueType;
		return this;
	}

	public Class<?> getElementType() {
		return elementType;
	}

	public OpenApiHeaderBuilder setElementType(Class<?> elementType) {
		this.elementType = elementType;
		return this;
	}

	@Override
	public String toString() {
		return "OpenApiHeaderBuilder [name=" + name + ", description=" + description + ", valueType=" + valueType
				+ ", elementType=" + elementType + "]";
	}

	public OpenApiHeader build() {

		OpenApiDataTypeBuilder headerDataTypeBuilder = OpenApiDataTypeBuilder.create(openApiReusableMap, valueType)
																			.setElementType(elementType);

		OpenApiDataType headerDataType = headerDataTypeBuilder.build();

		OpenApiHeader openApiHeader = new OpenApiHeader(name, headerDataType);

		if (isNotBlank(description)) {
			openApiHeader.setDescription(description);
		}

		return openApiHeader;
	}
}
