/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.doc.openapi.spec.builder;

import static org.amdatu.web.rest.doc.openapi.OpenApiConfigService.URL_VARIABLE_FORMAT_PORT;
import static org.amdatu.web.rest.doc.openapi.OpenApiConfigService.URL_VARIABLE_FORMAT_PROTOCOL;
import static org.amdatu.web.rest.doc.openapi.OpenApiConfigService.URL_VARIABLE_FORMAT_SERVER;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

public final class BuilderTools {

	private BuilderTools() {}

	public static boolean isBlank(String string) {
		return string == null || string.isEmpty();
	}

	public static boolean isNotBlank(String string) {
		return !isBlank(string);
	}

	public static boolean isEmpty(List<?> list) {
		return list == null || list.isEmpty();
	}

	public static boolean isNotEmpty(List<?> list) {
		return !isEmpty(list);
	}

	public static String filterUrl(HttpServletRequest request, String url) {

		if (isBlank(url)) {
			return null;
		}

		if (url.contains(URL_VARIABLE_FORMAT_PROTOCOL)) {
			url = url.replace(URL_VARIABLE_FORMAT_PROTOCOL, findCurrentProtocol(request));
		}

		if (url.contains(URL_VARIABLE_FORMAT_SERVER)) {
			url = url.replace(URL_VARIABLE_FORMAT_SERVER, findCurrentServerName(request));
		}

		if (url.contains(URL_VARIABLE_FORMAT_PORT)) {
			String protocol = url.substring(url.indexOf("://"));
			url = url.replace(URL_VARIABLE_FORMAT_PORT, findCurrentPort(request, protocol));
		}

		if (url.endsWith("/")) {
			url = url.substring(0, url.length() - 1);
		}

		return url;
	}

	public static String findCurrentServerUrl(HttpServletRequest request) {

        String protocol = findCurrentProtocol(request);
        String serverName = findCurrentServerName(request);
        String port = findCurrentPort(request, protocol);

        return protocol + "://" + serverName + port;
    }

	public static String findCurrentProtocol(HttpServletRequest request) {

		String protocol = request.getHeader("X-Forwarded-Proto");

		if (isBlank(protocol)) {
			protocol = request.isSecure() ? "https" : "http";
		}

        return protocol;
	}

	public static String findCurrentServerName(HttpServletRequest request) {
        return request.getServerName();
	}

	public static String findCurrentPort(HttpServletRequest request, String protocol) {

		int port = request.getServerPort();

		if (port > 0) {
			return ":" + port;
		}

		if (port < 0 && "https".equals(protocol)) {
			return ":443";
		} else {
			return "";
		}
	}

	public static String findCurrentPath(HttpServletRequest request) {
		String path = request.getPathInfo();

        if (isBlank(path)) {
        		path = "";
        }

        return path;
	}

	/**
	 * Concatenates URI paths.
	 *
	 * @param basePath base path
	 * @param endPath endpoint path
	 * @return URI path
	 */
	public static String concatPaths(String basePath, String endPath) {

		if (isBlank(basePath)) {
			basePath = "";
		}

		if (isBlank(endPath)) {
			return basePath;
		}

		if (!basePath.endsWith("/") && !endPath.startsWith("/")) {
			basePath = basePath + "/";
		}

		return basePath + endPath;
	}
}
