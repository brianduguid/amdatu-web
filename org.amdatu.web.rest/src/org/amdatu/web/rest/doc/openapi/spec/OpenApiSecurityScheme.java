/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.doc.openapi.spec;

import static org.amdatu.web.rest.doc.openapi.spec.builder.BuilderTools.isBlank;

import java.util.Collections;
import java.util.List;
import java.util.TreeMap;

public class OpenApiSecurityScheme extends TreeMap<String, List<String>> {

	private static final long serialVersionUID = -2502874107580253378L;

	private transient boolean global = false;

	public OpenApiSecurityScheme addSecurityScheme(String schemeName) {

		if (isBlank(schemeName)) {
			return this;
		}

		this.put(schemeName, Collections.emptyList());
		return this;
	}

	public OpenApiSecurityScheme addSecurityScheme(String schemeName, List<String> scopes) {

		if (isBlank(schemeName)) {
			return this;
		}

		if (scopes == null || scopes.isEmpty()) {
			addSecurityScheme(schemeName);
			return this;
		}

		this.put(schemeName, scopes);
		return this;
	}



	public boolean isGlobal() {
		return global;
	}

	public OpenApiSecurityScheme setGlobal(boolean global) {
		this.global = global;
		return this;
	}

	@Override
	public String toString() {

		StringBuilder builder = new StringBuilder("OpenApiSecurityScheme [");
		builder.append("global=").append(global);

		this.entrySet().forEach(e -> builder.append(", ").append(e.getKey()).append("=").append(e.getValue()));

		return builder.append("]").toString();
	}
}
