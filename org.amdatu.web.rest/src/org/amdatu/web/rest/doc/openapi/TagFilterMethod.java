/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.doc.openapi;

/**
 * <p>Enum of available tag filtering methods.</p>
 *
 * @see OpenApiConfigService
 *
 * @since 1.0
 */
public enum TagFilterMethod {

	/**
	 * <p>Method to document an API operation only if it
	 * is tagged with one of the tags to be filtered. Other
	 * tags on included operations remain defined.</p>
	 */
	INCLUDE,

	/**
	 * <p>Method to document an API operation only if it
	 * is tagged with one of the tags to be filtered. Other
	 * tags are filtered from operations.</p>
	 */
	EXPLICIT,

	/**
	 * <p>Method to document all API operations except those
	 * tag with one of the tags to be filtered.</p>
	 */
	EXCLUDE,

	/**
	 * <p>Do not filter tags.</p>
	 */
	NONE;

	/**
	 * <p>Evaluates the string, ignoring case, to determine the equivalent
	 * tag filter method enum. If unknown, NONE is returned.</p>
	 *
	 * @param filterMethod potential tag filter method text
	 * @return tag filter method
	 */
	public static TagFilterMethod findValueOf(String filterMethod) {

		if (filterMethod == null) {
			return NONE;
		}

		for (TagFilterMethod method: TagFilterMethod.values()) {
			if (method.name().equalsIgnoreCase(filterMethod)) {
				return method;
			}
		}
		return NONE;
	}
}
