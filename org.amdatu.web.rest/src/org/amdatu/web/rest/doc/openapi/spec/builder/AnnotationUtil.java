/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.doc.openapi.spec.builder;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.HttpMethod;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import org.amdatu.web.rest.doc.ApiTagDefinition;
import org.amdatu.web.rest.doc.ApiTags;

public final class AnnotationUtil {

	/**
	 * Prevents instantiation.
	 */
	private AnnotationUtil() {}

	public static boolean isHttpMethod(Method method) {
		if (method == null) {
			return false;
		}
        Annotation[] annotations = method.getDeclaredAnnotations();
        for (Annotation annotation : annotations) {
            if (isAnnotationPresent(HttpMethod.class, annotation.annotationType().getAnnotations())) {
                return true;
            }
        }
        return false;
    }

	public static String findHttpOperation(Method method) {
		if (method == null) {
			return null;
		}
		Annotation[] annotations = method.getAnnotations();
        for (Annotation annotation : annotations) {
            if (isAnnotationPresent(HttpMethod.class, annotation.annotationType().getAnnotations())) {
                return annotation.annotationType().getAnnotation(HttpMethod.class).value();
            }
        }

        return null;
	}

	public static boolean isSubResourceLocator(Method method) {
        return !isHttpMethod(method) && isAnnotationPresent(Path.class, method.getAnnotations());
    }

	public static String findPath(Annotation[] annotations) {
		Path pathAnn = findAnnotation(Path.class, annotations);

		if (pathAnn == null || pathAnn.value() == null) {
			return "";
		}

        String path = pathAnn.value().trim();
        if (!path.startsWith("/") && path.length() > 1) {
            return "/".concat(path);
        }

        return path;
	}

	public static List<String> findTags(Annotation[] annotations) {
		List<String> tags = new ArrayList<>();

		ApiTags apiTags = findAnnotation(ApiTags.class, annotations);
		if (apiTags != null) {
			tags.addAll(Arrays.asList(apiTags.value()));
		}

		ApiTagDefinition apiTagDefinition = findAnnotation(ApiTagDefinition.class, annotations);
		if (apiTagDefinition != null) {
			tags.add(apiTagDefinition.name());
		}

        return tags.isEmpty() ? Collections.emptyList() : tags;
    }

	public static List<String> findProduces(Annotation[] annotations) {
		Produces produces = findAnnotation(Produces.class, annotations);
        return (produces != null) ? Arrays.asList(produces.value()) : Collections.emptyList();
    }

	public static List<String> findConsumes(Annotation[] annotations) {
		Consumes consumes = findAnnotation(Consumes.class, annotations);
        return (consumes != null) ? Arrays.asList(consumes.value()) : Collections.emptyList();
    }

	public static String findDefaultValue(Annotation[] annotations) {
		DefaultValue defaultValue = findAnnotation(DefaultValue.class, annotations);
        return (defaultValue != null) ? defaultValue.value() : null;
    }

	public static <T extends Annotation> boolean isAnnotationPresent(Class<T> type, Annotation[] annotations) {
		if (annotations == null || annotations.length == 0 || type == null) {
			return false;
		}
        for (Annotation ann : annotations) {
            if (type.isInstance(ann)) {
                return true;
            }
        }
        return false;
    }

	public static <T extends Annotation> T findAnnotation(Class<T> type, Annotation[] annotations) {
		if (annotations == null || annotations.length == 0 || type == null) {
			return null;
		}
        for (Annotation ann : annotations) {
            if (type.isInstance(ann)) {
                return type.cast(ann);
            }
        }
        return null;
    }

	public static <T extends Annotation> Annotation findFirstAnnotation(Class<?>[] types, Annotation[] annotations) {
		if (annotations == null || annotations.length == 0 || types == null || types.length == 0) {
			return null;
		}
        for (Class<?> type : types) {
        		for (Annotation ann : annotations) {
                if (type.isInstance(ann)) {
                    return (Annotation) type.cast(ann);
                }
            }
        }
        return null;
    }
}
