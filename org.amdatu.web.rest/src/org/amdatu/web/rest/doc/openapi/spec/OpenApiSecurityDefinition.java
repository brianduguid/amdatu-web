/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.doc.openapi.spec;

import java.util.HashMap;

import org.amdatu.web.rest.doc.ApiSecurityType;

public class OpenApiSecurityDefinition extends HashMap<String, String> {

	private static final long serialVersionUID = 1191890750898011329L;

	private transient String securitySchemeName;
	private transient ApiSecurityType apiSecurityType;

	public OpenApiSecurityDefinition(ApiSecurityType apiSecurityType, String securitySchemeName) {
		super();
		this.securitySchemeName = securitySchemeName;
		this.apiSecurityType = apiSecurityType;
		this.put("type", apiSecurityType.getTypeId());
	}

	public String getSecuritySchemeName() {
		return securitySchemeName;
	}

	public ApiSecurityType getApiSecurityType() {
		return apiSecurityType;
	}

	public OpenApiSecurityDefinition addSecurityProperty(String key, String value) {
		if (!containsKey(key)) {
			this.put(key, value);
		}
		return this;
	}

	@Override
	public String toString() {

		StringBuilder builder = new StringBuilder("OpenApiSecurityScheme [");
		builder.append("securitySchemeName=")
				.append(securitySchemeName)
				.append("apiSecurityType=")
				.append(apiSecurityType);


		this.entrySet().forEach(e -> builder.append(", ").append(e.getKey()).append("=").append(e.getValue()));

		return builder.append("]").toString();
	}

}
