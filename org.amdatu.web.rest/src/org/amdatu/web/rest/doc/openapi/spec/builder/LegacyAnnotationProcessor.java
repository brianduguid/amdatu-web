/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.doc.openapi.spec.builder;

import static org.amdatu.web.rest.doc.openapi.spec.builder.AnnotationUtil.findAnnotation;
import static org.amdatu.web.rest.doc.openapi.spec.builder.AnnotationUtil.findFirstAnnotation;
import static org.amdatu.web.rest.doc.openapi.spec.builder.AnnotationUtil.isAnnotationPresent;

import java.lang.annotation.Annotation;
import java.util.HashMap;
import java.util.Map;

import org.amdatu.web.rest.doc.Description;
import org.amdatu.web.rest.doc.NoDocumentation;
import org.amdatu.web.rest.doc.Notes;
import org.amdatu.web.rest.doc.ResponseMessage;
import org.amdatu.web.rest.doc.ResponseMessages;
import org.amdatu.web.rest.doc.ReturnType;

@SuppressWarnings("deprecation")
public class LegacyAnnotationProcessor {

	private static Class<?>[] LEGACY_ANNOTATIONS_ARRAY = { Description.class, Notes.class, ResponseMessages.class, ResponseMessage.class, ReturnType.class };
	private static Class<?>[] RESPONSE_TYPE_ANNOTATIONS = { ResponseMessages.class, ResponseMessage.class, ReturnType.class };

	/**
	 * TODO Is this needed?
	 * @param annotations
	 * @return
	 */
	public static boolean isLegacyAnnotationPresent(Annotation[] annotations) {
		return findFirstAnnotation(LEGACY_ANNOTATIONS_ARRAY, annotations) != null;
	}

	public static boolean isLegacyNoDocumentationPresent(Annotation[] annotations) {
		return isAnnotationPresent(NoDocumentation.class, annotations);
	}

	public static boolean isLegacyDescriptionPresent(Annotation[] annotations) {
		return isAnnotationPresent(Description.class, annotations);
	}

	public static String findLegacyDescription(Annotation[] annotations) {
		Description descriptionAnn = findAnnotation(Description.class, annotations);

		if (descriptionAnn != null) {
			return descriptionAnn.value();

		} else {
			return null;
		}
	}

	public static String findLegacyNotes(Annotation[] annotations) {
		Notes notesAnn = findAnnotation(Notes.class, annotations);

		if (notesAnn != null) {
			return notesAnn.value();

		} else {
			return null;
		}
	}

	public static boolean isLegacyResponseTypePresent(Annotation[] annotations) {
		return findFirstAnnotation(RESPONSE_TYPE_ANNOTATIONS, annotations) != null;
	}

	public static Class<?> findLegacyReturnType(Annotation[] annotations) {
		ReturnType returnTypeAnn = findAnnotation(ReturnType.class, annotations);
		if (returnTypeAnn != null) {
			return returnTypeAnn.value();
		}
		return null;
	}

	public static Map<Integer, String> findLegacyResponseMessages(Annotation[] annotations) {

		Map<Integer, String> responsesMap = new HashMap<>();

		ResponseMessage responseMessageAnn = findAnnotation(ResponseMessage.class, annotations);
		ResponseMessages responseMessagesAnn = findAnnotation(ResponseMessages.class, annotations);

		/**
		 * A response message may be by itself
		 */
		if (responseMessageAnn != null) {
			responsesMap.put(responseMessageAnn.code(), responseMessageAnn.message());
		}

		if (responseMessagesAnn != null) {
			for (ResponseMessage ann : responseMessagesAnn.value()) {
				responsesMap.put(ann.code(), ann.message());
			}
		}

		return responsesMap;
	}
}
