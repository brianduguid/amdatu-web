/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.doc.openapi.spec;

import static org.amdatu.web.rest.doc.openapi.spec.builder.BuilderTools.isBlank;

import java.util.Map;
import java.util.TreeMap;

public class OpenApiReusableMap {

	private static final String REF_PREFIX = "#/components/";

	private Map <String, OpenApiDataType> schemas;
	private Map <String, OpenApiParameter> parameters;
	private Map <String, OpenApiSecurityDefinition> securitySchemes;
	private Map <String, OpenApiRequestBody> requestBodies;
	private Map <String, OpenApiResponse> responses;
	private Map <String, OpenApiHeader> headers;
	private Map <String, Object> examples; // TODO add header object
	private Map <String, OpenApiLink> links;
	private Map <String, Object> callbacks; // TODO add header object

	public boolean containsComponentRef(ComponentType componentType, Object name) {
		@SuppressWarnings("rawtypes")
		Map map = getComponentMap(componentType);
		return map != null && map.containsKey(name);
	}

	public String getComponentRefByName(ComponentType componentType, Object name) {
		if (!containsComponentRef(componentType, name)) {
			return null;
		}
		return buildComponentRef(componentType, name);
	}

	public String getComponentRef(ComponentType componentType, Object reusable) {

		if (componentType == null || reusable == null) {
			return null;
		}

		switch(componentType) {
			case SCHEMA:
				return getSchemaRef((OpenApiDataType) reusable);
			case PARAMETER:
				return getParameterRef((OpenApiParameter) reusable);
			case SECURITY_SCHEME:
				return getSecuritySchemeRef((OpenApiSecurityDefinition) reusable);
			case REQUEST_BODY:
				return getRequestBodyRef((OpenApiRequestBody) reusable);
			case RESPONSE:
				return getResponseRef((OpenApiResponse) reusable);
			case HEADER:
				return getHeaderRef((OpenApiHeader) reusable);
			case EXAMPLE:
				return getExampleRef(reusable);
			case LINK:
				return getLinkRef((OpenApiLink) reusable);
			case CALLBACK:
				return getCallbackRef(reusable);
			default:
				return null;
		}
	}

	public String getSchemaRef(OpenApiDataType openApiDataType) {

		if (openApiDataType == null) {
			return null;
		}

		if (schemas == null) {
			schemas = new TreeMap<>();
		}

		String name = openApiDataType.getName();

		if (isBlank(name)) {
			return null;
		}

		if (containsComponentRef(ComponentType.SCHEMA, name)) {
			return buildComponentRef(ComponentType.SCHEMA, name);

		} else {
			schemas.put(name, openApiDataType);
			return buildComponentRef(ComponentType.SCHEMA, name);
		}
	}

	public String getParameterRef(OpenApiParameter openApiParameter) {

		if (openApiParameter == null) {
			return null;
		}

		if (parameters == null) {
			parameters = new TreeMap<>();
		}

		String name = openApiParameter.getName();

		if (isBlank(name)) {
			return null;
		}

		if (containsComponentRef(ComponentType.PARAMETER, name)) {
			return buildComponentRef(ComponentType.PARAMETER, name);

		} else {
			parameters.put(name, openApiParameter);
			return buildComponentRef(ComponentType.PARAMETER, name);
		}
	}

	public String getSecuritySchemeRef(OpenApiSecurityDefinition openApiSecurityDefinition) {

		if (openApiSecurityDefinition == null) {
			return null;
		}

		if (securitySchemes == null) {
			securitySchemes = new TreeMap<>();
		}

		String name = openApiSecurityDefinition.getSecuritySchemeName();

		if (isBlank(name)) {
			return null;
		}

		if (containsComponentRef(ComponentType.SECURITY_SCHEME, name)) {
			return buildComponentRef(ComponentType.SECURITY_SCHEME, name);

		} else {
			securitySchemes.put(name, openApiSecurityDefinition);
			return buildComponentRef(ComponentType.SECURITY_SCHEME, name);
		}
	}

	public String getRequestBodyRef(OpenApiRequestBody openApiRequestBody) {

		if (openApiRequestBody == null) {
			return null;
		}

		if (requestBodies == null) {
			requestBodies = new TreeMap<>();
		}

		String name = openApiRequestBody.getName();

		if (isBlank(name)) {
			return null;
		}

		if (containsComponentRef(ComponentType.REQUEST_BODY, name)) {
			return buildComponentRef(ComponentType.REQUEST_BODY, name);

		} else {
			requestBodies.put(name, openApiRequestBody);
			return buildComponentRef(ComponentType.REQUEST_BODY, name);
		}
	}

	public String getResponseRef(OpenApiResponse openApiResponse) {

		if (openApiResponse == null) {
			return null;
		}

		if (responses == null) {
			responses = new TreeMap<>();
		}

		String name = openApiResponse.getName();

		if (isBlank(name)) {
			return null;
		}

		if (containsComponentRef(ComponentType.RESPONSE, name)) {
			return buildComponentRef(ComponentType.RESPONSE, name);

		} else {
			responses.put(name, openApiResponse);
			return buildComponentRef(ComponentType.RESPONSE, name);
		}
	}

	public OpenApiResponse getResponseRefByName(String name) {

		if (isBlank(name) || responses == null) {
			return null;
		}

		OpenApiResponse openApiResponse = responses.get(name);

		if (openApiResponse == null) {
			return null;
		}

		return new OpenApiResponse(openApiResponse.getStatusCode())
						.setResponseRef(buildComponentRef(ComponentType.RESPONSE, name));
	}

	public String getHeaderRef(OpenApiHeader openApiHeader) {

		if (openApiHeader == null) {
			return null;
		}

		if (headers == null) {
			headers = new TreeMap<>();
		}

		String name = openApiHeader.getName();

		if (isBlank(name)) {
			return null;
		}

		if (containsComponentRef(ComponentType.HEADER, name)) {
			return buildComponentRef(ComponentType.HEADER, name);

		} else {
			headers.put(name, openApiHeader);
			return buildComponentRef(ComponentType.HEADER, name);
		}
	}

	public OpenApiHeader getHeaderRefByName(String name) {

		if (isBlank(name) || headers == null) {
			return null;
		}

		OpenApiHeader openApiHeader = headers.get(name);

		if (openApiHeader == null) {
			return null;
		}

		return new OpenApiHeader(openApiHeader.getName())
						.setHeaderRef(buildComponentRef(ComponentType.HEADER, name));
	}

	public String getExampleRef(Object openApiDataType) {
		// TODO Fix me
		return null;
	}

	public String getLinkRef(OpenApiLink openApiLink) {

		if (links == null) {
			links = new TreeMap<>();
		}

		String name = openApiLink.getName();

		if (isBlank(name)) {
			return null;
		}

		if (containsComponentRef(ComponentType.LINK, name)) {
			return buildComponentRef(ComponentType.LINK, name);

		} else {
			links.put(name, openApiLink);
			return buildComponentRef(ComponentType.LINK, name);
		}
	}

	public String getCallbackRef(Object openApiDataType) {
		// TODO Fix me
		return null;
	}

	@SuppressWarnings("rawtypes")
	private Map getComponentMap(ComponentType componentType) {

		switch(componentType) {
			case SCHEMA:
				return schemas;
			case PARAMETER:
				return parameters;
			case SECURITY_SCHEME:
				return securitySchemes;
			case REQUEST_BODY:
				return requestBodies;
			case RESPONSE:
				return responses;
			case HEADER:
				return headers;
			case EXAMPLE:
				return examples;
			case LINK:
				return links;
			case CALLBACK:
				return callbacks;
			default:
				return null;
		}
	}

	private String buildComponentRef(ComponentType componentType, Object name) {
		return REF_PREFIX + componentType.getTypeGroup() + "/" + name.toString();
	}
}
