/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.doc.openapi.spec.datatype;

import org.amdatu.web.rest.doc.openapi.spec.OpenApiDataType;

public class OpenApiDataTypeArray extends OpenApiDataType {

	private OpenApiDataType items;

	public OpenApiDataTypeArray(OpenApiDataType items) {
		super("array");
		this.items = items;
	}

	public OpenApiDataType getItems() {
		return items;
	}

	public OpenApiDataTypeArray setItems(OpenApiDataType items) {
		this.items = items;
		return this;
	}

	@Override
	public String toString() {
		return "OpenApiDataTypeArray [items=" + items + ", name=" + name + ", type=" + type + ", description="
				+ description + ", defaultValue=" + defaultValue + ", flaggedRequired=" + flaggedRequired + "]";
	}
}
