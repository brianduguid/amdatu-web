/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.doc.openapi.spec;

public class OpenApiParameter {

	public transient ParameterLocation parameterLocation;
	public String in;
	public String name;
	public String description;
	public boolean required;
	public OpenApiDataType schema;

	public OpenApiParameter(ParameterLocation parameterLocation, String name, OpenApiDataType schema) {
		super();
		this.parameterLocation = parameterLocation;
		this.in = parameterLocation.name().toLowerCase();
		this.name = name;
		this.schema = schema;
	}

	public ParameterLocation getParameterLocation() {
		return parameterLocation;
	}

	public OpenApiParameter setParameterLocation(ParameterLocation parameterLocation) {
		this.parameterLocation = parameterLocation;
		this.in = parameterLocation.name().toLowerCase();
		return this;
	}

	public String getIn() {
		return in;
	}

	public String getName() {
		return name;
	}

	public OpenApiParameter setName(String name) {
		this.name = name;
		return this;
	}

	public String getDescription() {
		return description;
	}

	public OpenApiParameter setDescription(String description) {
		this.description = description;
		return this;
	}

	public boolean isRequired() {
		return required;
	}

	public OpenApiParameter setRequired(boolean required) {
		this.required = required;
		return this;
	}

	public OpenApiDataType getSchema() {
		return schema;
	}

	public OpenApiParameter setSchema(OpenApiDataType schema) {
		this.schema = schema;
		return this;
	}

	@Override
	public String toString() {
		return "OpenApiParameter [parameterLocation=" + parameterLocation + ", in=" + in + ", name=" + name
				+ ", description=" + description + ", required=" + required + ", schema=" + schema + "]";
	}
}
