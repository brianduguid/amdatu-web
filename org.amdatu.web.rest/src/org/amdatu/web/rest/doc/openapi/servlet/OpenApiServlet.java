/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.doc.openapi.servlet;

import static org.osgi.service.http.whiteboard.HttpWhiteboardConstants.HTTP_WHITEBOARD_CONTEXT_NAME;
import static org.osgi.service.http.whiteboard.HttpWhiteboardConstants.HTTP_WHITEBOARD_CONTEXT_SELECT;
import static org.osgi.service.http.whiteboard.HttpWhiteboardConstants.HTTP_WHITEBOARD_DEFAULT_CONTEXT_NAME;
import static org.osgi.service.http.whiteboard.HttpWhiteboardConstants.HTTP_WHITEBOARD_SERVLET_PATTERN;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.MediaType;

import org.amdatu.web.rest.doc.openapi.OpenApiDefinitionService;
import org.amdatu.web.rest.doc.openapi.RestResource;
import org.amdatu.web.rest.spi.ApplicationService;
import org.apache.felix.dm.Component;
import org.osgi.framework.ServiceReference;
import org.osgi.service.http.whiteboard.HttpWhiteboardConstants;

@SuppressWarnings("serial")
public class OpenApiServlet extends HttpServlet {

	private static final String SWAGGER_UI_RESOURCE_PATH = "/swagger-ui-3.7.0";
	private static final String SERVLET_PATH = "openapi";
	private static final String JSON_NAME = "openapi.json";

	private volatile OpenApiDefinitionService m_openApiDefinitionService;
	private volatile Component m_component;

	private ApplicationService m_applicationService;

	protected final void onAdd(ServiceReference<ApplicationService> ref, ApplicationService applicationService) {
		m_applicationService = applicationService;
		updateServiceProperties();
	}

	protected final void onChange(ServiceReference<ApplicationService> ref) {
		updateServiceProperties();
	}

	private void updateServiceProperties() {

		Dictionary<Object, Object> properties = m_component.getServiceProperties();

		if (properties == null) {
			properties = new Hashtable<>();
		}

		String serviceOpenApiPath = m_applicationService.getBaseUri() + "/" + SERVLET_PATH;

		/**
		 * Configure servlet path.
		 */
		properties.put(HTTP_WHITEBOARD_SERVLET_PATTERN, serviceOpenApiPath + "/" + JSON_NAME);

		/**
		 * Configure Swagger UI path.
		 */
		properties.put(HttpWhiteboardConstants.HTTP_WHITEBOARD_RESOURCE_PREFIX, SWAGGER_UI_RESOURCE_PATH);
		properties.put(HttpWhiteboardConstants.HTTP_WHITEBOARD_RESOURCE_PATTERN, serviceOpenApiPath + "/*");


		if (m_applicationService.getContextName() != null
				&& !HTTP_WHITEBOARD_DEFAULT_CONTEXT_NAME.equals(m_applicationService.getContextName())) {

			properties.put(HTTP_WHITEBOARD_CONTEXT_SELECT,
					"(" + HTTP_WHITEBOARD_CONTEXT_NAME + "=" + m_applicationService.getContextName() + ")");

		} else {
			properties.remove(HTTP_WHITEBOARD_CONTEXT_SELECT);
		}

		m_component.setServiceProperties(properties);
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		if (m_applicationService == null) {
			return;
		}

		List<RestResource> restResources = new ArrayList<>();

		String baseUri = m_applicationService.getBaseUri();
		Object[] singletons = m_applicationService.getSingletons();

		if (singletons == null || singletons.length == 0) {
			return;
		}

		for (Object singleton : singletons) {
			restResources.add(m_openApiDefinitionService.createRestResource(baseUri, singleton.getClass()));
		}

		String openApiDefinition = m_openApiDefinitionService.createOpenApiDefinitionJson(request, restResources);

		response.setContentType(MediaType.APPLICATION_JSON);

		try (PrintWriter writer = response.getWriter()) {
			writer.append(openApiDefinition);
		}
	}
}
