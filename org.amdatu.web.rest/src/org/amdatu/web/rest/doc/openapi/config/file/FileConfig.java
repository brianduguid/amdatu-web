/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.doc.openapi.config.file;

import static org.amdatu.web.rest.doc.openapi.Constants.DEFAULT_BASE_URI;
import static org.amdatu.web.rest.doc.openapi.Constants.DEFAULT_DESCRIPTION;
import static org.amdatu.web.rest.doc.openapi.Constants.DEFAULT_TITLE;
import static org.amdatu.web.rest.doc.openapi.Constants.DEFAULT_VERSION;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.amdatu.web.rest.doc.openapi.TagFilterMethod;

public class FileConfig {

	private String baseUri = DEFAULT_BASE_URI;
	private String title = DEFAULT_TITLE;
	private String description = DEFAULT_DESCRIPTION;
	private String version = DEFAULT_VERSION;
	private boolean enabled = false;
	private Map<String, String> servers;
	private String termsOfServiceUrl;
	private String licenseUrl;
	private String licenseName;
	private String externalDocsUrl;
	private String externalDocsDescription;
	private String supportContactUrl;
	private String supportContactEmail;
	private String supportContactName;
	private TagFilterMethod tagFilterMethod = TagFilterMethod.NONE;
	private List<String> filteredTags = Collections.emptyList();
	private List<String> defaultConsumes;

	private List<String> defaultProduces;

	public String getBaseUri() {
		return baseUri;
	}

	public FileConfig setBaseUri(String baseUri) {
		this.baseUri = baseUri;
		return this;
	}

	public String getTitle() {
		return title;
	}

	public FileConfig setTitle(String title) {
		this.title = title;
		return this;
	}

	public String getDescription() {
		return description;
	}

	public FileConfig setDescription(String description) {
		this.description = description;
		return this;
	}

	public String getVersion() {
		return version;
	}

	public FileConfig setVersion(String version) {
		this.version = version;
		return this;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public FileConfig setEnabled(boolean enabled) {
		this.enabled = enabled;
		return this;
	}

	public Map<String, String> getServers() {
		return servers;
	}

	public FileConfig setServers(Map<String, String> servers) {
		this.servers = servers;
		return this;
	}

	public String getTermsOfServiceUrl() {
		return termsOfServiceUrl;
	}

	public FileConfig setTermsOfServiceUrl(String termsOfServiceUrl) {
		this.termsOfServiceUrl = termsOfServiceUrl;
		return this;
	}

	public String getLicenseUrl() {
		return licenseUrl;
	}

	public FileConfig setLicenseUrl(String licenseUrl) {
		this.licenseUrl = licenseUrl;
		return this;
	}

	public String getLicenseName() {
		return licenseName;
	}

	public FileConfig setLicenseName(String licenseName) {
		this.licenseName = licenseName;
		return this;
	}

	public String getExternalDocsUrl() {
		return externalDocsUrl;
	}

	public FileConfig setExternalDocsUrl(String externalDocsUrl) {
		this.externalDocsUrl = externalDocsUrl;
		return this;
	}

	public String getExternalDocsDescription() {
		return externalDocsDescription;
	}

	public FileConfig setExternalDocsDescription(String externalDocsDescription) {
		this.externalDocsDescription = externalDocsDescription;
		return this;
	}

	public String getSupportContactUrl() {
		return supportContactUrl;
	}

	public FileConfig setSupportContactUrl(String supportContactUrl) {
		this.supportContactUrl = supportContactUrl;
		return this;
	}

	public String getSupportContactEmail() {
		return supportContactEmail;
	}

	public FileConfig setSupportContactEmail(String supportContactEmail) {
		this.supportContactEmail = supportContactEmail;
		return this;
	}

	public String getSupportContactName() {
		return supportContactName;
	}

	public FileConfig setSupportContactName(String supportContactName) {
		this.supportContactName = supportContactName;
		return this;
	}

	public TagFilterMethod getTagFilterMethod() {
		return tagFilterMethod;
	}

	public FileConfig setTagFilterMethod(TagFilterMethod tagFilterMethod) {
		this.tagFilterMethod = tagFilterMethod;
		return this;
	}

	public List<String> getFilteredTags() {
		return filteredTags;
	}

	public FileConfig setFilteredTags(List<String> filteredTags) {
		this.filteredTags = filteredTags;
		return this;
	}

	public List<String> getDefaultConsumes() {
		return defaultConsumes;
	}

	public FileConfig setDefaultConsumes(List<String> defaultConsumes) {
		this.defaultConsumes = defaultConsumes;
		return this;
	}

	public List<String> getDefaultProduces() {
		return defaultProduces;
	}

	public FileConfig setDefaultProduces(List<String> defaultProduces) {
		this.defaultProduces = defaultProduces;
		return this;
	}

	@Override
	public String toString() {
		return "FileConfig [baseUri=" + baseUri + ", title=" + title + ", description=" + description + ", version="
				+ version + ", enabled=" + enabled + ", servers=" + servers + ", termsOfServiceUrl=" + termsOfServiceUrl
				+ ", licenseUrl=" + licenseUrl + ", licenseName=" + licenseName + ", externalDocsUrl=" + externalDocsUrl
				+ ", externalDocsDescription=" + externalDocsDescription + ", supportContactUrl=" + supportContactUrl
				+ ", supportContactEmail=" + supportContactEmail + ", supportContactName=" + supportContactName
				+ ", tagFilterMethod=" + tagFilterMethod + ", filteredTags=" + filteredTags + ", defaultConsumes="
				+ defaultConsumes + ", defaultProduces=" + defaultProduces + "]";
	}
}
