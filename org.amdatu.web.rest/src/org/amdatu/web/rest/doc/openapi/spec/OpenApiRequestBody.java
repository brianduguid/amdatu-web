/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.doc.openapi.spec;

public class OpenApiRequestBody {

	private transient String name;
	public String description;
	public boolean required = true;
	private OpenApiContentMap content = new OpenApiContentMap();

	public OpenApiRequestBody(String name) {
		super();
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	public OpenApiRequestBody setDescription(String description) {
		this.description = description;
		return this;
	}

	public boolean isRequired() {
		return required;
	}

	public OpenApiRequestBody setRequired(boolean required) {
		this.required = required;
		return this;
	}

	public boolean addContent(OpenApiCombineMethod combineMethod, OpenApiContent content) {
		return this.content.add(combineMethod, content);
	}

	public OpenApiContentMap getContent() {
		return content;
	}

	public OpenApiContent getOpenApiContentByMediaType(String mediaType) {
		return content.get(mediaType);
	}

	@Override
	public String toString() {
		return "OpenApiRequestBody [name=" + name + ", description=" + description + ", required=" + required
				+ ", content=" + content + "]";
	}
}
