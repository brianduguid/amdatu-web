/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.doc.openapi.spec.builder;

import static org.amdatu.web.rest.doc.openapi.spec.builder.AnnotationUtil.findAnnotation;
import static org.amdatu.web.rest.doc.openapi.spec.builder.AnnotationUtil.findFirstAnnotation;
import static org.amdatu.web.rest.doc.openapi.spec.builder.BuilderTools.isNotBlank;
import static org.amdatu.web.rest.doc.openapi.spec.builder.LegacyAnnotationProcessor.findLegacyDescription;

import java.lang.annotation.Annotation;
import java.lang.reflect.Parameter;
import java.util.Arrays;

import javax.ws.rs.CookieParam;
import javax.ws.rs.FormParam;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;

import org.amdatu.web.rest.doc.ApiParameter;
import org.amdatu.web.rest.doc.openapi.spec.OpenApiDataType;
import org.amdatu.web.rest.doc.openapi.spec.OpenApiParameter;
import org.amdatu.web.rest.doc.openapi.spec.OpenApiReusableMap;
import org.amdatu.web.rest.doc.openapi.spec.ParameterLocation;

public class OpenApiParameterBuilder {

	public static final Class<?>[] PARAMETER_LOCATION_ANNOTATIONS = { PathParam.class, QueryParam.class,
			HeaderParam.class, FormParam.class, Context.class };

	private OpenApiReusableMap openApiReusableMap;
	private Parameter parameter;
	private Annotation[] annotations;

	public static OpenApiParameterBuilder create(OpenApiReusableMap openApiReusableMap) {
		return new OpenApiParameterBuilder(openApiReusableMap);
	}

	private OpenApiParameterBuilder(OpenApiReusableMap openApiReusableMap) {
		super();
		this.openApiReusableMap = openApiReusableMap;
	}

	public OpenApiReusableMap getOpenApiReusableMap() {
		return openApiReusableMap;
	}

	public OpenApiParameterBuilder setOpenApiReusableMap(OpenApiReusableMap openApiReusableMap) {
		this.openApiReusableMap = openApiReusableMap;
		return this;
	}

	public Parameter getParameter() {
		return parameter;
	}

	public OpenApiParameterBuilder setParameter(Parameter parameter) {
		this.parameter = parameter;
		return this;
	}

	public Annotation[] getAnnotations() {
		return annotations;
	}

	public OpenApiParameterBuilder setAnnotations(Annotation[] annotations) {
		this.annotations = annotations;
		return this;
	}

	@Override
	public String toString() {
		return "OpenApiParameterBuilder [openApiReusableMap=" + openApiReusableMap + ", parameter=" + parameter
				+ ", annotations=" + Arrays.toString(annotations) + "]";
	}

	public OpenApiParameter build() {

		OpenApiDataType parameterDataType = createOpenApiDataType();

		Annotation paramLocationAnn = findFirstAnnotation(PARAMETER_LOCATION_ANNOTATIONS, annotations);
		ParameterLocation parameterLocation = ParameterLocation.valueOfAnnotation(paramLocationAnn);

		String name = null;
		boolean required = parameterDataType.isFlaggedRequired();
		String description = parameterDataType.getDescription();
		parameterDataType.setDescription(null); // moved description to parameter level.

		switch (parameterLocation) {
		case PATH:
			name = ((PathParam) paramLocationAnn).value();
			break;
		case QUERY:
			name = ((QueryParam) paramLocationAnn).value();
			break;
		case HEADER:
			name = ((HeaderParam) paramLocationAnn).value();
			break;
		case FORM:
			name = ((FormParam) paramLocationAnn).value();
			break;
		case COOKIE:
			name = ((CookieParam) paramLocationAnn).value();
			break;
		default:
			// no name for body;
		}

		// TODO add feature for reusable parameter
		// boolean reusableComponent = isAnnotationPresent(ApiReusableComponent.class, paramAnnotations);

		return new OpenApiParameter(parameterLocation, name, parameterDataType)
								.setRequired(required)
								.setDescription(description);
	}

	private OpenApiDataType createOpenApiDataType() {

		OpenApiDataTypeBuilder openApiDataTypeBuilder = OpenApiDataTypeBuilder.create(openApiReusableMap, parameter.getType());

		ApiParameter apiParameterAnn = findAnnotation(ApiParameter.class, annotations);

        if (apiParameterAnn != null) {
        		openApiDataTypeBuilder.setDescription(apiParameterAnn.value())
        							.setRequired(apiParameterAnn.required())
        							.setFormat(apiParameterAnn.format())
        							.setMinimum(apiParameterAnn.minimum())
        							.setExclusiveMinimum(apiParameterAnn.exclusiveMinimum())
        							.setMaximum(apiParameterAnn.maximum())
        							.setExclusiveMaximum(apiParameterAnn.exclusiveMaximum())
        							.setNullable(apiParameterAnn.nullable())
        							.setRead(apiParameterAnn.read())
        							.setWrite(apiParameterAnn.write())
        							.setPassword(apiParameterAnn.password());
        } else {

			/**
			 * Legacy Annotation Support
			 * TODO Remove when legacy annotation support is no longer required.
			 */
			String description = findLegacyDescription(annotations);
			if (isNotBlank(description)) {
				openApiDataTypeBuilder.setDescription(description);
			}
		}

		return openApiDataTypeBuilder.build();
	}
}
