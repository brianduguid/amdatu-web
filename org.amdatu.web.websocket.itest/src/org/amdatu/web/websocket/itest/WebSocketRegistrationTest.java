/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.websocket.itest;

import static org.amdatu.web.websocket.WebSocketConstants.WEBSOCKET_DEFAULT_SERVLET_NAME;
import static org.amdatu.web.websocket.WebSocketConstants.WEBSOCKET_NAME;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.osgi.service.http.whiteboard.HttpWhiteboardConstants.HTTP_WHITEBOARD_CONTEXT_NAME;
import static org.osgi.service.http.whiteboard.HttpWhiteboardConstants.HTTP_WHITEBOARD_CONTEXT_PATH;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.websocket.Endpoint;
import javax.websocket.EndpointConfig;
import javax.websocket.MessageHandler;
import javax.websocket.OnMessage;
import javax.websocket.Session;
import javax.websocket.WebSocketContainer;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;

import org.amdatu.testing.configurator.TestConfigurator;
import org.amdatu.web.websocket.WebSocketConstants;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Constants;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.ServiceRegistration;
import org.osgi.framework.dto.ServiceReferenceDTO;
import org.osgi.service.cm.ManagedService;
import org.osgi.service.http.context.ServletContextHelper;
import org.osgi.service.http.runtime.HttpServiceRuntime;
import org.osgi.service.http.runtime.HttpServiceRuntimeConstants;
import org.osgi.service.http.runtime.dto.RuntimeDTO;
import org.osgi.service.http.whiteboard.HttpWhiteboardConstants;

@RunWith(JUnit4.class)
public class WebSocketRegistrationTest {

    private volatile WebSocketContainer m_clientContainer;
    private volatile HttpServiceRuntime m_httpServiceRuntime;

    private final List<ServiceRegistration<?>> serviceRegistrations = new ArrayList<>();
    private String m_websocketEndpoint;

    @Before
    public void before() throws Exception {
        TestConfigurator.configure(this)
            .add(TestConfigurator.createServiceDependency().setService(WebSocketContainer.class).setRequired(true))
            .add(TestConfigurator.createServiceDependency().setService(HttpServiceRuntime.class).setRequired(true))
            .apply();

        m_websocketEndpoint = getWebSocketEndpoint();
    }

    /**
     * The test is using a dynamic HTTP port, get the endpoint information from the {@link HttpServiceRuntime} and use that to
     * create the websocket endpoint.
     *
     * @return Endpoint from the {@link HttpServiceRuntime} with the protocol changed from http to ws
     */
    private String getWebSocketEndpoint() {
        RuntimeDTO runtimeDTO = m_httpServiceRuntime.getRuntimeDTO();
        ServiceReferenceDTO serviceDTO = runtimeDTO.serviceDTO;
        String[] endpoint = (String[])serviceDTO.properties.get(HttpServiceRuntimeConstants.HTTP_SERVICE_ENDPOINT);
        return endpoint[0].replace("http","ws");
    }

    @After
    public void after() {
        serviceRegistrations.forEach(reg -> {
            try {
                reg.unregister();
            } catch (IllegalStateException e) {
                // ignore already unregistered
            }
        });
        serviceRegistrations.clear();

        TestConfigurator.cleanUp(this);
    }

    public abstract class BaseTestEndpoint {

        protected String m_endpointId;

        public BaseTestEndpoint() {
            m_endpointId = UUID.randomUUID().toString();
        }

        public String getEndpointId() {
            return m_endpointId;
        }
    }

    @ServerEndpoint("/echo")
    public class TestEndpoint extends BaseTestEndpoint {

        @OnMessage
        public String onMessage(String message, Session session) {
            return m_endpointId + "-" + message;
        }

    }

    @ServerEndpoint("/echo/{param}")
    public class TestEndpointWithPathParam extends BaseTestEndpoint {

        @OnMessage
        public String onMessage(@PathParam("param") String param, String message) {
            return getEndpointId() + "-" + param + "-" + message;
        }
    }

    @Test
    public void defaultServlet() throws Exception {
        TestEndpoint endpoint = new TestEndpoint();
        ServiceRegistration<?> endpointReg = registerEndpoint(endpoint, null);
        assertEndpointAvailable(endpoint.getEndpointId(), "websocket/echo");

        endpointReg.unregister();
        assertEndpointUnavailable("websocket/echo");

        endpoint = new TestEndpoint();
        endpointReg = registerEndpoint(endpoint, createServletSelectFilter(WEBSOCKET_DEFAULT_SERVLET_NAME));
        assertEndpointAvailable(endpoint.getEndpointId(), "websocket/echo");

        endpointReg.unregister();
        assertEndpointUnavailable("websocket/echo");
    }

    @Test
    public void defaultServletConfiguration() throws Exception {
        TestEndpoint endpoint = new TestEndpoint();
        registerEndpoint(endpoint, null);
        assertEndpointAvailable(endpoint.getEndpointId(), "websocket/echo");

        Hashtable<String, Object> properties = new Hashtable<>();
        properties.put("defaultWebSocketServletEnabled", false);
        updateConfig(properties);

        assertEndpointUnavailable("/websocket/echo");

        properties = new Hashtable<>();
        properties.put("defaultWebSocketServletPattern", "/default/*");
        updateConfig(properties);
        assertEndpointAvailable(endpoint.getEndpointId(), "default/echo");

        updateConfig(null);
        assertEndpointAvailable(endpoint.getEndpointId(), "websocket/echo");
    }

    private void updateConfig(Hashtable<String, Object> properties) throws Exception {
        BundleContext bundleContext = FrameworkUtil.getBundle(getClass()).getBundleContext();
        Collection<ServiceReference<ManagedService>> serviceReferences = bundleContext
            .getServiceReferences(ManagedService.class, "(" + Constants.SERVICE_PID + "=org.amdatu.web.websocket)");
        assertEquals(1, serviceReferences.size());
        ServiceReference<ManagedService> next = serviceReferences.iterator().next();

        ManagedService service = bundleContext.getService(next);
        service.updated(properties);
        bundleContext.ungetService(next);
    }

    @Test
    public void endpointWitoutSevletSelectOnlyBindsToDefaultServlet() throws Exception {
        registerServlet("test", "/test/*", null);

        TestEndpoint defaultServletEndpoint = new TestEndpoint();
        registerEndpoint(defaultServletEndpoint, null);
        assertEndpointAvailable(defaultServletEndpoint.getEndpointId(), "websocket/echo");

        assertEndpointUnavailable("test/echo");

        TestEndpoint testServletEndpoint = new TestEndpoint();
        registerEndpoint(testServletEndpoint, createServletSelectFilter("test"));
        assertEndpointAvailable(testServletEndpoint.getEndpointId(), "test/echo");

        // Make sure the endpoint on the default servlet is still the same servlet
        assertEndpointAvailable(defaultServletEndpoint.getEndpointId(), "websocket/echo");
    }

    @Test
    public void registerServletAfterEndpoint() throws Exception {
        assertEndpointUnavailable("test/echo");

        TestEndpoint testServletEndpoint = new TestEndpoint();
        registerEndpoint(testServletEndpoint, createServletSelectFilter("test"));
        assertEndpointUnavailable("test/echo");

        registerServlet("test", "/test/*", null);
        assertEndpointAvailable(testServletEndpoint.getEndpointId(), "test/echo");
    }

    @Test
    public void endpointWithInvalidServletSelectNotRegistered() throws Exception {
        registerServlet("test", "/test/*", null);

        TestEndpoint testServletEndpoint = new TestEndpoint();
        registerEndpoint(testServletEndpoint, createServletSelectFilter("test").substring(1));

        // Make sure it's not available at both servlets
        assertEndpointUnavailable("test/echo");
        assertEndpointUnavailable("websocket/echo");
    }

    @Test
    public void registerServletWithContextSelect() throws Exception {
        assertEndpointUnavailable("/test/echo");

        registerContext("/testcontext", "testContext", true);

        registerServlet("test", "/test/*", createContextSelectFilter("testContext"));

        TestEndpoint testServletEndpoint = new TestEndpoint();
        registerEndpoint(testServletEndpoint, createServletSelectFilter("test"));
        assertEndpointAvailable(testServletEndpoint.getEndpointId(), "testcontext/test/echo");
    }

    @Test
    public void contextSecurity() throws Exception {
        assertEndpointUnavailable("/test/echo");

        registerContext("/testcontext", "testContext", false);

        registerServlet("test", "/test/*", createContextSelectFilter("testContext"));

        TestEndpoint testServletEndpoint = new TestEndpoint();
        registerEndpoint(testServletEndpoint, createServletSelectFilter("test"));
        assertEndpointUnavailable("testcontext/test/echo");
    }

    @Test
    public void testEndpointWithPathParam() throws Exception {
        TestEndpointWithPathParam endpoint = new TestEndpointWithPathParam();
        registerEndpoint(endpoint, null);
        assertEndpointAvailable(endpoint.getEndpointId() + "-paramValue", "websocket/echo/paramValue");
        assertEndpointAvailable(endpoint.getEndpointId() + "-otherValue", "websocket/echo/otherValue");
    }

    private String createServletSelectFilter(String name) {
        return "(" + WEBSOCKET_NAME + "=" + name + ")";
    }

    private String createContextSelectFilter(String name) {
        return "(" + HTTP_WHITEBOARD_CONTEXT_NAME + "=" + name + ")";
    }

    private ServiceRegistration<?> registerContext(String path, String name, boolean handleSecurityResponse) throws Exception {
        Hashtable<String, Object> properties = new Hashtable<>();
        properties.put(HTTP_WHITEBOARD_CONTEXT_NAME, name);
        properties.put(HTTP_WHITEBOARD_CONTEXT_PATH, path);

        return registerService(ServletContextHelper.class.getName(), new ServletContextHelper() {
            @Override
            public boolean handleSecurity(HttpServletRequest request, HttpServletResponse response) throws IOException {
                return handleSecurityResponse;
            }
        }, properties);
    }

    private ServiceRegistration<?> registerServlet(String name, String pattern, String contextSelect) throws Exception {
        Hashtable<String, Object> properties = new Hashtable<>();
        properties.put(WebSocketConstants.WEBSOCKET_NAME, name);
        properties.put(WebSocketConstants.WEBSOCKET_SERVLET_PATTERN, pattern);
        if (contextSelect != null) {
            properties.put(HttpWhiteboardConstants.HTTP_WHITEBOARD_CONTEXT_SELECT, contextSelect);
        }

        return registerService(Object.class.getName(), new Object(), properties);
    }

    private ServiceRegistration<?> registerEndpoint(Object endpoint, String servletSelect) throws InterruptedException {
        Hashtable<String, Object> properties = new Hashtable<>();
        properties.put(WebSocketConstants.WEBSOCKET_ENDPOINT, true);
        if (servletSelect != null) {
            properties.put(WebSocketConstants.WEBSOCKET_SERVLET_SELECT, servletSelect);
        }

        return registerService(endpoint.getClass().getName(), endpoint, properties);
    }

    private ServiceRegistration<?> registerService(String objectClass, Object service, Dictionary<String, Object> props)
        throws InterruptedException {
        BundleContext bundleContext = FrameworkUtil.getBundle(getClass()).getBundleContext();
        ServiceRegistration<?> serviceReg = bundleContext.registerService(objectClass, service, props);
        this.serviceRegistrations.add(serviceReg);
        return serviceReg;
    }

    private void assertEndpointAvailable(String expectedPrefix, String path) throws Exception {
        CountDownLatch messageLatch = new CountDownLatch(1);

        String testMessage = UUID.randomUUID().toString();
        AtomicReference<String> result = new AtomicReference<>();
        Session session = m_clientContainer.connectToServer(new Endpoint() {

            @Override
            public void onOpen(Session session, EndpointConfig config) {
                session.addMessageHandler(new MessageHandler.Whole<String>() {

                    @Override
                    public void onMessage(String message) {
                        System.out.println("Received message: " + message);
                        result.set(message);
                        messageLatch.countDown();
                    }
                });

            }
        }, new URI(m_websocketEndpoint + path));

        session.getBasicRemote().sendText(testMessage);

        messageLatch.await(1, TimeUnit.SECONDS);
        assertEquals(expectedPrefix + "-" + testMessage, result.get());
    }

    private void assertEndpointUnavailable(String path) throws Exception {
        try {
            m_clientContainer.connectToServer(new Endpoint() {

                @Override
                public void onOpen(Session session, EndpointConfig config) {
                }

            }, new URI(m_websocketEndpoint + path));

            fail("Expected connection failure");
        } catch (IOException e) {
            assertEquals("Connect failure", e.getMessage());
        }
    }

}
