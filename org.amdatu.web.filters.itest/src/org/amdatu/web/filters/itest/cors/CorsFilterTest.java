/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.filters.itest.cors;

import static org.amdatu.testing.configurator.TestConfigurator.configure;
import static org.amdatu.testing.configurator.TestConfigurator.createComponent;
import static org.amdatu.testing.configurator.TestConfigurator.createServiceDependency;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.Properties;

import javax.servlet.Servlet;

import org.amdatu.testing.configurator.TestConfigurator;
import org.amdatu.web.filters.itest.TestServlet;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.osgi.framework.Constants;
import org.osgi.framework.ServiceReference;
import org.osgi.service.cm.ManagedServiceFactory;
import org.osgi.service.http.HttpService;
import org.osgi.service.http.whiteboard.HttpWhiteboardConstants;

import com.github.kevinsawicki.http.HttpRequest;

@RunWith(JUnit4.class)
public class CorsFilterTest {

    private static final String DEFAULT_ALLOW_CREDENTIALS = "true";
    private static final String DEFAULT_ALLOW_HEADERS = "Origin, Content-Type, If-Match";
    private static final String DEFAULT_ALLOW_METHODS = "GET, POST, PUT, DELETE, OPTIONS, HEAD";
    private static final String DEFAULT_ALLOW_ORIGIN = "*";
    private static final String DEFAULT_EXPOSE_HEADERS = "Link, Location, ETag";

    private String m_testUrl;
    private volatile ManagedServiceFactory m_filterFactory;

    @Before
    public void before() {
        Properties testServletProps = new Properties();
        testServletProps.put(HttpWhiteboardConstants.HTTP_WHITEBOARD_SERVLET_PATTERN, "/*");

        String managedServiceFactoryFilter = String.format("(%s=%s)", Constants.SERVICE_PID, "org.amdatu.web.filters.cors");
        configure(this)
            .add(createComponent()
                    .setInterface(Servlet.class.getName(), testServletProps)
                    .setImplementation(new TestServlet()))
            .add(createServiceDependency()
                    .setService(ManagedServiceFactory.class, managedServiceFactoryFilter)
                    .setRequired(true))
            .add(createServiceDependency().setService(HttpService.class)
                .setCallbacks("addHttpService", null)
                .setRequired(true))
            .apply();
    }

    protected final void addHttpService(ServiceReference<HttpService> ref) {
        String[] endpoint = (String[]) ref.getProperty("osgi.http.endpoint");
        m_testUrl = endpoint[0].concat("redirect");
    }

    @After
    public void after() {
        TestConfigurator.cleanUp(this);
    }

    @Test
    public void testCorsFilterConfiguration() throws Exception {
        Dictionary<String, Object> props = new Hashtable<>();
        props.put(HttpWhiteboardConstants.HTTP_WHITEBOARD_FILTER_REGEX, ".*");
        m_filterFactory.updated("test", props);
        assertHeaderValue(m_testUrl, "Access-Control-Allow-Credentials", DEFAULT_ALLOW_CREDENTIALS);
        assertHeaderValue(m_testUrl, "Access-Control-Allow-Headers", DEFAULT_ALLOW_HEADERS);
        assertHeaderValue(m_testUrl, "Access-Control-Allow-Methods", DEFAULT_ALLOW_METHODS);
        assertHeaderValue(m_testUrl, "Access-Control-Allow-Origin", DEFAULT_ALLOW_ORIGIN);
        assertHeaderValue(m_testUrl, "Access-Control-Expose-Headers", DEFAULT_EXPOSE_HEADERS);


        props.put("allowCredentials", "allowCredentials");;
        m_filterFactory.updated("test", props);
        assertHeaderValue(m_testUrl, "Access-Control-Allow-Credentials", "allowCredentials");
        assertHeaderValue(m_testUrl, "Access-Control-Allow-Headers", DEFAULT_ALLOW_HEADERS);
        assertHeaderValue(m_testUrl, "Access-Control-Allow-Methods", DEFAULT_ALLOW_METHODS);
        assertHeaderValue(m_testUrl, "Access-Control-Allow-Origin", DEFAULT_ALLOW_ORIGIN);
        assertHeaderValue(m_testUrl, "Access-Control-Expose-Headers", DEFAULT_EXPOSE_HEADERS);

        props.put("allowHeaders", "allowHeaders");
        m_filterFactory.updated("test", props);
        assertHeaderValue(m_testUrl, "Access-Control-Allow-Credentials", "allowCredentials");
        assertHeaderValue(m_testUrl, "Access-Control-Allow-Headers", "allowHeaders");
        assertHeaderValue(m_testUrl, "Access-Control-Allow-Methods", DEFAULT_ALLOW_METHODS);
        assertHeaderValue(m_testUrl, "Access-Control-Allow-Origin", DEFAULT_ALLOW_ORIGIN);
        assertHeaderValue(m_testUrl, "Access-Control-Expose-Headers", DEFAULT_EXPOSE_HEADERS);

        props.put("allowMethods", "allowMethods");
        m_filterFactory.updated("test", props);
        assertHeaderValue(m_testUrl, "Access-Control-Allow-Credentials", "allowCredentials");
        assertHeaderValue(m_testUrl, "Access-Control-Allow-Headers", "allowHeaders");
        assertHeaderValue(m_testUrl, "Access-Control-Allow-Methods", "allowMethods");
        assertHeaderValue(m_testUrl, "Access-Control-Allow-Origin", DEFAULT_ALLOW_ORIGIN);
        assertHeaderValue(m_testUrl, "Access-Control-Expose-Headers", DEFAULT_EXPOSE_HEADERS);

        props.put("allowOrigin", "allowOrigin");
        m_filterFactory.updated("test", props);
        assertHeaderValue(m_testUrl, "Access-Control-Allow-Credentials", "allowCredentials");
        assertHeaderValue(m_testUrl, "Access-Control-Allow-Headers", "allowHeaders");
        assertHeaderValue(m_testUrl, "Access-Control-Allow-Methods", "allowMethods");
        assertHeaderValue(m_testUrl, "Access-Control-Allow-Origin", "allowOrigin");
        assertHeaderValue(m_testUrl, "Access-Control-Expose-Headers", DEFAULT_EXPOSE_HEADERS);

        props.put("exposeHeaders", "exposeHeaders");
        m_filterFactory.updated("test", props);
        assertHeaderValue(m_testUrl, "Access-Control-Allow-Credentials", "allowCredentials");
        assertHeaderValue(m_testUrl, "Access-Control-Allow-Headers", "allowHeaders");
        assertHeaderValue(m_testUrl, "Access-Control-Allow-Methods", "allowMethods");
        assertHeaderValue(m_testUrl, "Access-Control-Allow-Origin", "allowOrigin");
        assertHeaderValue(m_testUrl, "Access-Control-Expose-Headers", "exposeHeaders");

        m_filterFactory.deleted("test");
        assertHeaderValue(m_testUrl, "Access-Control-Allow-Credentials", null);
        assertHeaderValue(m_testUrl, "Access-Control-Allow-Headers", null);
        assertHeaderValue(m_testUrl, "Access-Control-Allow-Methods", null);
        assertHeaderValue(m_testUrl, "Access-Control-Allow-Origin", null);
        assertHeaderValue(m_testUrl, "Access-Control-Expose-Headers", null);

    }

    private void assertHeaderValue(String url, String header, String expected) throws IOException {
        assertEquals(expected, getHeader(url, header));
    }

    private String getHeader(String url, String header) throws IOException {
        HttpRequest connection = HttpRequest.get(url);
        connection.followRedirects(false);

        int responseCode = connection.code();
        assertTrue(responseCode >= 200 && responseCode < 400);

        return connection.header(header);
    }

}
