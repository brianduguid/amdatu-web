/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest;

import static org.amdatu.web.rest.jaxrs.AmdatuWebRestConstants.JAX_RS_APPLICATION_BASE;
import static org.amdatu.web.rest.jaxrs.AmdatuWebRestConstants.JAX_RS_APPLICATION_NAME;
import static org.amdatu.web.rest.jaxrs.AmdatuWebRestConstants.JAX_RS_APPLICATION_TYPE;
import static org.amdatu.web.rest.jaxrs.AmdatuWebRestConstants.JAX_RS_APPLICATION_TYPE_DYNAMIC;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Application;
import javax.ws.rs.ext.Provider;

import org.amdatu.web.rest.jaxrs.JaxRsRequestInterceptor;
import org.amdatu.web.rest.jaxrs.RequestContext;

/**
 * Tests the usage of {@link JaxRsRequestInterceptorTest}
 */
public class JaxRsRequestInterceptorTest extends RegistrationTestBase {
    private static final String TEST_APPLICATION_NAME = "org.amdatu.web.rest.JaxRsRequestInterceptorTest";
    private static final String PREFIX = "/prefix";
    private static final String RESOURCE = "/test";

    @Path(RESOURCE)
    public static class TestResource {

        @GET
        @Path("{arg}")
        public String handleGetRequest(@PathParam("arg") Integer status) {
            return "test";
        }
    }

    @Provider
    public static class TestJaxRsRequestInterceptor implements JaxRsRequestInterceptor {
        @Override
        public void intercept(RequestContext context) {
            throw new WebApplicationException((Integer) context.getParams()[0]);
        }
    }

    public void testJaxRsRequestInterceptor() throws Exception {
        registerService(Application.class, new Application(),
            JAX_RS_APPLICATION_BASE, PREFIX,
            JAX_RS_APPLICATION_NAME, TEST_APPLICATION_NAME,
            JAX_RS_APPLICATION_TYPE, JAX_RS_APPLICATION_TYPE_DYNAMIC);

        registerService(Object.class, new TestResource(), JAX_RS_APPLICATION_NAME, TEST_APPLICATION_NAME);
        registerService(Object.class, new TestJaxRsRequestInterceptor(), JAX_RS_APPLICATION_NAME, TEST_APPLICATION_NAME);

        assertResponseCode(123, PREFIX + RESOURCE + "/" + 123);
        assertResponseCode(456, PREFIX + RESOURCE + "/" + 456);
        assertResponseCode(789, PREFIX + RESOURCE + "/" + 789);
    }

}
