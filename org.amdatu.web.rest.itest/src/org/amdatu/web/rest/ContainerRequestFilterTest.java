/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest;

import static org.amdatu.web.rest.jaxrs.AmdatuWebRestConstants.JAX_RS_APPLICATION_BASE;
import static org.amdatu.web.rest.jaxrs.AmdatuWebRestConstants.JAX_RS_FILTER_BASE;
import static org.amdatu.web.rest.jaxrs.AmdatuWebRestConstants.JAX_RS_RESOURCE_BASE;

import java.io.IOException;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.Collections;
import java.util.Set;

import javax.ws.rs.GET;
import javax.ws.rs.NameBinding;
import javax.ws.rs.Path;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

import org.osgi.framework.ServiceRegistration;

/**
 * Tests the usage of {@link ContainerRequestFilter}
 */
public class ContainerRequestFilterTest extends RegistrationTestBase {

    private static final String PREFIX = "/prefix";
    private static final String RESOURCE = "/test";
    private static final String APP = "/app";

    @Path(RESOURCE)
    public static class TestResource {

        @GET
        public String handleGetRequest() {
            return "test";
        }
    }

    @Path(APP)
    public static class ApplicationResource {

        @GET
        public String handleGetRequest() {
            return "test";
        }
    }

    public static class TestApplication extends Application {

        @Override
        public Set<Object> getSingletons() {
            return Collections.singleton(new ApplicationResource());
        }

    }

    @Provider
    public static class TestContainerRequestFilter implements ContainerRequestFilter {

        private final Integer m_status;

        public TestContainerRequestFilter(int status) {
            m_status = status;
        }

        @Override
        public void filter(ContainerRequestContext requestContext) throws IOException {
            requestContext.abortWith(Response.status(m_status).build());
        }

    }

    @Retention(RetentionPolicy.RUNTIME)
    @Target( { ElementType.TYPE, ElementType.METHOD })
    @NameBinding
    public @interface NameBindingFilter { }

    @Provider
    @NameBindingFilter
    public static class TestNameBindingFilter extends TestContainerRequestFilter {
        public TestNameBindingFilter() {
            super(HTTP_FORBIDDEN);
        }
    }

    @Path(RESOURCE)
    @NameBindingFilter
    public static class TestResourceWithNameBindingFilterOnType extends TestResource {
    }

    @Path(RESOURCE)
    public static class TestResourceWithNameBindingFilterOnMethod {

        @GET
        @NameBindingFilter
        public String handleGetRequest() {
            return "test";
        }

        @GET
        @Path("other")
        public String handleOtherGetRequest() {
            return "test";
        }
    }

    @NameBindingFilter
    public static class TestApplicationWithNameBindingInterceptor extends Application {

        @Override
        public Set<Object> getSingletons() {
            return Collections.singleton(new ApplicationResource());
        }
    }

    public void testWhiteboardResourceInterceptor() throws Exception {
        registerService(Object.class, new TestResource(), JAX_RS_RESOURCE_BASE, PREFIX);
        registerService(Object.class, new TestContainerRequestFilter(HTTP_FORBIDDEN), JAX_RS_FILTER_BASE, PREFIX);

        assertResponseCode(HTTP_FORBIDDEN, PREFIX + RESOURCE);
    }

    public void testWhiteboardResourceInterceptor_twoSegmentBase() throws Exception {
        registerService(Object.class, new TestResource(), JAX_RS_RESOURCE_BASE, PREFIX + PREFIX);
        registerService(Object.class, new TestContainerRequestFilter(HTTP_FORBIDDEN), JAX_RS_FILTER_BASE, PREFIX);

        assertResponseCode(HTTP_FORBIDDEN, PREFIX + PREFIX + RESOURCE);
    }

    public void testWhiteboardResourceInterceptor_notMatching() throws Exception {
        registerService(Object.class, new TestResource(), JAX_RS_RESOURCE_BASE, PREFIX );
        registerService(Object.class, new TestContainerRequestFilter(HTTP_FORBIDDEN), JAX_RS_FILTER_BASE, PREFIX + "/nomatch");

        assertResponseCode(200, PREFIX + RESOURCE);
    }

    public void testWhiteboardApplicationInterceptor() throws Exception {
        registerService(Application.class, new TestApplication(), JAX_RS_APPLICATION_BASE, PREFIX );
        registerService(Object.class, new TestContainerRequestFilter(HTTP_FORBIDDEN), JAX_RS_FILTER_BASE, PREFIX);

        assertResponseCode(HTTP_FORBIDDEN, PREFIX + APP);
    }

    public void testWhiteboardApplicationInterceptor_twoSegmentBase() throws Exception {
        registerService(Application.class, new TestApplication(), JAX_RS_APPLICATION_BASE, PREFIX + PREFIX);
        registerService(Object.class, new TestContainerRequestFilter(HTTP_FORBIDDEN), JAX_RS_FILTER_BASE, PREFIX);

        assertResponseCode(HTTP_FORBIDDEN, PREFIX + PREFIX + APP);
    }

    public void testWhiteboardApplicationInterceptor_notMatching() throws Exception {
        registerService(Application.class, new TestApplication(), JAX_RS_APPLICATION_BASE, PREFIX );
        registerService(Object.class, new TestContainerRequestFilter(HTTP_FORBIDDEN), JAX_RS_FILTER_BASE, PREFIX + "/nomatch");

        assertResponseCode(HTTP_OK, PREFIX + APP);
    }

    public void testWhiteboardResourceAndApplicationInterceptor() throws Exception {
        registerService(Application.class, new TestApplication(), JAX_RS_APPLICATION_BASE, PREFIX + APP );
        registerService(Object.class, new TestResource(), JAX_RS_RESOURCE_BASE, PREFIX + RESOURCE);
        ServiceRegistration<Object> interceptorReg = registerService(Object.class, new TestContainerRequestFilter(HTTP_FORBIDDEN), JAX_RS_FILTER_BASE, PREFIX);

        assertResponseCode(HTTP_FORBIDDEN, PREFIX + APP + APP);
        assertResponseCode(HTTP_FORBIDDEN, PREFIX + RESOURCE + RESOURCE);

        interceptorReg.unregister();

        assertResponseCode(HTTP_OK, PREFIX + APP + APP);
        assertResponseCode(HTTP_OK, PREFIX + RESOURCE + RESOURCE);
    }

    public void testWhiteboardResourceNameBindingInterceptor() throws Exception {
        registerService(Object.class, new TestResourceWithNameBindingFilterOnType(), JAX_RS_RESOURCE_BASE, PREFIX);
        registerService(Object.class, new TestNameBindingFilter(), JAX_RS_FILTER_BASE, PREFIX);

        assertResponseCode(HTTP_FORBIDDEN, PREFIX + RESOURCE);
    }

    public void testWhiteboardResourceNameBindingInterceptor_method() throws Exception {
        registerService(Object.class, new TestResourceWithNameBindingFilterOnMethod(), JAX_RS_RESOURCE_BASE, PREFIX);
        registerService(Object.class, new TestNameBindingFilter(), JAX_RS_FILTER_BASE, PREFIX);

        assertResponseCode(HTTP_FORBIDDEN, PREFIX + RESOURCE);
        assertResponseCode(HTTP_OK, PREFIX + RESOURCE + "/other");
    }

    public void testWhiteboardApplicationNameBindingInterceptor() throws Exception {
        registerService(Application.class, new TestApplicationWithNameBindingInterceptor(), JAX_RS_APPLICATION_BASE, PREFIX + "/intercept");
        registerService(Application.class, new TestApplication(), JAX_RS_APPLICATION_BASE, PREFIX + "/no-intercept");
        registerService(Object.class, new TestNameBindingFilter(), JAX_RS_FILTER_BASE, PREFIX);

        assertResponseCode(HTTP_FORBIDDEN, PREFIX + "/intercept" + APP);
        assertResponseCode(HTTP_OK, PREFIX + "/no-intercept" + APP);
    }
}
