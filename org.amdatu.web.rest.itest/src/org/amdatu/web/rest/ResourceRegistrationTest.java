/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest;


import static org.amdatu.web.rest.jaxrs.AmdatuWebRestConstants.JAX_RS_RESOURCE_BASE;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.Response;

import org.osgi.framework.ServiceRegistration;

/**
 * Tests the (de-)registration of resources
 */
public class ResourceRegistrationTest extends RegistrationTestBase {
    private static final String PREFIX = "/prefix";
    private static final String ASYNC = "/async";
    private static final String BASE = "/base";
    private static final String SUB = "/base/sub";

    @Path(BASE)
    public static class TestBaseResource {
        @GET
        public String handleGetRequest() {
            return "base";
        }
    }

    @Path(SUB)
    public static class TestSubResource {
        @GET
        public String handleGetRequest() {
            return "sub";
        }
    }
    
    @Path(ASYNC)
    public class AcyncResource {
        @GET
        public void handleGetRequestAsync(@Suspended final AsyncResponse response) throws Exception {
            new Thread() {
                @Override
                public void run() {
                    try {
                        response.resume(Response.ok("basic").build());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }.start();
        }
    }

    public void testBaseResourceRegistration() throws Exception {
        assertResponseCode(HTTP_NOT_FOUND, BASE);

        ServiceRegistration<Object> srvReg =
            registerService(Object.class, new TestBaseResource(), JAX_RS_RESOURCE_BASE, "");

        assertResponseCode(HTTP_OK, BASE);

        srvReg.unregister();

        assertResponseCode(HTTP_NOT_FOUND, BASE);
    }

    public void testResourceRegistrationResourceBaseSlash() throws Exception {
        registerService(Object.class, new TestBaseResource(), JAX_RS_RESOURCE_BASE, "/");
        assertResponseCode(HTTP_OK, BASE);
    }

    public void testResourceRegistrationResourceBaseEndsWithSlash() throws Exception {
        registerService(Object.class, new TestBaseResource(), JAX_RS_RESOURCE_BASE, PREFIX.concat("/"));
        assertResponseCode(HTTP_OK, PREFIX + BASE);
    }

    public void testBaseResourceRegistrationWithPrefixOverride() throws Exception {
        String prefixedBase = PREFIX.concat(BASE);

        assertResponseCode(HTTP_NOT_FOUND, prefixedBase);

        ServiceRegistration<Object> srvReg =
            registerService(Object.class, new TestBaseResource(), JAX_RS_RESOURCE_BASE, PREFIX);

        assertResponseCode(HTTP_NOT_FOUND, BASE);
        assertResponseCode(HTTP_OK, prefixedBase);

        srvReg.unregister();

        assertResponseCode(HTTP_NOT_FOUND, prefixedBase);
        assertResponseCode(HTTP_NOT_FOUND, BASE);
    }

    /**
     * Tests that we can "extend" a resource by using the same base path an existing resource.
     */
    public void testBaseWithSubResourceRegistration() throws Exception {

        assertResponseCode(HTTP_NOT_FOUND, BASE);
        assertResponseCode(HTTP_NOT_FOUND, SUB);

        ServiceRegistration<Object> baseSrvReg =
            registerService(Object.class, new TestBaseResource(), JAX_RS_RESOURCE_BASE, "");
        ServiceRegistration<Object> subSrvReg =
            registerService(Object.class, new TestSubResource(), JAX_RS_RESOURCE_BASE, "");

        assertResponseCode(HTTP_OK, BASE);

        assertResponseCode(HTTP_OK,  SUB);

        baseSrvReg.unregister();

        assertResponseCode(HTTP_NOT_FOUND, BASE);
        assertResponseCode(HTTP_OK, SUB);

        subSrvReg.unregister();

        assertResponseCode(HTTP_NOT_FOUND, BASE);
        assertResponseCode(HTTP_NOT_FOUND, BASE + SUB);
    }

    public void testBaseWithSubResourceRegistrationWithPrefixOverride() throws Exception {
        String prefixedBase = PREFIX.concat(BASE);
        String prefixedSub = PREFIX.concat(SUB);

        assertResponseCode(HTTP_NOT_FOUND, prefixedBase);
        assertResponseCode(HTTP_NOT_FOUND, prefixedSub);

        ServiceRegistration<Object> baseSrvReg =
            registerService(Object.class, new TestBaseResource(), JAX_RS_RESOURCE_BASE, PREFIX);
        ServiceRegistration<Object> subSrvReg =
            registerService(Object.class, new TestSubResource(), JAX_RS_RESOURCE_BASE, PREFIX);

        assertResponseCode(HTTP_OK, prefixedBase);
        assertResponseCode(HTTP_OK, prefixedSub);

        baseSrvReg.unregister();

        assertResponseCode(HTTP_NOT_FOUND, prefixedBase);
        assertResponseCode(HTTP_OK, prefixedSub);

        subSrvReg.unregister();

        assertResponseCode(HTTP_NOT_FOUND, prefixedBase);
        assertResponseCode(HTTP_NOT_FOUND, prefixedSub);
    }

    
    public void testAsyncResourceRegistration() throws Exception {
        assertResponseCode(HTTP_NOT_FOUND, ASYNC);

        ServiceRegistration<Object> srvReg =
            registerService(Object.class, new AcyncResource(), JAX_RS_RESOURCE_BASE, "");

        assertResponseCode(HTTP_OK, ASYNC);

        srvReg.unregister();

        assertResponseCode(HTTP_NOT_FOUND, ASYNC);
    }

}
