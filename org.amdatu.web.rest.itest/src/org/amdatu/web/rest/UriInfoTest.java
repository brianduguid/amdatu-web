/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest;

import static org.amdatu.web.rest.jaxrs.AmdatuWebRestConstants.JAX_RS_APPLICATION_BASE;
import static org.amdatu.web.rest.jaxrs.AmdatuWebRestConstants.JAX_RS_APPLICATION_CONTEXT;
import static org.amdatu.web.rest.jaxrs.AmdatuWebRestConstants.JAX_RS_APPLICATION_NAME;
import static org.amdatu.web.rest.jaxrs.AmdatuWebRestConstants.JAX_RS_APPLICATION_TYPE;
import static org.amdatu.web.rest.jaxrs.AmdatuWebRestConstants.JAX_RS_APPLICATION_TYPE_DYNAMIC;
import static org.amdatu.web.rest.jaxrs.AmdatuWebRestConstants.JAX_RS_RESOURCE_BASE;
import static org.osgi.service.http.whiteboard.HttpWhiteboardConstants.HTTP_WHITEBOARD_CONTEXT_NAME;
import static org.osgi.service.http.whiteboard.HttpWhiteboardConstants.HTTP_WHITEBOARD_CONTEXT_PATH;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;

import org.osgi.service.http.context.ServletContextHelper;

public class UriInfoTest extends RegistrationTestBase {

    private static final String CONTEXT_NAME = "contextName";
    private static final String CONTEXT_PATH = "/context";
    private static final String APPLICATION_NAME = "testApp";
    private static final String PREFIX = "/prefix";
    private static final String RESOURCE = "/resource";

    @Path(RESOURCE)
    public static class TestBaseResource {

        @GET
        public String handleGetRequest(@Context UriInfo uriInfo) {
            return uriInfo.getBaseUri().getPath();
        }
    }

    public void testBaseResourceRegistration() throws Exception {
        registerService(Object.class, new TestBaseResource(), JAX_RS_RESOURCE_BASE, PREFIX);
        assertResponseContent(PREFIX + "/", PREFIX + RESOURCE);
    }

    public void test() throws Exception {

        registerService(ServletContextHelper.class, new ServletContextHelper() {
        },
            HTTP_WHITEBOARD_CONTEXT_NAME, CONTEXT_NAME,
            HTTP_WHITEBOARD_CONTEXT_PATH, CONTEXT_PATH);

        registerService(Application.class, new Application(),
            JAX_RS_APPLICATION_NAME, APPLICATION_NAME,
            JAX_RS_APPLICATION_BASE, PREFIX,
            JAX_RS_APPLICATION_CONTEXT, CONTEXT_NAME,
            JAX_RS_APPLICATION_TYPE, JAX_RS_APPLICATION_TYPE_DYNAMIC);

        registerService(Object.class, new TestBaseResource(),
            JAX_RS_APPLICATION_NAME, APPLICATION_NAME);

        assertResponseContent(CONTEXT_PATH + PREFIX + "/", CONTEXT_PATH + PREFIX + RESOURCE);
    }

}
