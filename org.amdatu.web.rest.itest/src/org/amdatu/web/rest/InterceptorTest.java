/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest;

import static org.amdatu.web.rest.jaxrs.AmdatuWebRestConstants.JAX_RS_APPLICATION_BASE;
import static org.amdatu.web.rest.jaxrs.AmdatuWebRestConstants.JAX_RS_INTERCEPTOR_BASE;
import static org.amdatu.web.rest.jaxrs.AmdatuWebRestConstants.JAX_RS_RESOURCE_BASE;

import java.io.IOException;
import java.io.PrintWriter;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.Collections;
import java.util.Set;

import javax.ws.rs.GET;
import javax.ws.rs.NameBinding;
import javax.ws.rs.Path;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Application;
import javax.ws.rs.ext.Provider;
import javax.ws.rs.ext.WriterInterceptor;
import javax.ws.rs.ext.WriterInterceptorContext;

import org.osgi.framework.ServiceRegistration;

/**
 * Tests the usage of {@link ContainerRequestFilter}
 */
public class InterceptorTest extends RegistrationTestBase {

    private static final String PREFIX = "/prefix";
    private static final String RESOURCE = "/test";
    private static final String APP = "/app";

    @Path(RESOURCE)
    public static class TestResource {

        @GET
        public String handleGetRequest() {
            return "test";
        }
    }

    @Path(APP)
    public static class ApplicationResource {

        @GET
        public String handleGetRequest() {
            return "test";
        }
    }

    public static class TestApplication extends Application {

        @Override
        public Set<Object> getSingletons() {
            return Collections.singleton(new ApplicationResource());
        }

    }

    @Provider
    public static class TestWriterInterceptor implements WriterInterceptor {

        @Override
        public void aroundWriteTo(WriterInterceptorContext context) throws IOException, WebApplicationException {
            PrintWriter printWriter = new PrintWriter(context.getOutputStream());
            printWriter.write("[interceptor]");
            printWriter.flush();
            context.proceed();
        }

    }

    @Retention(RetentionPolicy.RUNTIME)
    @Target( { ElementType.TYPE, ElementType.METHOD })
    @NameBinding
    public @interface NameBindingInterceptor { }

    @Provider
    @NameBindingInterceptor
    public static class TestNameBindingWriterInterceptor extends TestWriterInterceptor { }

    @Path(RESOURCE)
    @NameBindingInterceptor
    public static class TestResourceWithNameBindingInterceptorOnType extends TestResource {
    }

    @Path(RESOURCE)
    public static class TestResourceWithNameBindingInterceptorOnMethod {

        @GET
        @NameBindingInterceptor
        public String handleGetRequest() {
            return "test";
        }

        @GET
        @Path("other")
        public String handleOtherGetRequest() {
            return "test";
        }
    }

    @NameBindingInterceptor
    public static class TestApplicationWithNameBindingInterceptor extends Application {

        @Override
        public Set<Object> getSingletons() {
            return Collections.singleton(new ApplicationResource());
        }
    }

    public void testWhiteboardResourceInterceptor() throws Exception {
        registerService(Object.class, new TestResource(), JAX_RS_RESOURCE_BASE, PREFIX);
        registerService(Object.class, new TestWriterInterceptor(), JAX_RS_INTERCEPTOR_BASE, PREFIX);

        assertResponseContent("[interceptor]test", PREFIX + RESOURCE);
    }

    public void testWhiteboardResourceInterceptor_twoSegmentBase() throws Exception {
        registerService(Object.class, new TestResource(), JAX_RS_RESOURCE_BASE, PREFIX + PREFIX);
        registerService(Object.class, new TestWriterInterceptor(), JAX_RS_INTERCEPTOR_BASE, PREFIX);

        assertResponseContent("[interceptor]test", PREFIX + PREFIX + RESOURCE);
    }

    public void testWhiteboardResourceInterceptor_notMatching() throws Exception {
        registerService(Object.class, new TestResource(), JAX_RS_RESOURCE_BASE, PREFIX );
        registerService(Object.class, new TestWriterInterceptor(), JAX_RS_INTERCEPTOR_BASE, PREFIX + "/nomatch");

        assertResponseContent("test",  PREFIX + RESOURCE);
    }

    public void testWhiteboardApplicationInterceptor() throws Exception {
        registerService(Application.class, new TestApplication(), JAX_RS_APPLICATION_BASE, PREFIX );
        registerService(Object.class, new TestWriterInterceptor(), JAX_RS_INTERCEPTOR_BASE, PREFIX);

        assertResponseContent("[interceptor]test",  PREFIX + APP);
    }

    public void testWhiteboardApplicationInterceptor_twoSegmentBase() throws Exception {
        registerService(Application.class, new TestApplication(), JAX_RS_APPLICATION_BASE, PREFIX + PREFIX);
        registerService(Object.class, new TestWriterInterceptor(), JAX_RS_INTERCEPTOR_BASE, PREFIX);

        assertResponseContent("[interceptor]test",  PREFIX + PREFIX + APP);
    }

    public void testWhiteboardApplicationInterceptor_notMatching() throws Exception {
        registerService(Application.class, new TestApplication(), JAX_RS_APPLICATION_BASE, PREFIX );
        registerService(Object.class, new TestWriterInterceptor(), JAX_RS_INTERCEPTOR_BASE, PREFIX + "/nomatch");

        assertResponseContent("test",  PREFIX + APP);
    }

    public void testWhiteboardResourceAndApplicationInterceptor() throws Exception {
        registerService(Application.class, new TestApplication(), JAX_RS_APPLICATION_BASE, PREFIX + APP );
        registerService(Object.class, new TestResource(), JAX_RS_RESOURCE_BASE, PREFIX + RESOURCE);
        ServiceRegistration<Object> interceptorReg = registerService(Object.class, new TestWriterInterceptor(), JAX_RS_INTERCEPTOR_BASE, PREFIX);

        assertResponseContent("[interceptor]test",  PREFIX + APP + APP);
        assertResponseContent("[interceptor]test",  PREFIX + RESOURCE + RESOURCE);

        interceptorReg.unregister();

        assertResponseContent("test",  PREFIX + APP + APP);
        assertResponseContent("test",  PREFIX + RESOURCE + RESOURCE);
    }

    public void testWhiteboardResourceNameBindingInterceptor() throws Exception {
        registerService(Object.class, new TestResourceWithNameBindingInterceptorOnType(), JAX_RS_RESOURCE_BASE, PREFIX);
        registerService(Object.class, new TestNameBindingWriterInterceptor(), JAX_RS_INTERCEPTOR_BASE, PREFIX);

        assertResponseContent("[interceptor]test",  PREFIX + RESOURCE);
    }

    public void testWhiteboardResourceNameBindingInterceptor_method() throws Exception {
        registerService(Object.class, new TestResourceWithNameBindingInterceptorOnMethod(), JAX_RS_RESOURCE_BASE, PREFIX);
        registerService(Object.class, new TestNameBindingWriterInterceptor(), JAX_RS_INTERCEPTOR_BASE, PREFIX);

        assertResponseContent("[interceptor]test",  PREFIX + RESOURCE);
        assertResponseContent("test",  PREFIX + RESOURCE + "/other");
    }

    public void testWhiteboardApplicationNameBindingInterceptor() throws Exception {
        registerService(Application.class, new TestApplicationWithNameBindingInterceptor(), JAX_RS_APPLICATION_BASE, PREFIX + "/intercept");
        registerService(Application.class, new TestApplication(), JAX_RS_APPLICATION_BASE, PREFIX + "/no-intercept");
        registerService(Object.class, new TestNameBindingWriterInterceptor(), JAX_RS_INTERCEPTOR_BASE, PREFIX);

        assertResponseContent("[interceptor]test",  PREFIX + "/intercept" + APP);
        assertResponseContent("test",  PREFIX + "/no-intercept" + APP);
    }
}
