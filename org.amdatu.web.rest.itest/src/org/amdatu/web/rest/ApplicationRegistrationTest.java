/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest;

import static org.amdatu.web.rest.jaxrs.AmdatuWebRestConstants.JAX_RS_APPLICATION_BASE;
import static org.amdatu.web.rest.jaxrs.AmdatuWebRestConstants.JAX_RS_APPLICATION_CONTEXT;
import static org.amdatu.web.rest.jaxrs.AmdatuWebRestConstants.JAX_RS_APPLICATION_NAME;
import static org.amdatu.web.rest.jaxrs.AmdatuWebRestConstants.JAX_RS_APPLICATION_TYPE;
import static org.amdatu.web.rest.jaxrs.AmdatuWebRestConstants.JAX_RS_APPLICATION_TYPE_DYNAMIC;
import static org.amdatu.web.rest.jaxrs.AmdatuWebRestConstants.JAX_RS_APPLICATION_TYPE_LEGACY;
import static org.amdatu.web.rest.jaxrs.AmdatuWebRestConstants.JAX_RS_APPLICATION_TYPE_STATIC;
import static org.osgi.service.http.whiteboard.HttpWhiteboardConstants.HTTP_WHITEBOARD_CONTEXT_NAME;
import static org.osgi.service.http.whiteboard.HttpWhiteboardConstants.HTTP_WHITEBOARD_CONTEXT_PATH;

import java.util.Collections;
import java.util.Set;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Application;

import org.osgi.framework.Constants;
import org.osgi.framework.ServiceRegistration;
import org.osgi.service.http.context.ServletContextHelper;

/**
 * Tests the (de-)registration of applications
 */
public class ApplicationRegistrationTest extends RegistrationTestBase {

    private static final String TEST_APPLICATION_NAME = "org.amdatu.web.rest.ApplicationRegistrationTestApp";
    private static final String CONTEXT_NAME = "testContext";
    private static final String CONTEXT_PATH = "/context";
    private static final String PREFIX = "/prefix";
    private static final String BASE = "/base";
    private static final String SUB = "/base/sub";

    @Path(BASE)
    public static class TestBaseResource {
        @GET
        public String handleGetRequest() {
            return "base";
        }
    }

    @Path(SUB)
    public static class TestSubResource {
        @GET
        public String handleGetRequest() {
            return "sub";
        }
    }

    public void testDynamicApplication() throws Exception {
        assertResponseCode(HTTP_NOT_FOUND, BASE);

        ServiceRegistration<Application> applicationReg = registerService(Application.class, new Application(),
            JAX_RS_APPLICATION_BASE, "",
            JAX_RS_APPLICATION_NAME, TEST_APPLICATION_NAME,
            JAX_RS_APPLICATION_TYPE, JAX_RS_APPLICATION_TYPE_DYNAMIC);

        registerService(Object.class, new TestBaseResource(), JAX_RS_APPLICATION_NAME, TEST_APPLICATION_NAME);

        assertResponseCode(HTTP_OK, BASE);

        applicationReg.unregister();
        assertResponseCode(HTTP_NOT_FOUND, BASE);
    }

    public void testApplicationInContext() throws Exception {
        assertResponseCode(HTTP_NOT_FOUND, CONTEXT_PATH + BASE);

        registerService(ServletContextHelper.class, new ServletContextHelper() {

        },
            HTTP_WHITEBOARD_CONTEXT_PATH, CONTEXT_PATH,
            HTTP_WHITEBOARD_CONTEXT_NAME, CONTEXT_NAME
            );

        registerService(Application.class, new Application(),
            JAX_RS_APPLICATION_CONTEXT, CONTEXT_NAME,
            JAX_RS_APPLICATION_BASE, "",
            JAX_RS_APPLICATION_NAME, TEST_APPLICATION_NAME,
            JAX_RS_APPLICATION_TYPE, JAX_RS_APPLICATION_TYPE_DYNAMIC);

        registerService(Object.class, new TestBaseResource(), JAX_RS_APPLICATION_NAME, TEST_APPLICATION_NAME);


        // Register a second context with the same resource path

        registerService(ServletContextHelper.class, new ServletContextHelper() {

        },
            HTTP_WHITEBOARD_CONTEXT_PATH, "/secondContext",
            HTTP_WHITEBOARD_CONTEXT_NAME, "context2"
            );

        registerService(Application.class, new Application(),
            JAX_RS_APPLICATION_CONTEXT, "context2",
            JAX_RS_APPLICATION_BASE, "",
            JAX_RS_APPLICATION_NAME, "application2",
            JAX_RS_APPLICATION_TYPE, JAX_RS_APPLICATION_TYPE_DYNAMIC
            );

        registerService(Object.class, new TestBaseResource(), JAX_RS_APPLICATION_NAME, "application2");

        assertResponseCode(HTTP_OK, "/secondContext" + BASE);


        // Register an second application on the test context

        ServiceRegistration<Application> applicationReg = registerService(Application.class, new Application(),
            HTTP_WHITEBOARD_CONTEXT_NAME, CONTEXT_NAME,
            JAX_RS_APPLICATION_BASE, "",
            JAX_RS_APPLICATION_NAME, "application3",
            JAX_RS_APPLICATION_TYPE, JAX_RS_APPLICATION_TYPE_DYNAMIC);

        registerService(Object.class, new TestSubResource(), JAX_RS_APPLICATION_NAME, "application3");

        assertResponseCode(HTTP_NOT_FOUND, CONTEXT_PATH + SUB);
        applicationReg.unregister();


        registerService(Application.class, new Application(),
            Constants.SERVICE_RANKING, Integer.MAX_VALUE,
            JAX_RS_APPLICATION_CONTEXT, CONTEXT_NAME,
            JAX_RS_APPLICATION_BASE, "",
            JAX_RS_APPLICATION_NAME, "application3",
            JAX_RS_APPLICATION_TYPE, JAX_RS_APPLICATION_TYPE_DYNAMIC);

        assertResponseCode(HTTP_OK, CONTEXT_PATH  + SUB);
        assertResponseCode(HTTP_NOT_FOUND, CONTEXT_PATH + BASE);
    }

    public void testPrefixedApplicationRegistration() throws Exception {
        String prefixedBase = PREFIX + BASE;
        assertResponseCode(HTTP_NOT_FOUND, prefixedBase);

        ServiceRegistration<Application> applicationReg = registerService(Application.class, new Application(),
            JAX_RS_APPLICATION_BASE, PREFIX,
            JAX_RS_APPLICATION_NAME, TEST_APPLICATION_NAME,
            JAX_RS_APPLICATION_TYPE, JAX_RS_APPLICATION_TYPE_DYNAMIC);

        registerService(Object.class, new TestBaseResource(), JAX_RS_APPLICATION_NAME, TEST_APPLICATION_NAME);

        assertResponseCode(HTTP_OK, prefixedBase);

        applicationReg.unregister();
        assertResponseCode(HTTP_NOT_FOUND, prefixedBase);
    }

    public void testLegacyResourceRegistrationSupport() throws Exception {

        assertResponseCode(HTTP_NOT_FOUND, BASE);

        registerService(Application.class, new Application(),
            JAX_RS_APPLICATION_BASE, "",
            JAX_RS_APPLICATION_NAME, TEST_APPLICATION_NAME,
            JAX_RS_APPLICATION_TYPE, JAX_RS_APPLICATION_TYPE_LEGACY);

        ServiceRegistration<Object> srvReg = registerService(Object.class, new TestBaseResource());

        assertResponseCode(HTTP_OK, BASE);

        srvReg.unregister();
        assertResponseCode(HTTP_NOT_FOUND, BASE);
    }

    public void testPrefixedLegacyResourceRegistrationSupport() throws Exception {
        String prefixedBase = PREFIX + BASE;

        assertResponseCode(HTTP_NOT_FOUND, prefixedBase);

        registerService(Application.class, new Application(),
            JAX_RS_APPLICATION_BASE, PREFIX,
            JAX_RS_APPLICATION_NAME, TEST_APPLICATION_NAME,
            JAX_RS_APPLICATION_TYPE, JAX_RS_APPLICATION_TYPE_LEGACY);

        ServiceRegistration<Object> srvReg = registerService(Object.class, new TestBaseResource());

        assertResponseCode(HTTP_OK, prefixedBase);

        srvReg.unregister();
        assertResponseCode(HTTP_NOT_FOUND, prefixedBase);
    }

    public void testStaticApplication() throws Exception {
        String prefixedBase = PREFIX + BASE;
        String prefixedSub = prefixedBase + SUB;
        assertResponseCode(HTTP_NOT_FOUND, prefixedBase);

        registerService(Application.class, new Application() {
            @Override
            public Set<Object> getSingletons() {
                return Collections.singleton(new TestBaseResource());
            }
        },
            JAX_RS_APPLICATION_BASE, PREFIX,
            JAX_RS_APPLICATION_NAME, TEST_APPLICATION_NAME,
            JAX_RS_APPLICATION_TYPE, JAX_RS_APPLICATION_TYPE_STATIC);

        registerService(Object.class, new TestSubResource(), JAX_RS_APPLICATION_NAME, TEST_APPLICATION_NAME);
        registerService(Object.class, new TestSubResource());

        assertResponseCode(HTTP_OK, prefixedBase);
        assertResponseCode(HTTP_NOT_FOUND, prefixedSub);

    }

}
